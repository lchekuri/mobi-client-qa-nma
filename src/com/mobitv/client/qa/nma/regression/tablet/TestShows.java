package com.mobitv.client.qa.nma.regression.tablet;

import io.appium.java_client.AppiumDriver;

import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.mobitv.client.qa.nma.common.Constants;
import com.mobitv.client.qa.nma.common.DataParser;
import com.mobitv.client.qa.nma.common.ExecuteShellComand;
import com.mobitv.client.qa.nma.common.SwipeAndScroll;
import com.mobitv.client.qa.nma.common.Util;

public class TestShows {
	WebElement wb_ShowName;
	private static Util util = new Util();
	// private static WebDriver driver = null;
	private static AppiumDriver driver = null;
	private DataParser mJsonParser = new DataParser();
	private ExecuteShellComand command = new ExecuteShellComand();

	@BeforeTest
	public void setUp() throws Exception {
		driver = util.getAndroidDriver(driver);
		command.executeCommand("Tablet_Shows");
	}

	@AfterTest
	public void tearDown() throws Exception {
		driver.quit();
		command.kill();
		Thread.sleep(Constants.ThreadSleep);
	}
	/**
	 * TEST : 
	 * 		Detail overlay for Episodes/Segment  
	 * ACCEPTANCE CRITERIA
	 *		Episode/segment detail overlay loads on top of current page when a clip detail is selected
	 *		Overlay is dismissed when outside of the box is clicked
	 *		All meta-data and images are displayed as stated in the story
	 *		Clicking on the play button will dismiss the overlay and launches the player or purchase flow
	 *		Clicking on the Show will dismiss the overlay and launches the shows detail page (captured in AT2.7)
	 */
	@SuppressWarnings("static-access")
	@Test
	public void Test_9070_8997_8998_9067_8601() throws Exception {

		System.out.println("Start TestCase  ---> Test_9070_8997_8998_9067_8601()");
		System.out.println("==========================================================================");

		WebElement Shows_Tab = driver.findElement(By.id(Constants.app_Package + ":id/shows_tab"));
		String str_ShowsTab = Shows_Tab.getText();
		Shows_Tab.click();
		System.out.println("Clicked on TAB : " + str_ShowsTab);
		util.fileWriting("Clicked on TAB : " + str_ShowsTab);
		Thread.sleep(5000);
		//Verify tile orientation
		util.takeScreenShot(driver, "Test_Shows", "Shows_MainScreen");
		System.out.println("Screen shot available to verify tile oreintation");
		
		WebElement clip_FilterBar = driver.findElement(By.id(Constants.app_Package+":id/filter_bar"));
		clip_FilterBar.click();
		System.out.println("Clicked on Filter to reset");
		Thread.sleep(3000);
		WebElement clip_FilterReset = driver.findElement(By.id(Constants.app_Package+":id/dialog_filters_reset"));
		clip_FilterReset.click();
		System.out.println("RESET Button Clicked");
		WebElement clip_FilterApplyBtn = driver.findElement(By.id(Constants.app_Package+":id/dialog_filters_apply_btn"));
		clip_FilterApplyBtn.click();

		JSONObject jsnObj_ShowMainURL = mJsonParser.callHttpRequest(Constants.show_MainURL, "GET", null);
		Thread.sleep(Constants.ThreadSleep);

		int str_ShowTotal = jsnObj_ShowMainURL.getInt("total");
		System.out.println("str_ShowTotal : " + str_ShowTotal);
		util.fileWriting("str_ShowTotal : " + str_ShowTotal);
		JSONArray jsnArray_Hits = jsnObj_ShowMainURL.getJSONArray("hits");

		int m_Length;
		if (jsnArray_Hits.length() >= 5) {
			m_Length = 5;
		} else {
			m_Length = jsnArray_Hits.length();
		}

		// for(int i=0; i<m_Length; i++){
		for (int i = 0; i < m_Length; i++) {
			System.out.println("**********" + (i + 1) + "**********");
			util.fileWriting("**********" + (i + 1) + "**********");
			JSONObject json_ShowsInfo = jsnArray_Hits.getJSONObject(i);
			JSONObject json_ShowResult = json_ShowsInfo.getJSONObject("result");
			JSONObject json_ShowSeries = json_ShowResult.getJSONObject("series");

			String str_ShowID = json_ShowSeries.getString("id");
			System.out.println("str_ShowID : " + str_ShowID);
			util.fileWriting("str_ShowID : " + str_ShowID);
			String str_ShowName = json_ShowSeries.getString("name");
			System.out.println("str_ShowName : " + str_ShowName);
			util.fileWriting("str_ShowName : " + str_ShowName);
			try {
				// wb_ShowName =
				// driver.findElement(By.id(Constants.app_Package+":id/base_grid").xpath("//android.widget.LinearLayout["+(i+1)+"]").id(Constants.app_Package+":id/cell_area_txt").id(Constants.app_Package+":id/cell_txt_title"));
				wb_ShowName = driver.findElement(By.id(Constants.app_Package + ":id/base_grid").xpath(
						"//android.widget.LinearLayout[" + (i + 1) + "]/android.widget.LinearLayout[1]/android.widget.TextView[1]"));
				System.out.println(wb_ShowName.getText());

			} catch (Exception e) {
				System.out.println("X-Path of the Element Not Found");
			}
			String str_wbShowName = wb_ShowName.getText();

			if (str_ShowName != null) {
				if (str_ShowName.equalsIgnoreCase(str_wbShowName)) {

					util.takeScreenShot(driver, "Test_Shows", "Show_" + (i + 1));

					wb_ShowName.click();
					System.out.println("Clicked on SHOW: " + str_wbShowName);
					util.fileWriting("Clicked on SHOW: " + str_wbShowName);
					Thread.sleep(Constants.ThreadSleep);

					JSONObject jsnObj_ShowEpisodesURL = mJsonParser.callHttpRequest(Constants.show_EpisodesURL + str_ShowID, "GET", null);
					Thread.sleep(Constants.ThreadSleep);

					int int_ShowEpisodesTotal = jsnObj_ShowEpisodesURL.getInt("total");
					System.out.println("int_ShowEpisodesTotal : " + int_ShowEpisodesTotal);
					util.fileWriting("int_ShowEpisodesTotal : " + int_ShowEpisodesTotal);
					if (int_ShowEpisodesTotal > 0) {
						JSONArray jsnArray_EpisodesHits = jsnObj_ShowEpisodesURL.getJSONArray("hits");

						if (jsnArray_EpisodesHits.length() > 0) {
							JSONObject json_ShowsEpisodeInfo = jsnArray_EpisodesHits.getJSONObject(0);
							JSONObject json_ShowEpisodeResult = json_ShowsEpisodeInfo.getJSONObject("result");
							JSONObject json_ShowEpisodeVOD = json_ShowEpisodeResult.getJSONObject("vod");
							json_ShowEpisodeVOD.getString("id");
							String str_ShowEpisodeName = json_ShowEpisodeVOD.getString("name");
							System.out.println("str_ShowEpisodeName : " + str_ShowEpisodeName);
							util.fileWriting("str_ShowEpisodeName : " + str_ShowEpisodeName);

							SwipeAndScroll.swipeGrid_One(driver);

							// WebElement wb_ShowEpisodeName =
							// driver.findElement(By.id(Constants.app_Package+":id/details_series_recycler_view").id(Constants.app_Package+":id/tile_1").xpath("//android.widget.LinearLayout[1]/android.widget.LinearLayout[1]/android.widget.LinearLayout[1]/android.widget.TextView[1]"));
							WebElement wb_ShowEpisodeName = driver
									.findElement(By
											.id(Constants.app_Package + ":id/details_series_recycler_view")
											.xpath("//android.widget.LinearLayout[3]/android.widget.LinearLayout[1]/android.widget.LinearLayout[1]/android.widget.TextView[1]"));
							String str_wbShowEpisodeName = wb_ShowEpisodeName.getText();
							System.out.println("wb_ShowEpisodeName : " + str_wbShowEpisodeName);
							util.fileWriting("wb_ShowEpisodeName : " + str_wbShowEpisodeName);

							// if(str_ShowEpisodeName.equalsIgnoreCase(str_wbShowEpisodeName)){
							util.takeScreenShot(driver, "Test_Shows", "Show_" + (i + 1) + "-Episode");
							wb_ShowEpisodeName.click();
							Thread.sleep(Constants.ThreadSleep);

							System.out.println("Clicked on EPISODE: " + str_wbShowEpisodeName);
							util.fileWriting("Clicked on EPISODE: " + str_wbShowEpisodeName);
							util.takeScreenShot(driver, "Test_Shows", "Show_" + (i + 1) + "-EpisodeDeatils");

							WebElement wb_ThumbnailImage = driver.findElement(By.id(Constants.app_Package + ":id/details_thumb"));
							Assert.assertTrue(wb_ThumbnailImage.isEnabled());
							System.out.println("Thumbnail Image exists");

							WebElement wb_PlayButton = driver.findElement(By.id(Constants.app_Package + ":id/details_btn"));
							Assert.assertTrue(wb_PlayButton.isDisplayed());
							//Assert.assertEquals(wb_PlayButton.getText(), "PLAY");
							System.out.println("Play Button exists");

							wb_PlayButton.click();
							WebElement wb_playback_video = driver.findElement(By.id(Constants.app_Package + ":id/playback_video_view"));
							wb_playback_video.isEnabled();
							System.out.println("Clicked on playbutton lauches the player");
							driver.navigate().back();

							WebElement wb_DetailsTxtTitle = driver.findElement(By.id(Constants.app_Package + ":id/details_txt_title"));
							String str_wbDetailsTxtTitle = wb_DetailsTxtTitle.getText();
							Assert.assertEquals(str_wbShowEpisodeName, str_wbDetailsTxtTitle);
							System.out.println("Title of Episode exists");

							/*
							 * WebElement wb_SeasonEpisodeNum =
							 * driver.findElement(By.id(Constants.app_Package+
							 * ":id/details_season_episode_number")); String
							 * str_wbSeasonEpisodeNum =
							 * wb_SeasonEpisodeNum.getText();
							 * System.out.println(str_wbSeasonEpisodeNum+
							 * "Season Episode Num exists on detail overlay for episodes"
							 * );
							 * 
							 * WebElement wb_DetailsTxtAirdate =
							 * driver.findElement(By.id(Constants.app_Package+
							 * ":id/details_txt_airdate")); String
							 * str_DetailsTxtAirdate =
							 * wb_DetailsTxtAirdate.getText();
							 * System.out.println(str_DetailsTxtAirdate+
							 * " exists on detail overlay for episodes");
							 */
							try {
								WebElement wb_DetailsTxtExpireDate = driver.findElement(By.id(Constants.app_Package + ":id/details_txt_expiredate"));
								String str_DetailsTxtExpireDate = wb_DetailsTxtExpireDate.getText();
								System.out.println(str_DetailsTxtExpireDate + " exists on detail overlay for episodes");

								WebElement wb_DetailsTxtDuration = driver.findElement(By.id(Constants.app_Package + ":id/details_txt_duration"));
								String str_DetailsTxtDuration = wb_DetailsTxtDuration.getText();
								System.out.println("Duration " + str_DetailsTxtDuration + " exists on detail overlay for episodes");

								WebElement wb_MainLayout = driver.findElement(By.id(Constants.app_Package + ":id/main_layout"));
								wb_MainLayout.click();
								System.out.println("Overlay is dismissed when Show/outside of the box is clicked");
							} catch (Exception e) {
								throw new Exception("Detail overlay popup for episodes is not displayed");
							}
						}
						driver.navigate().back();
						Thread.sleep(Constants.ThreadSleep);
					}
					driver.navigate().back();
					Thread.sleep(Constants.ThreadSleep);
				}
			} else {
				System.out.println("Show Data is not available");
			}
		}

		System.out.println("==========================================================================");
		System.out.println("End TestCase  ---> Test_9070_8997_8998_9067_8601()");
		System.out.println("==========================================================================");
	}

	/**
	 * TEST : 
	 * 		Grid Template 2 - Shows  
	 * ACCEPTANCE CRITERIA
	 *		Only scroll up and down - Vertical scrolling
	 *		Pagination to support large numbers like hundreds  (pagination after 5+rows for best optimization)
	 *		Tiles are in portrait orientation
	 */
	@Test
	public void Test_NMA_8619_9268() throws Exception {
		System.out.println("Start TestCase  ---> Test_NMA_8619()");
		System.out.println("==========================================================================");

		WebElement Shows_Tab = driver.findElement(By.id(Constants.app_Package + ":id/shows_tab"));
		String str_ShowsTab = Shows_Tab.getText();
		Shows_Tab.click();
		System.out.println("Clicked on TAB : " + str_ShowsTab);
		util.fileWriting("Clicked on TAB : " + str_ShowsTab);
		Thread.sleep(5000);
		
		WebElement clip_FilterBar = driver.findElement(By.id(Constants.app_Package+":id/filter_bar"));
		clip_FilterBar.click();
		System.out.println("Clicked on Filter to reset");
		Thread.sleep(3000);
		WebElement clip_FilterReset = driver.findElement(By.id(Constants.app_Package+":id/dialog_filters_reset"));
		clip_FilterReset.click();
		System.out.println("RESET Button Clicked");
		WebElement clip_FilterApplyBtn = driver.findElement(By.id(Constants.app_Package+":id/dialog_filters_apply_btn"));
		clip_FilterApplyBtn.click();

		JSONObject jsnObj_ShowMainURL = mJsonParser.callHttpRequest(Constants.show_MainURL, "GET", null);
		Thread.sleep(Constants.ThreadSleep);

		int str_ShowTotal = jsnObj_ShowMainURL.getInt("total");
		System.out.println("str_ShowTotal : " + str_ShowTotal);
		JSONArray jsnArray_Hits = jsnObj_ShowMainURL.getJSONArray("hits");

		List<WebElement> list_ShowCellThumb = driver.findElements(By.id(Constants.app_Package + ":id/cell_thumb"));
		List<WebElement> list_ShowCellTitle = driver.findElements(By.id(Constants.app_Package + ":id/cell_txt_title"));

		for (int i = 0; i < list_ShowCellTitle.size(); i++) {
			System.out.println("*****************************");

			JSONObject json_ShowsInfo = jsnArray_Hits.getJSONObject(i);
			JSONObject json_ShowResult = json_ShowsInfo.getJSONObject("result");
			JSONObject json_ShowSeries = json_ShowResult.getJSONObject("series");

			String str_ShowID = json_ShowSeries.getString("id");
			String str_ShowName = json_ShowSeries.getString("name");

			String str_wbShowCellTitle = list_ShowCellTitle.get(i).getText();
			Assert.assertEquals(str_wbShowCellTitle, str_ShowName);
			System.out.println(str_wbShowCellTitle + " :Show-" + (i + 1) + " name is available in Shows Grid Template");

			WebElement wbShowThumb = list_ShowCellThumb.get(i);
			wbShowThumb.isEnabled();
			System.out.println(str_wbShowCellTitle + " :Show-" + (i + 1) + " image tile is in portrait orientation on Shows Grid Template");

		}
		SwipeAndScroll.swipeGrid_One(driver);
		System.out.println("Vertical Scrolling up and down available on Shows");

		System.out.println("==========================================================================");
		System.out.println("End TestCase  ---> Test_NMA_8619()");
		System.out.println("==========================================================================");
	}
	/**
	 * TEST : 
	 * 		List of Shows populated  
	 * 		Images/metadata displayed
	 * ACCEPTANCE CRITERIA
	 *		Shows page loads when SHOWS tab is clicked
	 *		Shows page includes all shows (with pagination) in the order of date the shows latest episode is made available (latest first)
	 *		Only vertically scrollable
	 *		All meta-data and images are displayed as stated in the story
	 *		Clicking the tile opens the shows detail page (show detail page is handled in a separate story, AT2.7)
	 */
	@Test
	public void Test_NMA_9064() throws Exception {
		System.out.println("Start TestCase  ---> Test_NMA_9064()");
		System.out.println("==========================================================================");

		WebElement Shows_Tab = driver.findElement(By.id(Constants.app_Package + ":id/shows_tab"));
		String str_ShowsTab = Shows_Tab.getText();
		Shows_Tab.click();
		System.out.println("Clicked on TAB : " + str_ShowsTab);
		util.fileWriting("Clicked on TAB : " + str_ShowsTab);
		Thread.sleep(5000);
		
		WebElement clip_FilterBar = driver.findElement(By.id(Constants.app_Package+":id/filter_bar"));
		clip_FilterBar.click();
		System.out.println("Clicked on Filter to reset");
		Thread.sleep(3000);
		WebElement clip_FilterReset = driver.findElement(By.id(Constants.app_Package+":id/dialog_filters_reset"));
		clip_FilterReset.click();
		System.out.println("RESET Button Clicked");
		WebElement clip_FilterApplyBtn = driver.findElement(By.id(Constants.app_Package+":id/dialog_filters_apply_btn"));
		clip_FilterApplyBtn.click();

		JSONObject jsnObj_ShowMainURL = mJsonParser.callHttpRequest(Constants.show_MainURL, "GET", null);
		Thread.sleep(Constants.ThreadSleep);

		int str_ShowTotal = jsnObj_ShowMainURL.getInt("total");
		System.out.println("str_ShowTotal : " + str_ShowTotal);
		JSONArray jsnArray_Hits = jsnObj_ShowMainURL.getJSONArray("hits");

		List<WebElement> list_ShowCellThumb = driver.findElements(By.id(Constants.app_Package + ":id/cell_thumb"));
		List<WebElement> list_ShowCellTitle = driver.findElements(By.id(Constants.app_Package + ":id/cell_txt_title"));

		for (int i = 0; i < list_ShowCellTitle.size(); i++) {
			System.out.println("*****************************");

			JSONObject json_ShowsInfo = jsnArray_Hits.getJSONObject(i);
			JSONObject json_ShowResult = json_ShowsInfo.getJSONObject("result");
			JSONObject json_ShowSeries = json_ShowResult.getJSONObject("series");

			String str_ShowID = json_ShowSeries.getString("id");
			String str_ShowName = json_ShowSeries.getString("name");

			WebElement wb_ShowCellTitle = list_ShowCellTitle.get(i);
			String str_wbShowCellTitle = list_ShowCellTitle.get(i).getText();
			Assert.assertEquals(str_wbShowCellTitle, str_ShowName);
			System.out.println(str_wbShowCellTitle + " :Show-" + (i + 1) + " name is available in Shows Grid Template");

			WebElement wbMovieCellThumb = list_ShowCellThumb.get(i);
			wbMovieCellThumb.isEnabled();
			System.out.println(str_wbShowCellTitle + " :Show-" + (i + 1) + " image tile is in portrait orientation on Shows Grid Template");

			wb_ShowCellTitle.click();
			System.out.println("Clicked on Show: " + str_wbShowCellTitle);
			Thread.sleep(Constants.ThreadSleep);

			WebElement wb_ShowDetailsTitle = driver.findElement(By.id(Constants.app_Package + ":id/details_txt_title"));
			String str_wbShowDetailsTitle = wb_ShowDetailsTitle.getText();
			System.out.println("Landed on " + str_wbShowCellTitle + " details page");
			Assert.assertEquals(str_wbShowDetailsTitle, str_wbShowCellTitle);

			driver.navigate().back();
			Thread.sleep(Constants.ThreadSleep);
		}
		System.out.println("==========================================================================");
		System.out.println("End TestCase  ---> Test_NMA_9064()");
		System.out.println("==========================================================================");
	}
}
