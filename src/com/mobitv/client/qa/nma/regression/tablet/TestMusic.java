package com.mobitv.client.qa.nma.regression.tablet;

import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.mobitv.client.qa.nma.common.Constants;
import com.mobitv.client.qa.nma.common.DataParser;
import com.mobitv.client.qa.nma.common.ExecuteShellComand;
import com.mobitv.client.qa.nma.common.Util;

public class TestMusic {
	private static Util util = new Util();
	private static WebDriver driver = null;
	private ExecuteShellComand command = new ExecuteShellComand();
	private DataParser mJsonParser = new DataParser();


	@BeforeTest
	public void setUp() throws Exception {		
		driver = util.getAndroidDriver(driver);
		command.executeCommand("Tablet_Networks");
	}

	@AfterTest
	public void tearDown() throws Exception{
		driver.quit();
		command.kill();
		Thread.sleep(Constants.ThreadSleep);
	}

	@Test
	public void Test_NMA_9066() throws Exception {

	
		System.out.println("start Music test");
		
		WebElement Music_Tab = driver.findElement(By.id(Constants.app_Package + ":id/music_tab"));
		String str_MusicTab = Music_Tab.getText();
		Music_Tab.click();
		System.out.println("Clicked on TAB : " + str_MusicTab);
		util.fileWriting("Clicked on TAB : " + str_MusicTab);
		Thread.sleep(5000);
		
		WebElement clip_FilterBar = driver.findElement(By.id(Constants.app_Package+":id/filter_bar"));
		clip_FilterBar.click();
		System.out.println("Clicked on Filter to reset");
		Thread.sleep(3000);
		WebElement clip_FilterReset = driver.findElement(By.id(Constants.app_Package+":id/dialog_filters_reset"));
		clip_FilterReset.click();
		System.out.println("RESET Button Clicked");
		WebElement clip_FilterApplyBtn = driver.findElement(By.id(Constants.app_Package+":id/dialog_filters_apply_btn"));
		clip_FilterApplyBtn.click();
		
		JSONObject jsnObj_MusicMainURL = mJsonParser.callHttpRequest(Constants.Music_MainURL,"GET", null);
		Thread.sleep(Constants.ThreadSleep);

		int str_MusicTotal = jsnObj_MusicMainURL.getInt("total");
		System.out.println("str_MusicTotal : "+str_MusicTotal);
		util.fileWriting("str_MusicTotal : "+str_MusicTotal);
		JSONArray jsnArray_MusicHits = jsnObj_MusicMainURL.getJSONArray("hits");
		
		List<WebElement> list_MusicCellThumb = driver.findElements(By.id(Constants.app_Package + ":id/cell_thumb"));
		List<WebElement> list_MusicCellIconPlay = driver.findElements(By.id(Constants.app_Package + ":id/cell_icon_play"));
		List<WebElement> list_MusicCellTitle = driver.findElements(By.id(Constants.app_Package + ":id/cell_txt_title"));
		List<WebElement> list_MusicCelldesc = driver.findElements(By.id(Constants.app_Package + ":id/cell_txt_desc"));
		
		
		for (int i = 0; i < list_MusicCellTitle.size(); i++) {
			System.out.println("*****************************");
			
			JSONObject json_MusicsInfo = jsnArray_MusicHits.getJSONObject(i);
			JSONObject json_MusicResult = json_MusicsInfo.getJSONObject("result");
			JSONObject json_MusicSeries = json_MusicResult.getJSONObject("vod");
			
			String str_MusicID = json_MusicSeries.getString("id");
			String str_MusicName = json_MusicSeries.getString("name");

			WebElement wb_MusicCellName = list_MusicCellTitle.get(i);
			String str_wbMusicCellTitle = wb_MusicCellName.getText();
			Assert.assertEquals(str_MusicName, str_wbMusicCellTitle);
			System.out.println(str_wbMusicCellTitle + " :Music-" + (i + 1) + " name is available in Music Grid Template");

			WebElement wbMusicCellThumb = list_MusicCellThumb.get(i);
			wbMusicCellThumb.isEnabled();
			System.out.println(str_wbMusicCellTitle + " :Music-" + (i + 1) + " image thumb is in landscape orientation on Music Grid Template");

			WebElement wbMusicIconPlay = list_MusicCellIconPlay.get(i);
			wbMusicIconPlay.isEnabled();
			System.out.println(str_wbMusicCellTitle + " :Music-" + (i + 1) + " play icon is available in Music Grid Template");
			
			WebElement wbMusiccellDesc = list_MusicCelldesc.get(i);
			wbMusiccellDesc.isEnabled();
			System.out.println(str_wbMusicCellTitle + " :Music-" + (i + 1) + " Description is available in Music Grid Template");
			
			System.out.println("Music detail page");			
			wb_MusicCellName.click();
			Thread.sleep(2000);
			
			WebElement Music_details_Toolbar = driver.findElement(By.id(Constants.app_Package + ":id/toolbar"));			
			WebElement Music_details_back = driver.findElement(By.name("Navigate up"));
			WebElement Music_details_search = driver.findElement(By.id(Constants.app_Package + ":id/action_search"));
			WebElement Music_details_Thumb = driver.findElement(By.id(Constants.app_Package + ":id/details_thumb"));
			WebElement Music_details_vevo = driver.findElement(By.id(Constants.app_Package + ":id/vod_vevo_logo"));
			WebElement Music_details_text = driver.findElement(By.id(Constants.app_Package + ":id/details_txt_title"));
			WebElement Music_details_atrist = driver.findElement(By.id(Constants.app_Package + ":id/details_txt_artist"));
			WebElement Music_details_duration = driver.findElement(By.id(Constants.app_Package + ":id/details_txt_duration"));
			WebElement Music_details_btn = driver.findElement(By.id(Constants.app_Package + ":id/details_btn"));
			WebElement Music_details_rel = driver.findElement(By.id(Constants.app_Package + ":id/cell_txt_title"));
			WebElement Music_details_cell= driver.findElement(By.id(Constants.app_Package + ":id/details_cell"));	
			
			//String music_details_title= Music_details_Toolbar.getText();			
			//Assert.assertEquals(music_details_title, str_wbMusicCellTitle);
			Assert.assertTrue(Music_details_Toolbar.isEnabled());
			System.out.println("Tool bar is available on Music Detail page");
			Assert.assertTrue(Music_details_back.isEnabled());
			System.out.println("back button is available on Music Detail page");
			Assert.assertTrue(Music_details_search.isEnabled());
			System.out.println("search is available on Music Detail page");
			Assert.assertTrue(Music_details_Thumb.isEnabled());
			System.out.println("image thumb is available on Music Detail page");
			Assert.assertTrue(Music_details_vevo.isEnabled());
			System.out.println("vevo is available on Music Detail page");
			Assert.assertEquals(Music_details_text.getText(), str_wbMusicCellTitle);
			System.out.println("Details text available on Music Detail page");
			Assert.assertTrue(Music_details_atrist.isEnabled());	
			System.out.println("artist available on Music Detail page");
			Assert.assertTrue(Music_details_duration.isEnabled());
			System.out.println("Duration available on Music Detail page");
			Assert.assertTrue(Music_details_btn.getText().equals("RESUME") || Music_details_btn.getText().equals("PLAY"));
			System.out.println("Play/Resume available on Music Detail page");
			Assert.assertTrue(Music_details_rel.isEnabled());
			System.out.println("Details available on Music Detail page");
			Assert.assertTrue(Music_details_cell.isEnabled());	
			
			//Verify navigate back 
			
			Music_details_back.click();
			Thread.sleep(2000);
			
			}
		
		
	}

}
