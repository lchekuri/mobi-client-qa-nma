package com.mobitv.client.qa.nma.regression.tablet;

import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.mobitv.client.qa.nma.common.Constants;
import com.mobitv.client.qa.nma.common.DataParser;
import com.mobitv.client.qa.nma.common.ExecuteShellComand;
import com.mobitv.client.qa.nma.common.SwipeAndScroll;
import com.mobitv.client.qa.nma.common.Util;


public class TestMovies {
	private static Util util = new Util();
	private static WebDriver driver = null;
	private ExecuteShellComand command = new ExecuteShellComand();
	private DataParser mJsonParser = new DataParser();

	@BeforeTest
	public void setUp() throws Exception {		
		driver = util.getAndroidDriver(driver);
		command.executeCommand("Tablet_Movies");
	}

	@AfterTest
	public void tearDown() throws Exception{
		driver.quit();
		command.kill();
		Thread.sleep(Constants.ThreadSleep);
	}
	/**
	 * TEST: 
	 * 		Grid Template 2 - Movies
	 * ACCEPTANCE CRITERIA
	 *		Only scroll up and down - Vertical scrolling
	 *		Pagination to support large numbers like hundreds  (pagination after 5+ rows for best optimization)
	 *		Tiles are in landscape orientation
	 */
	@Test
	public void Test_NMA_8619() throws Exception {
		System.out.println("Start TestCase  ---> Test_NMA_8619()");
		System.out.println("==========================================================================");

		WebElement Movies_Tab = driver.findElement(By.id(Constants.app_Package+":id/movies_tab"));
		String str_MoviesTab = Movies_Tab.getText();
		Movies_Tab.click();		
		System.out.println("Clicked on TAB : "+str_MoviesTab);
		util.fileWriting("Clicked on TAB : "+str_MoviesTab);
		Thread.sleep(5000);
		
		WebElement clip_FilterBar = driver.findElement(By.id(Constants.app_Package+":id/filter_bar"));
		clip_FilterBar.click();
		System.out.println("Clicked on Filter to reset");
		Thread.sleep(3000);
		WebElement clip_FilterReset = driver.findElement(By.id(Constants.app_Package+":id/dialog_filters_reset"));
		clip_FilterReset.click();
		System.out.println("RESET Button Clicked");
		WebElement clip_FilterApplyBtn = driver.findElement(By.id(Constants.app_Package+":id/dialog_filters_apply_btn"));
		clip_FilterApplyBtn.click();

		JSONObject jsnObj_MovieMainURL = mJsonParser.callHttpRequest(Constants.Movies_MainURL,"GET", null);
		Thread.sleep(Constants.ThreadSleep);

		int str_MovieTotal = jsnObj_MovieMainURL.getInt("total");
		System.out.println("str_MovieTotal : "+str_MovieTotal);
		util.fileWriting("str_MovieTotal : "+str_MovieTotal);
		JSONArray jsnArray_Hits = jsnObj_MovieMainURL.getJSONArray("hits");

		List<WebElement> list_MoviesCellThumb = driver.findElements(By.id(Constants.app_Package+":id/cell_thumb"));
		List<WebElement> list_MoviesCellTitle = driver.findElements(By.id(Constants.app_Package+":id/cell_txt_title"));

		for(int i=0; i<list_MoviesCellTitle.size(); i++){
			System.out.println("*****************************");

			JSONObject json_MoviesInfo = jsnArray_Hits.getJSONObject(i);
			JSONObject json_MovieResult = json_MoviesInfo.getJSONObject("result");
			JSONObject json_MovieSeries = json_MovieResult.getJSONObject("vod");			
			String str_MovieName = json_MovieSeries.getString("name");	

			String str_wbMoviesCellTitle= list_MoviesCellTitle.get(i).getText();
			Assert.assertEquals(str_wbMoviesCellTitle, str_MovieName);			
			System.out.println(str_wbMoviesCellTitle+" :Movie-"+(i+1)+" name is available in Movies Grid Template");


			WebElement wbMovieThumb = list_MoviesCellThumb.get(i);
			wbMovieThumb.isEnabled();
			util.takeScreenShot(driver,"Test_tablet_Movies","Movies_MainScreen");
			System.out.println("Screenshot availble to verify image orientation");
			System.out.println(str_wbMoviesCellTitle+" :Movie-"+(i+1)+" image tile is in portrait orientation on Movies Grid Template");

		}
		// Verify vertical scrolling up
		for (int j = 0; j < 2; j++) {
			try {
				SwipeAndScroll.swipeGrid_One(driver);
				Thread.sleep(Constants.ThreadSleep);
				System.out.println("Vertical scrolling down available on Live");
			} catch (Exception e) {
				e.getMessage();
			}

		}
		// Verify vertical scrolling scroll down
		for (int j = 0; j < 2; j++) {
			try {
				SwipeAndScroll.swipeGrid_Three(driver);
				Thread.sleep(Constants.ThreadSleep);
				System.out.println("Vertical scrolling up available on Live");
			} catch (Exception e) {
				e.getMessage();
			}
		}
		System.out.println("==========================================================================");
		System.out.println("End TestCase  ---> Test_NMA_8619()");
		System.out.println("==========================================================================");
	}
	/**
	 * TEST: Movies Page
	 * 		List of movies populated
	 * 		Images/metadata displayed
	 * ACCEPTANCE CRITERIA
	 *		Movies page loads when MOVIES tab is clicked
	 *		Movies page includes all movies (with pagination) in the order of date made available (latest first)
	 *		Only vertically scrollable
	 *		All meta-data and images are displayed as stated in the story
	 *		Clicking the tile opens the movies detail page (show detail page is handled in a separate story, AT2.8)
	 */

	@Test
	public void Test_NMA_9065() throws Exception {
		System.out.println("Start TestCase  ---> Test_NMA_9065()");
		System.out.println("==========================================================================");

		WebElement Movies_Tab = driver.findElement(By.id(Constants.app_Package+":id/movies_tab"));
		String str_MoviesTab = Movies_Tab.getText();
		Movies_Tab.click();		
		System.out.println("Clicked on TAB : "+str_MoviesTab);
		util.fileWriting("Clicked on TAB : "+str_MoviesTab);
		Thread.sleep(5000);
		
		WebElement clip_FilterBar = driver.findElement(By.id(Constants.app_Package+":id/filter_bar"));
		clip_FilterBar.click();
		System.out.println("Clicked on Filter to reset");
		Thread.sleep(3000);
		WebElement clip_FilterReset = driver.findElement(By.id(Constants.app_Package+":id/dialog_filters_reset"));
		clip_FilterReset.click();
		System.out.println("RESET Button Clicked");
		WebElement clip_FilterApplyBtn = driver.findElement(By.id(Constants.app_Package+":id/dialog_filters_apply_btn"));
		clip_FilterApplyBtn.click();

		JSONObject jsnObj_MovieMainURL = mJsonParser.callHttpRequest(Constants.Movies_MainURL,"GET", null);
		Thread.sleep(Constants.ThreadSleep);

		int str_MovieTotal = jsnObj_MovieMainURL.getInt("total");
		System.out.println("str_MovieTotal : "+str_MovieTotal);
		util.fileWriting("str_MovieTotal : "+str_MovieTotal);
		JSONArray jsnArray_Hits = jsnObj_MovieMainURL.getJSONArray("hits");

		List<WebElement> list_MovieCellThumb = driver.findElements(By.id(Constants.app_Package+":id/cell_thumb"));
		List<WebElement> list_MovieCellTitle = driver.findElements(By.id(Constants.app_Package+":id/cell_txt_title"));

		for(int i=0; i<list_MovieCellTitle.size(); i++){
			System.out.println("*****************************");
			JSONObject json_MoviesInfo = jsnArray_Hits.getJSONObject(i);
			JSONObject json_MovieResult = json_MoviesInfo.getJSONObject("result");
			JSONObject json_MovieSeries = json_MovieResult.getJSONObject("vod");			
			String str_MovieName = json_MovieSeries.getString("name");
			

			WebElement wb_MovieCellTitle = list_MovieCellTitle.get(i);
			String str_wbMovieCellTitle= list_MovieCellTitle.get(i).getText();
			Assert.assertEquals(str_wbMovieCellTitle, str_MovieName);
			System.out.println(str_wbMovieCellTitle+" :Movie-"+(i+1)+" name is available in Movies Grid Template");

			WebElement wbMovieCellThumb = list_MovieCellThumb.get(i);
			wbMovieCellThumb.isEnabled();
			System.out.println(str_wbMovieCellTitle+" :Movie-"+(i+1)+" image tile is in portrait orientation on Movies Grid Template");

			wb_MovieCellTitle.click();
			System.out.println("Clicked on Movie: "+str_wbMovieCellTitle);
			Thread.sleep(Constants.ThreadSleep);

			WebElement wb_MovieDetailsTitle = driver.findElement(By.id(Constants.app_Package+":id/details_txt_title"));
			String str_wbMovieDetailsTitle = wb_MovieDetailsTitle.getText();
			System.out.println("Landed on "+str_wbMovieCellTitle+ " details page");
			Assert.assertEquals(str_wbMovieDetailsTitle, str_wbMovieCellTitle);


			driver.navigate().back();
			Thread.sleep(Constants.ThreadSleep);
		}
		System.out.println("==========================================================================");
		System.out.println("End TestCase  ---> Test_NMA_9065()");
		System.out.println("==========================================================================");

	}
	/**
	 * TEST: 
	 * 		Movie Details Page
	 * ACCEPTANCE CRITERIA
	 *		Movies detail page loads when a movie is selected
	 *		Only vertically scrollable
	 *		All meta-data and images are displayed as stated in the story
	 *		Clicking on the play button will launch the player or purchase flow
	 */
	@Test
	public void Test_NMA_8999_9068_9276() throws Exception {
		System.out.println("Start TestCase  ---> Test_NMA_8999_9068()");
		System.out.println("==========================================================================");

		WebElement Movies_Tab = driver.findElement(By.id(Constants.app_Package+":id/movies_tab"));
		String str_MoviesTab = Movies_Tab.getText();
		Movies_Tab.click();		
		System.out.println("Clicked on TAB : "+str_MoviesTab);
		util.fileWriting("Clicked on TAB : "+str_MoviesTab);
		Thread.sleep(5000);
		
		WebElement clip_FilterBar = driver.findElement(By.id(Constants.app_Package+":id/filter_bar"));
		clip_FilterBar.click();
		System.out.println("Clicked on Filter to reset");
		Thread.sleep(3000);
		WebElement clip_FilterReset = driver.findElement(By.id(Constants.app_Package+":id/dialog_filters_reset"));
		clip_FilterReset.click();
		System.out.println("RESET Button Clicked");
		WebElement clip_FilterApplyBtn = driver.findElement(By.id(Constants.app_Package+":id/dialog_filters_apply_btn"));
		clip_FilterApplyBtn.click();

		JSONObject jsnObj_MovieMainURL = mJsonParser.callHttpRequest(Constants.Movies_MainURL,"GET", null);
		Thread.sleep(Constants.ThreadSleep);

		int str_MovieTotal = jsnObj_MovieMainURL.getInt("total");
		System.out.println("str_MovieTotal : "+str_MovieTotal);
		util.fileWriting("str_MovieTotal : "+str_MovieTotal);
		JSONArray jsnArray_Hits = jsnObj_MovieMainURL.getJSONArray("hits");

		List<WebElement> list_MovieCellThumb = driver.findElements(By.id(Constants.app_Package+":id/cell_thumb"));
		List<WebElement> list_MovieCellTitle = driver.findElements(By.id(Constants.app_Package+":id/cell_txt_title"));

		for(int i=0; i<list_MovieCellTitle.size(); i++){
			System.out.println("*****************************");
			JSONObject json_MoviesInfo = jsnArray_Hits.getJSONObject(i);
			JSONObject json_MovieResult = json_MoviesInfo.getJSONObject("result");
			JSONObject json_MovieSeries = json_MovieResult.getJSONObject("vod");			
			String str_MovieName = json_MovieSeries.getString("name");			

			WebElement wb_MovieCellTitle = list_MovieCellTitle.get(i);
			String str_wbMovieCellTitle= list_MovieCellTitle.get(i).getText();
			Assert.assertEquals(str_wbMovieCellTitle, str_MovieName);
			System.out.println(str_wbMovieCellTitle+" :Movie-"+(i+1)+" name is available in Movies Grid Template");

			WebElement wbMovieCellThumb = list_MovieCellThumb.get(i);
			wbMovieCellThumb.isEnabled();
			System.out.println(str_wbMovieCellTitle+" :Movie-"+(i+1)+" image tile is in portrait orientation on Movies Grid Template");

			wb_MovieCellTitle.click();
			System.out.println("Clicked on Movie: "+str_wbMovieCellTitle);
			Thread.sleep(Constants.ThreadSleep);
					
			System.out.println("Landed on "+str_wbMovieCellTitle+ " details page");
			Assert.assertEquals(str_MovieName, str_wbMovieCellTitle);
			if(json_MovieSeries.has("actors_list"))
			{
				System.out.println("metadata available");
                                   
			WebElement wb_MovieCastCrew = driver.findElement(By.id(Constants.app_Package+":id/details_txt_cast_crew_title"));
			String str_wbMovieCastCrew = wb_MovieCastCrew.getText();
			System.out.println(str_wbMovieCellTitle+ " movie "+str_wbMovieCastCrew);

			WebElement wb_MovieDirectorTitle = driver.findElement(By.id(Constants.app_Package+":id/txt_value"));
			String str_MovieDirectorTitle = wb_MovieDirectorTitle.getText();
			System.out.println("Director : "+str_MovieDirectorTitle);
			}

			//Verify play button
			WebElement wb_MoviePlay_btn = driver.findElement(By.id(Constants.app_Package+":id/details_btn"));
			Assert.assertTrue(wb_MoviePlay_btn.isDisplayed());

			//Verify background image
			util.takeScreenShot(driver,"Test_tablet_Movies","Movies_Detailscreen");
			System.out.println("Screenshot availble to verify Movie details page background image");

			//Verify Child page doesn't show up when metatdata not available
			//Bug filed waiting for confirmation --NMA-9370

			//Verify MORE/LESS on screen


			driver.navigate().back();
			Thread.sleep(Constants.ThreadSleep);
		}
		System.out.println("==========================================================================");
		System.out.println("End TestCase  ---> Test_NMA_8999_9068()");
		System.out.println("==========================================================================");
	}

}
