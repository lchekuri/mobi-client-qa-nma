package com.mobitv.client.qa.nma.regression.tablet;

import io.appium.java_client.AppiumDriver;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.mobitv.client.qa.nma.common.Constants;
import com.mobitv.client.qa.nma.common.DataParser;
import com.mobitv.client.qa.nma.common.ExecuteShellComand;
import com.mobitv.client.qa.nma.common.Util;

public class TestSearch {
	private static Util util = new Util();
	//private static WebDriver driver = null;
	private static AppiumDriver driver = null;
	private DataParser mJsonParser = new DataParser();
	private ExecuteShellComand command = new ExecuteShellComand();
	String str_SearchClearHistory;
	WebElement wb_SearchSrcTxt;
	String str_SearchSrcTxt;

	@BeforeTest
	public void setUp() throws Exception {		
		driver = util.getAndroidDriver(driver);
		//command.executeCommand("Search");
	}

	@AfterTest
	public void tearDown() {
		driver.quit();
		//command.kill();
	}	

	@Test
	public void Test_NMA_9074() throws Exception{
		/*---- SEARCH PAGE AVAILABLE -----------
		 * Launch Application
		 * Select FEATURED tab on HomeScreen
		 * Click on SEARCH action icon in FEATURED Mainscreen
		 * Check Input cursor and embedded 'Search' text in search field when blank or cleared in Search screen.
		 * Check native soft KEYBOARD displayed in Search screen.
		 * Enter a keyword in the SEARCH field
		 * Select CLEAR button.
		 * Check keyword in the search field is removed and displayed blank search field and blank suggestions field
		 * Click device BACK button to hide keyboard
		 * Click application BACK button to navigate previous page.
		 * Select any one of the Tile in FEATURED MainScreen to navigate ChildPage.
		 * Check SEARCH action icon is displayed.
		 * Click BACK button to navigate previous page.
		 * Repeat steps 2-10 on Tabs LIVE,SHOWS, MOVIES, MUSIC, CLIPS and NETWORKS.
		 * Click on MENU in HomeScreen
		 * Select SHOP menu
		 * Check SEARCH action is available.
		 * Click BACK button to navigate previous page.
		 */
		
		System.out.println("Start Test_NMA_9074");
		util.fileWriting("Start Test_NMA_9074");
		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");

		WebElement Featured_Tab = driver.findElement(By.id(Constants.app_Package+":id/home_tab"));
		String str_FeaturedTab = Featured_Tab.getText();
		Featured_Tab.click();		
		System.out.println("Clicked on TAB : "+str_FeaturedTab);
		Thread.sleep(5000);

		util.takeScreenShot(driver,"NMA_9074","Screen1");

		WebElement wb_ActionSearch = driver.findElement(By.id(Constants.app_Package+":id/action_search"));
		wb_ActionSearch.click();
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Clicked on SEARCH action icon");

		util.takeScreenShot(driver,"NMA_9074","Screen2");

		wb_SearchSrcTxt = driver.findElement(By.id("android:id/search_src_text"));
		str_SearchSrcTxt = wb_SearchSrcTxt.getText();
		if(str_SearchSrcTxt.equalsIgnoreCase("Search")){
			System.out.println("Displayed input cursor and embedded 'Search' text in search field when blank or cleared");
			System.out.println("Displayed Native Soft Keyboard");
		}else{
			System.out.println("The SEARCH field is not empty");
		}

		WebElement wb_SearchSrcTxt = driver.findElement(By.id("android:id/search_src_text"));
		wb_SearchSrcTxt.sendKeys("Vivo");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver,"NMA_9074","Screen3");

		WebElement wb_SearchCloseBtn = driver.findElement(By.id("android:id/search_close_btn"));
		wb_SearchCloseBtn.click();		
		System.out.println("Removed the entered keyword in the search field and displayed blank search field");

		util.takeScreenShot(driver,"NMA_9074","Screen4");

		driver.navigate().back();
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver,"NMA_9074","Screen6");

		/*WebElement wb_UpButton = driver.findElement(By.id("android:id/up"));
		wb_UpButton.click();
		Thread.sleep(Constants.ThreadSleep);*/
		driver.navigate().back();
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Clicked on BACK button");


		util.takeScreenShot(driver,"NMA_9074","Screen7");

		wb_ActionSearch.isEnabled();
		System.out.println("Search action button is available on child page");

		driver.navigate().back();
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver,"NMA_9074","Screen8");

		WebElement Live_Tab = driver.findElement(By.id(Constants.app_Package+":id/live_guide_tab"));
		String str_LiveTab = Live_Tab.getText();
		Live_Tab.click();	
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Clicked on TAB : "+str_LiveTab);

		util.takeScreenShot(driver,"NMA_9074","Screen9");

		wb_ActionSearch = driver.findElement(By.id(Constants.app_Package+":id/action_search"));
		wb_ActionSearch.click();
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Clicked on SEARCH action icon");

		util.takeScreenShot(driver,"NMA_9074","Screen10");

		wb_SearchSrcTxt = driver.findElement(By.id("android:id/search_src_text"));
		str_SearchSrcTxt = wb_SearchSrcTxt.getText();
		if(str_SearchSrcTxt.equalsIgnoreCase("Search")){
			System.out.println("Displayed input cursor and embedded 'Search' text in search field when blank or cleared");
			System.out.println("Displayed Native Soft Keyboard");
		}else{
			System.out.println("The SEARCH field is not empty");
		}

		wb_SearchSrcTxt = driver.findElement(By.id("android:id/search_src_text"));
		wb_SearchSrcTxt.sendKeys("Vivo");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver,"NMA_9074","Screen11");

		wb_SearchCloseBtn = driver.findElement(By.id("android:id/search_close_btn"));
		wb_SearchCloseBtn.click();	
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Removed the entered keyword in the search field and displayed blank search field");

		util.takeScreenShot(driver,"NMA_9074","Screen12");

		driver.navigate().back();
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver,"NMA_9074","Screen13");

		/*wb_UpButton = driver.findElement(By.id("android:id/up"));
		wb_UpButton.click();
		Thread.sleep(Constants.ThreadSleep);*/
		driver.navigate().back();
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Clicked on BACK button");

		util.takeScreenShot(driver,"NMA_9074","Screen14");

		WebElement Shows_Tab = driver.findElement(By.id(Constants.app_Package+":id/shows_tab"));
		String str_ShowsTab = Shows_Tab.getText();
		Shows_Tab.click();	
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Clicked on TAB : "+str_ShowsTab);

		util.takeScreenShot(driver,"NMA_9074","Screen15");

		wb_ActionSearch = driver.findElement(By.id(Constants.app_Package+":id/action_search"));
		wb_ActionSearch.click();
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Clicked on SEARCH action icon");

		util.takeScreenShot(driver,"NMA_9074","Screen16");

		wb_SearchSrcTxt = driver.findElement(By.id("android:id/search_src_text"));
		str_SearchSrcTxt = wb_SearchSrcTxt.getText();
		if(str_SearchSrcTxt.equalsIgnoreCase("Search")){
			System.out.println("Displayed input cursor and embedded 'Search' text in search field when blank or cleared");
			System.out.println("Displayed Native Soft Keyboard");
		}else{
			System.out.println("The SEARCH field is not empty");
		}

		wb_SearchSrcTxt = driver.findElement(By.id("android:id/search_src_text"));
		wb_SearchSrcTxt.sendKeys("Vivo");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver,"NMA_9074","Screen17");

		wb_SearchCloseBtn = driver.findElement(By.id("android:id/search_close_btn"));
		wb_SearchCloseBtn.click();	
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Removed the entered keyword in the search field and displayed blank search field");

		util.takeScreenShot(driver,"NMA_9074","Screen18");

		driver.navigate().back();
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver,"NMA_9074","Screen19");

		/*wb_UpButton = driver.findElement(By.id("android:id/up"));
		wb_UpButton.click();
		Thread.sleep(Constants.ThreadSleep);*/
		driver.navigate().back();
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Clicked on BACK button");

		util.takeScreenShot(driver,"NMA_9074","Screen20");

		WebElement Movies_Tab = driver.findElement(By.id(Constants.app_Package+":id/movies_tab"));
		String str_MoviesTab = Movies_Tab.getText();
		Movies_Tab.click();		
		System.out.println("Clicked on TAB : "+str_MoviesTab);
		Thread.sleep(5000);

		util.takeScreenShot(driver,"NMA_9074","Screen21");

		wb_ActionSearch = driver.findElement(By.id(Constants.app_Package+":id/action_search"));
		wb_ActionSearch.click();
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Clicked on SEARCH action icon");

		util.takeScreenShot(driver,"NMA_9074","Screen22");

		wb_SearchSrcTxt = driver.findElement(By.id("android:id/search_src_text"));
		str_SearchSrcTxt = wb_SearchSrcTxt.getText();
		if(str_SearchSrcTxt.equalsIgnoreCase("Search")){
			System.out.println("Displayed input cursor and embedded 'Search' text in search field when blank or cleared");
			System.out.println("Displayed Native Soft Keyboard");
		}else{
			System.out.println("the SEARCH field is not empty");
		}

		wb_SearchSrcTxt = driver.findElement(By.id("android:id/search_src_text"));
		wb_SearchSrcTxt.sendKeys("Vivo");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver,"NMA_9074","Screen23");

		wb_SearchCloseBtn = driver.findElement(By.id("android:id/search_close_btn"));
		wb_SearchCloseBtn.click();	
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Removed the entered keyword in the search field and displayed blank search field");

		util.takeScreenShot(driver,"NMA_9074","Screen24");

		driver.navigate().back();
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver,"NMA_9074","Screen25");

		/*wb_UpButton = driver.findElement(By.id("android:id/up"));
		wb_UpButton.click();
		Thread.sleep(Constants.ThreadSleep);*/
		driver.navigate().back();
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Clicked on BACK button");

		util.takeScreenShot(driver,"NMA_9074","Screen26");

		/*WebElement wb_Tabs = driver.findElement(By.id(Constants.app_Package+":id/tabs"));
		SwipeAndScroll.scrollOnTab(driver, wb_Tabs, "Clips");
		Thread.sleep(5000);*/		

		WebElement Clip_Tab = driver.findElement(By.id(Constants.app_Package+":id/clips_tab"));
		String str_ClipTab = Clip_Tab.getText();
		Clip_Tab.click();		
		System.out.println("Clicked on TAB : "+str_ClipTab);
		Thread.sleep(5000);

		util.takeScreenShot(driver,"NMA_9074","Screen27");

		wb_ActionSearch = driver.findElement(By.id(Constants.app_Package+":id/action_search"));
		wb_ActionSearch.click();
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Clicked on SEARCH action icon");

		util.takeScreenShot(driver,"NMA_9074","Screen28");

		wb_SearchSrcTxt = driver.findElement(By.id("android:id/search_src_text"));
		str_SearchSrcTxt = wb_SearchSrcTxt.getText();
		if(str_SearchSrcTxt.equalsIgnoreCase("Search")){
			System.out.println("Displayed input cursor and embedded 'Search' text in search field when blank or cleared");
			System.out.println("Displayed Native Soft Keyboard");
		}else{
			System.out.println("the SEARCH field is not empty");
		}

		wb_SearchSrcTxt = driver.findElement(By.id("android:id/search_src_text"));
		wb_SearchSrcTxt.sendKeys("Vivo");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver,"NMA_9074","Screen29");

		wb_SearchCloseBtn = driver.findElement(By.id("android:id/search_close_btn"));
		wb_SearchCloseBtn.click();	
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Removed the entered keyword in the search field and displayed blank search field");

		util.takeScreenShot(driver,"NMA_9074","Screen30");

		driver.navigate().back();
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver,"NMA_9074","Screen31");

		/*wb_UpButton = driver.findElement(By.id("android:id/up"));
		wb_UpButton.click();
		Thread.sleep(Constants.ThreadSleep);*/
		driver.navigate().back();
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Clicked on BACK button");

		util.takeScreenShot(driver,"NMA_9074","Screen32");

		/*SwipeAndScroll.scrollOnTab(driver, wb_Tabs, "Music");
		Thread.sleep(5000);*/

		WebElement Music_Tab = driver.findElement(By.id(Constants.app_Package+":id/music_tab"));
		String str_MusicTab = Music_Tab.getText();
		Music_Tab.click();		
		System.out.println("Clicked on TAB : "+str_MusicTab);
		Thread.sleep(5000);

		util.takeScreenShot(driver,"NMA_9074","Screen33");

		wb_ActionSearch = driver.findElement(By.id(Constants.app_Package+":id/action_search"));
		wb_ActionSearch.click();
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Clicked on SEARCH action icon");

		util.takeScreenShot(driver,"NMA_9074","Screen34");

		wb_SearchSrcTxt = driver.findElement(By.id("android:id/search_src_text"));
		str_SearchSrcTxt = wb_SearchSrcTxt.getText();
		if(str_SearchSrcTxt.equalsIgnoreCase("Search")){
			System.out.println("Displayed input cursor and embedded 'Search' text in search field when blank or cleared");
			System.out.println("Displayed Native Soft Keyboard");
		}else{
			System.out.println("the SEARCH field is not empty");
		}

		wb_SearchSrcTxt = driver.findElement(By.id("android:id/search_src_text"));
		wb_SearchSrcTxt.sendKeys("Vivo");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver,"NMA_9074","Screen35");

		wb_SearchCloseBtn = driver.findElement(By.id("android:id/search_close_btn"));
		wb_SearchCloseBtn.click();	
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Removed the entered keyword in the search field and displayed blank search field");

		util.takeScreenShot(driver,"NMA_9074","Screen36");

		driver.navigate().back();
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver,"NMA_9074","Screen37");

		/*wb_UpButton = driver.findElement(By.id("android:id/up"));
		wb_UpButton.click();
		Thread.sleep(Constants.ThreadSleep);*/
		driver.navigate().back();
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Clicked on BACK button");

		util.takeScreenShot(driver,"NMA_9074","Screen38");

		/*SwipeAndScroll.scrollOnTab(driver, wb_Tabs, "Networks");
		Thread.sleep(5000);*/

		WebElement Networks_Tab = driver.findElement(By.id(Constants.app_Package+":id/networks_tab"));
		String str_Networks_Tab = Networks_Tab.getText();
		Networks_Tab.click();		
		System.out.println("Clicked on TAB : "+str_Networks_Tab);
		Thread.sleep(5000);

		util.takeScreenShot(driver,"NMA_9074","Screen39");

		wb_ActionSearch = driver.findElement(By.id(Constants.app_Package+":id/action_search"));
		wb_ActionSearch.click();
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Clicked on SEARCH action icon");

		util.takeScreenShot(driver,"NMA_9074","Screen40");

		wb_SearchSrcTxt = driver.findElement(By.id("android:id/search_src_text"));
		str_SearchSrcTxt = wb_SearchSrcTxt.getText();
		if(str_SearchSrcTxt.equalsIgnoreCase("Search")){
			System.out.println("Displayed input cursor and embedded 'Search' text in search field when blank or cleared");
			System.out.println("Displayed Native Soft Keyboard");
		}else{
			System.out.println("the SEARCH field is not empty");
		}

		wb_SearchSrcTxt = driver.findElement(By.id("android:id/search_src_text"));
		wb_SearchSrcTxt.sendKeys("Vivo");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver,"NMA_9074","Screen41");

		wb_SearchCloseBtn = driver.findElement(By.id("android:id/search_close_btn"));
		wb_SearchCloseBtn.click();		
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Removed the entered keyword in the search field and displayed blank search field and blank suggestions field");

		util.takeScreenShot(driver,"NMA_9074","Screen42");

		driver.navigate().back();
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver,"NMA_9074","Screen43");

		/*wb_UpButton = driver.findElement(By.id("android:id/up"));
		wb_UpButton.click();
		Thread.sleep(Constants.ThreadSleep);*/
		driver.navigate().back();
		Thread.sleep(1000);
		driver.navigate().back();
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Clicked on application BACK button");

		util.takeScreenShot(driver,"NMA_9074","Screen44");

		


	/* ---- CLEAR HISTORY -------
	 *Click on SEARCH action icon Home screen.
	 *Enter 10 different search keywords and get different search results.
	 *Select any one of the search history item and get search result.
	 *Enter any search keyword and select any one of the item in search suggestion list and get search result.
	 *Click on CLEAR HISTORY button.
	 *Displays blank search suggestion field and removes all search history results.
	 *Click on BACK button to navigate previous page.
	 */
		WebElement wb_aSearchSrcTxt = null;
		WebElement wb_aSearchActionClose = null;

		WebElement aFeatured_Tab = driver.findElement(By.id(Constants.app_Package+":id/home_tab"));
		String str_aFeaturedTab = aFeatured_Tab.getText();
		aFeatured_Tab.click();		
		System.out.println("Clicked on TAB : "+str_aFeaturedTab);
		Thread.sleep(5000);

		util.takeScreenShot(driver,"NMA_9074","Screen45");

		WebElement wb_aActionSearch = driver.findElement(By.id(Constants.app_Package+":id/action_search"));
		wb_aActionSearch.click();
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Clicked on SEARCH action icon");
		System.out.println("Entering 10 different search keywords and get different search results");
		String[] arr_SearchKeywords = {"ABC", "What", "Come", "Virgin", "Fleet", "Mob", "Virgin Fleet", "Dark", "Vivo", "Girl", "Wives"};

		util.takeScreenShot(driver,"NMA_9074","Screen46");

		for(int i=0; i<10; i++){

			wb_aSearchSrcTxt = driver.findElement(By.id("android:id/search_src_text"));
			wb_aSearchSrcTxt.sendKeys(arr_SearchKeywords[i]);
			Thread.sleep(Constants.ThreadSleep);

			util.takeScreenShot(driver,"NMA_9074","Screen47");

			command.executeAdbKeyEvent("66");
			Thread.sleep(Constants.ThreadSleep);
			System.out.println("Keyword-"+(i+1)+" Entered :"+arr_SearchKeywords[i]);

			wb_aSearchActionClose = driver.findElement(By.id(Constants.app_Package+":id/action_search_close"));
			wb_aSearchActionClose.click();
			Thread.sleep(Constants.ThreadSleep);			
		}

		List<WebElement> list_aMusicsReltVideo = driver.findElements(By.id(Constants.app_Package+":id/suggestion_list").id("android:id/text1"));

		if(list_aMusicsReltVideo.size()>0){
			util.takeScreenShot(driver,"NMA_9074","Screen48");

			list_aMusicsReltVideo.get(3).click();
			Thread.sleep(Constants.ThreadSleep);
			System.out.println("Clicked on 4th item of Historic search result");

			util.takeScreenShot(driver,"NMA_9074","Screen49");

			wb_aSearchActionClose.click();
			Thread.sleep(Constants.ThreadSleep);

			util.takeScreenShot(driver,"NMA_9074","Screen50");

			wb_aSearchSrcTxt.sendKeys("@#@$@$$");
			Thread.sleep(Constants.ThreadSleep);

			util.takeScreenShot(driver,"NMA_9074","Screen51");

			list_aMusicsReltVideo = driver.findElements(By.id(Constants.app_Package+":id/suggestion_list").id("android:id/text1"));
			if(list_aMusicsReltVideo.size()>0){
				list_aMusicsReltVideo.get(0).click();
				System.out.println("Selected First Item from Search Suggestion is included");
				util.takeScreenShot(driver,"NMA_9074","Screen52");

				wb_aSearchActionClose.click();
				Thread.sleep(Constants.ThreadSleep);
			}else{
				WebElement wb_aSearchCloseBtn = driver.findElement(By.id("android:id/search_close_btn"));
				wb_aSearchCloseBtn.click();
				Thread.sleep(Constants.ThreadSleep);
				util.takeScreenShot(driver,"NMA_9074","Screen53");
			}		

			WebElement wb_aSearchClearHistory = driver.findElement(By.id(Constants.app_Package+":id/search_clear_history"));
			str_SearchClearHistory = wb_aSearchClearHistory.getText();
			Assert.assertEquals("Clear History", str_SearchClearHistory);
			wb_aSearchClearHistory.click();
			Thread.sleep(Constants.ThreadSleep);
			System.out.println("Clicked on CLEAR HISTORY button");
			util.takeScreenShot(driver,"NMA_9074","Screen54");

			list_aMusicsReltVideo = driver.findElements(By.id(Constants.app_Package+":id/suggestion_list").id("android:id/text1"));
			if(list_aMusicsReltVideo.size() == 0){
				System.out.println("Displayed Blank Suggestion List");

			}

		}

		driver.navigate().back();
		Thread.sleep(1000);
		driver.navigate().back();
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Clicked on BACK button");

		util.takeScreenShot(driver,"NMA_9074","Screen55");

	/* -----SEARCH SUGGESTIONS
	 * Click on SEARCH action icon Home screen.
	 * Enter search text "Vivo" in SEARCH textbox.
	 * Check list search suggestion are displayed or not.
	 * Select one of search suggestion item in list.
	 * Close the search result list.
	 * Enter any one of character(Ex: V ) in search field.
	 * Check auto suggestions are displayed if minimum one character entered.
	 */

		WebElement bFeatured_Tab = driver.findElement(By.id(Constants.app_Package+":id/home_tab"));
		String str_bFeaturedTab = bFeatured_Tab.getText();
		bFeatured_Tab.click();		
		System.out.println("Clicked on TAB : "+str_bFeaturedTab);
		Thread.sleep(5000);

		util.takeScreenShot(driver,"NMA_9074","Screen56");

		WebElement wb_bActionSearch = driver.findElement(By.id(Constants.app_Package+":id/action_search"));
		wb_bActionSearch.click();
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Clicked on SEARCH icon");

		util.takeScreenShot(driver,"NMA_9074","Screen57");

		WebElement wb_bSearchSrcTxt = driver.findElement(By.id("android:id/search_src_text"));
		wb_bSearchSrcTxt.sendKeys("Vivo");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver,"NMA_9074","Screen58");

		JSONObject jsnObj_bSearchSuggestions = mJsonParser.callHttpRequest(Constants.Search_Suggestions,"GET", null);
		Thread.sleep(Constants.ThreadSleep);

		int int_bTotal = jsnObj_bSearchSuggestions.getInt("total");		

		if(int_bTotal>0){
			JSONArray jsnArray_bSearchSuggestions = jsnObj_bSearchSuggestions.getJSONArray("search_suggestions");
			System.out.println("Search Suggetions are avaibale");

			List<WebElement> list_bMusicsReltVideo = driver.findElements(By.id(Constants.app_Package+":id/suggestion_list").id("android:id/text1"));
			for(int i=0; i<list_bMusicsReltVideo.size(); i++){
				JSONObject json_bAutoSearchName = jsnArray_bSearchSuggestions.getJSONObject(i);
				String str_bAutoSearchName = json_bAutoSearchName.getString("name");
				System.out.println("str_AutoSearchName : "+str_bAutoSearchName);
				WebElement wb_bAutoSearchName = list_bMusicsReltVideo.get(i);
				String str_bwbAutoSearchName = wb_bAutoSearchName.getText();
				System.out.println("str_wbAutoSearchName : "+str_bwbAutoSearchName);
				//Assert.assertEquals(str_AutoSearchName, str_wbAutoSearchName);		    	
			}

			list_bMusicsReltVideo = driver.findElements(By.id(Constants.app_Package+":id/suggestion_list").id("android:id/text1"));
			System.out.println("list_MusicsReltVideo.size() : "+list_bMusicsReltVideo.size());
			list_bMusicsReltVideo.get(0).click();
			Thread.sleep(Constants.ThreadSleep);
			System.out.println("Selected first item from Search Suggestion is included");
			util.takeScreenShot(driver,"NMA_9074","Screen59");

			WebElement wb_bSearchActionClose = driver.findElement(By.id(Constants.app_Package+":id/action_search_close"));
			wb_bSearchActionClose.click();
			Thread.sleep(Constants.ThreadSleep);

			util.takeScreenShot(driver,"NMA_9074","Screen60");

			wb_bSearchSrcTxt.sendKeys("V");
			Thread.sleep(Constants.ThreadSleep);

			util.takeScreenShot(driver,"NMA_9074","Screen61");

			jsnObj_bSearchSuggestions = mJsonParser.callHttpRequest(Constants.Search_Suggestions,"GET", null);
			Thread.sleep(Constants.ThreadSleep);

			int_bTotal = jsnObj_bSearchSuggestions.getInt("total");
			if(int_bTotal>0){
				System.out.println("The Auto Search Suggestions are comming minimum character 1");
			}else{

			}

		}else{
			System.out.println("Search Suggestions are not available");
		}



	/* --------- SEARCH RESULT ---------------
	 * Click on SEARCH action icon Home screen.
	 * Enter search text "Vivo" in SEARCH textbox.
	 * Press Search/Enter key on native soft keyboard.
	 * The search result will display in SearchResult screen.
	 * Click on  particular search result item.
	 * It navigates SearchItem details screen.
	 */
		WebElement cFeatured_Tab = driver.findElement(By.id(Constants.app_Package+":id/home_tab"));
		String str_cFeaturedTab = cFeatured_Tab.getText();
		cFeatured_Tab.click();		
		System.out.println("Clicked on TAB : "+str_cFeaturedTab);
		Thread.sleep(5000);

		util.takeScreenShot(driver,"NMA_9074","Screen62");

		WebElement wb_cActionSearch = driver.findElement(By.id(Constants.app_Package+":id/action_search"));
		wb_cActionSearch.click();
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Clicked on SEARCH icon");

		util.takeScreenShot(driver,"NMA_9074","Screen63");

		WebElement wb_cSearchSrcTxt = driver.findElement(By.id("android:id/search_src_text"));
		wb_cSearchSrcTxt.sendKeys("yfbytg");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver,"NMA_9074","Screen64");

		command.executeAdbKeyEvent("66");
		Thread.sleep(Constants.ThreadSleep);

		JSONObject jsnObj_cSearchItemDetails = mJsonParser.callHttpRequest(Constants.Search_Details,"GET", null);
		Thread.sleep(Constants.ThreadSleep);

		int int_cTotal = jsnObj_cSearchItemDetails.getInt("total");

		if(int_cTotal>0){
			WebElement wb_Empty = driver.findElement(By.id("android:id/empty"));
			String str_wbEmpty = wb_Empty.getText();
			System.out.println(str_wbEmpty);
		}

		WebElement wb_cSearchActionClose = driver.findElement(By.id(Constants.app_Package+":id/action_search_close"));
		wb_cSearchActionClose.click();
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver,"NMA_9074","Screen65");

		wb_cSearchSrcTxt.sendKeys("Vivo");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver,"NMA_9074","Screen66");

		command.executeAdbKeyEvent("66");
		Thread.sleep(Constants.ThreadSleep);

		jsnObj_cSearchItemDetails = mJsonParser.callHttpRequest(Constants.Search_Details,"GET", null);
		Thread.sleep(Constants.ThreadSleep);

		int_cTotal = jsnObj_cSearchItemDetails.getInt("total");
		if(int_cTotal>0){
			JSONArray jsnArray_cSearchItemHits = jsnObj_cSearchItemDetails.getJSONArray("hits");
			List<WebElement> list_cSearchItemTitles = driver.findElements(By.id(Constants.app_Package+":id/search_result_list").id(Constants.app_Package+":id/item_search_title"));

			for(int j=0; j<list_cSearchItemTitles.size(); j++){
				try{
					JSONObject json_SeachDetailsInfo = jsnArray_cSearchItemHits.getJSONObject(j);
					JSONObject json_SeachDetailsResult = json_SeachDetailsInfo.getJSONObject("result");
					JSONObject json_SeachDetailsVOD = json_SeachDetailsResult.getJSONObject("vod");
					String str_SearchItemTitle = json_SeachDetailsVOD.getString("name");
					System.out.println("str_SearchItemTitle :"+str_SearchItemTitle);
					WebElement wbSearchItemTitle = list_cSearchItemTitles.get(j);
					String str_wbSearchItemTitle = list_cSearchItemTitles.get(j).getText();
					System.out.println("str_wbSearchItemTitle :"+str_wbSearchItemTitle);
					wbSearchItemTitle.click();
					System.out.println("Clicked on "+j+" item in search result list" );
					WebElement wb_ActionBarTitle = driver.findElement(By.id("android:id/action_bar_title"));
					String str_wbActionBarTitle = wb_ActionBarTitle.getText();
					Assert.assertEquals(str_wbActionBarTitle, str_wbSearchItemTitle);
					System.out.println(str_wbActionBarTitle+ "is available on search result details page");

					util.takeScreenShot(driver,"NMA_9074","Screen67");

					driver.navigate().back();
					Thread.sleep(Constants.ThreadSleep);
				}catch(Exception e){

				}
			}
		}

		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");
		System.out.println("End Test_NMA_9074");
		util.fileWriting("End Test_NMA_9074");
		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");

	}
}
