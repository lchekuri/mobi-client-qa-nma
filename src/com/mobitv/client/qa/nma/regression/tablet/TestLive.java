package com.mobitv.client.qa.nma.regression.tablet;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.mobitv.client.qa.nma.common.Constants;
import com.mobitv.client.qa.nma.common.DataParser;
import com.mobitv.client.qa.nma.common.ExecuteShellComand;
import com.mobitv.client.qa.nma.common.SwipeAndScroll;
import com.mobitv.client.qa.nma.common.Util;

public class TestLive {
	private static Util util = new Util();
	private static WebDriver driver = null;
	private ExecuteShellComand command = new ExecuteShellComand();
	private DataParser mJsonParser = new DataParser();
	private JSONArray tile_items, jsnArray_LiveTileItems, jArray_Programs;

	@BeforeTest
	public void setUp() throws Exception {
		driver = util.getAndroidDriver(driver);
		command.executeCommand("Tablet_Live");
	}

	@AfterTest
	public void tearDown() throws Exception {
		driver.quit();
		command.kill();
		Thread.sleep(Constants.ThreadSleep);
	}
	/**
	 * TEST: 
	 * 		Grid Template 1 - Live
	 * ACCEPTANCE CRITERIA
	 *		Only scroll up and down - Vertical scrolling
	 *		Pagination to support large numbers like hundreds  (pagination after 5+ rows for best optimization)
	 *		Each tile has a progress bar to represent how far the live programs have progressed
	 *		Each tile has a play button
	 *		Tiles are in landscape orientation
	 */
	@Test
	public void Test_NMA_8618_9072_9073() throws Exception {
		System.out.println("Start TestCase  ---> Test_NMA_8618()");
		System.out.println("==========================================================================");

		WebElement Live_Tab = driver.findElement(By.id(Constants.app_Package + ":id/live_guide_tab"));
		String str_LiveTab = Live_Tab.getText();
		Live_Tab.click();
		System.out.println("Clicked on TAB : " + str_LiveTab);
		util.fileWriting("Clicked on TAB : " + str_LiveTab);
		Thread.sleep(5000);
		
		WebElement clip_FilterBar = driver.findElement(By.id(Constants.app_Package+":id/filter_bar"));
		clip_FilterBar.click();
		System.out.println("Clicked on Filter to reset");
		Thread.sleep(3000);
		WebElement clip_FilterReset = driver.findElement(By.id(Constants.app_Package+":id/dialog_filters_reset"));
		clip_FilterReset.click();
		System.out.println("RESET Button Clicked");
		WebElement clip_FilterApplyBtn = driver.findElement(By.id(Constants.app_Package+":id/dialog_filters_apply_btn"));
		clip_FilterApplyBtn.click();

		JSONObject jObj_ChannelListing = mJsonParser.callHttpRequest(Constants.Live_MainURL, "GET", null);
		Thread.sleep(Constants.ThreadSleep);
		jsnArray_LiveTileItems = jObj_ChannelListing.getJSONArray("tile_items");

		List<WebElement> list_LiveChannelThumb = driver.findElements(By.id(Constants.app_Package + ":id/channel_thumb"));
		List<WebElement> list_LiveChannelNumber = driver.findElements(By.id(Constants.app_Package + ":id/channel_number"));
		List<WebElement> list_LiveChannelName = driver.findElements(By.id(Constants.app_Package + ":id/channel_name"));
		List<WebElement> list_LiveChannelProgress = driver.findElements(By.id(Constants.app_Package + ":id/channel_progress"));
		List<WebElement> list_LiveChannelIconPlay = driver.findElements(By.id(Constants.app_Package + ":id/channel_icon_play"));
		List<WebElement> list_LiveProgName = driver.findElements(By.id(Constants.app_Package + ":id/program_name"));
		List<WebElement> list_LiveProgDuration = driver.findElements(By.id(Constants.app_Package + ":id/program_duration"));
		
		for (int i = 0; i < list_LiveChannelProgress.size(); i++) {
			System.out.println("*****************************");

			JSONObject json_TileItem = jsnArray_LiveTileItems.getJSONObject(i);
			int str_ChannelPosition = json_TileItem.getInt("display_position");
			String str_ChannelName = json_TileItem.getString("name");
			String str_ChannelRefID = json_TileItem.getString("ref_id");

			int str_wbLiveChannelNumber = Integer.parseInt(list_LiveChannelNumber.get(i).getText());
			System.out.println("Channel-" + str_wbLiveChannelNumber + "  position is available in Live Grid Template");
			Assert.assertEquals(str_wbLiveChannelNumber, str_ChannelPosition);

			String str_wbLiveChannelName = list_LiveChannelName.get(i).getText();
			System.out.println(str_wbLiveChannelName + " :Channel-" + str_wbLiveChannelNumber + " name is available in Live Grid Template");
			Assert.assertEquals(str_wbLiveChannelName, str_ChannelName);

			WebElement wbLiveChannelThumb = list_LiveChannelThumb.get(i);
			wbLiveChannelThumb.isEnabled();
			System.out.println(str_wbLiveChannelName + " :Channel-" + str_wbLiveChannelNumber + " image thumb is available in Live Grid Template");

			WebElement wbLiveChannelProgress = list_LiveChannelProgress.get(i);
			wbLiveChannelProgress.isEnabled();
			System.out.println(str_wbLiveChannelName + " :Channel-" + str_wbLiveChannelNumber + " progressbar is available in Live Grid Template");

			WebElement wbLiveChannelIconPlay = list_LiveChannelIconPlay.get(i);
			wbLiveChannelIconPlay.isEnabled();
			System.out.println(str_wbLiveChannelName + " :Channel-" + str_wbLiveChannelNumber + " play icon is available in Live Grid Template");
			
			Assert.assertTrue(list_LiveProgName.get(i).isEnabled());
			Assert.assertTrue(list_LiveProgDuration.get(i).isEnabled());		


		}
		//Verify tile orientation and play button
		util.takeScreenShot(driver,"Test_tablet_Live","Live_Mainscreen");
		System.out.println("Screenshot availble to verify Live page image orientation and play button");

		// Verify vertical scrolling up
		for (int j = 0; j < 2; j++) {
			try {
				SwipeAndScroll.swipeGrid_One(driver);
				Thread.sleep(Constants.ThreadSleep);
				System.out.println("Vertical scrolling down available on Live");
			} catch (Exception e) {
				e.getMessage();
			}

		}
		// Verify vertical scrolling scroll down
		for (int j = 0; j < 2; j++) {
			try {
				SwipeAndScroll.swipeGrid_Three(driver);
				Thread.sleep(Constants.ThreadSleep);
				System.out.println("Vertical scrolling up available on Live");
			} catch (Exception e) {
				e.getMessage();
			}
		}
		System.out.println("==========================================================================");
		System.out.println("End TestCase  ---> Test_NMA_8618()");
		System.out.println("==========================================================================");
	}
	
	@Test
	public void Test_NMA_9055() throws Exception {
		System.out.println("Start TestCase  ---> Test_NMA_9055()");
		System.out.println("==========================================================================");

		WebElement Live_Tab = driver.findElement(By.id(Constants.app_Package + ":id/live_guide_tab"));
		String str_LiveTab = Live_Tab.getText();
		Live_Tab.click();
		System.out.println("Clicked on TAB : " + str_LiveTab);
		util.fileWriting("Clicked on TAB : " + str_LiveTab);
		Thread.sleep(5000);
		
		WebElement wb_FullGuide = driver.findElement(By.id(Constants.app_Package + ":id/full_guide"));
		String str_wbFullGuide = wb_FullGuide.getText();
		wb_FullGuide.click();
		System.out.println("Clicked on : " + str_wbFullGuide);
		Thread.sleep(5000);
		
		SwipeAndScroll.swipeGrid_One(driver);
		Thread.sleep(Constants.ThreadSleep);
		SwipeAndScroll.swipeGrid_Three(driver);
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Vertical Scrolling up and down available on Full Guide EPG");
		SwipeAndScroll.swipeLeft(driver);
		Thread.sleep(Constants.ThreadSleep);
		SwipeAndScroll.swipeRight(driver);
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("horizontal  Scrolling left and right available on Full Guide EPG");
		
		WebElement wb_ProgramName = driver.findElement(By.id(Constants.app_Package + ":id/program_cell"));
		wb_ProgramName.click();		
		System.out.println("Clicked on program name is displayed in time block");
		Thread.sleep(Constants.ThreadSleep);
		
		WebElement wb_ProgramDetailName = driver.findElement(By.id(Constants.app_Package + ":id/program_details_program_name"));
		String str_wbProgramDetailName = wb_ProgramDetailName.getText();
		System.out.println("Launched " +str_wbProgramDetailName + " program detail overlay");
					
		driver.navigate().back();
		
		WebElement wb_ChannelThumb = driver.findElement(By.id(Constants.app_Package+":id/channel_thumb"));
		wb_ChannelThumb.isDisplayed();
		System.out.println("Channel logo is displayed");
		wb_ChannelThumb.click();
		System.out.println("Clicked on channel logo");
		Thread.sleep(5000);
		
		WebElement wb_ChannelDetailName = driver.findElement(By.id(Constants.app_Package + ":id/channel_details_channel_name"));
		String str_wbChannelDetailName = wb_ChannelDetailName.getText();
		System.out.println("Launched " +str_wbChannelDetailName + " channel detail overlay");
		Thread.sleep(5000);
		
		driver.navigate().back();
		
		WebElement wb_DateTxtDay = driver.findElement(By.id(Constants.app_Package + ":id/date_txt_day"));
		String str_wbDateTxtDay = wb_DateTxtDay.getText();
		System.out.println("Top left corner of the grid displayed: "+str_wbDateTxtDay);
		wb_DateTxtDay.click();		
		System.out.println("Clicked on " + str_wbDateTxtDay+", displayed the overlay for date selection");
		Thread.sleep(Constants.ThreadSleep);
		
	}

}