package com.mobitv.client.qa.nma.regression.tablet;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.mobitv.client.qa.nma.common.Constants;
import com.mobitv.client.qa.nma.common.DataParser;
import com.mobitv.client.qa.nma.common.ExecuteShellComand;
import com.mobitv.client.qa.nma.common.Util;

public class TestFilters {
	private static Util util = new Util();
	private static WebDriver driver = null;
	private ExecuteShellComand command = new ExecuteShellComand();
	private DataParser mJsonParser = new DataParser();


	@BeforeTest
	public void setUp() throws Exception {		
		driver = util.getAndroidDriver(driver);
		command.executeCommand("Tablet_Networks");
	}

	@AfterTest
	public void tearDown() throws Exception{
		driver.quit();
		command.kill();
		Thread.sleep(Constants.ThreadSleep);
	}
	
	
	
	
/**
	 * TEST: 
	 * 		Build the filters Live (Content, Available On)
	 * 		Universally applied on the items that are relevant to the page.
	 * 		Filter bar is transparent and always stays at the top like mobile 
	 * ACCEPTANCE CRITERIA
	 *		Tablet has filters in Live, Movies, and Shows page
	 *		Set filters are displayed with ellipsis if ran out of space => this change applies to mobile as well
	 *		Common filters are applied universally 
	 */

	@Test
	public void Test_NMA_9075_Live() throws Exception {

		System.out.println("Start TestCase  ---> Test_NMA_9075_Live()");
		System.out.println("==========================================================================");


		WebElement Live_Tab = driver.findElement(By.id(Constants.app_Package+":id/live_guide_tab"));
		String str_Live_Tab = Live_Tab.getText();
		Live_Tab.click();		
		System.out.println("Clicked on TAB : "+str_Live_Tab);
		util.fileWriting("Clicked on TAB : "+str_Live_Tab);
		Thread.sleep(5000);

		util.takeScreenShot(driver,"Test_Filters","Live_MainScreen");

		try{
			WebElement Filter_Bar_Clear_Button = driver.findElement(By.id(Constants.app_Package+":id/filter_bar_clear_button"));
			System.out.println("Curretly there are some prvious filters, we are clearing them to verify our test case");
			util.fileWriting("Curretly there are some prvious filters, we are clearing them to verify our test case");
			util.takeScreenShot(driver,"Test_Filters","Live_Screen_With_Some_Previous_Filters");
			Filter_Bar_Clear_Button.click();
			Thread.sleep(Constants.ThreadSleep);
		}catch(Exception e){
			System.out.println("Currently Live screen is with no filters");
			util.fileWriting("Currently Live screen is with no filters");
		}

		WebElement Filter_Bar_Layout = driver.findElement(By.id(Constants.app_Package+":id/filter_bar_text_layout"));
		Filter_Bar_Layout.click();
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Clicked on Filter bar Layout");
		util.fileWriting("Clicked on Fitler bar Layout");


		WebElement Filter_Dialogue_Reset_Button = driver.findElement(By.id(Constants.app_Package+":id/dialog_filters_reset"));
		if(Filter_Dialogue_Reset_Button.isDisplayed()){
			System.out.println("Filters Dialogue on Live page Has bee opened");
			util.fileWriting("Filters Dialogue on Live page Has bee opened");
			util.takeScreenShot(driver,"Test_Filters","Filter_Dialogue_LiveScreen");

			System.out.println("Applying filters on Live screen");
			util.fileWriting("Applying filters on Live screen");

			List<WebElement> FilterCategories = driver.findElements(By.id(Constants.app_Package+":id/filter_category"));
			int count = FilterCategories.size();
			assertTrue(count == 2);

			List<WebElement> Filter_Childrens = driver.findElements(By.id(Constants.app_Package+":id/filter_children"));
			int size;
			String[] filters = new String[3];
			if(Filter_Childrens.size() > 4)
				size = 4;
			else
				size = Filter_Childrens.size();
			WebElement Filter_Children;
			for(int i = 1;i < size;i++){
				Filter_Children = Filter_Childrens.get(i);
				filters[i-1] = Filter_Children.getText().toLowerCase();
				System.out.println("Clicking on Filter "+filters[i-1]);
				Filter_Children.click();
				Thread.sleep(Constants.ThreadSleep);
			}

			WebElement Apply_Button = driver.findElement(By.id(Constants.app_Package+":id/dialog_filters_apply_btn"));
			Apply_Button.click();
			Thread.sleep(Constants.ThreadSleep);
			util.takeScreenShot(driver,"Test_Filters","LiveScreen_WithFilters_Applied");

			WebElement Filter_Bar_Layout_Text = driver.findElement(By.id(Constants.app_Package+":id/filter_bar"));
			String str_Filter_Bar_Layout_Text = Filter_Bar_Layout_Text.getText().toLowerCase();
			for(int i=1;i<size;i++){
				assertTrue(str_Filter_Bar_Layout_Text.contains(filters[i-1]));
			}

			if(str_Filter_Bar_Layout_Text.contains("...")){
				System.out.println("Filter Bar Layout text contains elipses");
				util.fileWriting("Filter Bar Layout text contains elipses");
			}else{
				System.out.println("Filter Bar Layout text not contains elipses");
				util.fileWriting("Filter Bar Layout text not contains elipses");
			}
			WebElement Filter_Bar_Clear_Button = driver.findElement(By.id(Constants.app_Package+":id/filter_bar_clear_button"));
			Filter_Bar_Clear_Button.click();
			Thread.sleep(Constants.ThreadSleep);
		}else{
			System.out.println("Something went wrong Filter dialogue not opened on Live screen");
			util.fileWriting("Something went wrong Filter dialogue not opened on Live screen");
			util.takeScreenShot(driver,"Test_Filters","Error_In_Showing_Filter_DialogueIn_Live_Screen");
		}

		System.out.println("==========================================================================");
		System.out.println("End TestCase  ---> Test_NMA_9075_Live()");
		System.out.println("==========================================================================");
	}

	/**
	 * TEST: 
	 * 		Build the filters Shows (Content, Available On, Genres, Parental Guidance)
	 * 		Universally applied on the items that are relevant to the page.
	 * 		Filter bar is transparent and always stays at the top like mobile 
	 * ACCEPTANCE CRITERIA
	 *		Tablet has filters in Live, Movies, and Shows page
	 *		Set filters are displayed with ellipsis if ran out of space => this change applies to mobile as well
	 *		Common filters are applied universally 
	 */
	@Test
	public void Test_NMA_9075_Shows() throws Exception {

		System.out.println("Start TestCase  ---> Test_NMA_9075_Shows()");
		System.out.println("==========================================================================");


		WebElement Shows_Tab = driver.findElement(By.id(Constants.app_Package+":id/shows_tab"));
		String str_Shows_Tab = Shows_Tab.getText();
		Shows_Tab.click();		
		System.out.println("Clicked on TAB : "+str_Shows_Tab);
		util.fileWriting("Clicked on TAB : "+str_Shows_Tab);
		Thread.sleep(5000);

		util.takeScreenShot(driver,"Test_Filters","Shows_MainScreen");

		try{
			WebElement Filter_Bar_Clear_Button = driver.findElement(By.id(Constants.app_Package+":id/filter_bar_clear_button"));
			System.out.println("Curretly there are some prvious filters, we are clearing them to verify our test case");
			util.fileWriting("Curretly there are some prvious filters, we are clearing them to verify our test case");
			util.takeScreenShot(driver,"Test_Filters","Shows_Screen_With_Some_Previous_Filters");
			Filter_Bar_Clear_Button.click();
			Thread.sleep(Constants.ThreadSleep);
		}catch(Exception e){
			System.out.println("Currently Shows screen is with no filters");
			util.fileWriting("Currently Shows screen is with no filters");
		}

		WebElement Filter_Bar_Layout = driver.findElement(By.id(Constants.app_Package+":id/filter_bar_text_layout"));
		Filter_Bar_Layout.click();
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Clicked on Filter bar Layout");
		util.fileWriting("Clicked on Fitler bar Layout");


		WebElement Filter_Dialogue_Reset_Button = driver.findElement(By.id(Constants.app_Package+":id/dialog_filters_reset"));
		if(Filter_Dialogue_Reset_Button.isDisplayed()){
			System.out.println("Filters Dialogue on Shows page Has bee opened");
			util.fileWriting("Filters Dialogue on Shows page Has bee opened");
			util.takeScreenShot(driver,"Test_Filters","Filter_Dialogue_ShowsScreen");

			System.out.println("Applying filters on Shows screen");
			util.fileWriting("Applying filters on Shows screen");

			List<WebElement> FilterCategories = driver.findElements(By.id(Constants.app_Package+":id/filter_category"));
			int count = FilterCategories.size();
			assertTrue(count == 4);

			List<WebElement> Filter_Childrens = driver.findElements(By.id(Constants.app_Package+":id/filter_children"));
			int size;
			String[] filters = new String[Filter_Childrens.size()];
			size = Filter_Childrens.size();
			WebElement Filter_Children;
			for(int i = 0;i < size;i++){
				Filter_Children = Filter_Childrens.get(i);
				filters[i] = Filter_Children.getText().toLowerCase();
				System.out.println("Clicking on Filter "+filters[i]);
				Filter_Children.click();
				Thread.sleep(Constants.ThreadSleep);
			}

			WebElement Apply_Button = driver.findElement(By.id(Constants.app_Package+":id/dialog_filters_apply_btn"));
			Apply_Button.click();
			Thread.sleep(Constants.ThreadSleep);
			util.takeScreenShot(driver,"Test_Filters","LiveScreen_WithFilters_Applied");

			WebElement Filter_Bar_Layout_Text = driver.findElement(By.id(Constants.app_Package+":id/filter_bar"));
			String str_Filter_Bar_Layout_Text = Filter_Bar_Layout_Text.getText().toLowerCase();
			System.out.println("Filter Bat Text is "+str_Filter_Bar_Layout_Text);
			for(int i=0;i<size;i++){
				if(!filters[i].equals("all"))
					assertTrue(str_Filter_Bar_Layout_Text.contains(filters[i]));
			}

			if(str_Filter_Bar_Layout_Text.contains("...")){
				System.out.println("Filter Bar Layout text contains elipses");
				util.fileWriting("Filter Bar Layout text contains elipses");
			}else{
				System.out.println("Filter Bar Layout text not contains elipses");
				util.fileWriting("Filter Bar Layout text not contains elipses");
			}
			WebElement Filter_Bar_Clear_Button = driver.findElement(By.id(Constants.app_Package+":id/filter_bar_clear_button"));
			Filter_Bar_Clear_Button.click();
			Thread.sleep(Constants.ThreadSleep);
		}else{
			System.out.println("Something went wrong Filter dialogue not opened on Shows screen");
			util.fileWriting("Something went wrong Filter dialogue not opened on Shows screen");
			util.takeScreenShot(driver,"Test_Filters","Error_In_Showing_Filter_DialogueIn_Shows_Screen");
		}

		System.out.println("==========================================================================");
		System.out.println("End TestCase  ---> Test_NMA_9075_Shows()");
		System.out.println("==========================================================================");
	}

	/**
	 * TEST: 
	 * 		Build the filters Movies (Content, Available On, Genres, Parental Guidance)
	 * 		Universally applied on the items that are relevant to the page.
	 * 		Filter bar is transparent and always stays at the top like mobile 
	 * ACCEPTANCE CRITERIA
	 *		Tablet has filters in Live, Movies, and Shows page
	 *		Set filters are displayed with ellipsis if ran out of space => this change applies to mobile as well
	 *		Common filters are applied universally 
	 */
	@Test
	public void Test_NMA_9075_Movies() throws Exception {

		System.out.println("Start TestCase  ---> Test_NMA_9075_Movies()");
		System.out.println("==========================================================================");


		WebElement Movies_Tab = driver.findElement(By.id(Constants.app_Package+":id/movies_tab"));
		String str_Movies_Tab = Movies_Tab.getText();
		Movies_Tab.click();		
		System.out.println("Clicked on TAB : "+str_Movies_Tab);
		util.fileWriting("Clicked on TAB : "+str_Movies_Tab);
		Thread.sleep(5000);

		util.takeScreenShot(driver,"Test_Filters","Movies_MainScreen");

		try{
			WebElement Filter_Bar_Clear_Button = driver.findElement(By.id(Constants.app_Package+":id/filter_bar_clear_button"));
			System.out.println("Curretly there are some prvious filters, we are clearing them to verify our test case");
			util.fileWriting("Curretly there are some prvious filters, we are clearing them to verify our test case");
			util.takeScreenShot(driver,"Test_Filters","Movies_Screen_With_Some_Previous_Filters");
			Filter_Bar_Clear_Button.click();
			Thread.sleep(Constants.ThreadSleep);
		}catch(Exception e){
			System.out.println("Currently Movies screen is with no filters");
			util.fileWriting("Currently Movies screen is with no filters");
		}

		WebElement Filter_Bar_Layout = driver.findElement(By.id(Constants.app_Package+":id/filter_bar_text_layout"));
		Filter_Bar_Layout.click();
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Clicked on Filter bar Layout");
		util.fileWriting("Clicked on Fitler bar Layout");


		WebElement Filter_Dialogue_Reset_Button = driver.findElement(By.id(Constants.app_Package+":id/dialog_filters_reset"));
		if(Filter_Dialogue_Reset_Button.isDisplayed()){
			System.out.println("Filters Dialogue on Movies page Has bee opened");
			util.fileWriting("Filters Dialogue on Movies page Has bee opened");
			util.takeScreenShot(driver,"Test_Filters","Filter_Dialogue_MoviesScreen");

			System.out.println("Applying filters on Movies screen");
			util.fileWriting("Applying filters on Movies screen");

			List<WebElement> FilterCategories = driver.findElements(By.id(Constants.app_Package+":id/filter_category"));
			int count = FilterCategories.size();
			assertTrue(count == 4);

			List<WebElement> Filter_Childrens = driver.findElements(By.id(Constants.app_Package+":id/filter_children"));
			int size;
			String[] filters = new String[Filter_Childrens.size()];
			size = Filter_Childrens.size();
			WebElement Filter_Children;
			for(int i = 0;i < size;i++){
				Filter_Children = Filter_Childrens.get(i);
				filters[i] = Filter_Children.getText().toLowerCase();
				System.out.println("Clicking on Filter "+filters[i]);
				Filter_Children.click();
				Thread.sleep(Constants.ThreadSleep);
			}

			WebElement Apply_Button = driver.findElement(By.id(Constants.app_Package+":id/dialog_filters_apply_btn"));
			Apply_Button.click();
			Thread.sleep(Constants.ThreadSleep);
			util.takeScreenShot(driver,"Test_Filters","LiveScreen_WithFilters_Applied");

			WebElement Filter_Bar_Layout_Text = driver.findElement(By.id(Constants.app_Package+":id/filter_bar"));
			String str_Filter_Bar_Layout_Text = Filter_Bar_Layout_Text.getText().toLowerCase();
			for(int i=0;i<size;i++){
				if(!filters[i].equals("all"))
					assertTrue(str_Filter_Bar_Layout_Text.contains(filters[i]));
			}

			if(str_Filter_Bar_Layout_Text.contains("...")){
				System.out.println("Filter Bar Layout text contains elipses");
				util.fileWriting("Filter Bar Layout text contains elipses");
			}else{
				System.out.println("Filter Bar Layout text not contains elipses");
				util.fileWriting("Filter Bar Layout text not contains elipses");
			}
			WebElement Filter_Bar_Clear_Button = driver.findElement(By.id(Constants.app_Package+":id/filter_bar_clear_button"));
			Filter_Bar_Clear_Button.click();
		}else{
			System.out.println("Something went wrong Filter dialogue not opened on Movies screen");
			util.fileWriting("Something went wrong Filter dialogue not opened on Movies screen");
			util.takeScreenShot(driver,"Test_Filters","Error_In_Showing_Filter_DialogueIn_Moviesa_Screen");
		}
		System.out.println("==========================================================================");
		System.out.println("End TestCase  ---> Test_NMA_9075_Movies()");
		System.out.println("==========================================================================");
	}
	@Test
	public void Test_NMA_9066_Clips() throws Exception {
		System.out.println("Start TestCase  ---> Test_NMA_9066()");
		System.out.println("==========================================================================");

		WebElement Clip_Tab = driver.findElement(By.id(Constants.app_Package + ":id/clips_tab"));
		String str_ClipTab = Clip_Tab.getText();
		Clip_Tab.click();
		System.out.println("Clicked on TAB : " + str_ClipTab);
		util.fileWriting("Clicked on TAB : " + str_ClipTab);
		Thread.sleep(5000);
		
		WebElement clip_FilterBar = driver.findElement(By.id(Constants.app_Package+":id/filter_bar"));
		String str_FilterBar = clip_FilterBar.getText();
		util.takeScreenShot(driver,"Test_NMA_9066","Screen1");
		clip_FilterBar.click();
		System.out.println(str_FilterBar+" is Selected");
		Thread.sleep(Constants.ThreadSleep);
		util.takeScreenShot(driver,"Test_NMA_9066","Screen2");

		WebElement clip_FilterReset = driver.findElement(By.id(Constants.app_Package+":id/dialog_filters_reset"));
		String str_FilterReset = clip_FilterReset.getText();
		clip_FilterReset.click();
		System.out.println(str_FilterReset+": Button Clicked");
		Thread.sleep(Constants.ThreadSleep);

		WebElement clip_FilterApplyBtn = driver.findElement(By.id(Constants.app_Package+":id/dialog_filters_apply_btn"));
		String str_FilterApplyBtn = clip_FilterApplyBtn.getText();
		clip_FilterApplyBtn.click();
		System.out.println(str_FilterApplyBtn+": Button Clicked");
		Thread.sleep(Constants.ThreadSleep);
		
		util.takeScreenShot(driver,"Test_NMA_9066","Screen3");

		JSONObject jsnObj_ClipMainURL = mJsonParser.callHttpRequest(Constants.Clips_MainURL, "GET", null);
		Thread.sleep(Constants.ThreadSleep);

		int str_ClipTotal = jsnObj_ClipMainURL.getInt("total");
		System.out.println("str_ClipTotal : " + str_ClipTotal);
		util.fileWriting("str_ClipTotal : " + str_ClipTotal);
		JSONArray jsnArray_Hits = jsnObj_ClipMainURL.getJSONArray("hits");

		List<WebElement> list_ClipsCellThumb = driver.findElements(By.id(Constants.app_Package + ":id/cell_thumb"));
		List<WebElement> list_CellIconPlay = driver.findElements(By.id(Constants.app_Package + ":id/cell_icon_play"));
		List<WebElement> list_ClipsCellTitle = driver.findElements(By.id(Constants.app_Package + ":id/cell_txt_title"));
		List<WebElement> list_ClipsCellDesc = driver.findElements(By.id(Constants.app_Package + ":id/cell_txt_desc"));

		for (int i = 0; i < list_ClipsCellDesc.size(); i++) {
			System.out.println("*****************************");

			JSONObject json_ClipsInfo = jsnArray_Hits.getJSONObject(i);
			JSONObject json_ClipResult = json_ClipsInfo.getJSONObject("result");
			JSONObject json_ClipSeries = json_ClipResult.getJSONObject("vod");

			String str_ClipID = json_ClipSeries.getString("id");
			String str_ClipName = json_ClipSeries.getString("name");
			String str_ClipNetwork = json_ClipSeries.getString("network");
			String str_clipDesc = json_ClipSeries.getString("description");
			int str_clipDuration = json_ClipSeries.getInt("duration");

			WebElement wb_ClipsCellName = list_ClipsCellTitle.get(i);
			String str_wbClipsCellTitle = wb_ClipsCellName.getText();
			Assert.assertEquals(str_ClipName, str_wbClipsCellTitle);
			System.out.println(str_wbClipsCellTitle + " :Clip-" + (i + 1) + " name is available in Clips Grid Template");

			String str_wbClipsNetwork = list_ClipsCellDesc.get(i).getText();
			Assert.assertEquals(str_ClipNetwork, str_wbClipsNetwork);
			System.out.println(str_wbClipsNetwork + " :Clip-" + (i + 1) + " network is available in Clips Grid Template");

			WebElement wbClipsCellThumb = list_ClipsCellThumb.get(i);
			wbClipsCellThumb.isEnabled();
			System.out.println(str_wbClipsCellTitle + " :Clips-" + (i + 1) + " image thumb is in landscape orientation on Clips Grid Template");

			WebElement wbClipsIconPlay = list_CellIconPlay.get(i);
			wbClipsIconPlay.isEnabled();
			System.out.println(str_wbClipsCellTitle + " :Clips-" + (i + 1) + " play icon is available in Clips Grid Template");
		}
		
		System.out.println("Click on filter");
		clip_FilterBar.click();
		
		WebElement clip_FilterChildren_MyPacks = driver.findElement(By.id(Constants.app_Package+":id/dialog_filters_container").id(Constants.app_Package+":id/filter_children").name("My Packs"));
		String str_FilterChildren_MyPacks = clip_FilterChildren_MyPacks.getText();
		clip_FilterChildren_MyPacks.click();
		System.out.println(str_FilterChildren_MyPacks+": Category selected");
		Thread.sleep(Constants.ThreadSleep);
		
		util.takeScreenShot(driver,"Test_NMA_9066","Screen5");

		WebElement clip_FilterApplyBtn1 = driver.findElement(By.id(Constants.app_Package+":id/dialog_filters_apply_btn"));
		String str_FilterApplyBtn1 = clip_FilterApplyBtn1.getText();
		clip_FilterApplyBtn1.click();
		System.out.println(str_FilterApplyBtn1+": Button Clicked");
		Thread.sleep(10000);

		WebElement clip_FilterBar2 = driver.findElement(By.id(Constants.app_Package+":id/filter_bar"));
		String str_FilterBar2 = clip_FilterBar2.getText();
		System.out.println("Clips Filter Name: "+str_FilterBar2);
		
		util.takeScreenShot(driver,"Test_NMA_9066","Screen6");
		
		WebElement Music_Tab = driver.findElement(By.id(Constants.app_Package + ":id/music_tab"));
		String str_MusicTab = Music_Tab.getText();
		Music_Tab.click();
		System.out.println("Clicked on TAB : " + str_MusicTab);
		util.fileWriting("Clicked on TAB : " + str_MusicTab);
		Thread.sleep(5000);
		
		JSONObject jsnObj_MusicMainURL = mJsonParser.callHttpRequest(Constants.Music_MainURL,"GET", null);
		Thread.sleep(Constants.ThreadSleep);

		WebElement music_FilterBar = driver.findElement(By.id(Constants.app_Package+":id/filter_bar"));
		String str_MusicFilterBar = music_FilterBar.getText();
		System.out.println("Music Filter Name: "+str_MusicFilterBar);
		Assert.assertEquals("The Clips and Music Filter is not same", str_FilterBar2, str_MusicFilterBar);
	}

}
