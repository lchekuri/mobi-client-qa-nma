package com.mobitv.client.qa.nma.regression.tablet;

import static org.junit.Assert.assertTrue;

import java.util.List;
import org.apache.commons.lang3.text.WordUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.mobitv.client.qa.nma.common.Constants;
import com.mobitv.client.qa.nma.common.DataParser;
import com.mobitv.client.qa.nma.common.ExecuteShellComand;
import com.mobitv.client.qa.nma.common.Util;

public class TestNetworks {
	private static Util util = new Util();
	private static WebDriver driver = null;
	private DataParser mJsonParser = new DataParser();
	private ExecuteShellComand command = new ExecuteShellComand();

	@BeforeTest
	public void setUp() throws Exception {		
		driver = util.getAndroidDriver(driver);
		command.executeCommand("Tablet_Networks");
	}

	@AfterTest
	public void tearDown() throws Exception{
		driver.quit();
		command.kill();
		Thread.sleep(Constants.ThreadSleep);
	}

	/**
	 * TEST: 
	 * 		Network Details Page
	 * ACCEPTANCE CRITERIA
	 *		Content belonging to the network is displayed in the order specified
	 *		Clicking onto respectable tiles/area launches either detail page, detail overlay, or player
	 */
	@Test
	public void Test_NMA_8995_9069() throws Exception {
		System.out.println("Start TestCase  ---> Test_NMA_8995_9069()");
		System.out.println("==========================================================================");

		WebElement Networks_Tab = driver.findElement(By.id(Constants.app_Package+":id/networks_tab"));
		String str_Networks_Tab = Networks_Tab.getText();
		Networks_Tab.click();		
		System.out.println("Clicked on TAB : "+str_Networks_Tab);
		util.fileWriting("Clicked on TAB : "+str_Networks_Tab);
		Thread.sleep(5000);

		util.takeScreenShot(driver,"Test_Networks","Networks_MainScreen");
		
		JSONObject jsnObj_NetworkMainURL = mJsonParser.callHttpRequest(Constants.Network_MainURL,"GET", null);
		Thread.sleep(Constants.ThreadSleep);

		int str_NetworkTotal = jsnObj_NetworkMainURL.getInt("total");
		System.out.println("str_NetworkTotal : "+str_NetworkTotal);
		util.fileWriting("str_NetworkTotal : "+str_NetworkTotal);
		JSONArray jsnArray_Navigator = jsnObj_NetworkMainURL.getJSONArray("navigators");
		JSONArray jsnArray_Entries = jsnArray_Navigator.getJSONObject(0).getJSONArray("entries");
		List<WebElement> list_wbNetworkName = driver.findElements(By.id(Constants.app_Package+":id/networks_cell_txt_title"));
		System.out.println(list_wbNetworkName.size());

		for(int i=0; i<list_wbNetworkName.size(); i++){
			System.out.println("**********"+i+"**********");

			JSONObject json_NetworksInfo = jsnArray_Entries.getJSONObject(i);
			String str_NetworkName = json_NetworksInfo.getString("name");
			WordUtils.capitalize(str_NetworkName);
			System.out.println("str_NetworkName : "+str_NetworkName);

			WebElement wb_NetworkName = list_wbNetworkName.get(i);
			String str_wbNetworkName = wb_NetworkName.getText();
			System.out.println(str_wbNetworkName);
			assertTrue(str_wbNetworkName.toUpperCase().equals(str_NetworkName.toUpperCase()));
			Assert.assertEquals(str_wbNetworkName.toUpperCase(), str_NetworkName.toUpperCase());

			wb_NetworkName.click();
			Thread.sleep(Constants.ThreadSleep);

			driver.navigate().back();

		}
		System.out.println("==========================================================================");
		System.out.println("End TestCase  ---> Test_NMA_8995_9069()");
		System.out.println("==========================================================================");
	}
}
