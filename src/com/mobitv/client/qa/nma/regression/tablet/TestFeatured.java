package com.mobitv.client.qa.nma.regression.tablet;

import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.mobitv.client.qa.nma.common.Constants;
import com.mobitv.client.qa.nma.common.DataParser;
import com.mobitv.client.qa.nma.common.ExecuteShellComand;
import com.mobitv.client.qa.nma.common.SwipeAndScroll;
import com.mobitv.client.qa.nma.common.Util;

public class TestFeatured {

	private static Util util = new Util();
	private static WebDriver driver = null;
	private ExecuteShellComand command = new ExecuteShellComand();
	private DataParser mJsonParser = new DataParser();

	@BeforeTest
	public void setUp() throws Exception {		
		driver = util.getAndroidDriver(driver);
		command.executeCommand("Tablet_Featured");
	}

	@AfterTest
	public void tearDown() throws Exception{
		driver.quit();
		command.kill();
		Thread.sleep(Constants.ThreadSleep);
	}
	/**
	 * TEST: 
	 * 		Template 1 - Marketing tile space within Featured
	 * ACCEPTANCE CRITERIA
	 *		Build is loaded on the tablet and marketing tile area UI is implemented
	 *		Images up to 13 can be supported
	 *		No client enforcement code is required
	 *		This 13 number is for QA to confirm and certify.
	 *		Data can be plugged in
	 *		Can swipe back and forth (not circular)
	 *		Tablet size (7", 10.5" - reference) - Resolution coverage - (Nexus 7, Galaxy note tab 2, 3)
	 */

	@Test
	public void Test_NMA_8613()throws Exception{
		System.out.println("Start TestCase  ---> Test_NMA_8613()");
		System.out.println("==========================================================================");

		WebElement Featured_Tab = driver.findElement(By.id(Constants.app_Package+":id/home_tab"));
		String str_FeaturedTab = Featured_Tab.getText();
		Featured_Tab.click();		
		System.out.println("Clicked on TAB : "+str_FeaturedTab);
		util.fileWriting("Clicked on TAB : "+str_FeaturedTab);
		Thread.sleep(5000);
		util.takeScreenShot(driver,"Test_Featured","Featured_MainScreen");

		WebElement featured_MktCellThumb = driver.findElement(By.id(Constants.app_Package+":id/mkt_cell_thumb"));
		Assert.assertTrue(featured_MktCellThumb.isEnabled());
		System.out.println("Marketing Tile Image exists on the UI of Featured MainScreen");
		util.fileWriting("Marketing Tile Image exists on the UI of Featured MainScreen");

		WebElement featured_Indicator = driver.findElement(By.id(Constants.app_Package+":id/indicator"));
		Assert.assertTrue(featured_Indicator.isEnabled());
		System.out.println("Marketing Tile Indicator exists on the UI of Featured MainScreen");
		util.fileWriting("Marketing Tile Indicator exists on the UI of Featured MainScreen");

		System.out.println("==========================================================================");
		System.out.println("End TestCase  ---> Test_NMA_8613()");
		System.out.println("==========================================================================");
	}

	@SuppressWarnings("static-access")
	@Test
	public void Test_NMA_9053()throws Exception{
		System.out.println("Start TestCase  ---> Test_NMA_9053()");
		System.out.println("==========================================================================");

		WebElement Featured_Tab = driver.findElement(By.id(Constants.app_Package+":id/home_tab"));
		String str_FeaturedTab = Featured_Tab.getText();
		Featured_Tab.click();		
		System.out.println("Clicked on TAB : "+str_FeaturedTab);
		util.fileWriting("Clicked on TAB : "+str_FeaturedTab);
		Thread.sleep(5000);
		util.takeScreenShot(driver,"Test_Featured","Featured_MainScreen");
	
		WebElement wb_RecyclerView = driver.findElement(By.id(Constants.app_Package+":id/landing_pager").id(Constants.app_Package+":id/recycler_view"));
		try{
		SwipeAndScroll.scrollOnTab(driver, wb_RecyclerView, "- RESUME WATCHING -");
		}catch(Exception e){
			Assert.fail("RESUME WATCHING template doesn't exists on the UI of Featured MainScreen");
		}
		System.out.println("RESUME WATCHING template exists on the UI of Featured MainScreen");
		Thread.sleep(5000);

		JSONObject jsnObj_TilesList = mJsonParser.callHttpRequest(Constants.widget_TileListHomeScreen, "GET", null);
		Thread.sleep(Constants.ThreadSleep);

		JSONArray jsnArray_Tiles = jsnObj_TilesList.getJSONArray("tiles");
		for (int i = 0; i < jsnArray_Tiles.length(); i++) {
			System.out.println("*****************************");
			JSONObject jsnObj_Tile = jsnArray_Tiles.getJSONObject(i);
			String str_TileName = jsnObj_Tile.getString("tile_name");
			if (!str_TileName.equalsIgnoreCase("Marketing Tiles")) {
				//WebElement wb_RecyclerView = driver.findElement(By.id(Constants.app_Package+":id/landing_pager").id(Constants.app_Package+":id/recycler_view"));
				SwipeAndScroll.scrollOnTab(driver, wb_RecyclerView, str_TileName);
				System.out.println(str_TileName.toUpperCase()+" template exists on the UI of Featured MainScreen");
				Thread.sleep(5000);
				
			}
		}

		try{
			SwipeAndScroll.scrollOnTab(driver, wb_RecyclerView, "NETWORK");
		}catch(Exception e){
			Assert.fail("NETWORK template doesn't exists on the UI of Featured MainScreen");
		}
		System.out.println("NETWORK template exists on the UI of Featured MainScreen");
		Thread.sleep(5000);

		System.out.println("==========================================================================");
		System.out.println("End TestCase  ---> Test_NMA_9053()");
		System.out.println("==========================================================================");
	}

	/**
	 * TEST: 
	 * 		Template 2 - Resume Watching within Featured Page
	 * ACCEPTANCE CRITERIA
	 *		Scrollable - Support up to 20 items at least
	 *		SEE ALL not visible
	 */
	@SuppressWarnings("static-access")
	@Test
	public void Test_NMA_8614()throws Exception{
		System.out.println("Start TestCase  ---> Test_NMA_8614()");
		System.out.println("==========================================================================");

		WebElement Featured_Tab = driver.findElement(By.id(Constants.app_Package+":id/home_tab"));
		String str_FeaturedTab = Featured_Tab.getText();
		Featured_Tab.click();		
		System.out.println("Clicked on TAB : "+str_FeaturedTab);
		util.fileWriting("Clicked on TAB : "+str_FeaturedTab);
		Thread.sleep(8000);
		util.takeScreenShot(driver,"Test_Featured","Featured_MainScreen");		

		WebElement wb_RecyclerView = driver.findElement(By.id(Constants.app_Package+":id/landing_pager").id(Constants.app_Package+":id/recycler_view"));
		try{
			SwipeAndScroll.scrollOnTab(driver, wb_RecyclerView, "- RESUME WATCHING -");
		}catch(Exception e){
			Assert.fail("RESUME WATCHING template doesn't exists on the UI of Featured MainScreen");
		}
		System.out.println("RESUME WATCHING template exists on the UI of Featured MainScreen");
		Thread.sleep(5000);

		//Verify Horizontal scroll		
		List<WebElement> wb_RecyclerView_list = wb_RecyclerView.findElements((By.id(Constants.app_Package+":id/recycler_view")));
		for(int i=0;i<wb_RecyclerView_list.size();i++)
			Assert.assertEquals("true", wb_RecyclerView_list.get(i).getAttribute("scrollable"));	
		Thread.sleep(3000);

		System.out.println("==========================================================================");
		System.out.println("End TestCase  ---> Test_NMA_8614()");
		System.out.println("==========================================================================");

	}
	/**
	 * TEST: 
	 * 		Template 3 - Recommendation within Featured
	 * ACCEPTANCE CRITERIA
	 *		Scrollable - Support up to 20 items at least
	 *		SEE ALL not visible
	 */
	@SuppressWarnings("static-access")
	@Test
	public void Test_NMA_8615()throws Exception{
		System.out.println("Start TestCase  ---> Test_NMA_8615()");
		System.out.println("==========================================================================");

		WebElement Featured_Tab = driver.findElement(By.id(Constants.app_Package+":id/home_tab"));
		String str_FeaturedTab = Featured_Tab.getText();
		Featured_Tab.click();		
		System.out.println("Clicked on TAB : "+str_FeaturedTab);
		util.fileWriting("Clicked on TAB : "+str_FeaturedTab);
		Thread.sleep(5000);
		util.takeScreenShot(driver,"Test_Featured","Featured_MainScreen");

		WebElement wb_RecyclerView = driver.findElement(By.id(Constants.app_Package+":id/landing_pager").id(Constants.app_Package+":id/recycler_view"));
		try{
			SwipeAndScroll.scrollOnTab(driver, wb_RecyclerView, "- RECOMMENDED FOR YOU -");
		}catch(Exception e){
			Assert.fail("RECOMMENDED template doesn't exists on the UI of Featured MainScreen");
		}
		System.out.println("RECOMMENDED template exists on the UI of Featured MainScreen");
		Thread.sleep(3000);

		//Verify Horizontal scroll		
		List<WebElement> wb_RecyclerView_list = wb_RecyclerView.findElements((By.id(Constants.app_Package+":id/recycler_view")));
		for(int i=0;i<wb_RecyclerView_list.size();i++)
			Assert.assertEquals("true", wb_RecyclerView_list.get(i).getAttribute("scrollable"));
		Thread.sleep(3000);	

		System.out.println("==========================================================================");
		System.out.println("End TestCase  ---> Test_NMA_8615()");
		System.out.println("==========================================================================");


	}
	/**
	 * TEST: 
	 * 		Template 4 - Free Videos within Featured
	 * ACCEPTANCE CRITERIA
	 *		Scrollable - Support up to 20 items at least
	 *		SEE ALL not visible
	 */
	@SuppressWarnings("static-access")
	@Test
	public void Test_NMA_8616()throws Exception{
		System.out.println("Start TestCase  ---> Test_NMA_8616()");
		System.out.println("==========================================================================");

		WebElement Featured_Tab = driver.findElement(By.id(Constants.app_Package+":id/home_tab"));
		String str_FeaturedTab = Featured_Tab.getText();
		Featured_Tab.click();		
		System.out.println("Clicked on TAB : "+str_FeaturedTab);
		util.fileWriting("Clicked on TAB : "+str_FeaturedTab);
		Thread.sleep(5000);
		util.takeScreenShot(driver,"Test_Featured","Featured_MainScreen");

		WebElement wb_RecyclerView = driver.findElement(By.id(Constants.app_Package+":id/landing_pager").id(Constants.app_Package+":id/recycler_view"));
		try{
			SwipeAndScroll.scrollOnTab(driver, wb_RecyclerView, "Free Videos");
		}catch(Exception e){
			Assert.fail("Free Videos template doesn't exists on the UI of Featured MainScreen");
		}
		System.out.println("Free Videos template exists on the UI of Featured MainScreen");
		Thread.sleep(3000);

		//Verify Horizontal scroll		
		List<WebElement> wb_RecyclerView_list = wb_RecyclerView.findElements((By.id(Constants.app_Package+":id/recycler_view")));
		for(int i=0;i<wb_RecyclerView_list.size();i++)
			Assert.assertEquals("true", wb_RecyclerView_list.get(i).getAttribute("scrollable"));

		//Verify play button on Free video
		util.takeScreenShot(driver,"Test_Featured","Freevideos_playbutton");
		System.out.println("screenshot availble to verify playbutton on free videos");
		Thread.sleep(3000);	

		System.out.println("==========================================================================");
		System.out.println("End TestCase  ---> Test_NMA_8616()");
		System.out.println("==========================================================================");
	}
	/**
	 * TEST: 
	 * 		Template 5 - Network list within Featured
	 * ACCEPTANCE CRITERIA
	 *		Horizontally scrollable - Support up to 15 items at least
	 *		SEE ALL will be visible
	 *		Depending on the size of the tablet, we may need to adjust a list of items showing up OR scale
	 */
	@SuppressWarnings("static-access")
	@Test
	public void Test_NMA_8617()throws Exception{
		System.out.println("Start TestCase  ---> Test_NMA_8617()");
		System.out.println("==========================================================================");

		WebElement Featured_Tab = driver.findElement(By.id(Constants.app_Package+":id/home_tab"));
		String str_FeaturedTab = Featured_Tab.getText();
		Featured_Tab.click();		
		System.out.println("Clicked on TAB : "+str_FeaturedTab);
		util.fileWriting("Clicked on TAB : "+str_FeaturedTab);
		Thread.sleep(5000);
		util.takeScreenShot(driver,"Test_Featured","Featured_MainScreen");

		WebElement wb_RecyclerView = driver.findElement(By.id(Constants.app_Package+":id/landing_pager").id(Constants.app_Package+":id/recycler_view"));
		try{
			SwipeAndScroll.scrollOnTab(driver, wb_RecyclerView, "NETWORK");
		}catch(Exception e){
			Assert.fail("NETWORK template doesn't exists on the UI of Featured MainScreen");
		}
		System.out.println("NETWORK template exists on the UI of Featured MainScreen");
		Thread.sleep(3000);



		//Verify Horizontal scroll		
		List<WebElement> wb_RecyclerView_list = wb_RecyclerView.findElements((By.id(Constants.app_Package+":id/recycler_view")));
		Assert.assertEquals("true", wb_RecyclerView_list.get(0).getAttribute("scrollable"));
		Thread.sleep(3000);

		//Verify SEE ALL will be visible	
		SwipeAndScroll.swipeGrid_One(driver);
		WebElement see_all = driver.findElement(By.id(Constants.app_Package+":id/section_see_all"));
		Assert.assertTrue(see_all.isDisplayed());		
		System.out.println("SEE ALL exists on the UI of Featured MainScreen");
		Thread.sleep(3000);

		System.out.println("==========================================================================");
		System.out.println("End TestCase  ---> Test_NMA_8617()");
		System.out.println("==========================================================================");
	}

}
