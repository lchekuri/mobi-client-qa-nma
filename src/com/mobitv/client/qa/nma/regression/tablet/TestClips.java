package com.mobitv.client.qa.nma.regression.tablet;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.mobitv.client.qa.nma.common.Constants;
import com.mobitv.client.qa.nma.common.DataParser;
import com.mobitv.client.qa.nma.common.ExecuteShellComand;
import com.mobitv.client.qa.nma.common.SwipeAndScroll;
import com.mobitv.client.qa.nma.common.Util;

public class TestClips {
	private static Util util = new Util();
	private static WebDriver driver = null;
	private ExecuteShellComand command = new ExecuteShellComand();
	private DataParser mJsonParser = new DataParser();

	@BeforeTest
	public void setUp() throws Exception {
		driver = util.getAndroidDriver(driver);
		command.executeCommand("Tablet_Clips");	
		
	}

	@AfterTest
	public void tearDown() throws Exception {
		driver.quit();
		command.kill();
		Thread.sleep(Constants.ThreadSleep);
	}



	/**
	 * TEST : 
	 * 		Grid Template 3 - Clips page 
	 * ACCEPTANCE CRITERIA
	 * 		Only scroll up and down - Vertical scrolling
	 * 		Pagination to support large numbers like hundreds  (pagination after 5+rows for best optimization)
	 * 		Each tile has a play button
	 * 		Tiles are in landscape orientation
	 * 		7" = 3, 10" = 4 columns
	 */

	@Test
	public void Test_NMA_8994() throws Exception {
		System.out.println("Start TestCase  ---> Test_NMA_8994()");
		System.out.println("==========================================================================");

		WebElement Clip_Tab = driver.findElement(By.id(Constants.app_Package + ":id/clips_tab"));
		String str_ClipTab = Clip_Tab.getText();
		Clip_Tab.click();
		System.out.println("Clicked on TAB : " + str_ClipTab);
		util.fileWriting("Clicked on TAB : " + str_ClipTab);
		Thread.sleep(5000);

		util.takeScreenShot(driver, "Test_NMA_8994", "Screen-1");
		
		System.out.println("RESET filter");
		WebElement clip_FilterBar = driver.findElement(By.id(Constants.app_Package+":id/filter_bar"));
		clip_FilterBar.click();
		WebElement clip_FilterReset = driver.findElement(By.id(Constants.app_Package+":id/dialog_filters_reset"));;
		clip_FilterReset.click();
		WebElement clip_FilterApplyBtn = driver.findElement(By.id(Constants.app_Package+":id/dialog_filters_apply_btn"));
		clip_FilterApplyBtn.click();
		

		JSONObject jsnObj_ClipMainURL = mJsonParser.callHttpRequest(Constants.Clips_MainURL, "GET", null);
		Thread.sleep(Constants.ThreadSleep);

		int str_ClipTotal = jsnObj_ClipMainURL.getInt("total");
		System.out.println("str_ClipTotal : " + str_ClipTotal);
		util.fileWriting("str_ClipTotal : " + str_ClipTotal);
		JSONArray jsnArray_Hits = jsnObj_ClipMainURL.getJSONArray("hits");

		List<WebElement> list_ClipsCellThumb = driver.findElements(By.id(Constants.app_Package + ":id/cell_thumb"));
		List<WebElement> list_CellIconPlay = driver.findElements(By.id(Constants.app_Package + ":id/cell_icon_play"));
		List<WebElement> list_ClipsCellTitle = driver.findElements(By.id(Constants.app_Package + ":id/cell_txt_title"));
		List<WebElement> list_ClipsCellDesc = driver.findElements(By.id(Constants.app_Package + ":id/cell_txt_desc"));

		for (int i = 0; i < list_ClipsCellDesc.size(); i++) {
			System.out.println("*****************************");

			JSONObject json_ClipsInfo = jsnArray_Hits.getJSONObject(i);
			JSONObject json_ClipResult = json_ClipsInfo.getJSONObject("result");
			JSONObject json_ClipSeries = json_ClipResult.getJSONObject("vod");

			String str_ClipID = json_ClipSeries.getString("id");
			String str_ClipName = json_ClipSeries.getString("name");
			String str_ClipNetwork = json_ClipSeries.getString("network");

			String str_wbClipsCellDesc = list_ClipsCellTitle.get(i).getText();
			Assert.assertEquals(str_ClipName, str_wbClipsCellDesc);
			System.out.println(str_wbClipsCellDesc + " :Clip-" + (i + 1) + " name is available in Clips Grid Template");

			String str_wbClipsNetwork = list_ClipsCellDesc.get(i).getText();
			Assert.assertEquals(str_ClipNetwork, str_wbClipsNetwork);
			System.out.println(str_wbClipsNetwork + " :Clip-" + (i + 1) + " network is available in Clips Grid Template");

			WebElement wbClipsCellThumb = list_ClipsCellThumb.get(i);
			wbClipsCellThumb.isEnabled();
			System.out.println(str_wbClipsCellDesc + " :Clips-" + (i + 1) + " image thumb is in landscape orientation on Clips Grid Template");

			WebElement wbClipsIconPlay = list_CellIconPlay.get(i);
			wbClipsIconPlay.isEnabled();
			System.out.println(str_wbClipsCellDesc + " :Clips-" + (i + 1) + " play icon is available in Clips Grid Template");

		}
		SwipeAndScroll.swipeGrid_One(driver);
		System.out.println("Vertical Scrolling up and down available on Clips");

		// Verify tile orientation
		util.takeScreenShot(driver, "Test_NMA_8994", "Screen-2");
		System.out.println("Screenshot availble to verify Clips tile orientation");


		System.out.println("==========================================================================");
		System.out.println("End TestCase  ---> Test_NMA_8994()");
		System.out.println("==========================================================================");

	}
	/**
	 * TEST:
	 * 		Overlay template 1 - Clips detail 
	 * ACCEPTANCE CRITERIA
	 * 		Overlay popup when user clicks on the clip meta data area with transparent background
	 * 		Overlay is dismissed when clicking outside
	 * 		Thumbnail with play button is included
	 * 		Ignore "+Playlist" link
	 * 		Ignore "-> FOX News" link for now (will handle it as a separate story)
	 * 		Page includes:  Network, Air date, Duration, Title, Detail
	 * 		Don't show if the meta data value doesn't exist
	 */

	@Test
	public void Test_NMA_8996_9071() throws Exception {
		System.out.println("Start TestCase  ---> Test_NMA_8996_9071()");
		System.out.println("==========================================================================");

		WebElement Clip_Tab = driver.findElement(By.id(Constants.app_Package + ":id/clips_tab"));
		String str_ClipTab = Clip_Tab.getText();
		Clip_Tab.click();
		System.out.println("Clicked on TAB : " + str_ClipTab);
		util.fileWriting("Clicked on TAB : " + str_ClipTab);
		Thread.sleep(5000);
		

		System.out.println("RESET filter");
		WebElement clip_FilterBar = driver.findElement(By.id(Constants.app_Package+":id/filter_bar"));
		clip_FilterBar.click();
		WebElement clip_FilterReset = driver.findElement(By.id(Constants.app_Package+":id/dialog_filters_reset"));;
		clip_FilterReset.click();
		WebElement clip_FilterApplyBtn = driver.findElement(By.id(Constants.app_Package+":id/dialog_filters_apply_btn"));
		clip_FilterApplyBtn.click();

		JSONObject jsnObj_ClipMainURL = mJsonParser.callHttpRequest(Constants.Clips_MainURL, "GET", null);
		Thread.sleep(Constants.ThreadSleep);

		int str_ClipTotal = jsnObj_ClipMainURL.getInt("total");
		System.out.println("str_ClipTotal : " + str_ClipTotal);
		util.fileWriting("str_ClipTotal : " + str_ClipTotal);
		JSONArray jsnArray_Hits = jsnObj_ClipMainURL.getJSONArray("hits");

		util.takeScreenShot(driver, "Test_NMA_8996_9071", "Screen-1");

		List<WebElement> list_ClipsCellThumb = driver.findElements(By.id(Constants.app_Package + ":id/cell_thumb"));
		List<WebElement> list_CellIconPlay = driver.findElements(By.id(Constants.app_Package + ":id/cell_icon_play"));
		List<WebElement> list_ClipsCellTitle = driver.findElements(By.id(Constants.app_Package + ":id/cell_txt_title"));
		List<WebElement> list_ClipsCellDesc = driver.findElements(By.id(Constants.app_Package + ":id/cell_txt_desc"));

		for (int i = 0; i < list_ClipsCellDesc.size(); i++) {
			System.out.println("*****************************");

			JSONObject json_ClipsInfo = jsnArray_Hits.getJSONObject(i);
			JSONObject json_ClipResult = json_ClipsInfo.getJSONObject("result");
			JSONObject json_ClipSeries = json_ClipResult.getJSONObject("vod");

			String str_ClipID = json_ClipSeries.getString("id");
			String str_ClipName = json_ClipSeries.getString("name");
			String str_ClipNetwork = json_ClipSeries.getString("network");
			String str_clipDesc = json_ClipSeries.getString("description");
			int str_clipDuration = json_ClipSeries.getInt("duration");

			WebElement wb_ClipsCellName = list_ClipsCellTitle.get(i);
			String str_wbClipsCellTitle = wb_ClipsCellName.getText();
			Assert.assertEquals(str_ClipName, str_wbClipsCellTitle);
			System.out.println(str_wbClipsCellTitle + " :Clip-" + (i + 1) + " name is available in Clips Grid Template");

			String str_wbClipsNetwork = list_ClipsCellDesc.get(i).getText();
			Assert.assertEquals(str_ClipNetwork, str_wbClipsNetwork);
			System.out.println(str_wbClipsNetwork + " :Clip-" + (i + 1) + " network is available in Clips Grid Template");

			WebElement wbClipsCellThumb = list_ClipsCellThumb.get(i);
			wbClipsCellThumb.isEnabled();
			System.out.println(str_wbClipsCellTitle + " :Clips-" + (i + 1) + " image thumb is in landscape orientation on Clips Grid Template");

			WebElement wbClipsIconPlay = list_CellIconPlay.get(i);
			wbClipsIconPlay.isEnabled();
			System.out.println(str_wbClipsCellTitle + " :Clips-" + (i + 1) + " play icon is available in Clips Grid Template");

			wb_ClipsCellName.click();
			System.out.println("Clicked on : " + str_wbClipsCellTitle);
			Thread.sleep(Constants.ThreadSleep);

			// Verify Pop up display based on meta data availability
			if (!str_ClipNetwork.isEmpty() && !str_clipDesc.isEmpty() && str_clipDuration!=0 && !str_ClipNetwork.isEmpty()) {

				try {

					WebElement wb_ThumbnailImage = driver.findElement(By.id(Constants.app_Package + ":id/details_thumb"));
					Assert.assertTrue(wb_ThumbnailImage.isEnabled());
					System.out.println("Thumbnail Image exists");

					WebElement wb_PlayButton = driver.findElement(By.id(Constants.app_Package + ":id/details_btn"));				
                   // Assert.assertTrue(wb_PlayButton.getText().contains("PLAY") || wb_PlayButton.getText().contains("RESUME"));			
					Assert.assertTrue(wb_PlayButton.isDisplayed());
					System.out.println("Play/Resume Button exists");

					wb_PlayButton.click();
					WebElement wb_playback_video = driver.findElement(By.id(Constants.app_Package + ":id/playback_video_view"));
					wb_playback_video.isEnabled();
					System.out.println("Clicked on playbutton lauches the player");
					driver.navigate().back();					
					Thread.sleep(3000);

					WebElement wb_DetailsTxtTitle = driver.findElement(By.id(Constants.app_Package + ":id/details_txt_title"));
					String str_wbDetailsTxtTitle = wb_DetailsTxtTitle.getText();
					Assert.assertEquals(str_wbClipsCellTitle, str_wbDetailsTxtTitle);
					System.out.println("Title of Clip exists");

					WebElement wb_DetailsTxtDuration = driver.findElement(By.id(Constants.app_Package + ":id/details_txt_duration"));
					String str_wbDetailsTxtDuration = wb_DetailsTxtDuration.getText();
					System.out.println(str_wbDetailsTxtDuration + " duration of Clip exists");
					Thread.sleep(3000);

					//WebElement wb_DetailsShowNetwork = driver.findElement(By.id(Constants.app_Package + ":id/details_show_netwrok_name"));
					//wb_DetailsShowNetwork.
					//String str_wbDetailsShowNetwork = wb_DetailsShowNetwork.getText();
					//System.out.println(str_wbDetailsShowNetwork + " network of Clip exists");

					WebElement wb_MainLayout = driver.findElement(By.id(Constants.app_Package + ":id/main_layout"));
					wb_MainLayout.click();

					System.out.println("Overlay is dismissed when Show/outside of the box is clicked");

				} catch (Exception e) {
					throw new Exception("Detail overlay popup for episodes is not displayed");
				}

				driver.navigate().back();
				Thread.sleep(Constants.ThreadSleep);

			} else {
				WebElement wb_ThumbnailImage = driver.findElement(By.id(Constants.app_Package + ":id/details_thumb_frame"));
				if (wb_ThumbnailImage.isEnabled())
					Assert.fail("There is no meta data still showing detail page");
			}
		}
		SwipeAndScroll.swipeGrid_One(driver);
		System.out.println("Vertical Scrolling up and down available on Clips");

		System.out.println("==========================================================================");
		System.out.println("End TestCase  ---> Test_NMA_8996_9071()");
		System.out.println("==========================================================================");

	}
}
