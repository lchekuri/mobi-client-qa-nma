package com.mobitv.client.qa.nma.regression.tablet;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.mobitv.client.qa.nma.common.Constants;
import com.mobitv.client.qa.nma.common.DataParser;
import com.mobitv.client.qa.nma.common.ExecuteShellComand;
import com.mobitv.client.qa.nma.common.Util;

public class Test_Default_Filter {

	private static Util util = new Util();
	private static WebDriver driver = null;
	private ExecuteShellComand command = new ExecuteShellComand();
	private DataParser mJsonParser = new DataParser();


	@BeforeTest
	public void setUp() throws Exception {		
		driver = util.getAndroidDriver(driver);
		command.executeCommand("Tablet_Networks");
	}

	@AfterTest
	public void tearDown() throws Exception{
		driver.quit();
		command.kill();
		Thread.sleep(Constants.ThreadSleep);
	}
	@Test
	public void Test_NMA_9527() throws Exception {
		System.out.println("Start TestCase  ---> Test_NMA_9527");
		System.out.println("==========================================================================");
		
		WebElement Live_Tab = driver.findElement(By.id(Constants.app_Package+":id/live_guide_tab"));
		String str_Live_Tab = Live_Tab.getText();
		Live_Tab.click();		
		System.out.println("Clicked on TAB : "+str_Live_Tab);
		Thread.sleep(5000);
		WebElement filter_bar = driver.findElement(By.id(Constants.app_Package+":id/filter_bar"));
		Assert.assertEquals("FILTER - Tablet",filter_bar.getText());
		System.out.println("Default filetr is Tablet on Live tab");
		
		WebElement Shows_Tab = driver.findElement(By.id(Constants.app_Package+":id/shows_tab"));
		String str_Shows_Tab = Shows_Tab.getText();
		Shows_Tab.click();		
		System.out.println("Clicked on TAB : "+str_Shows_Tab);
		Thread.sleep(5000);		
		Assert.assertEquals("FILTER - Tablet",filter_bar.getText());
		System.out.println("Default filetr is Tablet on Shows tab");
		
		WebElement movies_Tab = driver.findElement(By.id(Constants.app_Package+":id/movies_tab"));
		String str_movies_Tab = movies_Tab.getText();
		movies_Tab.click();		
		System.out.println("Clicked on TAB : "+str_movies_Tab);
		Thread.sleep(5000);		
		Assert.assertEquals("FILTER - Tablet",filter_bar.getText());
		System.out.println("Default filetr is Tablet on Movies tab");
		
		WebElement Clips_Tab = driver.findElement(By.id(Constants.app_Package+":id/clips_tab"));
		String str_Clips_Tab = Clips_Tab.getText();
		Clips_Tab.click();		
		System.out.println("Clicked on TAB : "+str_Clips_Tab);
		Thread.sleep(5000);		
		Assert.assertEquals("FILTER - Tablet",filter_bar.getText());
		System.out.println("Default filetr is Tablet on Clips tab");
		
		WebElement Music_Tab = driver.findElement(By.id(Constants.app_Package+":id/music_tab"));
		String str_Music_Tab = Music_Tab.getText();
		Music_Tab.click();		
		System.out.println("Clicked on TAB : "+str_Shows_Tab);
		Thread.sleep(5000);		
		Assert.assertEquals("FILTER - Tablet",filter_bar.getText());
		System.out.println("Default filetr is Tablet on Music tab");
		
		
	}
}
