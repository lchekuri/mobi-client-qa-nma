package com.mobitv.client.qa.nma.regression.tablet1;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.mobitv.client.qa.nma.common.BaseClass;
import com.mobitv.client.qa.nma.common.Constants;
import com.mobitv.client.qa.nma.common.SwipeAndScroll;

public class TestMovies extends BaseClass{

	WebElement wb_MovieName;

	@Test
	public void Test_Movies() throws Exception{

		System.out.println("Start Test_Movies");
		util.fileWriting("Start Test_Movies");
		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");

		if(Constants.app_Package.equals("com.mobitv.client.connect.mobile")){
			WebElement wb_Tabs = driver.findElement(By.id(Constants.app_Package+":id/tabs"));
			SwipeAndScroll.scrollOnTab(driver, wb_Tabs, "Movies");
			Thread.sleep(5000);

			WebElement Movies_Tab = driver.findElement(By.id(Constants.app_Package+":id/movies_tab"));
			String str_MoviesTab = Movies_Tab.getText();
			Movies_Tab.click();		
			System.out.println("Clicked on TAB : "+str_MoviesTab);
			util.fileWriting("Clicked on TAB : "+str_MoviesTab);
			Thread.sleep(5000);

			util.takeScreenShot(driver,"Test_Movies","Movies_MainScreen");

			JSONObject jsnObj_MovieMainURL = mJsonParser.callHttpRequest(Constants.Movies_MainURL,"GET", null);
			Thread.sleep(Constants.ThreadSleep);

			int str_MovieTotal = jsnObj_MovieMainURL.getInt("total");
			System.out.println("str_MovieTotal : "+str_MovieTotal);
			util.fileWriting("str_MovieTotal : "+str_MovieTotal);
			JSONArray jsnArray_Hits = jsnObj_MovieMainURL.getJSONArray("hits");

			int l=3;
			for(int i=2; i<jsnArray_Hits.length(); i++){
				System.out.println("**********"+i+"**********");
				util.fileWriting("**********"+i+"**********");
				JSONObject json_MoviesInfo = jsnArray_Hits.getJSONObject(i);
				JSONObject json_MovieResult = json_MoviesInfo.getJSONObject("result");
				JSONObject json_MovieSeries = json_MovieResult.getJSONObject("vod");
				String str_MovieID = json_MovieSeries.getString("id");
				System.out.println("str_MovieID : "+str_MovieID);
				util.fileWriting("str_MovieID : "+str_MovieID);
				String str_MovieName = json_MovieSeries.getString("name");
				System.out.println("str_MovieName : "+str_MovieName);
				util.fileWriting("str_MovieName : "+str_MovieName);
				try{
					String str_MovieDescription = json_MovieSeries.getString("description");
					System.out.println("str_MovieDescription : "+str_MovieDescription);
				}catch(JSONException e){
					System.out.println("The Discription Data Not Found");
				}
				String str_MovieNetwork = json_MovieSeries.getString("network");
				//System.out.println("str_MovieNetwork : "+str_MovieNetwork);
				try{
					if(l==1){
						wb_MovieName = driver.findElement(By.id(Constants.app_Package+":id/base_grid").xpath("//android.widget.LinearLayout["+l+"]").id(Constants.app_Package+":id/cell_area_txt").id(Constants.app_Package+":id/cell_txt_title"));
					}else{
						wb_MovieName = driver.findElement(By.id(Constants.app_Package+":id/base_grid").xpath("//android.widget.LinearLayout["+l+"]/android.widget.LinearLayout[1]/android.widget.TextView[1]"));
					}
				}catch(Exception e){
					System.out.println("X-Path of the element not found");
				}
				String str_wbMovieName = wb_MovieName.getText();


				if(str_MovieName!=null){
					if(str_MovieName.equalsIgnoreCase(str_wbMovieName)){
						util.takeScreenShot(driver,"Test_Movies","Movie_"+i);
						wb_MovieName.click();
						System.out.println("Clicked on MOVIE : "+str_wbMovieName);
						util.fileWriting("Clicked on MOVIE : "+str_wbMovieName);
						Thread.sleep(Constants.ThreadSleep);

						WebElement wb_MovieDetailTitle = driver.findElement(By.id(Constants.app_Package+":id/details_txt_title"));
						String str_wbMovieDetailTitle = wb_MovieDetailTitle.getText();
						Assert.assertEquals(str_MovieName, str_wbMovieDetailTitle);
						System.out.println("str_MovieNetwork : "+str_MovieName);
						util.fileWriting("str_MovieNetwork : "+str_MovieName);

						WebElement wb_MovieDesc = driver.findElement(By.id(Constants.app_Package+":id/details_txt_desc"));
						String str_wbMovieDesc = wb_MovieDesc.getText();
						//Assert.assertEquals(str_MovieDescription, str_wbMovieDesc);
						System.out.println("str_wbMovieDesc : "+str_wbMovieDesc);
						//util.fileWriting("str_MovieDescription : "+str_MovieDescription);
						util.takeScreenShot(driver,"Test_Movies","Movie_"+i+"-Deatils");
						driver.navigate().back();
						Thread.sleep(Constants.ThreadSleep);
					}
				}else{
					System.out.println("Movie Data is not available");
				}

				if (l == 3)
					l = 4;
				else{
					l = 3;
					if(i<4){
						try{
							//driver.swipe(100, 790, 100, 35, 2500);
							if(Constants.DeviceType.equalsIgnoreCase("Large")){
								SwipeAndScroll.swipeWithConstantValues(driver, 100.0, 800.0, 100.0, 30.0, 3.0);
							}else if(Constants.DeviceType.equalsIgnoreCase("Small")){
								SwipeAndScroll.swipeWithConstantValues(driver, 100.0, 420.0, 100.0, 30.0, 3.0);
							}else{
								throw new AssertionError("Please Check DeviceType in Constants.java file");
							}
							Thread.sleep(Constants.ThreadSleep);
						}catch(Exception e){
							e.getMessage();
						}
					}
					else
					{
						//driver.swipe(100, 670, 100, 35,2800);
						if(Constants.DeviceType.equalsIgnoreCase("Large")){
							SwipeAndScroll.swipeWithConstantValues(driver, 100.0, 680.0, 100.0, 30.0, 3.0);
						}else if(Constants.DeviceType.equalsIgnoreCase("Small")){
							SwipeAndScroll.swipeWithConstantValues(driver, 100.0, 350.0, 100.0, 30.0, 3.0);
						}else{
							throw new AssertionError("Please Check DeviceType in Constants.java file");
						}
						Thread.sleep(Constants.ThreadSleep);
					}
				}

				int length=(jsnArray_Hits.length()/4);
				if(i==20)
					break;
			}
		}

		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");
		System.out.println("End Test_Movies");
		util.fileWriting("End Test_Movies");
		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");
	}

	@SuppressWarnings("unused")
	@Test
	public void Test_NMA_2287() throws Exception, InterruptedException {

		System.out.println("Start TestCases  ---> TestCaseIDs: NMA_2287 ---- JIRA ID: NMA-6385");
		System.out.println("==========================================================================");
		if(Constants.app_Package  == "com.mobitv.client.connect.mobile"){
			WebElement wb_Tabs = driver.findElement(By.id(Constants.app_Package+":id/tabs"));
			SwipeAndScroll.scrollOnTab(driver, wb_Tabs, "Movies");
			Thread.sleep(5000);

			WebElement Movies_Tab = driver.findElement(By.id(Constants.app_Package+":id/movies_tab"));
			String str_MoviesTab = Movies_Tab.getText();
			Movies_Tab.click();		
			System.out.println(str_MoviesTab+": Clicked on Tab");
			util.fileWriting(str_MoviesTab+": Clicked on Tab");
			Thread.sleep(5000);

			util.takeScreenShot(driver,"NMA_2287","Screen1");

			wb_MovieName = driver.findElement(By.id(Constants.app_Package+":id/base_grid").xpath("//android.widget.LinearLayout[1]").id(Constants.app_Package+":id/cell_area_txt").id(Constants.app_Package+":id/cell_txt_title"));
			String str_wbMovieName = wb_MovieName.getText();
			wb_MovieName.click();
			System.out.println("Clicked on Movie: "+str_wbMovieName);
			Thread.sleep(Constants.ThreadSleep);

			util.takeScreenShot(driver,"NMA_2287","Screen2");

			WebElement wb_MusicPlayBtn = driver.findElement(By.id(Constants.app_Package+":id/details_btn"));
			String str_wbMusicPlayBtn = wb_MusicPlayBtn.getText();
			str_wbMusicPlayBtn = str_wbMusicPlayBtn.toUpperCase();
			System.out.println("Button: "+str_wbMusicPlayBtn);
			if(str_wbMusicPlayBtn.equalsIgnoreCase("PLAY")){
				Assert.assertEquals("PLAY", str_wbMusicPlayBtn);
			}else if(str_wbMusicPlayBtn.equalsIgnoreCase("RESUME")){
				Assert.assertEquals("RESUME", str_wbMusicPlayBtn);
			}else if(str_wbMusicPlayBtn.equalsIgnoreCase("REPLAY")){
				Assert.assertEquals("REPLAY", str_wbMusicPlayBtn);
			}

			wb_MusicPlayBtn.click();
			Thread.sleep(Constants.ThreadSleep);

			util.takeScreenShot(driver,"NMA_2283","Screen1");

			driver.navigate().back();
		}

		System.out.println("==========================================================================");
		System.out.println("End TestCases  ---> TestCaseIDs: NMA_2287 ---- JIRA ID: NMA-6385");
		System.out.println("==========================================================================");
	}

	@SuppressWarnings("unused")
	@Test
	public void Test_NMA_2252() throws Exception, InterruptedException {
		System.out.println("Start TestCases  ---> TestCaseIDs: NMA_2252 ---- JIRA ID: NMA-6391");
		System.out.println("==========================================================================");
		if(Constants.app_Package == "com.mobitv.client.connect.mobile"){
			WebElement wb_Tabs = driver.findElement(By.id(Constants.app_Package+":id/tabs"));
			SwipeAndScroll.scrollOnTab(driver, wb_Tabs, "Movies");
			Thread.sleep(5000);

			WebElement Movies_Tab = driver.findElement(By.id(Constants.app_Package+":id/movies_tab"));
			String str_MoviesTab = Movies_Tab.getText();
			Movies_Tab.click();		
			System.out.println(str_MoviesTab+": Clicked on Tab");
			util.fileWriting(str_MoviesTab+": Clicked on Tab");
			Thread.sleep(5000);

			util.takeScreenShot(driver,"NMA_2252","Screen1");

			wb_MovieName = driver.findElement(By.id(Constants.app_Package+":id/base_grid").xpath("//android.widget.LinearLayout[1]").id(Constants.app_Package+":id/cell_area_txt").id(Constants.app_Package+":id/cell_txt_title"));
			String str_wbMovieName = wb_MovieName.getText();
			wb_MovieName.click();
			System.out.println("Clicked on Movie: "+str_wbMovieName);
			Thread.sleep(Constants.ThreadSleep);

			util.takeScreenShot(driver,"NMA_2252","Screen2");

			WebElement movies_DetailsThumb_CP = driver.findElement(By.id(Constants.app_Package+":id/details_thumb"));
			Assert.assertTrue(movies_DetailsThumb_CP.isEnabled());
		}
		System.out.println("==========================================================================");
		System.out.println("End TestCases  ---> TestCaseIDs: NMA_2252 ---- JIRA ID: NMA-6391");
		System.out.println("==========================================================================");
	}
}
