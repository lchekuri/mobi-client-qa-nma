package com.mobitv.client.qa.nma.regression.tablet1;

import java.util.List;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.mobitv.client.qa.nma.common.BaseClass;
import com.mobitv.client.qa.nma.common.Constants;
import com.mobitv.client.qa.nma.common.SwipeAndScroll;

public class TestClips extends BaseClass{
	WebElement wb_ClipName, wb_ClipNetwork;
	
	@Test
	public void Test_Clips() throws Exception{

		System.out.println("Start Test_Clips");
		util.fileWriting("Start Test_Clips");
		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");

		WebElement wb_Tabs = driver.findElement(By.id(Constants.app_Package+":id/tabs"));
		SwipeAndScroll.scrollOnTab(driver, wb_Tabs, "Clips");
		Thread.sleep(5000);

		WebElement Clip_Tab = driver.findElement(By.id(Constants.app_Package+":id/clips_tab"));
		String str_ClipTab = Clip_Tab.getText();
		Clip_Tab.click();		
		System.out.println("Clicked on TAB : "+str_ClipTab);
		util.fileWriting("Clicked on TAB : "+str_ClipTab);
		Thread.sleep(5000);

		util.takeScreenShot(driver,"Test_Clips","Clips_MainScreen");

		JSONObject jsnObj_ClipMainURL = mJsonParser.callHttpRequest(Constants.Clips_MainURL,"GET", null);
		Thread.sleep(Constants.ThreadSleep);

		int str_ClipTotal = jsnObj_ClipMainURL.getInt("total");
		System.out.println("str_ClipTotal : "+str_ClipTotal);
		JSONArray jsnArray_Hits = jsnObj_ClipMainURL.getJSONArray("hits");

		int l=3;
		for(int i=2; i<jsnArray_Hits.length(); i++){
			System.out.println("**********"+i+"**********");
			util.fileWriting("**********"+i+"**********");
			JSONObject json_ClipsInfo = jsnArray_Hits.getJSONObject(i);
			JSONObject json_ClipResult = json_ClipsInfo.getJSONObject("result");
			JSONObject json_ClipSeries = json_ClipResult.getJSONObject("vod");

			String str_ClipID = json_ClipSeries.getString("id");
			System.out.println("str_ClipID : "+str_ClipID);
			util.fileWriting("str_ClipID : "+str_ClipID);
			String str_ClipName = json_ClipSeries.getString("name");
			System.out.println("str_ClipName : "+str_ClipName);
			util.fileWriting("str_ClipName : "+str_ClipName);
			try{
				String str_ClipDescription = json_ClipSeries.getString("description");
				System.out.println("str_ClipDescription : "+str_ClipDescription);
			}catch(JSONException e){
				System.out.println("The Discription Data Not Found");
			}

			String str_ClipNetwork = json_ClipSeries.getString("network");
			System.out.println("str_ClipNetwork : "+str_ClipNetwork);

			try{
				if(l==1){
					wb_ClipNetwork = driver.findElement(By.id(Constants.app_Package+":id/base_grid").xpath("//android.widget.LinearLayout["+l+"]").id(Constants.app_Package+":id/cell_area_txt").id(Constants.app_Package+":id/cell_txt_title"));
					wb_ClipName = driver.findElement(By.id(Constants.app_Package+":id/base_grid").xpath("//android.widget.LinearLayout["+l+"]").id(Constants.app_Package+":id/cell_area_txt").id(Constants.app_Package+":id/cell_txt_desc"));
				}else{
					wb_ClipNetwork = driver.findElement(By.id(Constants.app_Package+":id/base_grid").xpath("//android.widget.LinearLayout["+l+"]/android.widget.LinearLayout[1]/android.widget.TextView[1]"));
					wb_ClipName = driver.findElement(By.id(Constants.app_Package+":id/base_grid").xpath("//android.widget.LinearLayout["+l+"]/android.widget.LinearLayout[1]/android.widget.TextView[2]"));
				}
			}catch(Exception e){
				System.out.println("X-Path of element not found");
			}
			String str_wbClipName = wb_ClipName.getText();
			String str_wbClipNetwork = wb_ClipNetwork.getText();

			if(str_ClipName!=null && str_ClipNetwork!=null){
				if(str_ClipName.equalsIgnoreCase(str_wbClipName)|| str_ClipNetwork.equalsIgnoreCase(str_wbClipNetwork)){

					util.takeScreenShot(driver,"Test_Clips","Clip_"+i);

					wb_ClipName.click();

					System.out.println("Clicked on CLIP: "+str_wbClipName);
					util.fileWriting("Clicked on CLIP: "+str_wbClipName);
					Thread.sleep(Constants.ThreadSleep);

					WebElement wb_ClipDetailTitle = driver.findElement(By.id(Constants.app_Package+":id/details_txt_title"));
					String str_wbClipDetailTitle = wb_ClipDetailTitle.getText();
					Assert.assertEquals(str_wbClipName, str_wbClipDetailTitle);
					System.out.println("str_wbClipName : "+str_ClipNetwork);
					util.fileWriting("str_wbClipName : "+str_ClipNetwork);

					WebElement wb_ClipDesc = driver.findElement(By.id(Constants.app_Package+":id/details_txt_desc"));
					String str_wbClipDesc = wb_ClipDesc.getText();
					//	Assert.assertEquals(str_ClipDescription, str_wbClipDesc);
					//System.out.println("str_ClipDescription : "+str_ClipDescription);
					//util.fileWriting("str_ClipDescription : "+str_ClipDescription);
					util.takeScreenShot(driver,"Test_Clips","Clip_"+i+"-Deatils");
					driver.navigate().back();
					Thread.sleep(Constants.ThreadSleep);
				}
			}else{
				System.out.println("Clip Data is not available");
			}

			if (l == 3)
				l = 4;
			else{
				l = 3;
				if(i<4){
					try{
						//driver.swipe(100, 790, 100, 35, 2500);
						if(Constants.DeviceType.equalsIgnoreCase("Large")){
							SwipeAndScroll.swipeWithConstantValues(driver, 100.0, 800.0, 100.0, 30.0, 3.0);
						}else if(Constants.DeviceType.equalsIgnoreCase("Small")){
							SwipeAndScroll.swipeWithConstantValues(driver, 100.0, 420.0, 100.0, 30.0, 3.0);
						}else{
							throw new AssertionError("Please Check DeviceType in Constants.java file");
						}
						Thread.sleep(Constants.ThreadSleep);
					}catch(Exception e){
						e.getMessage();
					}
				}
				else
				{
					//driver.swipe(100, 670, 100, 35,2800);
					if(Constants.DeviceType.equalsIgnoreCase("Large")){
						SwipeAndScroll.swipeWithConstantValues(driver, 100.0, 680.0, 100.0, 30.0, 3.0);
					}else if(Constants.DeviceType.equalsIgnoreCase("Small")){
						SwipeAndScroll.swipeWithConstantValues(driver, 100.0, 350.0, 100.0, 30.0, 3.0);
					}else{
						throw new AssertionError("Please Check DeviceType in Constants.java file");
					}
					Thread.sleep(Constants.ThreadSleep);
				}
			}
			if(i==20)
				break;
		}

		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");
		System.out.println("End Test_Clips");
		util.fileWriting("End Test_Clips");
		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");
	}

	@Test
	public void Test_NMA_8611() throws Exception{
		System.out.println("Start TestCase  ---> Test_NMA_8611()");
		System.out.println("==========================================================================");

		WebElement wb_Tabs = driver.findElement(By.id(Constants.app_Package+":id/tabs"));
		SwipeAndScroll.scrollOnTab(driver, wb_Tabs, "Clips");
		Thread.sleep(5000);

		WebElement Clip_Tab = driver.findElement(By.id(Constants.app_Package+":id/clips_tab"));
		String str_ClipTab = Clip_Tab.getText();
		Clip_Tab.click();		
		System.out.println("Clicked on TAB : "+str_ClipTab);
		util.fileWriting("Clicked on TAB : "+str_ClipTab);
		Thread.sleep(5000);

		util.takeScreenShot(driver,"NMA_8611","Clips_MainScreen");

		JSONObject jsnObj_ClipMainURL = mJsonParser.callHttpRequest(Constants.Clips_MainURL,"GET", null);
		Thread.sleep(Constants.ThreadSleep);

		int str_ClipTotal = jsnObj_ClipMainURL.getInt("total");
		System.out.println("str_ClipTotal : "+str_ClipTotal);
		util.fileWriting("str_ClipTotal : "+str_ClipTotal);
		JSONArray jsnArray_Hits = jsnObj_ClipMainURL.getJSONArray("hits");

		int l=3;
		for(int i=2; i<jsnArray_Hits.length(); i++){
			System.out.println("**********"+i+"**********");
			util.fileWriting("**********"+i+"**********");
			JSONObject json_ClipsInfo = jsnArray_Hits.getJSONObject(i);
			JSONObject json_ClipResult = json_ClipsInfo.getJSONObject("result");
			JSONObject json_ClipSeries = json_ClipResult.getJSONObject("vod");

			String str_ClipID = json_ClipSeries.getString("id");
			System.out.println("str_ClipID : "+str_ClipID);
			util.fileWriting("str_ClipID : "+str_ClipID);
			String str_ClipName = json_ClipSeries.getString("name");
			System.out.println("str_ClipName : "+str_ClipName);
			util.fileWriting("str_ClipName : "+str_ClipName);
			try{
				String str_ClipDescription = json_ClipSeries.getString("description");
				System.out.println("str_ClipDescription : "+str_ClipDescription);
			}catch(JSONException e){
				System.out.println("The Discription Data Not Found");
			}

			String str_ClipNetwork = json_ClipSeries.getString("network");
			System.out.println("str_ClipNetwork : "+str_ClipNetwork);

			try{
				if(l==1){
					wb_ClipNetwork = driver.findElement(By.id(Constants.app_Package+":id/base_grid").xpath("//android.widget.LinearLayout["+l+"]").id(Constants.app_Package+":id/cell_area_txt").id(Constants.app_Package+":id/cell_txt_title"));
					wb_ClipName = driver.findElement(By.id(Constants.app_Package+":id/base_grid").xpath("//android.widget.LinearLayout["+l+"]").id(Constants.app_Package+":id/cell_area_txt").id(Constants.app_Package+":id/cell_txt_desc"));
				}else{
					wb_ClipNetwork = driver.findElement(By.id(Constants.app_Package+":id/base_grid").xpath("//android.widget.LinearLayout["+l+"]/android.widget.LinearLayout[1]/android.widget.TextView[1]"));
					wb_ClipName = driver.findElement(By.id(Constants.app_Package+":id/base_grid").xpath("//android.widget.LinearLayout["+l+"]/android.widget.LinearLayout[1]/android.widget.TextView[2]"));
				}
			}catch(Exception e){
				System.out.println("X-Path of element not found");
			}
			String str_wbClipName = wb_ClipName.getText();
			String str_wbClipNetwork = wb_ClipNetwork.getText();

			if(str_ClipName!=null && str_ClipNetwork!=null){
				if(str_ClipName.equalsIgnoreCase(str_wbClipName)|| str_ClipNetwork.equalsIgnoreCase(str_wbClipNetwork)){
					util.takeScreenShot(driver,"NMA_8611","Clip_"+i);
					wb_ClipName.click();
					System.out.println("Clicked on CLIP: "+str_wbClipName);
					Thread.sleep(Constants.ThreadSleep);

					if(Constants.DeviceType.equalsIgnoreCase("Large")){
						SwipeAndScroll.swipeWithConstantValues(driver, 100.0, 1000.0, 100.0, 30.0, 2.0);
					}else if(Constants.DeviceType.equalsIgnoreCase("Small")){
						SwipeAndScroll.swipeWithConstantValues(driver, 100.0, 600.0, 100.0, 30.0, 2.0);
					}else{
						throw new AssertionError("Please Check DeviceType in Constants.java file");
					}
					Thread.sleep(Constants.ThreadSleep);

					util.takeScreenShot(driver,"NMA_8611","Clip_"+i+"-Deatils");
					List<WebElement> list_ClipsReltVideoNetworks = driver.findElements(By.id(Constants.app_Package+":id/details_list").id(Constants.app_Package+":id/cell_txt_title"));
					List<WebElement> list_ClipsReltVideoName = driver.findElements(By.id(Constants.app_Package+":id/details_list").id(Constants.app_Package+":id/cell_txt_subtitle"));

					if(list_ClipsReltVideoName.size()>0){
						for(int j=0; j<1; j++){
							try{
								WebElement wb_ClipReltVideosName = list_ClipsReltVideoName.get(j);
								//System.out.println(wb_ClipReltVideosName.getText());
								wb_ClipReltVideosName.click();							
								Thread.sleep(5000);

								JSONObject jsnObj_ClipsReltVideoURL = mJsonParser.callHttpRequest(Constants.Clips_ReltVideoURL,"GET", null);
								Thread.sleep(Constants.ThreadSleep);							
								int int_ClipReltVideoTotal = jsnObj_ClipsReltVideoURL.getInt("total");							
								JSONArray jsnArray_ReltVideoHits = jsnObj_ClipsReltVideoURL.getJSONArray("hits");
								JSONObject json_ClipsReltVideosInfo = jsnArray_ReltVideoHits.getJSONObject(j);
								JSONObject json_ClipsReltVideosResult = json_ClipsReltVideosInfo.getJSONObject("result");
								JSONObject json_ClipsReltVideosVOD = json_ClipsReltVideosResult.getJSONObject("vod");

								String str_ClipReltVideosName = json_ClipsReltVideosVOD.getString("name");
								System.out.println("str_ClipReltVideosName		:"+str_ClipReltVideosName);
								System.out.println("wb_ClipReltVideosName		:"+wb_ClipReltVideosName.getText());
								String str_ClipReltVideosNetwork = json_ClipsReltVideosVOD.getString("network");
								System.out.println("str_ClipReltVideosNetwork	:"+str_ClipReltVideosNetwork);

								if(list_ClipsReltVideoNetworks.size()>1){
									WebElement wb_ClipsReltVideoNetwork = list_ClipsReltVideoNetworks.get(j+1);
									System.out.println("wb_ClipsReltVideoNetwork	:"+wb_ClipsReltVideoNetwork.getText());
								}


								for(int k=-1; k<j; k++){
									if(Constants.DeviceType.equalsIgnoreCase("Large")){
										SwipeAndScroll.swipeWithConstantValues(driver, 100.0, 1000.0, 100.0, 30.0, 2.0);
									}else if(Constants.DeviceType.equalsIgnoreCase("Small")){
										SwipeAndScroll.swipeWithConstantValues(driver, 100.0, 600.0, 100.0, 30.0, 2.0);
									}else{
										throw new AssertionError("Please Check DeviceType in Constants.java file");
									}
									Thread.sleep(5000);
								}
								int m = int_ClipReltVideoTotal/2;
								if(j>=m)
									break;

							}catch(JSONException e){
								System.out.println("The Discription Data Not Found");
							}
						}
					}
					driver.navigate().back();
					Thread.sleep(Constants.ThreadSleep);
				}
			}else{
				System.out.println("Clip Data is not available");
			}

			if (l == 3)
				l = 4;
			else{
				l = 3;
				if(i<4){
					try{
						//driver.swipe(100, 790, 100, 35, 2500);
						if(Constants.DeviceType.equalsIgnoreCase("Large")){
							SwipeAndScroll.swipeWithConstantValues(driver, 100.0, 800.0, 100.0, 30.0, 3.0);
						}else if(Constants.DeviceType.equalsIgnoreCase("Small")){
							SwipeAndScroll.swipeWithConstantValues(driver, 100.0, 420.0, 100.0, 30.0, 3.0);
						}else{
							throw new AssertionError("Please Check DeviceType in Constants.java file");
						}
						Thread.sleep(Constants.ThreadSleep);
					}catch(Exception e){
						e.getMessage();
					}
				}
				else
				{
					//driver.swipe(100, 670, 100, 35,2800);
					if(Constants.DeviceType.equalsIgnoreCase("Large")){
						SwipeAndScroll.swipeWithConstantValues(driver, 100.0, 680.0, 100.0, 30.0, 3.0);
					}else if(Constants.DeviceType.equalsIgnoreCase("Small")){
						SwipeAndScroll.swipeWithConstantValues(driver, 100.0, 350.0, 100.0, 30.0, 3.0);
					}else{
						throw new AssertionError("Please Check DeviceType in Constants.java file");
					}
					Thread.sleep(Constants.ThreadSleep);
				}
			}
			if(i==10)
				break;
		}

		System.out.println("==========================================================================");
		System.out.println("End TestCase  ---> Test_NMA_8611()");
		System.out.println("==========================================================================");

	}
	
	@Test
	public void Test_Clips_PlayPause() throws Exception{
		WebElement wb_Tabs = driver.findElement(By.id(Constants.app_Package+":id/tabs"));
		SwipeAndScroll.scrollOnTab(driver, wb_Tabs, "Clips");
		Thread.sleep(5000);

		WebElement Clip_Tab = driver.findElement(By.id(Constants.app_Package+":id/clips_tab"));
		String str_ClipTab = Clip_Tab.getText();
		Clip_Tab.click();		
		System.out.println("Clicked on TAB : "+str_ClipTab);
		util.fileWriting("Clicked on TAB : "+str_ClipTab);
		Thread.sleep(5000);
		
		WebElement wb_CellIconPlay = driver.findElement(By.id(Constants.app_Package+":id/cell_icon_play"));
		wb_CellIconPlay.click();
		System.out.println("Selected clip to play the video ");
		Thread.sleep(10000);
		
		WebElement wb_PlaybackControls = driver.findElement(By.id(Constants.app_Package+":id/playback_container"));
		SwipeAndScroll.tapOnElement(driver, wb_PlaybackControls);
		
		WebElement wb_BtnPlayPause = driver.findElement(By.id(Constants.app_Package+":id/playback_btn_playpause"));
		wb_BtnPlayPause.click();
		System.out.println("Clicked on Pause button ");
		Thread.sleep(Constants.ThreadSleep);
		
		WebElement wb_PlaybackControls1 = driver.findElement(By.id(Constants.app_Package+":id/playback_container"));
		SwipeAndScroll.tapOnElement(driver, wb_PlaybackControls1);
		
		wb_BtnPlayPause.click();
		System.out.println("Clicked on Play button ");
		Thread.sleep(5000);
		
		driver.navigate().back();
		
	}
	
	
	
	@Test
	public void Test_Clips_Mute() throws Exception{
		WebElement wb_Tabs = driver.findElement(By.id(Constants.app_Package+":id/tabs"));
		SwipeAndScroll.scrollOnTab(driver, wb_Tabs, "Clips");
		Thread.sleep(5000);

		WebElement Clip_Tab = driver.findElement(By.id(Constants.app_Package+":id/clips_tab"));
		String str_ClipTab = Clip_Tab.getText();
		Clip_Tab.click();		
		System.out.println("Clicked on TAB : "+str_ClipTab);
		util.fileWriting("Clicked on TAB : "+str_ClipTab);
		Thread.sleep(5000);
		
		WebElement wb_CellIconPlay = driver.findElement(By.id(Constants.app_Package+":id/cell_icon_play"));
		wb_CellIconPlay.click();
		System.out.println("Selected clip to play the video ");
		Thread.sleep(10000);
		
		WebElement wb_PlaybackControls = driver.findElement(By.id(Constants.app_Package+":id/playback_container"));
		SwipeAndScroll.tapOnElement(driver, wb_PlaybackControls);
		
		WebElement wb_BtnMute = driver.findElement(By.id(Constants.app_Package+":id/playback_btn_mute"));
		wb_BtnMute.click();
		System.out.println("Clicked on Mute button ");
		Thread.sleep(5000);
		
		SwipeAndScroll.tapOnElement(driver, wb_PlaybackControls);
		
		wb_BtnMute.click();
		System.out.println("Released Mute button ");
		Thread.sleep(5000);
		
		driver.navigate().back();
		
	}

}
