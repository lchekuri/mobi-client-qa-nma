
package com.mobitv.client.qa.nma.regression.mobile;

import io.appium.java_client.AppiumDriver;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.internal.Locatable;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.mobitv.client.qa.nma.common.Constants;
import com.mobitv.client.qa.nma.common.DataParser;
import com.mobitv.client.qa.nma.common.DateandTimeClass;
import com.mobitv.client.qa.nma.common.ExecuteShellComand;
import com.mobitv.client.qa.nma.common.SwipeAndScroll;
import com.mobitv.client.qa.nma.common.Util;

public class TestShows {

	WebElement wb_ShowName;
	private static Util util = new Util();
	// private static WebDriver driver = null;
	private static AppiumDriver driver = null;
	private DataParser mJsonParser = new DataParser();
	private ExecuteShellComand command = new ExecuteShellComand();
	private DateandTimeClass dt = new DateandTimeClass();
	String currdate = dt.getcurrentDate();

	@AfterClass
	public void callPerl() throws InterruptedException {

		System.out.println("Enable GA logs on device");
		command.enableGALog();
		System.out.println("Capture adb logs");
		command.executeCommand("Shows");
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Call perl script");
		command.executeCommandinvokeperl("Shows.pl");
		Thread.sleep(30000);
		command.kill();
	}

	@BeforeMethod
	public void setUp() throws Exception {

		driver = util.getAndroidDriver(driver);

	}

	@AfterMethod
	public void tearDown() throws Exception {

		driver.quit();
		Thread.sleep(Constants.ThreadSleep);

	}

	@Test
	public void Test_Shows() throws Exception {

		System.out.println("Start Test_Shows");
		util.fileWriting("Start Test_Shows");
		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");

		WebElement Shows_Tab = driver.findElement(By.id(Constants.app_Package + ":id/shows_tab"));
		String str_ShowsTab = Shows_Tab.getText();
		Shows_Tab.click();
		System.out.println("Clicked on TAB : " + str_ShowsTab);
		util.fileWriting("Clicked on TAB : " + str_ShowsTab);
		Thread.sleep(5000);

		util.takeScreenShot(driver, "Test_Shows", "Shows_MainScreen");

		JSONObject jsnObj_ShowMainURL = mJsonParser.callHttpRequest(Constants.show_MainURL, "GET", null);
		Thread.sleep(Constants.ThreadSleep);

		int str_ShowTotal = jsnObj_ShowMainURL.getInt("total");
		System.out.println("str_ShowTotal : " + str_ShowTotal);
		util.fileWriting("str_ShowTotal : " + str_ShowTotal);
		JSONArray jsnArray_Hits = jsnObj_ShowMainURL.getJSONArray("hits");

		int l = 3;
		for (int i = 2; i < jsnArray_Hits.length(); i++) {
			System.out.println("**********" + (i + 1) + "**********");
			util.fileWriting("**********" + (i + 1) + "**********");
			JSONObject json_ShowsInfo = jsnArray_Hits.getJSONObject(i);
			JSONObject json_ShowResult = json_ShowsInfo.getJSONObject("result");
			JSONObject json_ShowSeries = json_ShowResult.getJSONObject("series");
			String str_ShowID = json_ShowSeries.getString("id");
			System.out.println("str_ShowID : " + str_ShowID);
			util.fileWriting("str_ShowID : " + str_ShowID);
			String str_ShowName = json_ShowSeries.getString("name");
			System.out.println("str_ShowName : " + str_ShowName);
			util.fileWriting("str_ShowName : " + str_ShowName);
			try {
				if (l == 1) {
					wb_ShowName = driver.findElement(By.id(Constants.app_Package + ":id/base_grid").xpath("//android.widget.LinearLayout[" + l + "]")
							.id(Constants.app_Package + ":id/cell_area_txt").id(Constants.app_Package + ":id/cell_txt_title"));
				} else {
					wb_ShowName = driver.findElement(By.id(Constants.app_Package + ":id/base_grid").xpath(
							"//android.widget.LinearLayout[" + l + "]/android.widget.LinearLayout[1]/android.widget.TextView[1]"));
				}
			} catch (Exception e) {
				System.out.println("X-Path of the Element Not Found");
			}
			String str_wbShowName = wb_ShowName.getText();

			if (str_ShowName != null) {
				if (str_ShowName.equalsIgnoreCase(str_wbShowName)) {

					util.takeScreenShot(driver, "Test_Shows", "Show_" + (i + 1));

					wb_ShowName.click();
					System.out.println("Clicked on SHOW: " + str_wbShowName);
					util.fileWriting("Clicked on SHOW: " + str_wbShowName);
					Thread.sleep(Constants.ThreadSleep);

					JSONObject jsnObj_ShowEpisodesURL = mJsonParser.callHttpRequest(Constants.show_EpisodesURL + str_ShowID, "GET", null);
					Thread.sleep(Constants.ThreadSleep);

					int str_ShowEpisodesTotal = jsnObj_ShowEpisodesURL.getInt("total");
					System.out.println("str_ShowTotal : " + str_ShowEpisodesTotal);
					util.fileWriting("str_ShowTotal : " + str_ShowEpisodesTotal);
					JSONArray jsnArray_EpisodesHits = jsnObj_ShowEpisodesURL.getJSONArray("hits");

					List<WebElement> list_ShowsEpisodes = driver.findElements(By.id(Constants.app_Package + ":id/details_list").id(Constants.app_Package + ":id/cell_txt_title"));

					for (int j = 0; j < jsnArray_EpisodesHits.length(); j++) {

						JSONObject json_ShowsEpisodeInfo = jsnArray_EpisodesHits.getJSONObject(j);
						JSONObject json_ShowEpisodeResult = json_ShowsEpisodeInfo.getJSONObject("result");
						JSONObject json_ShowEpisodeVOD = json_ShowEpisodeResult.getJSONObject("vod");
						String str_ShowEpisodeID = json_ShowEpisodeVOD.getString("id");
						// System.out.println("str_ShowEpisodeID : "+str_ShowEpisodeID);
						String str_ShowEpisodeName = json_ShowEpisodeVOD.getString("name");
						System.out.println("str_ShowEpisodeName : " + str_ShowEpisodeName);
						util.fileWriting("str_ShowEpisodeName : " + str_ShowEpisodeName);
						try {
							String str_ShowEpisodeDescription = json_ShowEpisodeVOD.getString("description");
							System.out.println("str_ShowEpisodeDescription : " + str_ShowEpisodeDescription);
							util.fileWriting("str_ShowEpisodeDescription : " + str_ShowEpisodeDescription);
						} catch (JSONException e) {
							System.out.println("The Discription Data Not Found");
						}
						String str_wbShowEpisodeName = list_ShowsEpisodes.get(j + 1).getText();
						System.out.println("wb_ShowEpisodeName : " + str_wbShowEpisodeName);
						util.fileWriting("wb_ShowEpisodeName : " + str_wbShowEpisodeName);

						if (str_ShowEpisodeName.equalsIgnoreCase(str_wbShowEpisodeName)) {
							util.takeScreenShot(driver, "Test_Shows", "Show_" + (i + 1) + "-Episode");
							list_ShowsEpisodes.get(j + 1).click();
							Thread.sleep(Constants.ThreadSleep);
							System.out.println("Clicked on EPISODE: " + str_wbShowEpisodeName);
							util.fileWriting("Clicked on EPISODE: " + str_wbShowEpisodeName);
							WebElement wb_EpisodeDesc = driver.findElement(By.id(Constants.app_Package + ":id/details_txt_desc"));
							String str_wbEpisodeDesc = wb_EpisodeDesc.getText();
							// Assert.assertEquals(str_ShowEpisodeDescription,
							// str_wbEpisodeDesc);
							util.takeScreenShot(driver, "Test_Shows", "Show_" + (i + 1) + "-EpisodeDeatils");
						}

						driver.navigate().back();
						Thread.sleep(Constants.ThreadSleep);

						if (j >= (list_ShowsEpisodes.size() / 2))
							break;
					}

					driver.navigate().back();
					Thread.sleep(Constants.ThreadSleep);
				}
			} else {
				System.out.println("Music Data is not available");
			}

			if (l == 3)
				l = 4;
			else {
				l = 3;
				if (i < 4) {
					try {
						SwipeAndScroll.swipeGrid_One(driver);
						Thread.sleep(Constants.ThreadSleep);
					} catch (Exception e) {
						e.getMessage();
					}
				} else {
					SwipeAndScroll.swipeGrid_Two(driver);
					Thread.sleep(Constants.ThreadSleep);
				}
			}
			if (i == 20)
				break;
		}

		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");
		System.out.println("End Test_Shows");
		util.fileWriting("End Test_Shows");
		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");
	}

	@Test
	public void TestShows_NMA8840_NMA8841() throws InterruptedException {

		WebElement Shows_Tab = driver.findElement(By.id(Constants.app_Package + ":id/shows_tab"));
		String str_ShowsTab = Shows_Tab.getText();
		Shows_Tab.click();
		System.out.println("Clicked on TAB : " + str_ShowsTab);

		WebElement Shows_thumb = driver.findElement(By.id(Constants.app_Package + ":id/com.mobitv.client.sprinttvng:id/cell_thumb"));
		Shows_thumb.click();
		System.out.println("Clicked on shows thumb : " + Shows_thumb.getText());

		WebElement showswatchlist = driver.findElement(By.id(Constants.app_Package + ":id/com.mobitv.client.sprinttvng:id/details_watchlist_btn"));
		// TODO Verify different states -- Bug filed - NMA-9735
		Assert.assertTrue(showswatchlist.isEnabled());

		WebElement wb_Tabs = driver.findElement(By.id(Constants.app_Package + ":id/tabs"));
		SwipeAndScroll.scrollOnTab(driver, wb_Tabs, "Watchlist");
		Thread.sleep(5000);

		WebElement Networks_Tab = driver.findElement(By.id(Constants.app_Package + ":id/networks_tab"));
		String str_Networks_Tab = Networks_Tab.getText();
		Networks_Tab.click();
		System.out.println("Clicked on TAB : " + str_Networks_Tab);

	}

	@Test
	public void TestShows_NMA9654() throws InterruptedException {

		List<WebElement> menuTabs = driver.findElements(By.id(Constants.app_Package + ":id/tabs").className("android.widget.TextView"));
		System.out.println("name is" + menuTabs.get(1).getText());
		for (WebElement menu : menuTabs) {
			String menuName = menu.getText();
			// SwipeAndScroll.scrollOnTab(driver, menu, menuName);
			if (menuName.equalsIgnoreCase("Shows") || menuName.equalsIgnoreCase("Movies")) {
				menu.click();
				System.out.println("clicked on" + menuName);
				Thread.sleep(5000);
				List<WebElement> imageTiles = driver.findElements(By.id(Constants.app_Package + ":id/cell_thumb"));
				for (WebElement imageTile : imageTiles) {
					Dimension imageSize = imageTile.getSize();
					int imageWidth = imageSize.getWidth();
					int imageHeight = imageSize.getHeight();
					Assert.assertTrue(imageHeight > imageWidth, "Error: Image Tiles are in Landscape Mode");
					break;
				}
				int totalImageTiles = imageTiles.size();
				System.out.println("Image tiles" + totalImageTiles);
				Assert.assertTrue((totalImageTiles % 3) < 3, "Error: Image tile are more than 3 in a row");
			}
		}

	}
}

