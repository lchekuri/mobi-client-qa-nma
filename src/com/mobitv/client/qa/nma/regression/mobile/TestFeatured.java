
package com.mobitv.client.qa.nma.regression.mobile;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.UnreachableBrowserException;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.mobitv.client.qa.nma.common.Constants;
import com.mobitv.client.qa.nma.common.DataParser;
import com.mobitv.client.qa.nma.common.DateandTimeClass;
import com.mobitv.client.qa.nma.common.ExecuteShellComand;
import com.mobitv.client.qa.nma.common.SwipeAndScroll;
import com.mobitv.client.qa.nma.common.Util;

public class TestFeatured {

	WebElement wb_ShowName;
	private static Util util = new Util();
	private static WebDriver driver = null;
	private DataParser mJsonParser = new DataParser();
	private ExecuteShellComand command = new ExecuteShellComand();
	private DateandTimeClass dt = new DateandTimeClass();
	String currdate = dt.getcurrentDate();

	@AfterClass
	public void callPerl() throws InterruptedException {

		System.out.println("Enable GA logs on device");
		command.enableGALog();
		System.out.println("Capture adb logs");
		command.executeCommand("Featured");
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Call perl script");
		command.executeCommandinvokeperl("Featured.pl");
		Thread.sleep(30000);
		command.kill();
	}

	@BeforeMethod
	public void setUp() throws Exception {

		driver = util.getAndroidDriver(driver);

	}

	@AfterMethod
	public void tearDown() throws Exception {

		driver.quit();
		Thread.sleep(Constants.ThreadSleep);

	}

	@Test
	public void Test_Featured() throws Exception, UnreachableBrowserException {

		System.out.println("Start Test_Featured");
		util.fileWriting("Start Test_Featured");
		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");

		WebElement Featured_Tab = driver.findElement(By.id(Constants.app_Package + ":id/home_tab"));
		String str_FeaturedTab = Featured_Tab.getText();
		Featured_Tab.click();
		System.out.println("Clicked on TAB : " + str_FeaturedTab);
		util.fileWriting("Clicked on TAB : " + str_FeaturedTab);
		Thread.sleep(5000);
		util.takeScreenShot(driver, "Test_Featured", "Featured_MainScreen");

		WebElement featured_MktCellThumb = driver.findElement(By.id(Constants.app_Package + ":id/mkt_cell_thumb"));
		Assert.assertTrue(featured_MktCellThumb.isEnabled());
		System.out.println("Marketing Tile Image exists on the UI of Featured MainScreen");
		util.fileWriting("Marketing Tile Image exists on the UI of Featured MainScreen");

		SwipeAndScroll.swipeWithConstantValues(driver, 100.0, 690.0, 100.0, 35.0, 3.0);
		Thread.sleep(Constants.ThreadSleep);

		System.out.println("Verifying All FEATURED Channels");
		util.fileWriting("Verifying All FEATURED Channels");
		for (int i = 0; i < 2; i++) {

			WebElement featured_CellGroupName = driver.findElement(By.id(Constants.app_Package + ":id/home_list").xpath("//android.widget.LinearLayout[1]")
					.id(Constants.app_Package + ":id/group_header").id(Constants.app_Package + ":id/group_name"));
			String str_CellGroupName = featured_CellGroupName.getText();
			Assert.assertNotNull("Channel Category Genre should not be Null in Featured MainScreen", str_CellGroupName);
			System.out.println(str_CellGroupName + ": Featured Channel Category Genre exists on the UI of Featured MainScreen");
			util.fileWriting(str_CellGroupName + ": Featured Channel Category Genre exists on the UI of Featured MainScreen");

			WebElement featured_SEEAll = driver.findElement(By.id(Constants.app_Package + ":id/genre_see_all_text"));
			String str_SEEAll = featured_CellGroupName.getText();
			System.out.println(str_SEEAll + ": SEE ALL exists on the UI of Featured MainScreen");
			util.fileWriting(str_SEEAll + ": SEE ALL exists on the UI of Featured MainScreen");

			WebElement featured_CellTxtLeftTitle = driver.findElement(By.id(Constants.app_Package + ":id/left_tile").xpath("//android.widget.LinearLayout[1]")
					.id(Constants.app_Package + ":id/cell_area_txt").id(Constants.app_Package + ":id/cell_txt_title"));
			String str_CellTxtLeftTitle = featured_CellTxtLeftTitle.getText();
			Assert.assertNotNull("Left Tile Channel Name should not be Null in Featured MainScreen", str_CellTxtLeftTitle);
			System.out.println(str_CellTxtLeftTitle + ": Left Tile Featured Channel Name exists on the UI of Featured MainScreen");
			util.fileWriting(str_CellTxtLeftTitle + ": Left Tile Featured Channel Name exists on the UI of Featured MainScreen");

			WebElement featured_CellTxtLeftDesc = driver.findElement(By.id(Constants.app_Package + ":id/left_tile").xpath(
					"//android.widget.LinearLayout[1]/android.widget.LinearLayout[1]/android.widget.TextView[2]"));
			String str_CellTxtLeftDesc = featured_CellTxtLeftDesc.getText();
			Assert.assertNotNull("Left Tile Channel Description should not be Null in Featured MainScreen", str_CellTxtLeftDesc);
			System.out.println(str_CellTxtLeftDesc + ": Left Tile Featured Channel Description exists on the UI of Featured MainScreen");
			util.fileWriting(str_CellTxtLeftDesc + ": Left Tile Featured Channel Description exists on the UI of Featured MainScreen");

			WebElement featured_CellTxtRightTitle = driver.findElement(By.id(Constants.app_Package + ":id/left_tile").xpath(
					"//android.widget.LinearLayout[1]/android.widget.LinearLayout[1]/android.widget.TextView[1]"));
			String str_CellTxtRightTitle = featured_CellTxtRightTitle.getText();
			Assert.assertNotNull("Right Tile Channel Name should not be Null in Featured MainScreen", str_CellTxtRightTitle);
			System.out.println(str_CellTxtRightTitle + ": Right Tile Featured Channel Name exists on the UI of Featured MainScreen");
			util.fileWriting(str_CellTxtRightTitle + ": Right Tile Featured Channel Name exists on the UI of Featured MainScreen");

			WebElement featured_CellTxtRightDesc = driver.findElement(By.id(Constants.app_Package + ":id/left_tile").xpath(
					"//android.widget.LinearLayout[1]/android.widget.LinearLayout[1]/android.widget.TextView[2]"));
			String str_CellTxtRightDesc = featured_CellTxtRightDesc.getText();
			Assert.assertNotNull("Left Tile Channel Description should not be Null in Featured MainScreen", str_CellTxtRightDesc);
			System.out.println(str_CellTxtRightDesc + ": Left Tile Featured Channel Description exists on the UI of Featured MainScreen");
			util.fileWriting(str_CellTxtRightDesc + ": Left Tile Featured Channel Description exists on the UI of Featured MainScreen");

			featured_SEEAll.click();
			System.out.println("Clicked on Featured CHANNEL CATEGORY GROUP : " + str_CellGroupName);
			util.fileWriting("Clicked on Featured CHANNEL CATEGORY GROUP : " + str_CellGroupName);
			Thread.sleep(Constants.ThreadSleep);

			util.takeScreenShot(driver, "Test_Featured", (i + 1) + "_Featured_" + str_CellGroupName);

			/*
			 * WebElement featured_CS_ActionBarTitle =
			 * driver.findElement(By.id("android:id/action_bar_title")); String
			 * str_ActionBarTitle = featured_CS_ActionBarTitle.getText();
			 * Assert.assertNotNull(
			 * "ActionBar Title should not be Null in Featured Channels Screen",
			 * str_ActionBarTitle); System.out.println(str_ActionBarTitle
			 * +": ActionBar Title exists on the UI of Featured Channels Screen"
			 * ); util.fileWriting(str_ActionBarTitle
			 * +": ActionBar Title exists on the UI of Featured Channels Screen"
			 * );
			 */

			for (int j = 0; j < 3; j++) {
				try {
					WebElement featured_ChannelTitle = driver.findElement(By.id(Constants.app_Package + ":id/cell_txt_title"));
					String str_ChannelTitle = featured_ChannelTitle.getText();
					Assert.assertNotNull("Channel Name should not be Null in Featured Channels Screen", str_ChannelTitle);
					System.out.println(str_ChannelTitle + ": Channel Name exists on the UI of Featured Channels Screen");
					util.fileWriting(str_ChannelTitle + ": Channel Name exists on the UI of Featured Channels Screen");

					WebElement featured_ChannelDesc = driver.findElement(By.id(Constants.app_Package + ":id/cell_txt_desc"));
					String str_ChannelDesc = featured_ChannelDesc.getText();
					Assert.assertNotNull("Channel Description should not be Null in Featured Channels Screen", str_ChannelDesc);
					System.out.println(str_ChannelDesc + ": Channel Description exists on the UI of Featured Channels Screen");
					util.fileWriting(str_ChannelDesc + ": Channel Description exists on the UI of Featured Channels Screen");

					WebElement featured_CS_CellThumb = driver.findElement(By.id(Constants.app_Package + ":id/cell_thumb"));
					Assert.assertTrue(featured_CS_CellThumb.isEnabled());
					System.out.println("Channel Thumb  Image exists on the UI of Featured Channels Screen");
					util.fileWriting("Channel Thumb  Image exists on the UI of Featured Channels Screen");

					featured_ChannelTitle.click();
					Thread.sleep(Constants.ThreadSleep);
					System.out.println("Clicked on Network CHANNEL : " + str_ChannelTitle);
					util.fileWriting("Clicked on Network CHANNEL : " + str_ChannelTitle);

					WebElement featured_CDS_ActionBarTitle = driver.findElement(By.id("android:id/toolbar").xpath("//android.widget.TextView[1]"));
					String str_CDS_ActionBarTitle = featured_CDS_ActionBarTitle.getText();

					Assert.assertNotNull("ActionBar Title should not be Null in Channels Details Screen", str_CDS_ActionBarTitle);
					System.out.println(str_CDS_ActionBarTitle + ": ActionBar Title exists on the UI of Channels Details Screen");
					util.fileWriting(str_CDS_ActionBarTitle + ": ActionBar Title exists on the UI of Channels Details Screen");

					util.takeScreenShot(driver, "Test_Featured", (j + 1) + "_Featured_" + str_CellGroupName + "_VideoDetails");

				} catch (Exception e) {
					System.out.println("X-Path of the element not found");
					util.fileWriting("X-Path of the element not found");
				}

				driver.navigate().back();
				Thread.sleep(Constants.ThreadSleep);

				SwipeAndScroll.swipeWithConstantValues(driver, 100.0, 680.0, 100.0, 35.0, 3.0);
				Thread.sleep(Constants.ThreadSleep);
			}
			driver.navigate().back();
			Thread.sleep(Constants.ThreadSleep);

			SwipeAndScroll.swipeWithConstantValues(driver, 100.0, 760.0, 100.0, 35.0, 3.0);
			Thread.sleep(Constants.ThreadSleep);
		}

		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");
		System.out.println("End Test_Featured");
		util.fileWriting("End Test_Featured");
		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");
	}

	/*
	 * @Test public void Test_Login() throws Exception{ WebElement
	 * wb_AcceptEulaBtn =
	 * driver.findElement(By.id(Constants.app_Package+":id/accept_eula_button"
	 * )); String str_AcceptEulaBtn = wb_AcceptEulaBtn.getText();
	 * wb_AcceptEulaBtn.click();
	 * System.out.println("Clicked on : "+str_AcceptEulaBtn);
	 * Thread.sleep(5000);
	 * 
	 * WebElement wb_LoginUserName =
	 * driver.findElement(By.id(Constants.app_Package
	 * +":id/login_edit_username")); wb_LoginUserName.sendKeys("mobitest11");
	 * System.out.println("Entered UserName");
	 * Thread.sleep(Constants.ThreadSleep);
	 * 
	 * WebElement wb_LoginUserPassword =
	 * driver.findElement(By.id(Constants.app_Package+":id/login_edit_passwd"));
	 * wb_LoginUserPassword.sendKeys("mobitest11");
	 * System.out.println("Entered Password");
	 * Thread.sleep(Constants.ThreadSleep);
	 * 
	 * WebElement wb_LoginBtnSignIn =
	 * driver.findElement(By.id(Constants.app_Package+":id/login_btn_signin"));
	 * String str_LoginBtnSignIn = wb_LoginBtnSignIn.getText();
	 * wb_LoginBtnSignIn.click();
	 * System.out.println("Clicked on : "+str_LoginBtnSignIn);
	 * Thread.sleep(Constants.ThreadSleep);
	 * 
	 * WebElement wb_BtnYesSendMe =
	 * driver.findElement(By.id("android:id/button1")); String str_BtnYesSendMe
	 * = wb_BtnYesSendMe.getText(); wb_BtnYesSendMe.click();
	 * System.out.println("Clicked on : "+str_BtnYesSendMe);
	 * Thread.sleep(Constants.ThreadSleep);
	 * 
	 * WebElement Featured_Tab =
	 * driver.findElement(By.id(Constants.app_Package+":id/home_tab")); String
	 * str_FeaturedTab = Featured_Tab.getText(); Featured_Tab.click();
	 * System.out.println("Clicked on TAB : "+str_FeaturedTab);
	 * util.fileWriting("Clicked on TAB : "+str_FeaturedTab);
	 * Thread.sleep(5000); }
	 */

}
