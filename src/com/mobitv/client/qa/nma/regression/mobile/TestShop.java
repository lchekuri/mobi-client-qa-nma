
package com.mobitv.client.qa.nma.regression.mobile;

import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.mobitv.client.qa.nma.common.Constants;
import com.mobitv.client.qa.nma.common.DBUtil;
import com.mobitv.client.qa.nma.common.DataParser;
import com.mobitv.client.qa.nma.common.DateandTimeClass;
import com.mobitv.client.qa.nma.common.ExecuteShellComand;
import com.mobitv.client.qa.nma.common.Util;

/**
 * @author lchekuri
 * 
 */
public class TestShop {

	private static Util util = new Util();
	private static WebDriver driver = null;
	private ExecuteShellComand command = new ExecuteShellComand();
	private DataParser mJsonParser = new DataParser();
	DBUtil db = new DBUtil();
	int flag = 0;
	String user_id;
	private DateandTimeClass dt = new DateandTimeClass();
	String currdate = dt.getcurrentDate();

	@BeforeTest
	public void setUp() throws Exception {

		driver = util.getAndroidDriver(driver);
		
	}

	@AfterTest
	public void tearDown() throws Exception {

		driver.quit();
		Thread.sleep(Constants.ThreadSleep);
		command.executeCommand("Shop");
		Thread.sleep(Constants.ThreadSleep);
		command.kill();
		
	}

	/**
	 * @throws Exception
	 */
	@Test
	public void TestShop_Free() throws Exception {

		System.out.println("Start TestShop_Free");
		util.fileWriting("Start TestShop_Free");
		System.out.println("==========================================================================");

		// WebElement wb_Menu = driver.findElement(By.id("android:id/up"));
		WebElement wb_Menu = driver.findElement(By.name("Open navigation drawer"));
		util.takeScreenShot(driver, "TestShop_Free", "Screen1");
		wb_Menu.click();
		System.out.println("Clicked on Menu");
		util.fileWriting("Clicked on Menu");
		Thread.sleep(Constants.ThreadSleep);
		
		//Get user_id from settings
		System.out.println("//Get user_id from settings");
		WebElement wb_settings = driver.findElement(By.id(Constants.app_Package + "id/cell_edit_screen").name("Settings"));
		wb_settings.click();
		WebElement wb_user_id = driver.findElement(By.id(Constants.app_Package + ":id/user_id_value"));
		user_id= wb_user_id.getText().substring(0, 5);
		System.out.println("=====Userid is====="+user_id);
		
		wb_Menu.click();
		WebElement wb_ShopMenu = driver.findElement(By.id(Constants.app_Package + ":id/cell_edit_screen").name("Shop"));
		String str_wbShopMenu = wb_ShopMenu.getText();
		wb_ShopMenu.click();
		System.out.println("Clicked on Menu : " + str_wbShopMenu);
		Thread.sleep(Constants.ThreadSleep);

		String str_ShopOfferURL = "http://msfrn-func-cp-qa.mobitv.com/aggregator/v5/managedlist/" + Constants.carrier + "/" + Constants.product + "/" + Constants.version
				+ "/tile/shop-offers.json";

		List<WebElement> pack_price_list = driver.findElements(By.id(Constants.app_Package + ":id/shop_package_price"));

		JSONObject jsnObj_ShopOffeURL = mJsonParser.callHttpRequest(str_ShopOfferURL, "GET", null);
		Thread.sleep(Constants.ThreadSleep);

		JSONArray jsnArry_TileItems = jsnObj_ShopOffeURL.getJSONArray("tile_items");
		for (int i = 0; i < 2; i++) {
			System.out.println("Offer====" + i);
			JSONObject jsnObj_TileItem = jsnArry_TileItems.getJSONObject(i);
			int int_RefID = jsnObj_TileItem.getInt("ref_id");
			System.out.println("int_RefID: " + int_RefID);
			String str_Name = jsnObj_TileItem.getString("name");
			double price = jsnObj_TileItem.getDouble("price");
			String offer_type = jsnObj_TileItem.getString("offer_type");
			System.out.println("str_Name: " + str_Name);
			/*
			 * if (offer_type.equalsIgnoreCase("free")) {
			 * 
			 * if
			 * (pack_price_list.get(i).getText().contains("30 day trial available"
			 * )) { util.takeScreenShot(driver, "TestShop_Free", "Screen4");
			 * pack_price_list.get(i).click(); Thread.sleep(5000); WebElement
			 * wb_OfferDetailsThumb =
			 * driver.findElement(By.id(Constants.app_Package +
			 * ":id/offer_details_thumb")); WebElement wb_OfferDetailsName =
			 * driver.findElement(By.id(Constants.app_Package +
			 * ":id/offer_details_name")); WebElement
			 * wb_OfferDetailsDdescription =
			 * driver.findElement(By.id(Constants.app_Package +
			 * ":id/offer_details_description")); WebElement wb_ActionBarTitle =
			 * driver.findElement(By.id("android:id/toolbar").xpath(
			 * "//android.widget.TextView[1]")); String str_wbOfferDetailsName =
			 * wb_OfferDetailsName.getText();
			 * System.out.println("str_wbOfferDetailsName : " +
			 * str_wbOfferDetailsName); String str_wbOfferDetailsDdescription =
			 * wb_OfferDetailsDdescription.getText();
			 * System.out.println("str_wbOfferDetailsDdescription : " +
			 * str_wbOfferDetailsDdescription); String str_wbActionBarTitle =
			 * wb_ActionBarTitle.getText();
			 * System.out.println("str_wbActionBarTitle : " +
			 * str_wbActionBarTitle); Assert.assertEquals(str_Name,
			 * str_wbActionBarTitle); util.takeScreenShot(driver,
			 * "TestShop_Free", "Screen5"); driver.navigate().back();
			 * System.out.println(
			 * "=========================================================================="
			 * ); util.fileWriting(
			 * "=========================================================================="
			 * ); System.out.println("End TestShop_Free");
			 * util.fileWriting("End TestShop_Free"); System.out.println(
			 * "=========================================================================="
			 * ); util.fileWriting(
			 * "=========================================================================="
			 * ); } }
			 */

			if (offer_type.equalsIgnoreCase("subscription")) {

				if (pack_price_list.get(i).getText().startsWith("Subscription ends on")) {
					System.out.println("update DB field");
					db.updateExpireDate(int_RefID, user_id);
					Thread.sleep(90000);
					wb_Menu.click();
					WebElement wb_config = driver.findElement(By.id("com.mobitv.client.sprinttvng:id/cell_edit_screen").name("Configuration"));
					wb_config.click();
					WebElement wb_clrcache = driver.findElement(By.id("com.mobitv.client.sprinttvng:id/easteregg_btn_clear"));
					wb_clrcache.click();
					WebElement wb_confirm = driver.findElement(By.id("android:id/button1"));
					wb_confirm.click();
					Thread.sleep(10000);
					wb_Menu.click();
					wb_ShopMenu.click();
					flag = 1;
				}
				if (pack_price_list.get(i).getText().startsWith("$")) {
					
					//verify subscription flow
					System.out.println("=====Click on price button=====");
					pack_price_list.get(i).click();
					Thread.sleep(5000);
					WebElement wb_OfferDetailsThumb = driver.findElement(By.id(Constants.app_Package + ":id/offer_details_thumb"));
					WebElement wb_OfferDetailsName = driver.findElement(By.id(Constants.app_Package + ":id/offer_details_name"));
					WebElement wb_OfferDetailsPurchase_Btn = driver.findElement(By.id(Constants.app_Package + ":id/offer_details_purchase_btn"));
					WebElement wb_OfferDetailsDdescription = driver.findElement(By.id(Constants.app_Package + ":id/offer_details_description"));
					WebElement wb_ActionBarTitle = driver.findElement(By.id("android:id/toolbar").xpath("//android.widget.TextView[1]"));
					
					String str_wbOfferDetailsName = wb_OfferDetailsName.getText();
					System.out.println("==========str_wbOfferDetailsName : =========" + str_wbOfferDetailsName);
					String str_wbOfferDetailsDdescription = wb_OfferDetailsDdescription.getText();
					System.out.println("========str_wbOfferDetailsDdescription : ========" + str_wbOfferDetailsDdescription);
					String str_wbActionBarTitle = wb_ActionBarTitle.getText();
					System.out.println("=========str_wbActionBarTitle : ================" + str_wbActionBarTitle);
					
					// Assert.assertEquals(str_Name, str_wbActionBarTitle);
					wb_OfferDetailsPurchase_Btn.click();
					WebElement wb_DialogOffersTitle = driver.findElement(By.id(Constants.app_Package + ":id/dialog_offers_title"));
					String str_wbDialogOffersTitle = wb_DialogOffersTitle.getText();
					System.out.println("=======str_wbDialogOffersTitle : =======" + str_wbDialogOffersTitle);
					Assert.assertTrue(str_wbDialogOffersTitle.contains("Confirm your subscription"));
					
					//Verify Confirmation text on purchase confirm dialog
					WebElement wb_DialogOfferConfirmationText = driver.findElement(By.id(Constants.app_Package + ":id/dialog_offer_confirmation_text"));
					String str_wbDialogOfferConfirmationText = wb_DialogOfferConfirmationText.getText();
					Assert.assertTrue(str_wbDialogOfferConfirmationText.contains("You're about to purchase a premium content pack. You'll be billed monthly."));
					System.out.println("=======str_wbDialogOfferConfirmationText : ========" + str_wbDialogOfferConfirmationText);
					
					//Verify check box on purchase confirm dialog 				
					WebElement wb_OfferConfirmationcheckbox = driver.findElement(By.id(Constants.app_Package + ":id/dialog_offer_confirmation_checkbox"));
					Assert.assertTrue(wb_OfferConfirmationcheckbox.isEnabled());
					Assert.assertTrue(wb_OfferConfirmationcheckbox.getText().contains("I certify that I am the owner of this account and agree to these terms"));
					System.out.println("=======str_wbDialogOfferConfirmationText : ========" + str_wbDialogOfferConfirmationText);					
					
					//Verify price on purchase confirm dialog 
					WebElement wb_DialogOfferConfirmationPrice = driver.findElement(By.id(Constants.app_Package + ":id/dialog_offer_confirmation_price"));
					String str_wbDialogOfferConfirmationPrice = wb_DialogOfferConfirmationPrice.getText();
					System.out.println("============str_wbDialogOfferConfirmationPrice============" + str_wbDialogOfferConfirmationPrice);					
					// Assert.assertTrue(str_wbDialogOfferConfirmationPrice.contains("$" + price + "/month"));
					
					//Verify cancel button on purchase confirm dialog 
					WebElement wb_CancelButton = driver.findElement(By.id(Constants.app_Package + ":id/dialog_offers_negative_btn"));
					String str_wbCancelButton = wb_CancelButton.getText();
					Assert.assertEquals("Cancel", wb_CancelButton.getText());
					wb_CancelButton.click();
					System.out.println("=========Clicked on : =========" + str_wbCancelButton);					
					Thread.sleep(5000);
					
					//Verify subscribe button on purchase confirm dialog
					wb_OfferDetailsPurchase_Btn.click();
					System.out.println("Cliked on purchase button");					
					Thread.sleep(10000);
					WebElement wb_SubscribeButton = driver.findElement(By.id(Constants.app_Package + ":id/dialog_offers_positive_btn"));
					
					//Verify Subscribe button state
					Assert.assertFalse(wb_SubscribeButton.isEnabled());
					wb_OfferConfirmationcheckbox.click();
					Assert.assertTrue(wb_SubscribeButton.isEnabled());					
					
					wb_SubscribeButton.click();
					System.out.println("========Clicked on Subscribe===========");					
					Thread.sleep(30000);
					
					// Success message
					WebElement wb_msg = driver.findElement(By.id("android:id/message"));
					Assert.assertTrue(wb_msg.getText().contains("You have successfully subscribed"));
					// WebElement wb_alert = driver.findElement(By.id("android:id/alertTitle"));
					// Assert.assertTrue(wb_alert.getText().contains(str_Name));
					WebElement wb_alert_ok = driver.findElement(By.id("android:id/button1"));
					Assert.assertTrue(wb_alert_ok.getText().contains("OK"));
					wb_alert_ok.click();
					System.out.println("=========Cliked on subscription confirmation==========");
					Thread.sleep(5000);

					// Verify Unsubscribe functionality				
					Assert.assertEquals("Unsubscribe", wb_OfferDetailsPurchase_Btn.getText());
					wb_OfferDetailsPurchase_Btn.click();
					System.out.println("========Cliked on Unsubscribe=========");
					WebElement wb_Sub_Cancel = driver.findElement(By.id(Constants.app_Package + ":id/dialog_offers_title"));
					String str_DialogOffersTitle = wb_DialogOffersTitle.getText();
					System.out.println("=========str_DialogOffersTitle : =========" + str_DialogOffersTitle);
					
					Assert.assertEquals("Confirm Subscription Cancellation", str_DialogOffersTitle);
					WebElement wb_DialogOfferConfirmationText_Unsub = driver.findElement(By.id(Constants.app_Package + ":id/dialog_offer_confirmation_text"));
					String str_wbDialogOfferConfirmationText_UnSub = wb_DialogOfferConfirmationText_Unsub.getText();
					System.out.println("==========str_wbDialogOfferConfirmationText : ==========" + str_wbDialogOfferConfirmationText_UnSub);
					
					Assert.assertTrue(str_wbDialogOfferConfirmationText_UnSub.contains("You are about to unsubscribe from a content package. You can still play all content in this package until the end of the current subscription period."));
					WebElement wb_Unsub_Reason = driver.findElement(By.id(Constants.app_Package + ":id/unsubscribe_reason_parent"));
					Assert.assertTrue(wb_Unsub_Reason.isDisplayed());
					WebElement wb_Unsub_Button = driver.findElement(By.id(Constants.app_Package + ":id/dialog_offers_positive_btn"));
					String str_UnsubButton = wb_Unsub_Button.getText();
					Assert.assertEquals("Unsubscribe", str_UnsubButton);
					WebElement wb_CancelButton_unsub = driver.findElement(By.id(Constants.app_Package + ":id/dialog_offers_negative_btn"));
					String str_wbCancelButton_unsub = wb_CancelButton_unsub.getText();
					Assert.assertEquals("Cancel", str_wbCancelButton_unsub);
					wb_CancelButton_unsub.click();
					System.out.println("Clicked on : " + str_wbCancelButton_unsub);
					Thread.sleep(Constants.ThreadSleep);
					
					Assert.assertEquals("Unsubscribe", wb_OfferDetailsPurchase_Btn.getText());
					wb_OfferDetailsPurchase_Btn.click();
					System.out.println("Clicked on Unsub again");
					wb_Unsub_Button.click();
					Thread.sleep(5000);
					WebElement wb_msg1 = driver.findElement(By.id("android:id/message"));
					Assert.assertTrue(wb_msg.getText().contains("You are unsubscribed"));
					// WebElement wb_alert =
					// driver.findElement(By.id("android:id/alertTitle"));
					// Assert.assertTrue(wb_alert.getText().contains(str_Name));
					WebElement wb_alert_ok1 = driver.findElement(By.id("android:id/button1"));
					Assert.assertTrue(wb_alert_ok.getText().contains("OK"));
					wb_alert_ok.click();
					System.out.println("Cliked on unsubscription confirmation");
					Thread.sleep(5000);
					driver.navigate().back();
					if (flag == 0) {
						System.out.println("update DB field");
						db.updateExpireDate(int_RefID, user_id);
						Thread.sleep(90000);
						wb_Menu.click();
						WebElement wb_config = driver.findElement(By.id("com.mobitv.client.sprinttvng:id/cell_edit_screen").name("Configuration"));
						wb_config.click();
						WebElement wb_clrcache = driver.findElement(By.id("com.mobitv.client.sprinttvng:id/easteregg_btn_clear"));
						wb_clrcache.click();
						WebElement wb_confirm = driver.findElement(By.id("android:id/button1"));
						wb_confirm.click();
						Thread.sleep(10000);
						wb_Menu.click();
						wb_ShopMenu.click();
						Assert.assertTrue(pack_price_list.get(i).getText().contains("$"));
						Thread.sleep(5000);
						System.out.println("Cancelation success");

					}

				}
			}

		}
	}

}
