package com.mobitv.client.qa.nma.regression.mobile;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.mobitv.client.qa.nma.common.Constants;
import com.mobitv.client.qa.nma.common.DataParser;
import com.mobitv.client.qa.nma.common.DateandTimeClass;
import com.mobitv.client.qa.nma.common.ExecuteShellComand;
import com.mobitv.client.qa.nma.common.SwipeAndScroll;
import com.mobitv.client.qa.nma.common.Util;

public class TestLive {
	private JSONArray tile_items,jsnArray_LiveTileItems, jArray_Programs;
	private static Util util = new Util();
	private static WebDriver driver = null;
	private DataParser mJsonParser = new DataParser();
	private ExecuteShellComand command = new ExecuteShellComand();
	WebElement wb_MovieName = null;
	WebElement wb_ChannelPlayIcon= null;
	WebElement wb_ClipName= null;
	WebElement wb_ClipNetwork = null;
	WebElement wb_MusicName = null;
	private DateandTimeClass dt = new DateandTimeClass();
	String currdate = dt.getcurrentDate();

	@AfterClass
	public void callPerl() throws InterruptedException {

		System.out.println("Enable GA logs on device");
		command.enableGALog();
		System.out.println("Capture adb logs");
		command.executeCommand("Live");
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Call perl script");
		command.executeCommandinvokeperl("Live.pl");
		Thread.sleep(30000);
		command.kill();
	}
	@BeforeMethod
	public void setUp() throws Exception {		
		driver = util.getAndroidDriver(driver);
	
	}

	@AfterMethod
	public void tearDown() throws Exception {
		driver.quit();
		Thread.sleep(Constants.ThreadSleep);
		
		
		
	}

	/*1.Launch Application
	  2.Select Live Tab
	  3.Check Play icon button is available on playable thumbnail of Live Channel
	  4.Select Movies Tab (If App is Reference Application)
	  5.Check Play icon button is available on playable thumbnail of Movie(If App is Reference Application)
	  6.Select CLIPS Tab
	  7.Check Play icon button is available on playable thumbnail of Clip
	  8.Select MUSIC tab
	  9.Check Play icon button is available on playable thumbnail of Music*/

	@Test
	public void Test_NMA_8605() throws Exception{
		System.out.println("Start TestCase  ---> Test_NMA_8605()");
		System.out.println("==========================================================================");

		WebElement Live_Tab = driver.findElement(By.id(Constants.app_Package+":id/live_guide_tab"));
		String str_LiveTab = Live_Tab.getText();
		Live_Tab.click();		
		System.out.println("Clicked on TAB : "+str_LiveTab);
		util.fileWriting("Clicked on TAB : "+str_LiveTab);
		Thread.sleep(5000);

		util.takeScreenShot(driver,"NMA_8605","Live_MainScreen");

		JSONObject jObj_ChannelListing = mJsonParser.callHttpRequest(Constants.Live_MainURL,"GET", null);
		Thread.sleep(Constants.ThreadSleep);
		jsnArray_LiveTileItems = jObj_ChannelListing.getJSONArray("tile_items");

		for (int i = 0; i < jsnArray_LiveTileItems.length(); i++) {
			WebElement wb_ChannelName = driver.findElement(By.id(Constants.app_Package+":id/channel_name"));
			String str_wbChannelName = wb_ChannelName.getText();
			System.out.println("str_wbChannelName: "+str_wbChannelName);

			wb_ChannelPlayIcon = driver.findElement(By.id(Constants.app_Package+":id/channel_icon_play"));
			wb_ChannelPlayIcon.isEnabled();
			System.out.println("Play icon button is available on playable thumbnail of Channel: "+str_wbChannelName);

			util.takeScreenShot(driver,"NMA_8605","LiveChannel"+i+"_PlayThumb");

			try{
				//driver.swipe(100, 790, 100, 35, 2500);
				if(Constants.DeviceType.equalsIgnoreCase("Large")){
					SwipeAndScroll.swipeWithConstantValues(driver, 100.0, 880.0, 100.0, 30.0, 3.0);
				}else if(Constants.DeviceType.equalsIgnoreCase("Small")){
					SwipeAndScroll.swipeWithConstantValues(driver, 100.0, 443.0, 100.0, 30.0, 2.0);
				}else{
					throw new AssertionError("Please Check DeviceType in Constants.java file");
				}
				Thread.sleep(Constants.ThreadSleep);
			}catch(Exception e){
				e.getMessage();
			}
			if(i>=(jsnArray_LiveTileItems.length()/2)){
				break;
			}
		}

		if(Constants.app_Package.equalsIgnoreCase("com.mobitv.client.connect.mobile")){
			WebElement wb_Tabs = driver.findElement(By.id(Constants.app_Package+":id/tabs"));
			SwipeAndScroll.scrollOnTab(driver, wb_Tabs, "Movies");
			Thread.sleep(5000);

			WebElement Movies_Tab = driver.findElement(By.id(Constants.app_Package+":id/movies_tab"));
			String str_MoviesTab = Movies_Tab.getText();
			Movies_Tab.click();		
			System.out.println("Clicked on TAB : "+str_MoviesTab);
			util.fileWriting("Clicked on TAB : "+str_MoviesTab);
			Thread.sleep(5000);

			util.takeScreenShot(driver,"NMA_8605","Movie_MainScreen");

			JSONObject jsnObj_MovieMainURL = mJsonParser.callHttpRequest(Constants.Movies_MainURL,"GET", null);
			Thread.sleep(Constants.ThreadSleep);

			int str_MovieTotal = jsnObj_MovieMainURL.getInt("total");
			JSONArray jsnArray_Hits = jsnObj_MovieMainURL.getJSONArray("hits");

			int l=3;
			for(int i=2; i<jsnArray_Hits.length(); i++){
				try{
					wb_MovieName = driver.findElement(By.id(Constants.app_Package+":id/base_grid").xpath("//android.widget.LinearLayout["+l+"]/android.widget.LinearLayout[1]/android.widget.TextView[1]"));
					String str_wbMovieName = wb_MovieName.getText();
					System.out.println("str_wbMovieName"+str_wbMovieName);

					wb_ChannelPlayIcon = driver.findElement(By.id(Constants.app_Package+":id/base_grid").xpath("//android.widget.LinearLayout["+l+"]/android.widget.FrameLayout[1]/android.widget.ImageView[1]"));
					wb_ChannelPlayIcon.isEnabled();
					System.out.println("Play icon button is available on playable thumbnail of Movie: "+str_wbMovieName);
					util.takeScreenShot(driver,"NMA_8605","Movie"+i+"_PlayThumb");
				}catch(Exception e){
					System.out.println("X-Path of the element not found");
				}

				if (l == 3)
					l = 4;
				else{
					l = 3;
					if(i<4){
						try{
							//driver.swipe(100, 790, 100, 35, 2500);
							if(Constants.DeviceType.equalsIgnoreCase("Large")){
								SwipeAndScroll.swipeWithConstantValues(driver, 100.0, 800.0, 100.0, 30.0, 3.0);
							}else if(Constants.DeviceType.equalsIgnoreCase("Small")){
								SwipeAndScroll.swipeWithConstantValues(driver, 100.0, 420.0, 100.0, 30.0, 3.0);
							}else{
								throw new AssertionError("Please Check DeviceType in Constants.java file");
							}
							Thread.sleep(Constants.ThreadSleep);
						}catch(Exception e){
							e.getMessage();
						}
					}
					else
					{
						//driver.swipe(100, 670, 100, 35,2800);
						if(Constants.DeviceType.equalsIgnoreCase("Large")){
							SwipeAndScroll.swipeWithConstantValues(driver, 100.0, 680.0, 100.0, 30.0, 3.0);
						}else if(Constants.DeviceType.equalsIgnoreCase("Small")){
							SwipeAndScroll.swipeWithConstantValues(driver, 100.0, 350.0, 100.0, 30.0, 3.0);
						}else{
							throw new AssertionError("Please Check DeviceType in Constants.java file");
						}
						Thread.sleep(Constants.ThreadSleep);
					}
				}

				if(i==20)
					break;
			}
		}

		WebElement wb_Tabs = driver.findElement(By.id(Constants.app_Package+":id/tabs"));
		SwipeAndScroll.scrollOnTab(driver, wb_Tabs, "Clips");
		Thread.sleep(5000);

		WebElement Clip_Tab = driver.findElement(By.id(Constants.app_Package+":id/clips_tab"));
		String str_ClipTab = Clip_Tab.getText();
		Clip_Tab.click();		
		System.out.println("Clicked on TAB : "+str_ClipTab);
		Thread.sleep(5000);

		util.takeScreenShot(driver,"NMA_8605","Clips_MainScreen");

		JSONObject jsnObj_ClipMainURL = mJsonParser.callHttpRequest(Constants.Clips_MainURL,"GET", null);
		Thread.sleep(Constants.ThreadSleep);

		int str_ClipTotal = jsnObj_ClipMainURL.getInt("total");
		JSONArray jsnArray_Hits = jsnObj_ClipMainURL.getJSONArray("hits");

		int l=3;
		for(int i=2; i<jsnArray_Hits.length(); i++){
			wb_ClipName = driver.findElement(By.id(Constants.app_Package+":id/base_grid").xpath("//android.widget.LinearLayout["+l+"]/android.widget.LinearLayout[1]/android.widget.TextView[2]"));
			String str_wbClipName = wb_ClipName.getText();
			System.out.println("str_wbClipNetwork"+str_wbClipName);

			wb_ChannelPlayIcon = driver.findElement(By.id(Constants.app_Package+":id/base_grid").xpath("//android.widget.LinearLayout["+l+"]/android.widget.FrameLayout[1]/android.widget.ImageView[1]"));
			wb_ChannelPlayIcon.isEnabled();
			System.out.println("Play icon button is available on playable thumbnail of Clip: "+str_wbClipName);

			util.takeScreenShot(driver,"NMA_8605","Clip"+i+"_PlayThumb");

			if (l == 3)
				l = 4;
			else{
				l = 3;
				if(i<4){
					try{
						//driver.swipe(100, 790, 100, 35, 2500);
						if(Constants.DeviceType.equalsIgnoreCase("Large")){
							SwipeAndScroll.swipeWithConstantValues(driver, 100.0, 800.0, 100.0, 30.0, 2.5);
						}else if(Constants.DeviceType.equalsIgnoreCase("Small")){
							SwipeAndScroll.swipeWithConstantValues(driver, 100.0, 420.0, 100.0, 30.0, 2.5);
						}else{
							throw new AssertionError("Please Check DeviceType in Constants.java file");
						}
						Thread.sleep(Constants.ThreadSleep);
					}catch(Exception e){
						e.getMessage();
					}
				}
				else
				{
					//driver.swipe(100, 670, 100, 35,2800);
					if(Constants.DeviceType.equalsIgnoreCase("Large")){
						SwipeAndScroll.swipeWithConstantValues(driver, 100.0, 680.0, 100.0, 30.0, 3.0);
					}else if(Constants.DeviceType.equalsIgnoreCase("Small")){
						SwipeAndScroll.swipeWithConstantValues(driver, 100.0, 350.0, 100.0, 30.0, 3.0);
					}else{
						throw new AssertionError("Please Check DeviceType in Constants.java file");
					}
					Thread.sleep(Constants.ThreadSleep);
				}
			}
			if(i==20)
				break;
		}

		WebElement wb_MusicTabs = driver.findElement(By.id(Constants.app_Package+":id/tabs"));
		SwipeAndScroll.scrollOnTab(driver, wb_MusicTabs, "Music");
		Thread.sleep(5000);

		WebElement Music_Tab = driver.findElement(By.id(Constants.app_Package+":id/music_tab"));
		String str_MusicTab = Music_Tab.getText();
		Music_Tab.click();		
		System.out.println("Clicked on TAB : "+str_MusicTab);
		Thread.sleep(5000);

		util.takeScreenShot(driver,"NMA_8605","Music_MainScreen");

		JSONObject jsnObj_MusicMainURL = mJsonParser.callHttpRequest(Constants.Music_MainURL,"GET", null);
		Thread.sleep(Constants.ThreadSleep);

		int str_MusicTotal = jsnObj_MusicMainURL.getInt("total");
		JSONArray jsnArray_MusicHits = jsnObj_MusicMainURL.getJSONArray("hits");

		int m=3;
		for(int i=2; i<jsnArray_MusicHits.length(); i++){

			wb_MusicName = driver.findElement(By.id(Constants.app_Package+":id/base_grid").xpath("//android.widget.LinearLayout["+m+"]/android.widget.LinearLayout[1]/android.widget.TextView[1]"));
			String str_wbMusicName = wb_MusicName.getText();
			System.out.println("str_wbMusicName"+str_wbMusicName);

			wb_ChannelPlayIcon = driver.findElement(By.id(Constants.app_Package+":id/base_grid").xpath("//android.widget.LinearLayout["+m+"]/android.widget.FrameLayout[1]/android.widget.ImageView[1]"));
			wb_ChannelPlayIcon.isEnabled();
			System.out.println("Play icon button is available on playable thumbnail of Music: "+str_wbMusicName);

			util.takeScreenShot(driver,"NMA_8605","Music"+i+"_PlayThumb");

			if (m == 3)
				m = 4;
			else{
				m = 3;
				if(i<4){
					try{
						//driver.swipe(100, 790, 100, 35, 2500);
						if(Constants.DeviceType.equalsIgnoreCase("Large")){
							SwipeAndScroll.swipeWithConstantValues(driver, 100.0, 800.0, 100.0, 30.0, 3.0);
						}else if(Constants.DeviceType.equalsIgnoreCase("Small")){
							SwipeAndScroll.swipeWithConstantValues(driver, 100.0, 420.0, 100.0, 30.0, 3.0);
						}else{
							throw new AssertionError("Please Check DeviceType in Constants.java file");
						}
						Thread.sleep(Constants.ThreadSleep);
					}catch(Exception e){
						e.getMessage();
					}
				}
				else
				{
					//driver.swipe(100, 670, 100, 35,2800);
					if(Constants.DeviceType.equalsIgnoreCase("Large")){
						SwipeAndScroll.swipeWithConstantValues(driver, 100.0, 680.0, 100.0, 30.0, 3.0);
					}else if(Constants.DeviceType.equalsIgnoreCase("Small")){
						SwipeAndScroll.swipeWithConstantValues(driver, 100.0, 350.0, 100.0, 30.0, 3.0);
					}else{
						throw new AssertionError("Please Check DeviceType in Constants.java file");
					}
					Thread.sleep(Constants.ThreadSleep);
				}
			}

			if(i==20)
				break;
		}

		System.out.println("==========================================================================");
		System.out.println("End TestCase  ---> Test_NMA_8605()");
		System.out.println("==========================================================================");
	}

	/*
	 * 1.Launch Application
	 * 2.Select on LIVE tab
	 * 3.Check Each Channel Number, Name, Current ProgramName, Duration and Scroll UP
	 * 4.Check Thumb Image of Channels
	 * 5.Click on FULL GUIDE
	 * 6.Check Full guide (EPG) is launched as a child page
	 * 7.FULL GUIDE is title bar on child page
	 * 8.Check Each Channel Number and Click on it.
	 * 9.Verify Channel Name, Description and ProgramNames in  ProgramLineUp Screen
	 * 10.Click on Back Button to Navigate Previous Page
	 * 11.Click on Back Button to navigate Live MainPage and check data is refreshed
	 */
	@Test
	public void Test_NMA_8606() throws Exception{
		System.out.println("Start TestCase  ---> Test_NMA_8606()");
		System.out.println("==========================================================================");

		WebElement Live_Tab = driver.findElement(By.id(Constants.app_Package+":id/live_guide_tab"));
		String str_LiveTab = Live_Tab.getText();
		Live_Tab.click();		
		System.out.println("Clicked on TAB : "+str_LiveTab);
		util.fileWriting("Clicked on TAB : "+str_LiveTab);
		Thread.sleep(5000);

		util.takeScreenShot(driver,"NMA_8606","Screen1");

		JSONObject jObj_ChannelListing = mJsonParser.callHttpRequest(Constants.Live_MainURL,"GET", null);
		Thread.sleep(Constants.ThreadSleep);
		jsnArray_LiveTileItems = jObj_ChannelListing.getJSONArray("tile_items");

		WebElement wb_ChannelThumb = driver.findElement(By.id(Constants.app_Package+":id/channel_thumb"));
		Assert.assertTrue(wb_ChannelThumb.isEnabled());
		System.out.println("The Thumbn Image is Available");

		for (int i = 0; i < jsnArray_LiveTileItems.length(); i++) {
			WebElement wb_ChannelNumber = driver.findElement(By.id(Constants.app_Package+":id/channel_number"));
			String str_wbChannelNumber = wb_ChannelNumber.getText();
			System.out.println("str_wbChannelNumber: "+str_wbChannelNumber);
			WebElement wb_ChannelName = driver.findElement(By.id(Constants.app_Package+":id/channel_name"));
			String str_wbChannelName = wb_ChannelName.getText();
			System.out.println("str_wbChannelName: "+str_wbChannelName);
			WebElement wb_ProgramName = driver.findElement(By.id(Constants.app_Package+":id/program_name"));
			String str_wbProgramName = wb_ProgramName.getText();
			System.out.println("str_wbProgramName: "+str_wbProgramName);
			WebElement wb_ProgramDuration = driver.findElement(By.id(Constants.app_Package+":id/program_duration"));
			String str_wbProgramDuration = wb_ProgramDuration.getText();
			System.out.println("str_wbProgramDuration: "+str_wbProgramDuration);

			JSONObject json_TileItem = jsnArray_LiveTileItems.getJSONObject(i);

			int str_ChannelPosition = json_TileItem.getInt("display_position");
			//System.out.println("str_ChannelPosition: "+str_ChannelPosition);
			String str_ChannelName = json_TileItem.getString("name");
			//System.out.println("str_ChannelName: "+str_ChannelName);
			String str_ChannelRefID = json_TileItem.getString("ref_id");
			//System.out.println("str_ChannelRefID: "+str_ChannelRefID);

			Assert.assertEquals(str_wbChannelNumber,	str_ChannelPosition);
			Assert.assertEquals(str_wbChannelName,	str_ChannelName);

			util.takeScreenShot(driver,"NMA_8606","Screen2");

			try{
				//driver.swipe(100, 790, 100, 35, 2500);
				if(Constants.DeviceType.equalsIgnoreCase("Large")){
					SwipeAndScroll.swipeWithConstantValues(driver, 100.0, 880.0, 100.0, 30.0, 3.0);
				}else if(Constants.DeviceType.equalsIgnoreCase("Small")){
					SwipeAndScroll.swipeWithConstantValues(driver, 100.0, 443.0, 100.0, 30.0, 2.0);
				}else{
					throw new AssertionError("Please Check DeviceType in Constants.java file");
				}
				Thread.sleep(Constants.ThreadSleep);
			}catch(Exception e){
				e.getMessage();
			}
			if(i>=(jsnArray_LiveTileItems.length()/2)){
				break;
			}
		}

		WebElement wb_FullGuide = driver.findElement(By.id(Constants.app_Package+":id/full_guide"));
		String str_FullGuide = wb_FullGuide.getText();
		wb_FullGuide.click();
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Clicked on "+str_FullGuide+" and Full guide (EPG) is launched as a child page");

		util.takeScreenShot(driver,"NMA_8606","Screen3");

		List<WebElement> list_LiveChannelPosition = driver.findElements(By.id(Constants.app_Package+":id/channel_txt_num"));
		List<WebElement> list_LiveChannelThumb = driver.findElements(By.id(Constants.app_Package+":id/channel_thumb"));

		//for (int i = 0; i < jsnArray_LiveTileItems.length(); i++) {
		for (int i = 0; i < list_LiveChannelPosition.size(); i++) {
			String str_wbChannelPosition = list_LiveChannelPosition.get(i).getText();
			System.out.println("str_wbChannelPosition: "+str_wbChannelPosition);
			util.fileWriting("str_wbChannelPosition: "+str_wbChannelPosition);

			JSONObject json_TileItem = jsnArray_LiveTileItems.getJSONObject(i);
			String str_ChannelName = json_TileItem.getString("name");
			System.out.println("str_ChannelName: "+str_ChannelName);
			util.fileWriting("str_ChannelName: "+str_ChannelName);
			int str_ChannelPosition = json_TileItem.getInt("display_position");
			System.out.println("str_ChannelPosition: "+str_ChannelPosition);
			util.fileWriting("str_ChannelPosition: "+str_ChannelPosition);
			try{
				String str_ChannelDesc = json_TileItem.getString("description");
				System.out.println("str_ChannelDesc: "+str_ChannelDesc);
				util.fileWriting("str_ChannelDesc: "+str_ChannelDesc);
			}catch(JSONException e){
				System.out.println("The Discription Data Not Found");
			}
			String str_ChannelRefID = json_TileItem.getString("ref_id");
			System.out.println("str_ChannelRefID: "+str_ChannelRefID);
			util.fileWriting("str_ChannelRefID: "+str_ChannelRefID);

			Assert.assertEquals(str_wbChannelPosition,	str_ChannelPosition);

			list_LiveChannelThumb.get(i).click();
			System.out.println("Clicked on Live CHANNEL-"+str_wbChannelPosition);
			util.fileWriting("Clicked on Live CHANNEL-"+str_wbChannelPosition);
			Thread.sleep(5000);

			util.takeScreenShot(driver,"NMA_8606","Screen4_"+i);

			WebElement wb_LiveChannelName = driver.findElement(By.id(Constants.app_Package+":id/channel_details_channel_name"));
			String str_wbLiveChannelName = wb_LiveChannelName.getText();
			Assert.assertEquals(str_wbLiveChannelName,	str_ChannelName);

			WebElement wb_LiveChannelDesc = driver.findElement(By.id(Constants.app_Package+":id/channel_details_description"));
			String str_wbLiveChannelDesc = wb_LiveChannelDesc.getText();
			//Assert.assertEquals(str_wbLiveChannelDesc,	str_ChannelDesc);

			long epoch = System.currentTimeMillis() / 1000;

			JSONObject jObj_ChannelEPG = mJsonParser.callHttpRequest(Constants.live_ProgramLineupURL+str_ChannelRefID+ "&ml=en&start-time="+ epoch+ "&timeslice=86400","GET", null);
			Thread.sleep(5000);
			try{
				jArray_Programs = jObj_ChannelEPG.getJSONArray("programs");

				List<WebElement> list_ChannelProgram = driver.findElements(By.id(Constants.app_Package+":id/program_cell_name"));

				//for (int j = 0; j < jArray_Programs.length(); j++) {
				for (int j = 0; j < list_ChannelProgram.size(); j++) {
					String str_wbChannelProgramName = list_ChannelProgram.get(j).getText();
					System.out.println("str_wbChannelProgramName: "+str_wbChannelProgramName);

					JSONObject json_Program = jArray_Programs.getJSONObject(j);
					String str_ChannelProgramName = json_Program.getString("name");
					System.out.println("str_ChannelProgramName: "+str_ChannelProgramName);
					util.fileWriting("str_ChannelProgramName: "+str_ChannelProgramName);


					if(j==3)
						break;

				}
			}catch(Exception e){
				System.out.println(e.getMessage());
			}

			driver.navigate().back();
			Thread.sleep(Constants.ThreadSleep);

			if(i == 5)
				break;
		}

		driver.navigate().back();
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver,"NMA_8606","Screen5");

		System.out.println("==========================================================================");
		System.out.println("End TestCase  ---> Test_NMA_8606()");
		System.out.println("==========================================================================");
	}

	/*
	 * 1.Launch Application
	 * 2.Click on LIVE tab
	 * 3.Swipe on Marketing Tile
	 * 4.Check Full pages is swiped between tabs within Browse
	 */

	@Test
	public void Test_NMA_8607() throws Exception{
		System.out.println("Start TestCase  ---> Test_NMA_8607()");
		System.out.println("==========================================================================");

		WebElement Live_Tab = driver.findElement(By.id(Constants.app_Package+":id/live_guide_tab"));
		String str_LiveTab = Live_Tab.getText();
		Live_Tab.click();		
		System.out.println("Clicked on TAB : "+str_LiveTab);
		util.fileWriting("Clicked on TAB : "+str_LiveTab);
		Thread.sleep(10000);

		util.takeScreenShot(driver,"NMA_8607","Screen1");

		Dimension size = driver.manage().window().getSize(); 
		double startx = (int) (size.width * 0.8); 
		double endx = (int) (size.width * 0.2); 
		double starty = size.height / 2; 
		SwipeAndScroll.swipeWithConstantValues(driver, startx, starty, endx, starty, 1.0);
		System.out.println("Full page is swiped between tabs LIVE to SHOWS");
		Thread.sleep(10000);

		util.takeScreenShot(driver,"NMA_8607","Screen2");

		endx = (int) (size.width * 0.8); 
		startx = (int) (size.width * 0.20); 
		starty = size.height / 2; 
		SwipeAndScroll.swipeWithConstantValues(driver, startx, starty, endx, starty, 1.0);
		System.out.println("Full page is swiped between tabs SHOWS to LIVE");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver,"NMA_8607","Screen3");

		System.out.println("==========================================================================");
		System.out.println("End TestCase  ---> Test_NMA_8607()");
		System.out.println("==========================================================================");

	}

	/*
	 * 1.Launch Application
	 * 2.Select on LIVE tab
	 * 3.Click on FULL GUIDE
	 * 4.Check Full guide (EPG) is launched as a child page
	 * 5.FULL GUIDE is title bar on child page
	 * 6.Click on Back Button to Navigate Live Page
	 */
	@Test
	public void Test_NMA_8608() throws Exception{
		System.out.println("Start TestCase  ---> Test_NMA_8608()");
		System.out.println("==========================================================================");

		WebElement Live_Tab = driver.findElement(By.id(Constants.app_Package+":id/live_guide_tab"));
		String str_LiveTab = Live_Tab.getText();
		Live_Tab.click();		
		System.out.println("Clicked on TAB : "+str_LiveTab);
		util.fileWriting("Clicked on TAB : "+str_LiveTab);
		Thread.sleep(5000);

		util.takeScreenShot(driver,"NMA_8608","Screen1");

		WebElement wb_FullGuide = driver.findElement(By.id(Constants.app_Package+":id/full_guide"));
		String str_FullGuide = wb_FullGuide.getText();
		wb_FullGuide.click();
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Clicked on "+str_FullGuide+" and Full guide (EPG) is launched as a child page");

		util.takeScreenShot(driver,"NMA_8608","Screen2");

		WebElement wb_ActionTitleBar = driver.findElement(By.id("android:id/toolbar").xpath("//android.widget.TextView[1]"));
		String str_ActionTitleBar = wb_ActionTitleBar.getText();
		System.out.println(str_ActionTitleBar+" is titlebar on child page");

		driver.navigate().back();
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Clicked on Devicee Back Button");

		Assert.assertEquals(str_FullGuide, "FULL GUIDE");
		System.out.println("Navigated to LIVE MainPage");

		util.takeScreenShot(driver,"NMA_8608","Screen3");

		System.out.println("==========================================================================");
		System.out.println("End TestCase  ---> Test_NMA_8608()");
		System.out.println("==========================================================================");
	}

	/*
	 * 1.Launch Application
	 * 2.Select on LIVE tab
	 * 3.Check FIlTER is added on New Live Page
	 * 4.Click on FULL GUIDE
	 * 5.Check Full guide (EPG) is launched as a child page
	 * 6.Check Filter does not exist within the Full Guide (EPG) page
	 * 7.Click on Back Button to Navigate Live Page
	 */
	@Test
	public void Test_NMA_8609() throws Exception{
		System.out.println("Start TestCase  ---> Test_NMA_8609()");
		System.out.println("==========================================================================");

		WebElement Live_Tab = driver.findElement(By.id(Constants.app_Package+":id/live_guide_tab"));
		String str_LiveTab = Live_Tab.getText();
		Live_Tab.click();		
		System.out.println("Clicked on TAB : "+str_LiveTab);
		util.fileWriting("Clicked on TAB : "+str_LiveTab);
		Thread.sleep(5000);

		util.takeScreenShot(driver,"NMA_8609","Screen1");

		WebElement wb_FilterBar = driver.findElement(By.id(Constants.app_Package+":id/filter_bar"));
		String str_FilterBar = wb_FilterBar.getText();
		System.out.println(str_FilterBar+ " is added on New Live Page");

		WebElement wb_FullGuide = driver.findElement(By.id(Constants.app_Package+":id/full_guide"));
		String str_FullGuide = wb_FullGuide.getText();
		wb_FullGuide.click();
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Clicked on "+str_FullGuide+" and Full guide (EPG) is launched as a child page");

		util.takeScreenShot(driver,"NMA_8609","Screen2");

		try{
			WebElement wb_FilterBar1 = driver.findElement(By.id(Constants.app_Package+":id/filter_bar"));
			throw new Exception("Filter is exist within the Full Guide (EPG) page");
		}catch(Exception e){
			System.out.println("Filter does not exist within the Full Guide (EPG) page");
		}

		driver.navigate().back();
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Clicked on Devicee Back Button");

		Assert.assertEquals(str_FullGuide, "FULL GUIDE");
		System.out.println("Navigated to LIVE MainPage");

		util.takeScreenShot(driver,"NMA_8609","Screen3");

		System.out.println("==========================================================================");
		System.out.println("End TestCase  ---> Test_NMA_8609()");
		System.out.println("==========================================================================");

	}
	
	@Test
	public void Test_Live_Mute() throws Exception{
		WebElement Live_Tab = driver.findElement(By.id(Constants.app_Package+":id/live_guide_tab"));
		String str_LiveTab = Live_Tab.getText();
		Live_Tab.click();		
		System.out.println("Clicked on TAB : "+str_LiveTab);
		util.fileWriting("Clicked on TAB : "+str_LiveTab);
		Thread.sleep(5000);
		
		WebElement wb_ChannelPlayIcon = driver.findElement(By.id(Constants.app_Package+":id/channel_icon_play"));
		wb_ChannelPlayIcon.click();
		System.out.println("Selected clip to play the video ");
		Thread.sleep(10000);
		
		WebElement wb_PlaybackControls = driver.findElement(By.id(Constants.app_Package+":id/playback_container"));
		SwipeAndScroll.tapOnElement(driver, wb_PlaybackControls);
		
		WebElement wb_BtnMute = driver.findElement(By.id(Constants.app_Package+":id/playback_btn_mute"));
		wb_BtnMute.click();
		System.out.println("Clicked on Mute button ");
		Thread.sleep(5000);
		
		SwipeAndScroll.tapOnElement(driver, wb_PlaybackControls);
		
		wb_BtnMute.click();
		System.out.println("Released Mute button ");
		Thread.sleep(5000);
		
		driver.navigate().back();
		
	}



}
