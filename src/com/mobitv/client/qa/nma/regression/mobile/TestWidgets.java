
package com.mobitv.client.qa.nma.regression.mobile;

import io.appium.java_client.AppiumDriver;

import java.util.ArrayList;

import junit.framework.Assert;

import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.mobitv.client.qa.nma.common.Constants;
import com.mobitv.client.qa.nma.common.DataParser;
import com.mobitv.client.qa.nma.common.DateandTimeClass;
import com.mobitv.client.qa.nma.common.ExecuteShellComand;
import com.mobitv.client.qa.nma.common.SwipeAndScroll;
import com.mobitv.client.qa.nma.common.Util;

public class TestWidgets {

	WebElement wb_ShowName;
	private static Util util = new Util();
	// private static WebDriver driver = null;
	private static AppiumDriver driver = null;
	private DataParser mJsonParser = new DataParser();
	private ExecuteShellComand command = new ExecuteShellComand();
	private DateandTimeClass dt = new DateandTimeClass();
	String currdate = dt.getcurrentDate();

	@AfterClass
	public void callPerl() throws InterruptedException {

		System.out.println("Enable GA logs on device");
		command.enableGALog();
		System.out.println("Capture adb logs");
		command.executeCommand("Widget");
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Call perl script");
		command.executeCommandinvokeperl("Widget.pl");
		Thread.sleep(30000);
		command.kill();
	}

	@BeforeMethod
	public void setUp() throws Exception {

		driver = util.getAndroidDriver(driver);

	}

	@AfterMethod
	public void tearDown() throws Exception {

		driver.quit();
		Thread.sleep(Constants.ThreadSleep);

	}

	@Test
	public void Test_NMA_9255_9256_9257_9300() throws Exception {

		System.out.println("Start TestCase  ---> Test_NMA_9255_9256_9257_9300()");
		System.out.println("==========================================================================");

		// Verifying Widget Use Case 1: Update Frequency NewsClips

		System.out.println("Verifying Widget Use Case 1: Update Frequency NewsClips");
		System.out.println("---------------------------------------------------------------");
		ArrayList<String> list_NewsClips = new ArrayList<>();

		WebElement Featured_Tab = driver.findElement(By.id(Constants.app_Package + ":id/home_tab"));
		String str_FeaturedTab = Featured_Tab.getText();
		Featured_Tab.click();
		Thread.sleep(Constants.ThreadSleep);

		driver.navigate().back();
		driver.navigate().back();
		command.executeAdbKeyEvent("KEYCODE_HOME");
		Thread.sleep(Constants.ThreadSleep);

		JSONObject jsnObj_NewsClips = mJsonParser.callHttpRequest(Constants.widget_NewsClips, "GET", null);
		Thread.sleep(Constants.ThreadSleep);

		JSONArray jsnArray_TileItems = jsnObj_NewsClips.getJSONArray("tile_items");

		for (int j = 0; j < jsnArray_TileItems.length(); j++) {
			JSONObject jsnObj_TileItem = jsnArray_TileItems.getJSONObject(j);
			String str_RefID = jsnObj_TileItem.getString("ref_id");
			String str_Name = jsnObj_TileItem.getString("name");
			// System.out.println("RefID : " + str_RefID + ", Name: " +
			// str_Name);
			list_NewsClips.add(str_Name);
		}
		// System.out.println("List Of NewClips: " + list_NewsClips);

		for (int k = 0; k < 2; k++) {
			WebElement wb_WidgetTileImage = driver.findElement(By.id(Constants.app_Package + ":id/widget_tile_image"));
			WebElement wb_WidgetHeading = driver.findElement(By.id(Constants.app_Package + ":id/heading"));

			String str_WidgetHeading = wb_WidgetHeading.getText();

			for (String str : list_NewsClips) {
				if (str.trim().contains(str_WidgetHeading)) {

					wb_WidgetTileImage.click();
					System.out.println(str_WidgetHeading + ": Clicked on NewsClip");
					Thread.sleep(15000);

					WebElement wb_PlaybackControls = driver.findElement(By.id(Constants.app_Package + ":id/playback_container"));
					SwipeAndScroll.tapOnElement(driver, wb_PlaybackControls);
					System.out.println("The Media Player Started and Playing News Clip");

					WebElement wb_NewsClipName = driver.findElement(By.id(Constants.app_Package + ":id/playback_txt_title2"));
					String str_NewsClipName = wb_NewsClipName.getText();

					Assert.assertEquals(str_NewsClipName, str_WidgetHeading);

					driver.navigate().back();
					Thread.sleep(Constants.ThreadSleep);

					System.out.println("Navigated back to News Clip Details Page.");

					driver.navigate().back();
					Thread.sleep(Constants.ThreadSleep);
					System.out.println("Navigated to Home Screen.");
					System.out.println("===============================");
					break;
				}
			}
			WebElement wb_WidgetNextTile = driver.findElement(By.id(Constants.app_Package + ":id/widget_next_tile"));
			wb_WidgetNextTile.click();
			Thread.sleep(Constants.ThreadSleep);

		}

		// Verifying Widget Use Case 1: Update Frequency Live

		System.out.println("Verifying Widget Use Case 1: Update Frequency Live");
		System.out.println("---------------------------------------------------------------");
		ArrayList<String> list_LiveChannels = new ArrayList<>();

		JSONObject jsnObj_LiveChannel = mJsonParser.callHttpRequest(Constants.widget_Live, "GET", null);
		Thread.sleep(Constants.ThreadSleep);

		JSONArray jsnArray_LiveChannelTileItems = jsnObj_LiveChannel.getJSONArray("tile_items");

		for (int j = 0; j < jsnArray_LiveChannelTileItems.length(); j++) {
			JSONObject jsnObj_TileItem = jsnArray_LiveChannelTileItems.getJSONObject(j);
			String str_RefID = jsnObj_TileItem.getString("ref_id");
			String str_Name = jsnObj_TileItem.getString("name");
			// System.out.println("RefID : " + str_RefID + ", Name: " +
			// str_Name);
			list_LiveChannels.add(str_Name);
		}
		// System.out.println("List Of Live Channels: " + list_LiveChannels);

		for (int k = 0; k < 2; k++) {
			WebElement wb_WidgetTileImage = driver.findElement(By.id(Constants.app_Package + ":id/widget_tile_image"));
			WebElement wb_WidgetHeading = driver.findElement(By.id(Constants.app_Package + ":id/heading"));

			String str_WidgetHeading = wb_WidgetHeading.getText();

			for (String str : list_LiveChannels) {
				if (str.trim().contains(str_WidgetHeading)) {

					wb_WidgetTileImage.click();
					System.out.println(str_WidgetHeading + ": Clicked on Live Channel");
					Thread.sleep(15000);

					WebElement wb_ChannelDetailsChannelName = driver.findElement(By.id("android:id/toolbar").xpath("//android.widget.TextView[1]"));
					String str_ChannelDetailsChannelName = wb_ChannelDetailsChannelName.getText();
					Assert.assertEquals(str_ChannelDetailsChannelName, str_WidgetHeading);
					System.out.println(str_ChannelDetailsChannelName + " channel details page is landed");

					driver.navigate().back();
					Thread.sleep(Constants.ThreadSleep);
					System.out.println("Navigated to Home Screen.");
					System.out.println("===============================");
					break;
				}
			}
			WebElement wb_WidgetNextTile = driver.findElement(By.id(Constants.app_Package + ":id/widget_next_tile"));
			wb_WidgetNextTile.click();
			Thread.sleep(Constants.ThreadSleep);
		}

		// Verifying Widget Use Case 1: Update Frequency Marketing Tiles

		System.out.println("Verifying Widget Use Case 1: Update Frequency Marketing Tiles");
		System.out.println("---------------------------------------------------------------");
		ArrayList<String> list_MrktTileList = new ArrayList<>();

		JSONObject jsnObj_MarketingTiles = mJsonParser.callHttpRequest(Constants.widget_MarketingTiles, "GET", null);
		Thread.sleep(Constants.ThreadSleep);

		JSONArray jsnArray_MrktTileItems = jsnObj_MarketingTiles.getJSONArray("tile_items");

		for (int j = 0; j < jsnArray_MrktTileItems.length(); j++) {
			JSONObject jsnObj_TileItem = jsnArray_MrktTileItems.getJSONObject(j);
			String str_RefID = jsnObj_TileItem.getString("ref_id");
			String str_Name = jsnObj_TileItem.getString("name");
			// System.out.println("RefID : " + str_RefID + ", Name: " +
			// str_Name);
			list_MrktTileList.add(str_Name);
		}
		// System.out.println("List Of MarketingTiles: " + list_MrktTileList);

		for (int k = 0; k < 2; k++) {
			WebElement wb_WidgetTileImage = driver.findElement(By.id(Constants.app_Package + ":id/widget_tile_image"));
			WebElement wb_WidgetHeading = driver.findElement(By.id(Constants.app_Package + ":id/heading"));

			String str_WidgetHeading = wb_WidgetHeading.getText();

			for (String str : list_MrktTileList) {
				if (str.trim().contains(str_WidgetHeading)) {

					wb_WidgetTileImage.click();
					System.out.println(str_WidgetHeading + ": Clicked on Marketing Tile");
					Thread.sleep(15000);

					WebElement wb_ChannelDetailsChannelName = driver.findElement(By.id("android:id/toolbar").xpath("//android.widget.TextView[1]"));
					String str_ChannelDetailsChannelName = wb_ChannelDetailsChannelName.getText();
					Assert.assertEquals(str_ChannelDetailsChannelName, str_WidgetHeading);
					System.out.println(str_ChannelDetailsChannelName + " Marketing Tile page is landed");

					driver.navigate().back();
					Thread.sleep(Constants.ThreadSleep);
					System.out.println("Navigated to Home Screen.");
					System.out.println("===============================");
					break;
				}
			}
			WebElement wb_WidgetNextTile = driver.findElement(By.id(Constants.app_Package + ":id/widget_next_tile"));
			wb_WidgetNextTile.click();
			Thread.sleep(Constants.ThreadSleep);

		}

		// Refreshing data

		WebElement wb_WidgetHeading = driver.findElement(By.id(Constants.app_Package + ":id/heading"));
		String str_WidgetHeading = wb_WidgetHeading.getText();

		System.out.println("Tile Header Before Clicking Refresh Button :" + str_WidgetHeading);

		WebElement wb_WidgetRefreshData = driver.findElement(By.id(Constants.app_Package + ":id/widget_refresh_data"));
		wb_WidgetRefreshData.click();
		Thread.sleep(Constants.ThreadSleep);

		WebElement wb_WidgetHeading1 = driver.findElement(By.id(Constants.app_Package + ":id/heading"));
		String str_WidgetHeading1 = wb_WidgetHeading1.getText();
		System.out.println("Tile Header After Clicking Refresh Button :" + str_WidgetHeading1);

		System.out.println("==========================================================================");
		System.out.println("End TestCase  ---> Test_NMA_9255_9256_9257_9300()");
		System.out.println("==========================================================================");
	}
}
