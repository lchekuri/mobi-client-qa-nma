
package com.mobitv.client.qa.nma.regression.mobile;

import io.appium.java_client.AppiumDriver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.mobitv.client.qa.nma.common.Constants;
import com.mobitv.client.qa.nma.common.DataParser;
import com.mobitv.client.qa.nma.common.Util;

public class TestASignin {

	WebElement wb_ClipName, wb_ClipNetwork;
	private static Util util = new Util();
	private static AppiumDriver driver = null;

	@BeforeTest
	public void setUp() throws Exception {

		driver = util.getAndroidDriver(driver);
	}

	@AfterTest
	public void tearDown() throws Exception {

		driver.quit();

	}

	@Test
	public void Test_FBRemoval_NMA9647() throws Exception {

		// Verify EULA text

		WebElement eula = driver.findElement(By.id(Constants.app_Package + ":id/eula_textview"));
		Assert.assertTrue(eula.getText().contains("PLEASE READ THESE TERMS AND CONDITIONS CAREFULLY"));
		WebElement eula_btn = driver.findElement(By.id(Constants.app_Package + ":id/accept_eula_button"));
		Assert.assertEquals(eula_btn.getText(), "Accept");
		eula_btn.click();
		System.out.println("Accepted EULA");

		System.out.println("Start Test_FBRemoval");
		System.out.println("==========================================================================");

		Assert.assertTrue(driver.findElements(By.id(Constants.app_Package + ":id/fb_sign_in_btn")).size() == 0);
		System.out.println("FB Link is not present on Sigin");

	}
}
