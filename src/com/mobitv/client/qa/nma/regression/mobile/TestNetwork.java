package com.mobitv.client.qa.nma.regression.mobile;

import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.mobitv.client.qa.nma.common.Constants;
import com.mobitv.client.qa.nma.common.DataParser;
import com.mobitv.client.qa.nma.common.DateandTimeClass;
import com.mobitv.client.qa.nma.common.ExecuteShellComand;
import com.mobitv.client.qa.nma.common.SwipeAndScroll;
import com.mobitv.client.qa.nma.common.Util;

public class TestNetwork {
	WebElement wb_NetworkName;
	private static Util util = new Util();
	private static WebDriver driver = null;
	private DataParser mJsonParser = new DataParser();
	private ExecuteShellComand command = new ExecuteShellComand();
	private DateandTimeClass dt = new DateandTimeClass();
	String currdate = dt.getcurrentDate();

	@AfterClass
	public void callPerl() throws InterruptedException {

		System.out.println("Enable GA logs on device");
		command.enableGALog();
		System.out.println("Capture adb logs");
		command.executeCommand("Networks");
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Call perl script");
		command.executeCommandinvokeperl("Networks.pl");
		Thread.sleep(30000);
		command.kill();
	}
	@BeforeMethod
	public void setUp() throws Exception {		
		driver = util.getAndroidDriver(driver);
		
	}

	@AfterMethod
	public void tearDown() throws Exception {
		driver.quit();
		Thread.sleep(Constants.ThreadSleep);
		
		
	}

	@Test
	public void Test_Network() throws Exception{

		System.out.println("Start Test_Network");
		util.fileWriting("Start Test_Network");
		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");

		WebElement wb_Tabs = driver.findElement(By.id(Constants.app_Package+":id/tabs"));
		SwipeAndScroll.scrollOnTab(driver, wb_Tabs, "Networks");
		Thread.sleep(5000);

		WebElement Networks_Tab = driver.findElement(By.id(Constants.app_Package+":id/networks_tab"));
		String str_Networks_Tab = Networks_Tab.getText();
		Networks_Tab.click();		
		System.out.println("Clicked on TAB : "+str_Networks_Tab);
		util.fileWriting("Clicked on TAB : "+str_Networks_Tab);
		Thread.sleep(5000);

		util.takeScreenShot(driver,"Test_Networks","Networks_MainScreen");

		JSONObject jsnObj_NetworkMainURL = mJsonParser.callHttpRequest(Constants.Network_MainURL,"GET", null);
		Thread.sleep(Constants.ThreadSleep);

		int str_NetworkTotal = jsnObj_NetworkMainURL.getInt("total");
		System.out.println("str_NetworkTotal : "+str_NetworkTotal);
		util.fileWriting("str_NetworkTotal : "+str_NetworkTotal);
		JSONArray jsnArray_Navigator = jsnObj_NetworkMainURL.getJSONArray("navigators");
		JSONArray jsnArray_Entries = jsnArray_Navigator.getJSONObject(0).getJSONArray("entries");

		int l=3;
		for(int i=2; i<jsnArray_Entries.length(); i++){
			System.out.println("**********"+i+"**********");
			util.fileWriting("**********"+i+"**********");
			JSONObject json_NetworksInfo = jsnArray_Entries.getJSONObject(i);
			String str_NetworkName = json_NetworksInfo.getString("name");
			System.out.println("str_NetworkName : "+str_NetworkName);
			util.fileWriting("str_NetworkName : "+str_NetworkName);

			
			if(l==3){
				wb_NetworkName = driver.findElement(By.id(Constants.app_Package+":id/networks_grid").xpath("//android.widget.LinearLayout["+l+"]").id(Constants.app_Package+":id/networks_cell_area_txt").id(Constants.app_Package+":id/networks_cell_txt_title"));
			}else{
				wb_NetworkName = driver.findElement(By.id(Constants.app_Package+":id/networks_grid").xpath("//android.widget.LinearLayout["+l+"]/android.widget.LinearLayout[1]/android.widget.TextView[1]"));
			}
			String str_wbNetworkName = wb_NetworkName.getText();

			if(str_NetworkName !=null){
				if(str_NetworkName.equalsIgnoreCase(str_wbNetworkName)){

					util.takeScreenShot(driver,"Test_Networks","Network_"+i);

					wb_NetworkName.click();
					System.out.println("Clicked on NETWORK : "+str_wbNetworkName);
					util.fileWriting("Clicked on NETWORK : "+str_wbNetworkName);
					Thread.sleep(Constants.ThreadSleep);		

					JSONObject jsnObj_NetworkChildPageURL = mJsonParser.callHttpRequest(Constants.Network_ChildPageURL,"GET", null);
					Thread.sleep(Constants.ThreadSleep);

					int int_NetworkChildTotal = jsnObj_NetworkChildPageURL.getInt("total");
					//System.out.println("str_ShowTotal : "+int_NetworkChildTotal);
					if(int_NetworkChildTotal >0 && i==1){
						JSONArray jsnArray_NetworkClipsHits = jsnObj_NetworkChildPageURL.getJSONArray("hits");

						List<WebElement> list_NetworkClips = driver.findElements(By.id(Constants.app_Package+":id/details_list").id(Constants.app_Package+":id/cell_txt_title"));

						for(int j=0; j<jsnArray_NetworkClipsHits.length(); j++){

							JSONObject json_NetworkClipInfo = jsnArray_NetworkClipsHits.getJSONObject(j);
							JSONObject json_ShowClipResult = json_NetworkClipInfo.getJSONObject("result");
							JSONObject json_ShowClipVOD = json_ShowClipResult.getJSONObject("vod");
							String str_ShowClipID = json_ShowClipVOD.getString("id");
							//System.out.println("str_ShowClipID : "+str_ShowClipID);
							String str_ShowClipName = json_ShowClipVOD.getString("name");
							System.out.println("str_ShowClipName : "+str_ShowClipName);
							util.fileWriting("str_ShowClipName : "+str_ShowClipName);
							try{
								String str_ShowClipDescription = json_ShowClipVOD.getString("description");
								System.out.println("str_ShowClipDescription : "+str_ShowClipDescription);
								util.fileWriting("str_ShowClipDescription : "+str_ShowClipDescription);
							}catch(JSONException e){
								System.out.println("The Discription Data Not Found");
							}
							String str_wbShowClipName = list_NetworkClips.get(j+1).getText();
							System.out.println("wb_ShowClipName : "+str_wbShowClipName);
							util.fileWriting("wb_ShowClipName : "+str_wbShowClipName);

							util.takeScreenShot(driver,"Test_Networks","Network_"+i+"_VideoClips");

							if(str_ShowClipName.equalsIgnoreCase(str_wbShowClipName)){
								list_NetworkClips.get(j+1).click();
								Thread.sleep(Constants.ThreadSleep);
								System.out.println("Clicked on CLIP: "+str_wbShowClipName);
								util.fileWriting("Clicked on CLIP: "+str_wbShowClipName);
								WebElement wb_ClipDesc = driver.findElement(By.id(Constants.app_Package+":id/details_txt_desc"));
								String str_wbClipDesc = wb_ClipDesc.getText();
								//Assert.assertEquals(str_ShowClipDescription, str_wbClipDesc);
								util.takeScreenShot(driver,"Test_Networks","Network_"+i+"_VideoClip"+j+"-Deatils");
							}

							driver.navigate().back();
							Thread.sleep(Constants.ThreadSleep);

							if(j==3)
								break;
						}
					}

					driver.navigate().back();
					Thread.sleep(Constants.ThreadSleep);
				}
			}else{
				System.out.println("Music Data is not available");
			}

			if (l == 3)
				l = 4;
			else{
				l = 3;
				if(i<4){
					try{
						//driver.swipe(100, 790, 100, 35, 2500);
						if(Constants.DeviceType.equalsIgnoreCase("Large")){
							SwipeAndScroll.swipeWithConstantValues(driver, 100.0, 800.0, 100.0, 30.0, 3.0);
						}else if(Constants.DeviceType.equalsIgnoreCase("Small")){
							SwipeAndScroll.swipeWithConstantValues(driver, 100.0, 420.0, 100.0, 30.0, 3.0);
						}else{
							throw new AssertionError("Please Check DeviceType in Constants.java file");
						}
						Thread.sleep(Constants.ThreadSleep);
					}catch(Exception e){
						e.getMessage();
					}
				}
				else
				{
					//driver.swipe(100, 670, 100, 35,2800);
					if(Constants.DeviceType.equalsIgnoreCase("Large")){
						SwipeAndScroll.swipeWithConstantValues(driver, 100.0, 680.0, 100.0, 30.0, 3.0);
					}else if(Constants.DeviceType.equalsIgnoreCase("Small")){
						SwipeAndScroll.swipeWithConstantValues(driver, 100.0, 350.0, 100.0, 30.0, 3.0);
					}else{
						throw new AssertionError("Please Check DeviceType in Constants.java file");
					}
					Thread.sleep(Constants.ThreadSleep);
				}
			}

			int length=(jsnArray_Entries.length()/4);
			if(i==20)
				break;
		}

		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");
		System.out.println("End Test_Network");
		util.fileWriting("End Test_Network");
		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");
	}
}
