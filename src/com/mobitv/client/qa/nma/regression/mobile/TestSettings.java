
package com.mobitv.client.qa.nma.regression.mobile;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.mobitv.client.qa.nma.common.Constants;
import com.mobitv.client.qa.nma.common.DateandTimeClass;
import com.mobitv.client.qa.nma.common.ExecuteShellComand;
import com.mobitv.client.qa.nma.common.SwipeAndScroll;
import com.mobitv.client.qa.nma.common.Util;

public class TestSettings {

	private static Util util = new Util();
	private static WebDriver driver = null;
	private ExecuteShellComand command = new ExecuteShellComand();
	private DateandTimeClass dt = new DateandTimeClass();
	String currdate = dt.getcurrentDate();

	@AfterClass
	public void callPerl() throws InterruptedException {

		System.out.println("Enable GA logs on device");
		command.enableGALog();
		System.out.println("Capture adb logs");
		command.executeCommand("Settings");
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Call perl script");
		command.executeCommandinvokeperl("Settings.pl");
		Thread.sleep(30000);
		command.kill();
	}

	@BeforeMethod
	public void setUp() throws Exception {

		driver = util.getAndroidDriver(driver);

	}

	@AfterMethod
	public void tearDown() throws Exception {

		driver.quit();
		Thread.sleep(Constants.ThreadSleep);

	}

	// NMA-Android-2088/2089/2090/2091
	@Test
	public void Test_Settings() throws Exception {

		// System.out.println("Start TestCases  ---> TestCaseIDs: NMA-Android-2088/89/90/91 ---- JIRA ID: NMA-5793/94/95/96");
		System.out.println("Start Test_Settings");
		util.fileWriting("Start Test_Settings");
		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");

		WebElement Menu = driver.findElement(By.name("Open navigation drawer"));
		util.takeScreenShot(driver, "Test_Settings", "Screen1");
		Menu.click();
		Thread.sleep(Constants.ThreadSleep);
		util.takeScreenShot(driver, "Test_Settings", "Screen2");

		WebElement Menu_Settings = driver.findElement(By.id(Constants.app_Package + ":id/cell_edit_screen").name("Settings"));
		Menu_Settings.click();
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver, "Test_Settings", "Screen3");

		WebElement Setting_About = driver.findElement(By.id(Constants.app_Package + ":id/about_tab"));
		Setting_About.click();
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver, "Test_Settings", "Screen4");

		WebElement Setting_AppVersionTitle = driver.findElement(By.id(Constants.app_Package + ":id/app_version_title"));
		WebElement Setting_AppVersionValue = driver.findElement(By.id(Constants.app_Package + ":id/app_version_value"));
		System.out.println(Setting_AppVersionTitle.getText() + " : " + Setting_AppVersionValue.getText());

		WebElement Setting_UserIDTitle = driver.findElement(By.id(Constants.app_Package + ":id/user_id_title"));
		WebElement Setting_UserIDValue = driver.findElement(By.id(Constants.app_Package + ":id/user_id_value"));
		System.out.println(Setting_UserIDTitle.getText() + " : " + Setting_UserIDValue.getText());

		WebElement Setting_DeviceIDTitle = driver.findElement(By.id(Constants.app_Package + ":id/device_id_title"));
		WebElement Setting_DeviceIDValue = driver.findElement(By.id(Constants.app_Package + ":id/device_id_value"));
		System.out.println(Setting_DeviceIDTitle.getText() + " : " + Setting_DeviceIDValue.getText());

		WebElement Setting_ProductTitle = driver.findElement(By.id(Constants.app_Package + ":id/product_title"));
		WebElement Setting_ProductValue = driver.findElement(By.id(Constants.app_Package + ":id/product_value"));
		System.out.println(Setting_ProductTitle.getText() + " : " + Setting_ProductValue.getText());

		SwipeAndScroll.swipeWithConstantValues(driver, 100.0, 695.0, 100.0, 30.0, 3.0);

		WebElement Setting_Terms = driver.findElement(By.id(Constants.app_Package + ":id/terms_tab"));
		Setting_Terms.click();
		util.takeScreenShot(driver, "Test_Settings", "Screen5");

		WebElement Setting_TermsPolicyTitle = driver.findElement(By.id(Constants.app_Package + ":id/privacy_policy_title"));
		WebElement Setting_TermsAttrValue = driver.findElement(By.id(Constants.app_Package + ":id/attribution_value"));
		System.out.println(Setting_TermsPolicyTitle.getText());
		System.out.println(Setting_TermsAttrValue.getText());

		SwipeAndScroll.swipeWithConstantValues(driver, 100.0, 695.0, 100.0, 30.0, 3.0);

		util.takeScreenShot(driver, "Test_Settings", "Screen6");

		// System.out.println("End   TestCases  ---> TestCaseIDs: NMA-Android-2088/89/90/91 ---- JIRA ID: NMA-5793/94/95/96");
		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");
		System.out.println("End Test_Settings");
		util.fileWriting("End Test_Settings");
		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");
	}

	public void Test_NMA_2118() throws Exception, InterruptedException {

		System.out.println("Start TestCases  ---> TestCaseIDs: NMA_2118 ---- JIRA ID: NMA-6116");
		util.fileWriting("Start TestCases  ---> TestCaseIDs: NMA_2118 ---- JIRA ID: NMA-6116");
		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");

		WebElement Menu = driver.findElement(By.name("Open navigation drawer"));
		util.takeScreenShot(driver, "NMA_2118", "Screen1");
		Menu.click();
		Thread.sleep(Constants.ThreadSleep);
		util.takeScreenShot(driver, "NMA_2118", "Screen2");

		WebElement Menu_Shop = driver.findElement(By.id(Constants.app_Package + ":id/cell_edit_screen").name("Settings"));
		Menu_Shop.click();
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver, "NMA_2118", "Screen3");

		WebElement Setting_Terms = driver.findElement(By.id(Constants.app_Package + ":id/help_tab"));
		Setting_Terms.click();
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver, "NMA_2118", "Screen4");

		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");
		System.out.println("End TestCases  ---> TestCaseIDs: NMA_2118 ---- JIRA ID: NMA-6116");
		util.fileWriting("End TestCases  ---> TestCaseIDs: NMA_2118 ---- JIRA ID: NMA-6116");
		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");

	}

}
