package com.mobitv.client.qa.nma.regression.mobile;

import io.appium.java_client.AppiumDriver;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.mobitv.client.qa.nma.common.Constants;
import com.mobitv.client.qa.nma.common.DataParser;
import com.mobitv.client.qa.nma.common.DateandTimeClass;
import com.mobitv.client.qa.nma.common.ExecuteShellComand;
import com.mobitv.client.qa.nma.common.SwipeAndScroll;
import com.mobitv.client.qa.nma.common.Util;

public class TestSearch {
	private static Util util = new Util();
	//private static WebDriver driver = null;
	private static AppiumDriver driver = null;
	private DataParser mJsonParser = new DataParser();
	private ExecuteShellComand command = new ExecuteShellComand();
	String str_SearchClearHistory;
	WebElement wb_SearchSrcTxt;
	String str_SearchSrcTxt;
	private DateandTimeClass dt = new DateandTimeClass();
	String currdate = dt.getcurrentDate();

	@AfterClass
	public void callPerl() throws InterruptedException {

		System.out.println("Enable GA logs on device");
		command.enableGALog();
		System.out.println("Capture adb logs");
		command.executeCommand("Search");
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Call perl script");
		command.executeCommandinvokeperl("Search.pl");
		Thread.sleep(30000);
		command.kill();
	} 
	@BeforeMethod
	public void setUp() throws Exception {		
		driver = util.getAndroidDriver(driver);
		
	}

	@AfterMethod
	public void tearDown() throws Exception {
		driver.quit();
		Thread.sleep(Constants.ThreadSleep);
		
	}

	/*
	 1.Launch Application
	 2.Select FEATURED tab on HomeScreen
	 3.Click on SEARCH action icon in FEATURED Mainscreen
	 4.Check Input cursor and embedded 'Search' text in search field when blank or cleared in Search screen.
	 5.Check native soft KEYBOARD displayed in Search screen.
	 6.Enter a keyword in the SEARCH field
	 7.Select CLEAR button.
	 8.Check keyword in the search field is removed and displayed blank search field and blank suggestions field
	 8.Click device BACK button to hide keyboard
	 9.Click application BACK button to navigate previous page.
	 10.Select any one of the Tile in FEATURED MainScreen to navigate ChildPage.
	 11.Check SEARCH action icon is displayed.
	 12.Click BACK button to navigate previous page.
	 13.Repeat steps 2-10 on Tabs LIVE,SHOWS, MOVIES, MUSIC, CLIPS and NETWORKS.
	 14.Click on MENU in HomeScreen
	 15.Select SHOP menu
	 16.Check SEARCH action is available.
	 17.Click BACK button to navigate previous page.
	 */
	@Test
	public void Test_NMA_6644() throws Exception{
		System.out.println("Start Test_NMA_6644");
		util.fileWriting("Start Test_NMA_6644");
		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");

		WebElement Featured_Tab = driver.findElement(By.id(Constants.app_Package+":id/home_tab"));
		String str_FeaturedTab = Featured_Tab.getText();
		Featured_Tab.click();		
		System.out.println("Clicked on TAB : "+str_FeaturedTab);
		Thread.sleep(5000);

		util.takeScreenShot(driver,"NMA_6644","Screen1");

		WebElement wb_ActionSearch = driver.findElement(By.id(Constants.app_Package+":id/action_search"));
		wb_ActionSearch.click();
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Clicked on SEARCH action icon");

		util.takeScreenShot(driver,"NMA_6644","Screen2");

		wb_SearchSrcTxt = driver.findElement(By.id("android:id/search_src_text"));
		str_SearchSrcTxt = wb_SearchSrcTxt.getText();
		if(str_SearchSrcTxt.equalsIgnoreCase("Search")){
			System.out.println("Displayed input cursor and embedded 'Search' text in search field when blank or cleared");
			System.out.println("Displayed Native Soft Keyboard");
		}else{
			System.out.println("The SEARCH field is not empty");
		}

		WebElement wb_SearchSrcTxt = driver.findElement(By.id("android:id/search_src_text"));
		wb_SearchSrcTxt.sendKeys("Vivo");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver,"NMA_6644","Screen3");

		WebElement wb_SearchCloseBtn = driver.findElement(By.id("android:id/search_close_btn"));
		wb_SearchCloseBtn.click();		
		System.out.println("Removed the entered keyword in the search field and displayed blank search field");

		util.takeScreenShot(driver,"NMA_6644","Screen4");

		driver.navigate().back();
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver,"NMA_6644","Screen6");

		/*WebElement wb_UpButton = driver.findElement(By.id("android:id/up"));
		wb_UpButton.click();
		Thread.sleep(Constants.ThreadSleep);*/
		driver.navigate().back();
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Clicked on BACK button");

		util.takeScreenShot(driver,"NMA_6644","Screen7");

		WebElement featured_CellTxtLeftTitle = driver.findElement(By.id(Constants.app_Package+":id/left_tile").xpath("//android.widget.LinearLayout[1]").id(Constants.app_Package+":id/cell_area_txt").id(Constants.app_Package+":id/cell_txt_title"));
		String str_CellTxtLeftTitle = featured_CellTxtLeftTitle.getText();
		Assert.assertNotNull("Left Tile Channel Name should not be Null in Featured MainScreen", str_CellTxtLeftTitle);
		featured_CellTxtLeftTitle.click();
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver,"NMA_6644","Screen7");

		wb_ActionSearch.isEnabled();
		System.out.println("Search action button is available on child page");

		driver.navigate().back();
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver,"NMA_6644","Screen8");

		WebElement Live_Tab = driver.findElement(By.id(Constants.app_Package+":id/live_guide_tab"));
		String str_LiveTab = Live_Tab.getText();
		Live_Tab.click();	
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Clicked on TAB : "+str_LiveTab);

		util.takeScreenShot(driver,"NMA_6644","Screen9");

		wb_ActionSearch = driver.findElement(By.id(Constants.app_Package+":id/action_search"));
		wb_ActionSearch.click();
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Clicked on SEARCH action icon");

		util.takeScreenShot(driver,"NMA_6644","Screen10");

		wb_SearchSrcTxt = driver.findElement(By.id("android:id/search_src_text"));
		str_SearchSrcTxt = wb_SearchSrcTxt.getText();
		if(str_SearchSrcTxt.equalsIgnoreCase("Search")){
			System.out.println("Displayed input cursor and embedded 'Search' text in search field when blank or cleared");
			System.out.println("Displayed Native Soft Keyboard");
		}else{
			System.out.println("the SEARCH fiels is not empty");
		}

		wb_SearchSrcTxt = driver.findElement(By.id("android:id/search_src_text"));
		wb_SearchSrcTxt.sendKeys("Vivo");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver,"NMA_6644","Screen11");

		wb_SearchCloseBtn = driver.findElement(By.id("android:id/search_close_btn"));
		wb_SearchCloseBtn.click();	
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Removed the entered keyword in the search field and displayed blank search field");

		util.takeScreenShot(driver,"NMA_6644","Screen12");

		driver.navigate().back();
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver,"NMA_6644","Screen13");

		/*wb_UpButton = driver.findElement(By.id("android:id/up"));
		wb_UpButton.click();
		Thread.sleep(Constants.ThreadSleep);*/
		driver.navigate().back();
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Clicked on BACK button");

		util.takeScreenShot(driver,"NMA_6644","Screen14");

		WebElement Shows_Tab = driver.findElement(By.id(Constants.app_Package+":id/shows_tab"));
		String str_ShowsTab = Shows_Tab.getText();
		Shows_Tab.click();	
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Clicked on TAB : "+str_ShowsTab);

		util.takeScreenShot(driver,"NMA_6644","Screen15");

		wb_ActionSearch = driver.findElement(By.id(Constants.app_Package+":id/action_search"));
		wb_ActionSearch.click();
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Clicked on SEARCH action icon");

		util.takeScreenShot(driver,"NMA_6644","Screen16");

		wb_SearchSrcTxt = driver.findElement(By.id("android:id/search_src_text"));
		str_SearchSrcTxt = wb_SearchSrcTxt.getText();
		if(str_SearchSrcTxt.equalsIgnoreCase("Search")){
			System.out.println("Displayed input cursor and embedded 'Search' text in search field when blank or cleared");
			System.out.println("Displayed Native Soft Keyboard");
		}else{
			System.out.println("The SEARCH field is not empty");
		}

		wb_SearchSrcTxt = driver.findElement(By.id("android:id/search_src_text"));
		wb_SearchSrcTxt.sendKeys("Vivo");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver,"NMA_6644","Screen17");

		wb_SearchCloseBtn = driver.findElement(By.id("android:id/search_close_btn"));
		wb_SearchCloseBtn.click();	
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Removed the entered keyword in the search field and displayed blank search field");

		util.takeScreenShot(driver,"NMA_6644","Screen18");

		driver.navigate().back();
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver,"NMA_6644","Screen19");

		/*wb_UpButton = driver.findElement(By.id("android:id/up"));
		wb_UpButton.click();
		Thread.sleep(Constants.ThreadSleep);*/
		driver.navigate().back();
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Clicked on BACK button");

		util.takeScreenShot(driver,"NMA_6644","Screen20");

		WebElement Movies_Tab = driver.findElement(By.id(Constants.app_Package+":id/movies_tab"));
		String str_MoviesTab = Movies_Tab.getText();
		Movies_Tab.click();		
		System.out.println("Clicked on TAB : "+str_MoviesTab);
		Thread.sleep(5000);

		util.takeScreenShot(driver,"NMA_6644","Screen21");

		wb_ActionSearch = driver.findElement(By.id(Constants.app_Package+":id/action_search"));
		wb_ActionSearch.click();
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Clicked on SEARCH action icon");

		util.takeScreenShot(driver,"NMA_6644","Screen22");

		wb_SearchSrcTxt = driver.findElement(By.id("android:id/search_src_text"));
		str_SearchSrcTxt = wb_SearchSrcTxt.getText();
		if(str_SearchSrcTxt.equalsIgnoreCase("Search")){
			System.out.println("Displayed input cursor and embedded 'Search' text in search field when blank or cleared");
			System.out.println("Displayed Native Soft Keyboard");
		}else{
			System.out.println("the SEARCH field is not empty");
		}

		wb_SearchSrcTxt = driver.findElement(By.id("android:id/search_src_text"));
		wb_SearchSrcTxt.sendKeys("Vivo");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver,"NMA_6644","Screen23");

		wb_SearchCloseBtn = driver.findElement(By.id("android:id/search_close_btn"));
		wb_SearchCloseBtn.click();	
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Removed the entered keyword in the search field and displayed blank search field");

		util.takeScreenShot(driver,"NMA_6644","Screen24");

		driver.navigate().back();
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver,"NMA_6644","Screen25");

		/*wb_UpButton = driver.findElement(By.id("android:id/up"));
		wb_UpButton.click();
		Thread.sleep(Constants.ThreadSleep);*/
		driver.navigate().back();
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Clicked on BACK button");

		util.takeScreenShot(driver,"NMA_6644","Screen26");

		WebElement wb_Tabs = driver.findElement(By.id(Constants.app_Package+":id/tabs"));
		SwipeAndScroll.scrollOnTab(driver, wb_Tabs, "Clips");
		Thread.sleep(5000);		

		WebElement Clip_Tab = driver.findElement(By.id(Constants.app_Package+":id/clips_tab"));
		String str_ClipTab = Clip_Tab.getText();
		Clip_Tab.click();		
		System.out.println("Clicked on TAB : "+str_ClipTab);
		Thread.sleep(5000);

		util.takeScreenShot(driver,"NMA_6644","Screen27");

		wb_ActionSearch = driver.findElement(By.id(Constants.app_Package+":id/action_search"));
		wb_ActionSearch.click();
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Clicked on SEARCH action icon");

		util.takeScreenShot(driver,"NMA_6644","Screen28");

		wb_SearchSrcTxt = driver.findElement(By.id("android:id/search_src_text"));
		str_SearchSrcTxt = wb_SearchSrcTxt.getText();
		if(str_SearchSrcTxt.equalsIgnoreCase("Search")){
			System.out.println("Displayed input cursor and embedded 'Search' text in search field when blank or cleared");
			System.out.println("Displayed Native Soft Keyboard");
		}else{
			System.out.println("the SEARCH field is not empty");
		}

		wb_SearchSrcTxt = driver.findElement(By.id("android:id/search_src_text"));
		wb_SearchSrcTxt.sendKeys("Vivo");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver,"NMA_6644","Screen29");

		wb_SearchCloseBtn = driver.findElement(By.id("android:id/search_close_btn"));
		wb_SearchCloseBtn.click();	
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Removed the entered keyword in the search field and displayed blank search field");

		util.takeScreenShot(driver,"NMA_6644","Screen30");

		driver.navigate().back();
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver,"NMA_6644","Screen31");

		/*wb_UpButton = driver.findElement(By.id("android:id/up"));
		wb_UpButton.click();
		Thread.sleep(Constants.ThreadSleep);*/
		driver.navigate().back();
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Clicked on BACK button");

		util.takeScreenShot(driver,"NMA_6644","Screen32");

		SwipeAndScroll.scrollOnTab(driver, wb_Tabs, "Music");
		Thread.sleep(5000);

		WebElement Music_Tab = driver.findElement(By.id(Constants.app_Package+":id/music_tab"));
		String str_MusicTab = Music_Tab.getText();
		Music_Tab.click();		
		System.out.println("Clicked on TAB : "+str_MusicTab);
		Thread.sleep(5000);

		util.takeScreenShot(driver,"NMA_6644","Screen33");

		wb_ActionSearch = driver.findElement(By.id(Constants.app_Package+":id/action_search"));
		wb_ActionSearch.click();
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Clicked on SEARCH action icon");

		util.takeScreenShot(driver,"NMA_6644","Screen34");

		wb_SearchSrcTxt = driver.findElement(By.id("android:id/search_src_text"));
		str_SearchSrcTxt = wb_SearchSrcTxt.getText();
		if(str_SearchSrcTxt.equalsIgnoreCase("Search")){
			System.out.println("Displayed input cursor and embedded 'Search' text in search field when blank or cleared");
			System.out.println("Displayed Native Soft Keyboard");
		}else{
			System.out.println("the SEARCH field is not empty");
		}

		wb_SearchSrcTxt = driver.findElement(By.id("android:id/search_src_text"));
		wb_SearchSrcTxt.sendKeys("Vivo");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver,"NMA_6644","Screen35");

		wb_SearchCloseBtn = driver.findElement(By.id("android:id/search_close_btn"));
		wb_SearchCloseBtn.click();	
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Removed the entered keyword in the search field and displayed blank search field");

		util.takeScreenShot(driver,"NMA_6644","Screen36");

		driver.navigate().back();
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver,"NMA_6644","Screen37");

		/*wb_UpButton = driver.findElement(By.id("android:id/up"));
		wb_UpButton.click();
		Thread.sleep(Constants.ThreadSleep);*/
		driver.navigate().back();
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Clicked on BACK button");

		util.takeScreenShot(driver,"NMA_6644","Screen38");

		SwipeAndScroll.scrollOnTab(driver, wb_Tabs, "Networks");
		Thread.sleep(5000);

		WebElement Networks_Tab = driver.findElement(By.id(Constants.app_Package+":id/networks_tab"));
		String str_Networks_Tab = Networks_Tab.getText();
		Networks_Tab.click();		
		System.out.println("Clicked on TAB : "+str_Networks_Tab);
		Thread.sleep(5000);

		util.takeScreenShot(driver,"NMA_6644","Screen39");

		wb_ActionSearch = driver.findElement(By.id(Constants.app_Package+":id/action_search"));
		wb_ActionSearch.click();
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Clicked on SEARCH action icon");

		util.takeScreenShot(driver,"NMA_6644","Screen40");

		wb_SearchSrcTxt = driver.findElement(By.id("android:id/search_src_text"));
		str_SearchSrcTxt = wb_SearchSrcTxt.getText();
		if(str_SearchSrcTxt.equalsIgnoreCase("Search")){
			System.out.println("Displayed input cursor and embedded 'Search' text in search field when blank or cleared");
			System.out.println("Displayed Native Soft Keyboard");
		}else{
			System.out.println("the SEARCH field is not empty");
		}

		wb_SearchSrcTxt = driver.findElement(By.id("android:id/search_src_text"));
		wb_SearchSrcTxt.sendKeys("Vivo");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver,"NMA_6644","Screen41");

		wb_SearchCloseBtn = driver.findElement(By.id("android:id/search_close_btn"));
		wb_SearchCloseBtn.click();		
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Removed the entered keyword in the search field and displayed blank search field and blank suggestions field");

		util.takeScreenShot(driver,"NMA_6644","Screen42");

		driver.navigate().back();
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver,"NMA_6644","Screen43");

		/*wb_UpButton = driver.findElement(By.id("android:id/up"));
		wb_UpButton.click();
		Thread.sleep(Constants.ThreadSleep);*/
		driver.navigate().back();
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Clicked on application BACK button");

		util.takeScreenShot(driver,"NMA_6644","Screen44");

		WebElement wb_Menu = driver.findElement(By.name("Open navigation drawer"));
		wb_Menu.click();
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Clicked on Menu");

		util.takeScreenShot(driver,"NMA_6644","Screen45");

		WebElement wb_ShopMenu = driver.findElement(By.id(Constants.app_Package+":id/cell_edit_screen").name("Shop"));
		String str_wbShopMenu = wb_ShopMenu.getText();
		wb_ShopMenu.click();
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Clicked on Menu : "+str_wbShopMenu);

		util.takeScreenShot(driver,"NMA_6644","Screen46");

		wb_ActionSearch.isEnabled();
		System.out.println("Search action button is available on child page");

		driver.navigate().back();
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver,"NMA_6644","Screen47");

		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");
		System.out.println("End Test_NMA_6644");
		util.fileWriting("End Test_NMA_6644");
		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");

	}

	/*
	 1.Launch Application.
	 2.Click on SEARCH action icon Home screen.
	 3.Enter 10 different search keywords and get different search results.
	 4.Select any one of the search history item and get search result.
	 5.Enter any search keyword and select any one of the item in search suggestion list and get search result.
	 6.Click on CLEAR HISTORY button.
	 7.Displays blank search suggestion field and removes all search history results.
	 8.Click on BACK button to navigate previous page.
	 */
	@Test
	public void Test_NMA_6645() throws Exception{
		System.out.println("Start Test_NMA_6645");
		util.fileWriting("Start Test_NMA_6645");
		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");
		WebElement wb_SearchSrcTxt = null;
		WebElement wb_SearchActionClose = null;

		WebElement Featured_Tab = driver.findElement(By.id(Constants.app_Package+":id/home_tab"));
		String str_FeaturedTab = Featured_Tab.getText();
		Featured_Tab.click();		
		System.out.println("Clicked on TAB : "+str_FeaturedTab);
		Thread.sleep(5000);

		util.takeScreenShot(driver,"NMA_6645","Screen1");

		WebElement wb_ActionSearch = driver.findElement(By.id(Constants.app_Package+":id/action_search"));
		wb_ActionSearch.click();
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Clicked on SEARCH action icon");
		System.out.println("Entering 10 different search keywords and get different search results");
		String[] arr_SearchKeywords = {"ABC", "What", "Come", "Virgin", "Fleet", "Mob", "Virgin Fleet", "Dark", "Vivo", "Girl", "Wives"};

		util.takeScreenShot(driver,"NMA_6645","Screen2");

		for(int i=0; i<10; i++){

			wb_SearchSrcTxt = driver.findElement(By.id("android:id/search_src_text"));
			wb_SearchSrcTxt.sendKeys(arr_SearchKeywords[i]);
			Thread.sleep(5000);

			util.takeScreenShot(driver,"NMA_6645","Screen3");

			command.executeAdbKeyEvent("66");
			Thread.sleep(5000);
			System.out.println("Keyword-"+(i+1)+" Entered :"+arr_SearchKeywords[i]);

			wb_SearchActionClose = driver.findElement(By.id(Constants.app_Package+":id/action_search_close"));
			wb_SearchActionClose.click();
			Thread.sleep(Constants.ThreadSleep);			
		}

		List<WebElement> list_MusicsReltVideo = driver.findElements(By.id(Constants.app_Package+":id/suggestion_list").id("android:id/text1"));

		if(list_MusicsReltVideo.size()>0){
			util.takeScreenShot(driver,"NMA_6645","Screen4");

			list_MusicsReltVideo.get(3).click();
			Thread.sleep(Constants.ThreadSleep);
			System.out.println("Clicked on 4th item of Historic search result");

			util.takeScreenShot(driver,"NMA_6645","Screen5");

			wb_SearchActionClose.click();
			Thread.sleep(Constants.ThreadSleep);

			util.takeScreenShot(driver,"NMA_6645","Screen6");

			wb_SearchSrcTxt.sendKeys("@#@$@$$");
			Thread.sleep(Constants.ThreadSleep);

			util.takeScreenShot(driver,"NMA_6645","Screen7");

			list_MusicsReltVideo = driver.findElements(By.id(Constants.app_Package+":id/suggestion_list").id("android:id/text1"));
			if(list_MusicsReltVideo.size()>0){
				list_MusicsReltVideo.get(0).click();
				System.out.println("Selected First Item from Search Suggestion is included");
				util.takeScreenShot(driver,"NMA_6645","Screen8");

				wb_SearchActionClose.click();
				Thread.sleep(Constants.ThreadSleep);
			}else{
				WebElement wb_SearchCloseBtn = driver.findElement(By.id("android:id/search_close_btn"));
				wb_SearchCloseBtn.click();
				Thread.sleep(Constants.ThreadSleep);
				util.takeScreenShot(driver,"NMA_6645","Screen9");
			}		

			WebElement wb_SearchClearHistory = driver.findElement(By.id(Constants.app_Package+":id/search_clear_history"));
			str_SearchClearHistory = wb_SearchClearHistory.getText();
			Assert.assertEquals("Clear History", str_SearchClearHistory);
			wb_SearchClearHistory.click();
			Thread.sleep(Constants.ThreadSleep);
			System.out.println("Clicked on CLEAR HISTORY button");
			util.takeScreenShot(driver,"NMA_6645","Screen10");

			list_MusicsReltVideo = driver.findElements(By.id(Constants.app_Package+":id/suggestion_list").id("android:id/text1"));
			if(list_MusicsReltVideo.size() == 0){
				System.out.println("Displayed Blank Suggestion List");

			}

		}

		driver.navigate().back();
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Clicked on BACK button");

		util.takeScreenShot(driver,"NMA_6645","Screen11");

		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");
		System.out.println("End Test_NMA_6645");
		util.fileWriting("End Test_NMA_6645");
		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");

	}

	/*
	 1.Launch Application.
	 2.Click on SEARCH action icon Home screen.
	 3.Enter search text "Vivo" in SEARCH textbox.
	 4.Check list search suggestion are displayed or not.
	 5.select on e of search suggestion item in list.
	 6.Close the search result list.
	 7.Enter any one of character(Ex: V ) in search field.
	 8.Check auto suggestions are displayed if minimum one character entered.

	 */
	@Test
	public void Test_NMA_6646() throws Exception{
		System.out.println("Start Test_NMA_6646");
		util.fileWriting("Start Test_NMA_6646");
		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");

		WebElement Featured_Tab = driver.findElement(By.id(Constants.app_Package+":id/home_tab"));
		String str_FeaturedTab = Featured_Tab.getText();
		Featured_Tab.click();		
		System.out.println("Clicked on TAB : "+str_FeaturedTab);
		Thread.sleep(5000);

		util.takeScreenShot(driver,"NMA_6646","Screen1");

		WebElement wb_ActionSearch = driver.findElement(By.id(Constants.app_Package+":id/action_search"));
		wb_ActionSearch.click();
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Clicked on SEARCH icon");

		util.takeScreenShot(driver,"NMA_6646","Screen2");

		WebElement wb_SearchSrcTxt = driver.findElement(By.id("android:id/search_src_text"));
		wb_SearchSrcTxt.sendKeys("Vivo");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver,"NMA_6646","Screen3");

		JSONObject jsnObj_SearchSuggestions = mJsonParser.callHttpRequest(Constants.Search_Suggestions,"GET", null);
		Thread.sleep(Constants.ThreadSleep);

		int int_Total = jsnObj_SearchSuggestions.getInt("total");		

		if(int_Total>0){
			JSONArray jsnArray_SearchSuggestions = jsnObj_SearchSuggestions.getJSONArray("search_suggestions");
			System.out.println("Search Suggetions are avaibale");

			List<WebElement> list_MusicsReltVideo = driver.findElements(By.id(Constants.app_Package+":id/suggestion_list").id("android:id/text1"));
			for(int i=0; i<list_MusicsReltVideo.size(); i++){
				JSONObject json_AutoSearchName = jsnArray_SearchSuggestions.getJSONObject(i);
				String str_AutoSearchName = json_AutoSearchName.getString("name");
				System.out.println("str_AutoSearchName : "+str_AutoSearchName);
				WebElement wb_AutoSearchName = list_MusicsReltVideo.get(i);
				String str_wbAutoSearchName = wb_AutoSearchName.getText();
				System.out.println("str_wbAutoSearchName : "+str_wbAutoSearchName);
				//Assert.assertEquals(str_AutoSearchName, str_wbAutoSearchName);		    	
			}

			list_MusicsReltVideo = driver.findElements(By.id(Constants.app_Package+":id/suggestion_list").id("android:id/text1"));
			System.out.println("list_MusicsReltVideo.size() : "+list_MusicsReltVideo.size());
			list_MusicsReltVideo.get(0).click();
			Thread.sleep(5000);
			System.out.println("Selected first item from Search Suggestion is included");
			util.takeScreenShot(driver,"NMA_6646","Screen4");

			WebElement wb_SearchActionClose = driver.findElement(By.id(Constants.app_Package+":id/action_search_close"));
			wb_SearchActionClose.click();
			Thread.sleep(Constants.ThreadSleep);

			util.takeScreenShot(driver,"NMA_6646","Screen5");

			wb_SearchSrcTxt.sendKeys("V");
			Thread.sleep(Constants.ThreadSleep);

			util.takeScreenShot(driver,"NMA_6646","Screen6");

			jsnObj_SearchSuggestions = mJsonParser.callHttpRequest(Constants.Search_Suggestions,"GET", null);
			Thread.sleep(Constants.ThreadSleep);

			int_Total = jsnObj_SearchSuggestions.getInt("total");
			if(int_Total>0){
				System.out.println("The Auto Search Suggestions are comming minimum character 1");
			}else{

			}

		}else{
			System.out.println("Search Suggestions are not available");
		}



		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");
		System.out.println("End Test_NMA_6646");
		util.fileWriting("End Test_NMA_6646");
		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");

	}

	/*
	 1.Launch Application.
	 2.Click on SEARCH action icon Home screen.
	 3.Enter search text "Vivo" in SEARCH textbox.
	 4.Press Search/Enter key on native soft keyboard.
	 5.The search result will display in SearchResult screen.
	 6.Click on  particular search result item.
	 7.It navigates SearchItem details screen.
	 */
	@Test
	public void Test_NMA_6647() throws Exception{
		System.out.println("Start Test_NMA_6647");
		util.fileWriting("Start Test_NMA_6647");
		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");

		WebElement Featured_Tab = driver.findElement(By.id(Constants.app_Package+":id/home_tab"));
		String str_FeaturedTab = Featured_Tab.getText();
		Featured_Tab.click();		
		System.out.println("Clicked on TAB : "+str_FeaturedTab);
		Thread.sleep(5000);

		util.takeScreenShot(driver,"NMA_6647","Screen1");

		WebElement wb_ActionSearch = driver.findElement(By.id(Constants.app_Package+":id/action_search"));
		wb_ActionSearch.click();
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Clicked on SEARCH icon");

		util.takeScreenShot(driver,"NMA_6647","Screen2");

		WebElement wb_SearchSrcTxt = driver.findElement(By.id("android:id/search_src_text"));
		wb_SearchSrcTxt.sendKeys("yfbytg");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver,"NMA_6647","Screen3");

		command.executeAdbKeyEvent("66");
		Thread.sleep(Constants.ThreadSleep);

		JSONObject jsnObj_SearchItemDetails = mJsonParser.callHttpRequest(Constants.Search_Details,"GET", null);
		Thread.sleep(5000);

		int int_Total = jsnObj_SearchItemDetails.getInt("total");

		WebElement wb_SearchActionClose = driver.findElement(By.id(Constants.app_Package+":id/action_search_close"));
		wb_SearchActionClose.click();
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver,"NMA_6647","Screen4");

		wb_SearchSrcTxt.sendKeys("Vivo");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver,"NMA_6647","Screen5");

		command.executeAdbKeyEvent("66");
		Thread.sleep(Constants.ThreadSleep);

		jsnObj_SearchItemDetails = mJsonParser.callHttpRequest(Constants.Search_Details,"GET", null);
		Thread.sleep(Constants.ThreadSleep);

		int_Total = jsnObj_SearchItemDetails.getInt("total");
		if(int_Total>0){
			JSONArray jsnArray_SearchItemHits = jsnObj_SearchItemDetails.getJSONArray("hits");
			List<WebElement> list_SearchItemTitles = driver.findElements(By.id(Constants.app_Package+":id/search_result_list").id(Constants.app_Package+":id/item_search_title"));

			for(int j=0; j<list_SearchItemTitles.size(); j++){
				try{
					JSONObject json_SeachDetailsInfo = jsnArray_SearchItemHits.getJSONObject(j);
					JSONObject json_SeachDetailsResult = json_SeachDetailsInfo.getJSONObject("result");
					JSONObject json_SeachDetailsVOD = json_SeachDetailsResult.getJSONObject("vod");
					String str_SearchItemTitle = json_SeachDetailsVOD.getString("name");
					System.out.println("str_SearchItemTitle :"+str_SearchItemTitle);
					WebElement wbSearchItemTitle = list_SearchItemTitles.get(j);
					String str_wbSearchItemTitle = list_SearchItemTitles.get(j).getText();
					System.out.println("str_wbSearchItemTitle :"+str_wbSearchItemTitle);
					wbSearchItemTitle.click();
					System.out.println("Clicked on "+j+" item in search result list" );
					
					WebElement wb_ActionBarTitle = driver.findElement(By.id("android:id/toolbar").xpath("//android.widget.TextView[1]"));
					String str_wbActionBarTitle = wb_ActionBarTitle.getText();
					Assert.assertEquals(str_wbActionBarTitle, str_wbSearchItemTitle);
					System.out.println(str_wbActionBarTitle+ "is available on search result details page");

					util.takeScreenShot(driver,"NMA_6647","Screen6");

					driver.navigate().back();
					Thread.sleep(Constants.ThreadSleep);
				}catch(Exception e){

				}
			}
		}

		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");
		System.out.println("End Test_NMA_6647");
		util.fileWriting("End Test_NMA_6647");
		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");

	}
}
