
package com.mobitv.client.qa.nma.regression.mobile;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.mobitv.client.qa.nma.common.Constants;
import com.mobitv.client.qa.nma.common.DateandTimeClass;
import com.mobitv.client.qa.nma.common.ExecuteShellComand;
import com.mobitv.client.qa.nma.common.SwipeAndScroll;
import com.mobitv.client.qa.nma.common.Util;

public class TestFilter {

	private static Util util = new Util();
	private static WebDriver driver = null;
	private ExecuteShellComand command = new ExecuteShellComand();
	private DateandTimeClass dt = new DateandTimeClass();
	String currdate = dt.getcurrentDate();

	@AfterClass
	public void callPerl() throws InterruptedException {

		System.out.println("Enable GA logs on device");
		command.enableGALog();
		System.out.println("Capture adb logs");
		command.executeCommand("Filter");
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Call perl script");
		command.executeCommandinvokeperl("Filter.pl");
		Thread.sleep(30000);
		command.kill();
	}
	@BeforeMethod
	public void setUp() throws Exception {

		driver = util.getAndroidDriver(driver);
	}

	@AfterMethod
	public void tearDown() throws Exception {

		driver.quit();
		Thread.sleep(Constants.ThreadSleep);
		

	}

	@Test
	public void Test_NMA_2424_2430() throws Exception, InterruptedException {

		System.out.println("Start TestCases  ---> TestCaseIDs: NMA_2424 ---- JIRA ID: NMA-6150_6152");
		util.fileWriting("Start TestCases  ---> TestCaseIDs: NMA_2424 ---- JIRA ID: NMA-6150_6152");
		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");
		WebElement shows_Tab = driver.findElement(By.id(Constants.app_Package + ":id/shows_tab"));
		String str_ShowsTab = shows_Tab.getText();
		shows_Tab.click();
		System.out.println("Clicked on TAB : " + str_ShowsTab);
		Thread.sleep(5000);

		util.takeScreenShot(driver, "NMA_2424_2430", "Screen1");

		try {
			WebElement shows_ShowTitle1 = driver.findElement(By.id(Constants.app_Package + ":id/base_grid").xpath("//android.widget.LinearLayout[1]")
					.id(Constants.app_Package + ":id/cell_area_txt").id(Constants.app_Package + ":id/cell_txt_title"));
			String str_ShowTitle1 = shows_ShowTitle1.getText();
			System.out.println("SHOW-: " + str_ShowTitle1);
			Thread.sleep(Constants.ThreadSleep);
		} catch (Exception e) {
			System.out.println("No content is available for the selected filter. Please reset or change the filter settings");
		}

		WebElement shows_FilterBar = driver.findElement(By.id(Constants.app_Package + ":id/filter_bar"));
		String str_FilterBar = shows_FilterBar.getText();
		shows_FilterBar.click();
		System.out.println(str_FilterBar + ": is Selected");
		Thread.sleep(Constants.ThreadSleep);

		WebElement shows_FilterGroup = driver.findElement(By.id(Constants.app_Package + ":id/filter_group").name("Content"));
		String str_FilterGroup = shows_FilterGroup.getText();
		shows_FilterGroup.click();
		System.out.println(str_FilterGroup + ": group is Expanded");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver, "NMA_2424_2430", "Screen2");

		WebElement shows_FilterChildren_MyPacks = driver.findElement(By.id(Constants.app_Package + ":id/filter_children").name("My Packs"));
		String str_FilterChildren_MyPacks = shows_FilterChildren_MyPacks.getText();
		// shows_FilterChildren_MyPacks.click();
		SwipeAndScroll.tapOnElement(driver, shows_FilterChildren_MyPacks);
		System.out.println(str_FilterChildren_MyPacks + ": Category selected");
		Thread.sleep(Constants.ThreadSleep);

		WebElement shows_FilterChildren_Free = driver.findElement(By.id(Constants.app_Package + ":id/filter_children").name("Free"));
		String str_FilterChildren_Free = shows_FilterChildren_Free.getText();
		// shows_FilterChildren_Free.click();
		SwipeAndScroll.tapOnElement(driver, shows_FilterChildren_Free);
		System.out.println(str_FilterChildren_Free + ": Category selected");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver, "NMA_2424_2430", "Screen3");

		WebElement shows_FilterApplyBtn = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_apply_btn"));
		String str_FilterApplyBtn = shows_FilterApplyBtn.getText();
		shows_FilterApplyBtn.click();
		System.out.println(str_FilterApplyBtn + ": Button Clicked");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver, "NMA_2424_2430", "Screen4");
		try {
			WebElement shows_ShowTitle1 = driver.findElement(By.id(Constants.app_Package + ":id/base_grid").xpath("//android.widget.LinearLayout[1]")
					.id(Constants.app_Package + ":id/cell_area_txt").id(Constants.app_Package + ":id/cell_txt_title"));
			String str_ShowTitle1 = shows_ShowTitle1.getText();
			System.out.println("Show-: " + str_ShowTitle1);
			Thread.sleep(Constants.ThreadSleep);
		} catch (Exception e) {
			System.out.println("No content is available for the selected filter. Please reset or change the filter settings");
		}

		WebElement shows_FilterBar1 = driver.findElement(By.id(Constants.app_Package + ":id/filter_bar"));
		String str_FilterBar1 = shows_FilterBar1.getText();
		shows_FilterBar1.click();
		System.out.println(str_FilterBar1 + ": is Selected");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver, "NMA_2424_2430", "Screen5");

		WebElement shows_FilterReset = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_reset"));
		String str_FilterReset = shows_FilterReset.getText();
		shows_FilterReset.click();
		System.out.println(str_FilterReset + ": Button Clicked");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver, "NMA_2424_2430", "Screen6");

		WebElement shows_FilterApplyBtn1 = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_apply_btn"));
		String str_FilterApplyBtn1 = shows_FilterApplyBtn1.getText();
		shows_FilterApplyBtn1.click();
		System.out.println(str_FilterApplyBtn1 + ": Button Clicked");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver, "NMA_2424_2430", "Screen7");

		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");
		System.out.println("End TestCases  ---> TestCaseIDs: NMA_2424 ---- JIRA ID: NMA-6150_6152");
		util.fileWriting("End TestCases  ---> TestCaseIDs: NMA_2424 ---- JIRA ID: NMA-6150_6152");
		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");
	}

	@Test
	public void Test_NMA_2425() throws Exception, InterruptedException {

		System.out.println("Start TestCases  ---> TestCaseIDs: NMA_2425 ---- JIRA ID: NMA-6150");
		util.fileWriting("Start TestCases  ---> TestCaseIDs: NMA_2425 ---- JIRA ID: NMA-6150");
		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");

		WebElement live_Tab = driver.findElement(By.id(Constants.app_Package + ":id/live_guide_tab"));
		String str_LiveTab = live_Tab.getText();
		live_Tab.click();
		System.out.println(str_LiveTab + ": Clicked on Tab");
		Thread.sleep(5000);

		util.takeScreenShot(driver, "NMA_2425", "Screen1");

		WebElement live_FilterBar = driver.findElement(By.id(Constants.app_Package + ":id/filter_bar"));
		String str_LiveFilterBar = live_FilterBar.getText();
		live_FilterBar.click();
		System.out.println(str_LiveFilterBar + " is Selected");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver, "NMA_2425", "Screen2");

		WebElement live_FilterCancelBtn = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_cancel_btn"));
		live_FilterCancelBtn.click();
		System.out.println("Cancel Button is clicked");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver, "NMA_2425", "Screen3");

		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");
		System.out.println("End TestCases  ---> TestCaseIDs: NMA_2425 ---- JIRA ID: NMA-6150");
		util.fileWriting("End TestCases  ---> TestCaseIDs: NMA_2425 ---- JIRA ID: NMA-6150");
		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");
	}

	@SuppressWarnings("static-access")
	@Test
	public void Test_NMA_2426() throws Exception, InterruptedException {

		System.out.println("Start TestCases  ---> TestCaseIDs: NMA_2426 ---- JIRA ID: NMA-6150");
		util.fileWriting("Start TestCases  ---> TestCaseIDs: NMA_2426 ---- JIRA ID: NMA-6150");
		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");

		WebElement live_Tab = driver.findElement(By.id(Constants.app_Package + ":id/live_guide_tab"));
		String str_LiveTab = live_Tab.getText();
		live_Tab.click();
		System.out.println(str_LiveTab + ": Clicked on Tab");
		Thread.sleep(5000);

		util.takeScreenShot(driver, "NMA_2426", "Screen1");

		WebElement live_FilterBar = driver.findElement(By.id(Constants.app_Package + ":id/filter_bar"));
		String str_FilterBar = live_FilterBar.getText();
		live_FilterBar.click();
		System.out.println(str_FilterBar + " is Selected");
		Thread.sleep(Constants.ThreadSleep);

		WebElement live_FilterCancelBtn = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_cancel_btn"));
		live_FilterCancelBtn.click();
		System.out.println("Cancel Button is clicked");
		Thread.sleep(Constants.ThreadSleep);

		WebElement live_FilterBar1 = driver.findElement(By.id(Constants.app_Package + ":id/filter_bar"));
		String str_FilterBar1 = live_FilterBar1.getText();
		live_FilterBar1.click();
		System.out.println(str_FilterBar1 + ": is Selected");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver, "NMA_2426", "Screen2");

		WebElement live_FilterGroup = driver.findElement(By.id(Constants.app_Package + ":id/filter_group").name("Content"));
		String str_FilterGroup = live_FilterGroup.getText();
		live_FilterGroup.click();
		System.out.println(str_FilterGroup + ": group is Expanded");
		Thread.sleep(Constants.ThreadSleep);

		WebElement live_FilterChildren_MyPacks = driver.findElement(By.id(Constants.app_Package + ":id/filter_children").name("My Packs"));
		String str_FilterChildren_MyPacks = live_FilterChildren_MyPacks.getText();
		// live_FilterChildren_MyPacks.click();
		SwipeAndScroll.tapOnElement(driver, live_FilterChildren_MyPacks);
		System.out.println(str_FilterChildren_MyPacks + ": Category selected");
		Thread.sleep(Constants.ThreadSleep);

		WebElement live_FilterChildren_Free = driver.findElement(By.id(Constants.app_Package + ":id/filter_children").name("Free"));
		String str_FilterChildren_Free = live_FilterChildren_Free.getText();
		// live_FilterChildren_Free.click();
		SwipeAndScroll.tapOnElement(driver, live_FilterChildren_Free);
		System.out.println(str_FilterChildren_Free + ": Category selected");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver, "NMA_2426", "Screen3");

		WebElement live_FilterApplyBtn = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_apply_btn"));
		String str_FilterApplyBtn = live_FilterApplyBtn.getText();
		live_FilterApplyBtn.click();
		System.out.println(str_FilterApplyBtn + ": Button Clicked");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver, "NMA_2426", "Screen4");

		WebElement live_FilterBar2 = driver.findElement(By.id(Constants.app_Package + ":id/filter_bar"));
		String str_FilterBar2 = live_FilterBar1.getText();
		live_FilterBar2.click();
		System.out.println(str_FilterBar2 + ": is Selected");
		Thread.sleep(Constants.ThreadSleep);

		WebElement live_FilterReset = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_reset"));
		String str_FilterReset = live_FilterReset.getText();
		live_FilterReset.click();
		System.out.println(str_FilterReset + ": Button Clicked");
		Thread.sleep(Constants.ThreadSleep);

		WebElement live_FilterApplyBtn1 = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_apply_btn"));
		String str_FilterApplyBtn1 = live_FilterApplyBtn1.getText();
		live_FilterApplyBtn1.click();
		System.out.println(str_FilterApplyBtn1 + ": Button Clicked");
		Thread.sleep(Constants.ThreadSleep);

		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");
		System.out.println("End TestCases  ---> TestCaseIDs: NMA_2426 ---- JIRA ID: NMA-6150");
		util.fileWriting("End TestCases  ---> TestCaseIDs: NMA_2426 ---- JIRA ID: NMA-6150");
		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");
	}

	@SuppressWarnings("static-access")
	@Test
	public void Test_NMA_2431() throws Exception, InterruptedException {

		System.out.println("Start TestCases  ---> TestCaseIDs: NMA_2431 ---- JIRA ID: NMA-6178");
		util.fileWriting("Start TestCases  ---> TestCaseIDs: NMA_2431 ---- JIRA ID: NMA-6178");
		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");

		WebElement shows_Tab = driver.findElement(By.id(Constants.app_Package + ":id/shows_tab"));
		String str_ShowsTab = shows_Tab.getText();
		shows_Tab.click();
		System.out.println(str_ShowsTab + ": Clicked on Tab");
		Thread.sleep(5000);

		util.takeScreenShot(driver, "NMA_2431", "Screen1");

		WebElement shows_FilterBar = driver.findElement(By.id(Constants.app_Package + ":id/filter_bar"));
		String str_FilterBar = shows_FilterBar.getText();
		shows_FilterBar.click();
		System.out.println(str_FilterBar + " is Selected");
		Thread.sleep(Constants.ThreadSleep);

		WebElement shows_FilterReset = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_reset"));
		String str_FilterReset = shows_FilterReset.getText();
		shows_FilterReset.click();
		System.out.println(str_FilterReset + ": Button Clicked");
		Thread.sleep(Constants.ThreadSleep);

		WebElement shows_FilterApplyBtn = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_apply_btn"));
		String str_FilterApplyBtn = shows_FilterApplyBtn.getText();
		shows_FilterApplyBtn.click();
		System.out.println(str_FilterApplyBtn + ": Button Clicked");
		Thread.sleep(Constants.ThreadSleep);

		WebElement shows_FilterBar1 = driver.findElement(By.id(Constants.app_Package + ":id/filter_bar"));
		String str_FilterBar1 = shows_FilterBar1.getText();
		shows_FilterBar1.click();
		System.out.println(str_FilterBar1 + " is Selected");
		Thread.sleep(Constants.ThreadSleep);

		WebElement shows_FilterCancelBtn = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_cancel_btn"));
		shows_FilterCancelBtn.click();
		System.out.println("Cancel Button is clicked");
		Thread.sleep(Constants.ThreadSleep);

		WebElement shows_FilterBar2 = driver.findElement(By.id(Constants.app_Package + ":id/filter_bar"));
		String str_FilterBar2 = shows_FilterBar2.getText();
		System.out.println("Filter Name " + str_FilterBar2);
		Assert.assertEquals("FILTER", str_FilterBar2);
		shows_FilterBar2.click();
		System.out.println(str_FilterBar2 + " is Selected");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver, "NMA_2431", "Screen2");

		WebElement shows_FilterGroup = driver.findElement(By.id(Constants.app_Package + ":id/filter_group").name("Content"));
		String str_FilterGroup = shows_FilterGroup.getText();
		shows_FilterGroup.click();
		System.out.println(str_FilterGroup + ": group is Expanded");
		Thread.sleep(Constants.ThreadSleep);

		// WebElement shows_FilterChildren_Premium =
		// driver.findElement(By.id(Constants.App_Package+":id/dialog_filters_container").xpath("//android.widget.LinearLayout[3]/android.widget.LinearLayout[1]/android.widget.TextView[1]"));
		WebElement shows_FilterChildren_Premium = driver.findElement(By.id(Constants.app_Package + ":id/filter_children").name("Premium"));
		String str_FilterChildren_Premium = shows_FilterChildren_Premium.getText();
		shows_FilterChildren_Premium.click();
		// SwipeAndScroll.tapOnElement(driver, shows_FilterChildren_Premium);
		System.out.println(str_FilterChildren_Premium + ": Category selected");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver, "NMA_2431", "Screen3");

		WebElement shows_FilterApplyBtn1 = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_apply_btn"));
		String str_FilterApplyBtn1 = shows_FilterApplyBtn1.getText();
		shows_FilterApplyBtn1.click();
		System.out.println(str_FilterApplyBtn1 + ": Button Clicked");
		Thread.sleep(5000);

		WebElement shows_FilterBar3 = driver.findElement(By.id(Constants.app_Package + ":id/filter_bar"));
		String str_FilterBar3 = shows_FilterBar3.getText();
		shows_FilterBar3.click();
		System.out.println(str_FilterBar3);
		util.takeScreenShot(driver, "NMA_2431", "Screen4");
		Assert.assertEquals("FILTER - Premium", str_FilterBar3);

		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");
		System.out.println("End TestCases  ---> TestCaseIDs: NMA_2431 ---- JIRA ID: NMA-6178");
		util.fileWriting("End TestCases  ---> TestCaseIDs: NMA_2431 ---- JIRA ID: NMA-6178");
		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");
	}

	@Test
	public void Test_NMA_2432() throws Exception, InterruptedException {

		System.out.println("Start TestCases  ---> TestCaseIDs: NMA_2432 ---- JIRA ID: NMA-6178");
		util.fileWriting("Start TestCases  ---> TestCaseIDs: NMA_2432 ---- JIRA ID: NMA-6178");
		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");

		WebElement shows_Tab = driver.findElement(By.id(Constants.app_Package + ":id/shows_tab"));
		String str_ShowsTab = shows_Tab.getText();
		shows_Tab.click();
		System.out.println(str_ShowsTab + ": Clicked on Tab");
		Thread.sleep(5000);

		util.takeScreenShot(driver, "NMA_2432", "Screen1");

		WebElement shows_FilterBar = driver.findElement(By.id(Constants.app_Package + ":id/filter_bar"));
		String str_FilterBar = shows_FilterBar.getText();
		shows_FilterBar.click();
		System.out.println(str_FilterBar + " is Selected");
		Thread.sleep(Constants.ThreadSleep);

		WebElement shows_FilterReset = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_reset"));
		String str_FilterReset = shows_FilterReset.getText();
		shows_FilterReset.click();
		System.out.println(str_FilterReset + ": Button Clicked");
		Thread.sleep(Constants.ThreadSleep);

		WebElement shows_FilterApplyBtn = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_apply_btn"));
		String str_FilterApplyBtn = shows_FilterApplyBtn.getText();
		shows_FilterApplyBtn.click();
		System.out.println(str_FilterApplyBtn + ": Button Clicked");
		Thread.sleep(Constants.ThreadSleep);

		WebElement shows_FilterBar1 = driver.findElement(By.id(Constants.app_Package + ":id/filter_bar"));
		String str_FilterBar1 = shows_FilterBar1.getText();
		shows_FilterBar1.click();
		System.out.println(str_FilterBar1 + " is Selected");
		Thread.sleep(Constants.ThreadSleep);

		WebElement shows_FilterCancelBtn = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_cancel_btn"));
		shows_FilterCancelBtn.click();
		System.out.println("Cancel Button is clicked");
		Thread.sleep(Constants.ThreadSleep);

		WebElement shows_FilterBar2 = driver.findElement(By.id(Constants.app_Package + ":id/filter_bar"));
		String str_FilterBar2 = shows_FilterBar2.getText();
		System.out.println("Filter Name " + str_FilterBar2);
		Assert.assertEquals("FILTER", str_FilterBar2);
		shows_FilterBar2.click();
		System.out.println(str_FilterBar2 + " is Selected");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver, "NMA_2432", "Screen2");

		WebElement shows_FilterGroup = driver.findElement(By.id(Constants.app_Package + ":id/filter_group").name("Available On"));
		String str_FilterGroup = shows_FilterGroup.getText();
		shows_FilterGroup.click();
		System.out.println(str_FilterGroup + ": group is Expanded");
		Thread.sleep(Constants.ThreadSleep);

		// WebElement shows_FilterChildren_Mobile =
		// driver.findElement(By.id(Constants.App_Package+":id/dialog_filters_container").xpath("//android.widget.LinearLayout[4]/android.widget.LinearLayout[1]/android.widget.TextView[1]"));
		WebElement shows_FilterChildren_Mobile = driver.findElement(By.id(Constants.app_Package + ":id/filter_children").name("Mobile"));
		String str_FilterChildren_Mobile = shows_FilterChildren_Mobile.getText();
		shows_FilterChildren_Mobile.click();
		// SwipeAndScroll.tapOnElement(driver, shows_FilterChildren_Mobile);
		System.out.println(str_FilterChildren_Mobile + ": Category selected");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver, "NMA_2432", "Screen3");

		WebElement shows_FilterApplyBtn1 = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_apply_btn"));
		String str_FilterApplyBtn1 = shows_FilterApplyBtn1.getText();
		shows_FilterApplyBtn1.click();
		System.out.println(str_FilterApplyBtn1 + ": Button Clicked");
		Thread.sleep(5000);

		WebElement shows_FilterBar3 = driver.findElement(By.id(Constants.app_Package + ":id/filter_bar"));
		String str_FilterBar3 = shows_FilterBar3.getText();
		System.out.println(str_FilterBar3);
		util.takeScreenShot(driver, "NMA_2432", "Screen4");
		Assert.assertEquals("FILTER - Mobile", str_FilterBar3);

		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");
		System.out.println("End TestCases  ---> TestCaseIDs: NMA_2432 ---- JIRA ID: NMA-6178");
		util.fileWriting("End TestCases  ---> TestCaseIDs: NMA_2432 ---- JIRA ID: NMA-6178");
		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");
	}

	@SuppressWarnings("static-access")
	@Test
	public void Test_NMA_2433() throws Exception, InterruptedException {

		System.out.println("Start TestCases  ---> TestCaseIDs: NMA_2433 ---- JIRA ID: NMA-6178");
		util.fileWriting("Start TestCases  ---> TestCaseIDs: NMA_2433 ---- JIRA ID: NMA-6178");
		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");

		WebElement shows_Tab = driver.findElement(By.id(Constants.app_Package + ":id/shows_tab"));
		String str_ShowsTab = shows_Tab.getText();
		shows_Tab.click();
		System.out.println(str_ShowsTab + ": Clicked on Tab");
		Thread.sleep(5000);

		util.takeScreenShot(driver, "NMA_2433", "Screen1");

		WebElement shows_FilterBar = driver.findElement(By.id(Constants.app_Package + ":id/filter_bar"));
		String str_FilterBar = shows_FilterBar.getText();
		shows_FilterBar.click();
		System.out.println(str_FilterBar + " is Selected");
		Thread.sleep(Constants.ThreadSleep);

		WebElement shows_FilterReset = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_reset"));
		String str_FilterReset = shows_FilterReset.getText();
		shows_FilterReset.click();
		System.out.println(str_FilterReset + ": Button Clicked");
		Thread.sleep(Constants.ThreadSleep);

		WebElement shows_FilterApplyBtn = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_apply_btn"));
		String str_FilterApplyBtn = shows_FilterApplyBtn.getText();
		shows_FilterApplyBtn.click();
		System.out.println(str_FilterApplyBtn + ": Button Clicked");
		Thread.sleep(Constants.ThreadSleep);

		WebElement shows_FilterBar1 = driver.findElement(By.id(Constants.app_Package + ":id/filter_bar"));
		String str_FilterBar1 = shows_FilterBar1.getText();
		shows_FilterBar1.click();
		System.out.println(str_FilterBar1 + " is Selected");
		Thread.sleep(Constants.ThreadSleep);

		WebElement shows_FilterCancelBtn = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_cancel_btn"));
		shows_FilterCancelBtn.click();
		System.out.println("Cancel Button is clicked");
		Thread.sleep(Constants.ThreadSleep);

		WebElement shows_FilterBar2 = driver.findElement(By.id(Constants.app_Package + ":id/filter_bar"));
		String str_FilterBar2 = shows_FilterBar2.getText();
		System.out.println("Filter Name " + str_FilterBar2);
		Assert.assertEquals("FILTER", str_FilterBar2);
		shows_FilterBar2.click();
		System.out.println(str_FilterBar2 + " is Selected");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver, "NMA_2433", "Screen2");

		WebElement shows_FilterGroup = driver.findElement(By.id(Constants.app_Package + ":id/filter_group").name("Genres"));
		String str_FilterGroup = shows_FilterGroup.getText();
		shows_FilterGroup.click();
		System.out.println(str_FilterGroup + ": group is Expanded");
		Thread.sleep(Constants.ThreadSleep);

		// WebElement shows_FilterChildren_Comedy =
		// driver.findElement(By.id(Constants.App_Package+":id/dialog_filters_container").xpath("//android.widget.LinearLayout[8]/android.widget.LinearLayout[1]/android.widget.TextView[1]"));
		WebElement shows_FilterChildren_Comedy = driver.findElement(By.id(Constants.app_Package + ":id/filter_children").name("Comedy"));
		String str_FilterChildren_Comedy = shows_FilterChildren_Comedy.getText();
		shows_FilterChildren_Comedy.click();
		// SwipeAndScroll.singleTap(driver, shows_FilterChildren_Mobile);
		System.out.println(str_FilterChildren_Comedy + ": Category selected");
		Thread.sleep(10000);

		util.takeScreenShot(driver, "NMA_2433", "Screen3");

		WebElement shows_FilterApplyBtn1 = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_apply_btn"));
		String str_FilterApplyBtn1 = shows_FilterApplyBtn1.getText();
		shows_FilterApplyBtn1.click();
		System.out.println(str_FilterApplyBtn1 + ": Button Clicked");
		Thread.sleep(10000);

		WebElement shows_FilterBar3 = driver.findElement(By.id(Constants.app_Package + ":id/filter_bar"));
		String str_FilterBar3 = shows_FilterBar3.getText();
		System.out.println(str_FilterBar3);
		util.takeScreenShot(driver, "NMA_2433", "Screen4");
		Assert.assertEquals("FILTER - Comedy", str_FilterBar3);

		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");
		System.out.println("End TestCases  ---> TestCaseIDs: NMA_2433 ---- JIRA ID: NMA-6178");
		util.fileWriting("End TestCases  ---> TestCaseIDs: NMA_2433 ---- JIRA ID: NMA-6178");
		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");
	}

	@Test
	public void Test_NMA_2434() throws Exception, InterruptedException {

		System.out.println("Start TestCases  ---> TestCaseIDs: NMA_2434 ---- JIRA ID: NMA-6178");
		util.fileWriting("Start TestCases  ---> TestCaseIDs: NMA_2434 ---- JIRA ID: NMA-6178");
		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");

		WebElement shows_Tab = driver.findElement(By.id(Constants.app_Package + ":id/shows_tab"));
		String str_ShowsTab = shows_Tab.getText();
		shows_Tab.click();
		System.out.println(str_ShowsTab + ": Clicked on Tab");
		Thread.sleep(5000);

		util.takeScreenShot(driver, "NMA_2434", "Screen1");

		WebElement shows_FilterBar = driver.findElement(By.id(Constants.app_Package + ":id/filter_bar"));
		String str_FilterBar = shows_FilterBar.getText();
		shows_FilterBar.click();
		System.out.println(str_FilterBar + " is Selected");
		Thread.sleep(Constants.ThreadSleep);

		WebElement shows_FilterReset = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_reset"));
		String str_FilterReset = shows_FilterReset.getText();
		shows_FilterReset.click();
		System.out.println(str_FilterReset + ": Button Clicked");
		Thread.sleep(Constants.ThreadSleep);

		WebElement shows_FilterApplyBtn = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_apply_btn"));
		String str_FilterApplyBtn = shows_FilterApplyBtn.getText();
		shows_FilterApplyBtn.click();
		System.out.println(str_FilterApplyBtn + ": Button Clicked");
		Thread.sleep(Constants.ThreadSleep);

		WebElement shows_FilterBar1 = driver.findElement(By.id(Constants.app_Package + ":id/filter_bar"));
		String str_FilterBar1 = shows_FilterBar1.getText();
		shows_FilterBar1.click();
		System.out.println(str_FilterBar1 + " is Selected");
		Thread.sleep(Constants.ThreadSleep);

		WebElement shows_FilterCancelBtn = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_cancel_btn"));
		shows_FilterCancelBtn.click();
		System.out.println("Cancel Button is clicked");
		Thread.sleep(Constants.ThreadSleep);

		WebElement shows_FilterBar2 = driver.findElement(By.id(Constants.app_Package + ":id/filter_bar"));
		String str_FilterBar2 = shows_FilterBar2.getText();
		System.out.println("Filter Name " + str_FilterBar2);
		Assert.assertEquals("FILTER", str_FilterBar2);
		shows_FilterBar2.click();
		System.out.println(str_FilterBar2 + " is Selected");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver, "NMA_2434", "Screen2");

		WebElement shows_FilterGroup = driver.findElement(By.id(Constants.app_Package + ":id/filter_group").name("Parental Guidance"));
		String str_FilterGroup = shows_FilterGroup.getText();
		shows_FilterGroup.click();
		System.out.println(str_FilterGroup + ": group is Expanded");
		Thread.sleep(Constants.ThreadSleep);

		WebElement shows_FilterChildren_Kids = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_container").id(Constants.app_Package + ":id/filter_children")
				.name("Kids"));
		String str_FilterChildren_Kids = shows_FilterChildren_Kids.getText();
		shows_FilterChildren_Kids.click();
		System.out.println(str_FilterChildren_Kids + ": Category selected");
		Thread.sleep(10000);

		util.takeScreenShot(driver, "NMA_2434", "Screen3");

		WebElement shows_FilterApplyBtn1 = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_apply_btn"));
		String str_FilterApplyBtn1 = shows_FilterApplyBtn1.getText();
		shows_FilterApplyBtn1.click();
		System.out.println(str_FilterApplyBtn1 + ": Button Clicked");
		Thread.sleep(10000);

		WebElement shows_FilterBar3 = driver.findElement(By.id(Constants.app_Package + ":id/filter_bar"));
		String str_FilterBar3 = shows_FilterBar3.getText();
		System.out.println(str_FilterBar3);
		util.takeScreenShot(driver, "NMA_2434", "Screen4");
		Assert.assertEquals("FILTER - Kids", str_FilterBar3);

		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");
		System.out.println("End TestCases  ---> TestCaseIDs: NMA_2434 ---- JIRA ID: NMA-6178");
		util.fileWriting("End TestCases  ---> TestCaseIDs: NMA_2434 ---- JIRA ID: NMA-6178");
		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");
	}

	@Test
	public void Test_NMA_2435() throws Exception, InterruptedException {

		System.out.println("Start TestCases  ---> TestCaseIDs: NMA_2435 ---- JIRA ID: NMA-6178");
		util.fileWriting("Start TestCases  ---> TestCaseIDs: NMA_2435 ---- JIRA ID: NMA-6178");
		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");

		WebElement live_Tab = driver.findElement(By.id(Constants.app_Package + ":id/live_guide_tab"));
		String str_liveTab = live_Tab.getText();
		live_Tab.click();
		System.out.println(str_liveTab + ": Clicked on Tab");
		Thread.sleep(5000);

		util.takeScreenShot(driver, "NMA_2435", "Screen1");

		WebElement live_FilterBar = driver.findElement(By.id(Constants.app_Package + ":id/filter_bar"));
		String str_FilterBar = live_FilterBar.getText();
		live_FilterBar.click();
		System.out.println(str_FilterBar + " is Selected");
		Thread.sleep(Constants.ThreadSleep);

		WebElement live_FilterReset = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_reset"));
		String str_FilterReset = live_FilterReset.getText();
		live_FilterReset.click();
		System.out.println(str_FilterReset + ": Button Clicked");
		Thread.sleep(Constants.ThreadSleep);

		WebElement live_FilterApplyBtn = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_apply_btn"));
		String str_FilterApplyBtn = live_FilterApplyBtn.getText();
		live_FilterApplyBtn.click();
		System.out.println(str_FilterApplyBtn + ": Button Clicked");
		Thread.sleep(Constants.ThreadSleep);

		WebElement live_FilterBar1 = driver.findElement(By.id(Constants.app_Package + ":id/filter_bar"));
		String str_FilterBar1 = live_FilterBar1.getText();
		System.out.println("Filter Name " + str_FilterBar1);
		live_FilterBar1.click();
		System.out.println(str_FilterBar1 + " is Selected");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver, "NMA_2435", "Screen2");

		WebElement live_FilterGroup = driver.findElement(By.id(Constants.app_Package + ":id/filter_group").name("Content"));
		String str_FilterGroup = live_FilterGroup.getText();
		live_FilterGroup.click();
		System.out.println(str_FilterGroup + ": group is Expanded");
		Thread.sleep(Constants.ThreadSleep);

		WebElement live_FilterChildren_MyPacks = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_container").id(Constants.app_Package + ":id/filter_children")
				.name("My Packs"));
		String str_FilterChildren_MyPacks = live_FilterChildren_MyPacks.getText();
		live_FilterChildren_MyPacks.click();
		System.out.println(str_FilterChildren_MyPacks + ": Category selected");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver, "NMA_2435", "Screen3");

		WebElement live_FilterGroup1 = driver.findElement(By.id(Constants.app_Package + ":id/filter_group").name("Available On"));
		String str_FilterGroup1 = live_FilterGroup1.getText();
		live_FilterGroup1.click();
		System.out.println(str_FilterGroup1 + ": group is Expanded");
		Thread.sleep(Constants.ThreadSleep);

		WebElement live_FilterChildren_Mobile = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_container").id(Constants.app_Package + ":id/filter_children")
				.name("Mobile"));
		String str_FilterChildren_Mobile = live_FilterChildren_Mobile.getText();
		live_FilterChildren_Mobile.click();
		System.out.println(str_FilterChildren_Mobile + ": Category selected");
		Thread.sleep(10000);

		util.takeScreenShot(driver, "NMA_2435", "Screen4");

		WebElement live_FilterApplyBtn1 = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_apply_btn"));
		String str_FilterApplyBtn1 = live_FilterApplyBtn1.getText();
		live_FilterApplyBtn1.click();
		System.out.println(str_FilterApplyBtn1 + ": Button Clicked");
		Thread.sleep(10000);

		WebElement live_FilterBar3 = driver.findElement(By.id(Constants.app_Package + ":id/filter_bar"));
		String str_FilterBar3 = live_FilterBar3.getText();
		System.out.println(str_FilterBar3);
		util.takeScreenShot(driver, "NMA_2435", "Screen5");
		Assert.assertEquals("FILTER - My Packs, Mobile", str_FilterBar3);

		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");
		System.out.println("End TestCases  ---> TestCaseIDs: NMA_2435 ---- JIRA ID: NMA-6178");
		util.fileWriting("End TestCases  ---> TestCaseIDs: NMA_2435 ---- JIRA ID: NMA-6178");
		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");
	}

	@Test
	public void Test_NMA_2436() throws Exception, InterruptedException {

		System.out.println("Start TestCases  ---> TestCaseIDs: NMA_2436 ---- JIRA ID: NMA-6178");
		util.fileWriting("Start TestCases  ---> TestCaseIDs: NMA_2436 ---- JIRA ID: NMA-6178");
		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");

		WebElement live_Tab = driver.findElement(By.id(Constants.app_Package + ":id/live_guide_tab"));
		String str_liveTab = live_Tab.getText();
		live_Tab.click();
		System.out.println(str_liveTab + ": Clicked on Tab");
		Thread.sleep(5000);

		util.takeScreenShot(driver, "NMA_2436", "Screen1");

		WebElement live_FilterBar = driver.findElement(By.id(Constants.app_Package + ":id/filter_bar"));
		String str_FilterBar = live_FilterBar.getText();
		live_FilterBar.click();
		System.out.println(str_FilterBar + " is Selected");
		Thread.sleep(Constants.ThreadSleep);

		WebElement live_FilterReset = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_reset"));
		String str_FilterReset = live_FilterReset.getText();
		live_FilterReset.click();
		System.out.println(str_FilterReset + ": Button Clicked");
		Thread.sleep(Constants.ThreadSleep);

		WebElement live_FilterApplyBtn = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_apply_btn"));
		String str_FilterApplyBtn = live_FilterApplyBtn.getText();
		live_FilterApplyBtn.click();
		System.out.println(str_FilterApplyBtn + ": Button Clicked");
		Thread.sleep(Constants.ThreadSleep);

		WebElement live_FilterBar1 = driver.findElement(By.id(Constants.app_Package + ":id/filter_bar"));
		String str_FilterBar1 = live_FilterBar1.getText();
		System.out.println("Filter Name " + str_FilterBar1);
		live_FilterBar1.click();
		System.out.println(str_FilterBar1 + " is Selected");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver, "NMA_2436", "Screen2");

		WebElement live_FilterGroup = driver.findElement(By.id(Constants.app_Package + ":id/filter_group").name("Content"));
		String str_FilterGroup = live_FilterGroup.getText();
		live_FilterGroup.click();
		System.out.println(str_FilterGroup + ": group is Expanded");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver, "NMA_2436", "Screen3");

		WebElement live_FilterChildren_MyPacks = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_container").id(Constants.app_Package + ":id/filter_children")
				.name("My Packs"));
		String str_FilterChildren_MyPacks = live_FilterChildren_MyPacks.getText();
		live_FilterChildren_MyPacks.click();
		System.out.println(str_FilterChildren_MyPacks + ": Category selected");
		Thread.sleep(Constants.ThreadSleep);

		WebElement live_FilterChildren_Free = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_container").id(Constants.app_Package + ":id/filter_children")
				.name("Free"));
		String str_FilterChildren_Free = live_FilterChildren_Free.getText();
		live_FilterChildren_Free.click();
		System.out.println(str_FilterChildren_Free + ": Category selected");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver, "NMA_2436", "Screen4");

		WebElement live_FilterGroup1 = driver.findElement(By.id(Constants.app_Package + ":id/filter_group").name("Available On"));
		String str_FilterGroup1 = live_FilterGroup1.getText();
		live_FilterGroup1.click();
		System.out.println(str_FilterGroup1 + ": group is Expanded");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver, "NMA_2436", "Screen5");

		WebElement live_FilterChildren_Mobile = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_container").id(Constants.app_Package + ":id/filter_children")
				.name("Mobile"));
		String str_FilterChildren_Mobile = live_FilterChildren_Mobile.getText();
		live_FilterChildren_Mobile.click();
		System.out.println(str_FilterChildren_Mobile + ": Category selected");
		Thread.sleep(Constants.ThreadSleep);

		WebElement live_FilterChildren_Tablet = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_container").id(Constants.app_Package + ":id/filter_children")
				.name("Tablet"));
		String str_FilterChildren_Tablet = live_FilterChildren_Tablet.getText();
		live_FilterChildren_Tablet.click();
		System.out.println(str_FilterChildren_Tablet + ": Category selected");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver, "NMA_2436", "Screen6");

		WebElement live_FilterApplyBtn1 = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_apply_btn"));
		String str_FilterApplyBtn1 = live_FilterApplyBtn1.getText();
		live_FilterApplyBtn1.click();
		System.out.println(str_FilterApplyBtn1 + ": Button Clicked");
		Thread.sleep(10000);

		WebElement live_FilterBar3 = driver.findElement(By.id(Constants.app_Package + ":id/filter_bar"));
		String str_FilterBar3 = live_FilterBar3.getText();
		System.out.println(str_FilterBar3);
		util.takeScreenShot(driver, "NMA_2436", "Screen7");
		Assert.assertEquals("FILTER - My Packs, Free, Mobile, Tablet", str_FilterBar3);

		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");
		System.out.println("End TestCases  ---> TestCaseIDs: NMA_2436 ---- JIRA ID: NMA-6178");
		util.fileWriting("End TestCases  ---> TestCaseIDs: NMA_2436 ---- JIRA ID: NMA-6178");
		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");
	}

	@Test
	public void Test_NMA_2437_2438() throws Exception, InterruptedException {

		System.out.println("Start TestCases  ---> TestCaseIDs: NMA_2437_2438 ---- JIRA ID: NMA-6197_6200");
		util.fileWriting("Start TestCases  ---> TestCaseIDs: NMA_2437_2438 ---- JIRA ID: NMA-6197_6200");
		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");

		WebElement live_Tab = driver.findElement(By.id(Constants.app_Package + ":id/live_guide_tab"));
		String str_liveTab = live_Tab.getText();
		live_Tab.click();
		System.out.println(str_liveTab + ": Clicked on Tab");
		Thread.sleep(5000);

		util.takeScreenShot(driver, "NMA_2437_2438", "Screen1");

		WebElement live_FilterBar = driver.findElement(By.id(Constants.app_Package + ":id/filter_bar"));
		String str_FilterBar = live_FilterBar.getText();
		live_FilterBar.click();
		System.out.println(str_FilterBar + " is Selected");
		Thread.sleep(Constants.ThreadSleep);

		WebElement live_FilterReset = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_reset"));
		String str_FilterReset = live_FilterReset.getText();
		live_FilterReset.click();
		System.out.println(str_FilterReset + ": Button Clicked");
		Thread.sleep(Constants.ThreadSleep);

		WebElement live_FilterApplyBtn = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_apply_btn"));
		String str_FilterApplyBtn = live_FilterApplyBtn.getText();
		live_FilterApplyBtn.click();
		System.out.println(str_FilterApplyBtn + ": Button Clicked");
		Thread.sleep(Constants.ThreadSleep);

		WebElement live_FilterBar1 = driver.findElement(By.id(Constants.app_Package + ":id/filter_bar"));
		String str_FilterBar1 = live_FilterBar1.getText();
		System.out.println("Filter Name " + str_FilterBar1);
		live_FilterBar1.click();
		System.out.println(str_FilterBar1 + " is Selected");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver, "NMA_2437_2438", "Screen2");

		WebElement live_FilterGroup = driver.findElement(By.id(Constants.app_Package + ":id/filter_group").name("Content"));
		String str_FilterGroup = live_FilterGroup.getText();
		live_FilterGroup.click();
		System.out.println(str_FilterGroup + ": group is Expanded");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver, "NMA_2437_2438", "Screen3");

		WebElement live_FilterChildren_MyPacks = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_container").id(Constants.app_Package + ":id/filter_children")
				.name("My Packs"));
		String str_FilterChildren_MyPacks = live_FilterChildren_MyPacks.getText();
		live_FilterChildren_MyPacks.click();
		System.out.println(str_FilterChildren_MyPacks + ": Category selected");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver, "NMA_2437_2438", "Screen4");

		WebElement live_FilterGroup1 = driver.findElement(By.id(Constants.app_Package + ":id/filter_group").name("Available On"));
		String str_FilterGroup1 = live_FilterGroup1.getText();
		live_FilterGroup1.click();
		System.out.println(str_FilterGroup1 + ": group is Expanded");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver, "NMA_2437_2438", "Screen5");

		WebElement live_FilterChildren_Mobile = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_container").id(Constants.app_Package + ":id/filter_children")
				.name("Mobile"));
		String str_FilterChildren_Mobile = live_FilterChildren_Mobile.getText();
		live_FilterChildren_Mobile.click();
		System.out.println(str_FilterChildren_Mobile + ": Category selected");
		Thread.sleep(10000);

		util.takeScreenShot(driver, "NMA_2437_2438", "Screen6");

		WebElement live_FilterApplyBtn1 = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_apply_btn"));
		String str_FilterApplyBtn1 = live_FilterApplyBtn1.getText();
		live_FilterApplyBtn1.click();
		System.out.println(str_FilterApplyBtn1 + ": Button Clicked");
		Thread.sleep(10000);

		util.takeScreenShot(driver, "NMA_2437_2438", "Screen7");

		WebElement live_FilterBar3 = driver.findElement(By.id(Constants.app_Package + ":id/filter_bar"));
		String str_FilterBar3 = live_FilterBar3.getText();
		System.out.println(str_FilterBar3);

		live_FilterBar.click();
		System.out.println(str_FilterBar + " is Selected");
		Thread.sleep(Constants.ThreadSleep);

		WebElement live_FilterReset1 = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_reset"));
		String str_FilterReset1 = live_FilterReset1.getText();
		live_FilterReset1.click();
		System.out.println(str_FilterReset1 + ": Button Clicked");
		Thread.sleep(Constants.ThreadSleep);

		WebElement live_FilterApplyBtn2 = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_apply_btn"));
		String str_FilterApplyBtn2 = live_FilterApplyBtn2.getText();
		live_FilterApplyBtn2.click();
		System.out.println(str_FilterApplyBtn2 + ": Button Clicked");
		Thread.sleep(Constants.ThreadSleep);

		WebElement live_FilterBar2 = driver.findElement(By.id(Constants.app_Package + ":id/filter_bar"));
		String str_FilterBar2 = live_FilterBar1.getText();
		System.out.println("Filter Name " + str_FilterBar2);
		live_FilterBar2.click();
		System.out.println(str_FilterBar2 + " is Selected");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver, "NMA_2437_2438", "Screen8");

		WebElement live_FilterGroup2 = driver.findElement(By.id(Constants.app_Package + ":id/filter_group").name("Content"));
		String str_FilterGroup2 = live_FilterGroup2.getText();
		live_FilterGroup2.click();
		System.out.println(str_FilterGroup2 + ": group is Expanded");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver, "NMA_2437_2438", "Screen9");

		WebElement live_FilterChildren_MyPacks2 = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_container")
				.id(Constants.app_Package + ":id/filter_children").name("My Packs"));
		String str_FilterChildren_MyPacks2 = live_FilterChildren_MyPacks.getText();
		live_FilterChildren_MyPacks2.click();
		System.out.println(str_FilterChildren_MyPacks2 + ": Category selected");
		Thread.sleep(Constants.ThreadSleep);

		WebElement live_FilterChildren_Free2 = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_container").id(Constants.app_Package + ":id/filter_children")
				.name("Free"));
		String str_FilterChildren_Free2 = live_FilterChildren_Free2.getText();
		live_FilterChildren_Free2.click();
		System.out.println(str_FilterChildren_Free2 + ": Category selected");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver, "NMA_2437_2438", "Screen10");

		WebElement live_FilterGroup3 = driver.findElement(By.id(Constants.app_Package + ":id/filter_group").name("Available On"));
		String str_FilterGroup3 = live_FilterGroup1.getText();
		live_FilterGroup3.click();
		System.out.println(str_FilterGroup3 + ": group is Expanded");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver, "NMA_2437_2438", "Screen11");

		WebElement live_FilterChildren_Mobile3 = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_container").id(Constants.app_Package + ":id/filter_children")
				.name("Mobile"));
		String str_FilterChildren_Mobile3 = live_FilterChildren_Mobile3.getText();
		live_FilterChildren_Mobile3.click();
		System.out.println(str_FilterChildren_Mobile3 + ": Category selected");
		Thread.sleep(Constants.ThreadSleep);

		WebElement live_FilterChildren_Tablet3 = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_container").id(Constants.app_Package + ":id/filter_children")
				.name("Tablet"));
		String str_FilterChildren_Tablet3 = live_FilterChildren_Tablet3.getText();
		live_FilterChildren_Tablet3.click();
		System.out.println(str_FilterChildren_Tablet3 + ": Category selected");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver, "NMA_2437_2438", "Screen12");

		WebElement live_FilterApplyBtn3 = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_apply_btn"));
		String str_FilterApplyBtn3 = live_FilterApplyBtn3.getText();
		live_FilterApplyBtn3.click();
		System.out.println(str_FilterApplyBtn3 + ": Button Clicked");
		Thread.sleep(10000);

		WebElement live_FilterBar4 = driver.findElement(By.id(Constants.app_Package + ":id/filter_bar"));
		String str_FilterBar4 = live_FilterBar4.getText();
		System.out.println(str_FilterBar4);
		util.takeScreenShot(driver, "NMA_2437_2438", "Screen13");
		Assert.assertEquals("FILTER - My Packs, Free, Mobile, Tablet", str_FilterBar4);

		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");
		System.out.println("End TestCases  ---> TestCaseIDs: NMA_2437_2438 ---- JIRA ID: NMA-6197_6200");
		util.fileWriting("End TestCases  ---> TestCaseIDs: NMA_2437_2438 ---- JIRA ID: NMA-6197_6200");
		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");
	}

	@Test
	public void Test_NMA_2443() throws Exception, InterruptedException {

		System.out.println("Start TestCases  ---> TestCaseIDs: NMA_2443 ---- JIRA ID: NMA-6201");
		util.fileWriting("Start TestCases  ---> TestCaseIDs: NMA_2443 ---- JIRA ID: NMA-6201");
		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");

		WebElement live_Tab = driver.findElement(By.id(Constants.app_Package + ":id/live_guide_tab"));
		String str_liveTab = live_Tab.getText();
		live_Tab.click();
		System.out.println(str_liveTab + ": Clicked on Tab");
		Thread.sleep(5000);

		util.takeScreenShot(driver, "NMA_2443", "Screen1");

		WebElement live_FilterBar = driver.findElement(By.id(Constants.app_Package + ":id/filter_bar"));
		String str_FilterBar = live_FilterBar.getText();
		live_FilterBar.click();
		System.out.println(str_FilterBar + " is Selected");
		Thread.sleep(Constants.ThreadSleep);

		WebElement live_FilterReset = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_reset"));
		String str_FilterReset = live_FilterReset.getText();
		live_FilterReset.click();
		System.out.println(str_FilterReset + ": Button Clicked");
		Thread.sleep(Constants.ThreadSleep);

		WebElement live_FilterApplyBtn = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_apply_btn"));
		String str_FilterApplyBtn = live_FilterApplyBtn.getText();
		live_FilterApplyBtn.click();
		System.out.println(str_FilterApplyBtn + ": Button Clicked");
		Thread.sleep(Constants.ThreadSleep);

		WebElement live_FilterBar1 = driver.findElement(By.id(Constants.app_Package + ":id/filter_bar"));
		String str_FilterBar1 = live_FilterBar1.getText();
		System.out.println("Filter Name " + str_FilterBar1);
		live_FilterBar1.click();
		System.out.println(str_FilterBar1 + " is Selected");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver, "NMA_2443", "Screen2");

		WebElement live_FilterGroup = driver.findElement(By.id(Constants.app_Package + ":id/filter_group").name("Content"));
		String str_FilterGroup = live_FilterGroup.getText();
		live_FilterGroup.click();
		System.out.println(str_FilterGroup + ": group is Expanded");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver, "NMA_2443", "Screen3");

		WebElement live_FilterChildren_MyPacks = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_container").xpath(
				"//android.widget.LinearLayout[5]/android.widget.LinearLayout[1]/android.widget.TextView[1]"));
		String str_FilterChildren_MyPacks = live_FilterChildren_MyPacks.getText();
		live_FilterChildren_MyPacks.click();
		System.out.println(str_FilterChildren_MyPacks + ": Category selected");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver, "NMA_2443", "Screen4");

		WebElement live_FilterGroup1 = driver.findElement(By.id(Constants.app_Package + ":id/filter_group").name("Available On"));
		String str_FilterGroup1 = live_FilterGroup1.getText();
		live_FilterGroup1.click();
		System.out.println(str_FilterGroup1 + ": group is Expanded");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver, "NMA_2443", "Screen5");

		WebElement live_FilterChildren_Mobile = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_container").xpath(
				"//android.widget.LinearLayout[4]/android.widget.LinearLayout[1]/android.widget.TextView[1]"));
		String str_FilterChildren_Mobile = live_FilterChildren_Mobile.getText();
		live_FilterChildren_Mobile.click();
		System.out.println(str_FilterChildren_Mobile + ": Category selected");
		Thread.sleep(10000);

		util.takeScreenShot(driver, "NMA_2443", "Screen6");

		WebElement live_FilterApplyBtn1 = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_apply_btn"));
		String str_FilterApplyBtn1 = live_FilterApplyBtn1.getText();
		live_FilterApplyBtn1.click();
		System.out.println(str_FilterApplyBtn1 + ": Button Clicked");
		Thread.sleep(10000);

		WebElement live_FilterBar3 = driver.findElement(By.id(Constants.app_Package + ":id/filter_bar"));
		String str_FilterBar3 = live_FilterBar3.getText();
		System.out.println(str_FilterBar3);
		util.takeScreenShot(driver, "NMA_2443", "Screen7");
		Assert.assertEquals("FILTER - Premium, Free", str_FilterBar3);
		live_FilterBar.click();
		System.out.println(str_FilterBar3 + " is Selected");
		Thread.sleep(Constants.ThreadSleep);

		WebElement live_FilterReset1 = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_reset"));
		String str_FilterReset1 = live_FilterReset1.getText();
		live_FilterReset1.click();
		System.out.println(str_FilterReset1 + ": Button Clicked");
		Thread.sleep(Constants.ThreadSleep);

		WebElement live_FilterApplyBtn2 = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_apply_btn"));
		String str_FilterApplyBtn2 = live_FilterApplyBtn2.getText();
		live_FilterApplyBtn2.click();
		System.out.println(str_FilterApplyBtn2 + ": Button Clicked");
		Thread.sleep(Constants.ThreadSleep);

		WebElement live_FilterBar2 = driver.findElement(By.id(Constants.app_Package + ":id/filter_bar"));
		String str_FilterBar2 = live_FilterBar2.getText();
		System.out.println("Filter Name " + str_FilterBar2);
		live_FilterBar2.click();
		System.out.println(str_FilterBar2 + " is Selected");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver, "NMA_2443", "Screen8");

		WebElement live_FilterGroup2 = driver.findElement(By.id(Constants.app_Package + ":id/filter_group").name("Content"));
		String str_FilterGroup2 = live_FilterGroup2.getText();
		live_FilterGroup2.click();
		System.out.println(str_FilterGroup2 + ": group is Expanded");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver, "NMA_2443", "Screen9");

		WebElement live_FilterChildren_MyPacks2 = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_container").xpath(
				"//android.widget.LinearLayout[3]/android.widget.LinearLayout[1]/android.widget.TextView[1]"));
		String str_FilterChildren_MyPacks2 = live_FilterChildren_MyPacks2.getText();
		live_FilterChildren_MyPacks2.click();
		System.out.println(str_FilterChildren_MyPacks2 + ": Category selected");
		Thread.sleep(Constants.ThreadSleep);

		WebElement live_FilterChildren_Free2 = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_container").xpath(
				"//android.widget.LinearLayout[4]/android.widget.LinearLayout[1]/android.widget.TextView[1]"));
		String str_FilterChildren_Free2 = live_FilterChildren_Free2.getText();
		live_FilterChildren_Free2.click();
		System.out.println(str_FilterChildren_Free2 + ": Category selected");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver, "NMA_2443", "Screen10");

		WebElement live_FilterGroup3 = driver.findElement(By.id(Constants.app_Package + ":id/filter_group").name("Available On"));
		String str_FilterGroup3 = live_FilterGroup3.getText();
		live_FilterGroup3.click();
		System.out.println(str_FilterGroup3 + ": group is Expanded");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver, "NMA_2443", "Screen11");

		WebElement live_FilterChildren_Mobile3 = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_container").xpath(
				"//android.widget.LinearLayout[8]/android.widget.LinearLayout[1]/android.widget.TextView[1]"));
		String str_FilterChildren_Mobile3 = live_FilterChildren_Mobile3.getText();
		live_FilterChildren_Mobile3.click();
		System.out.println(str_FilterChildren_Mobile3 + ": Category selected");
		Thread.sleep(Constants.ThreadSleep);

		WebElement live_FilterChildren_Tablet3 = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_container").xpath(
				"//android.widget.LinearLayout[9]/android.widget.LinearLayout[1]/android.widget.TextView[1]"));
		String str_FilterChildren_Tablet3 = live_FilterChildren_Tablet3.getText();
		live_FilterChildren_Tablet3.click();
		System.out.println(str_FilterChildren_Tablet3 + ": Category selected");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver, "NMA_2443", "Screen12");

		WebElement live_FilterApplyBtn3 = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_apply_btn"));
		String str_FilterApplyBtn3 = live_FilterApplyBtn3.getText();
		live_FilterApplyBtn3.click();
		System.out.println(str_FilterApplyBtn3 + ": Button Clicked");
		Thread.sleep(10000);

		WebElement live_FilterBar4 = driver.findElement(By.id(Constants.app_Package + ":id/filter_bar"));
		String str_FilterBar4 = live_FilterBar4.getText();
		System.out.println(str_FilterBar4);
		util.takeScreenShot(driver, "NMA_2443", "Screen13");
		Assert.assertEquals("FILTER - My Packs, Free, Mobile, Tablet", str_FilterBar4);
		live_FilterBar4.click();
		Thread.sleep(Constants.ThreadSleep);

		WebElement live_FilterReset2 = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_reset"));
		String str_FilterReset2 = live_FilterReset2.getText();
		live_FilterReset2.click();
		System.out.println(str_FilterReset2 + ": Button Clicked");
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver, "NMA_2443", "Screen14");

		WebElement live_FilterApplyBtn4 = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_apply_btn"));
		String str_FilterApplyBtn4 = live_FilterApplyBtn4.getText();
		live_FilterApplyBtn4.click();
		System.out.println(str_FilterApplyBtn4 + ": Button Clicked");
		Thread.sleep(Constants.ThreadSleep);

		WebElement live_FilterBar5 = driver.findElement(By.id(Constants.app_Package + ":id/filter_bar"));
		String str_FilterBar5 = live_FilterBar5.getText();
		System.out.println("Filter Name " + str_FilterBar5);
		util.takeScreenShot(driver, "NMA_2443", "Screen15");
		Assert.assertEquals("FILTER", str_FilterBar5);
		Thread.sleep(Constants.ThreadSleep);

		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");
		System.out.println("End TestCases  ---> TestCaseIDs: NMA_2443 ---- JIRA ID: NMA-6201");
		util.fileWriting("End TestCases  ---> TestCaseIDs: NMA_2443 ---- JIRA ID: NMA-6201");
		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");
	}

	@SuppressWarnings("static-access")
	@Test
	public void Test_NMA_9680() throws Exception {

		try {
			List<WebElement> menuTabs = driver.findElements(By.id(Constants.app_Package + ":id/tabs").className("android.widget.TextView"));
			for (WebElement menu : menuTabs) {
				String menuName = menu.getText();
				System.out.println("name is" + menuName);
				if (menuName.equalsIgnoreCase("Clips"))
					SwipeAndScroll.scrollOnTab(driver, menu, menuName);
				if (menuName.equalsIgnoreCase("Shows") || menuName.equalsIgnoreCase("Movies") || menuName.equalsIgnoreCase("Clips")) {
					menu.click();
					System.out.println("Cliked on"+menuName);
					Thread.sleep(5000);
					WebElement BTNfilter = driver.findElement(By.id(Constants.app_Package + ":id/filter_bar"));
					BTNfilter.click();
					WebElement DLGFilters = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_container"));
					Assert.assertTrue(DLGFilters.isEnabled(), "Error: Filter Dailog is not Displayed");
					List<WebElement> filterHeaders = driver.findElements(By.id(Constants.app_Package + ":id/filter_group"));
					for (WebElement filterHeader : filterHeaders) {
						String StrfilterHeader = filterHeader.getText();
						System.out.println(StrfilterHeader);
						if (StrfilterHeader.equalsIgnoreCase("Content Rating"))
							break;
					}
					WebElement BTNfilterCancel = driver.findElement(By.id(Constants.app_Package + ":id/dialog_filters_cancel_btn"));
					BTNfilterCancel.click();
				}
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}
}
