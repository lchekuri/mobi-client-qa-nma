
package com.mobitv.client.qa.nma.regression.mobile;

import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.mobitv.client.qa.nma.common.*;

public class TestPackageError {

	private static Util util = new Util();
	private static WebDriver driver = null;
	private ExecuteShellComand command = new ExecuteShellComand();
	private DataParser mJsonParser = new DataParser();
	DBUtil db = new DBUtil();
	String user_id;

	@BeforeTest
	public void setUp() throws Exception {

		driver = util.getAndroidDriver(driver);
		command.executeCommand("Settings");
	}

	@AfterTest
	public void tearDown() throws Exception {

		driver.quit();
		command.kill();
		Thread.sleep(Constants.ThreadSleep);
	}

	public void Package_Error() throws Exception, InterruptedException {

		try {
			JSch jsch = new JSch();

			String host = "rtv@svcar01p1.func-cp.qa.smf1.mobitv";
			String password = "e0rtv12";

			String user = host.substring(0, host.indexOf('@'));
			host = host.substring(host.indexOf('@') + 1);

			Session session = jsch.getSession("rtv", "svcar01p1.func-cp.qa.smf1.mobitv", 22);
			session.setConfig("StrictHostKeyChecking", "no");
			session.setPassword(password);

			session.connect();
			Channel channel = (ChannelSftp) session.openChannel("sftp");
			channel.connect();
			ChannelSftp sftpChannel = (ChannelSftp) channel;
			sftpChannel.cd("/opt/mobi-stub-sprint-ws/webapp/WEB-INF");
			sftpChannel.put(TestPackageError.class.getResourceAsStream("/applicationContext.xml"), "applicationContext.xml");
			System.out.println("File placed in folder");
			NoPromptRemoteHostUtil nprhu = new NoPromptRemoteHostUtil("10.174.27.105", 22, "rtv", "e0rtv12");
			nprhu.connect();
			System.out.println("connected");
			String commandRestart = "sudo service tomcat8080 restart";
			nprhu.exec(commandRestart, true, "e0rtv12");
			System.out.println("restarted");
			Thread.sleep(20000);
		} catch (Throwable t) {
			t.printStackTrace();
		}
	}



	@Test
	public void Test_Package_Error() throws Exception, InterruptedException {

		Package_Error();
		System.out.println("Done");

		WebElement wb_Menu = driver.findElement(By.name("Open navigation drawer"));
		wb_Menu.click();
		System.out.println("Clicked on Menu");
		Thread.sleep(Constants.ThreadSleep);
		
		System.out.println("//Get user_id from settings");
		WebElement wb_settings = driver.findElement(By.id(Constants.app_Package + "id/cell_edit_screen").name("Settings"));
		wb_settings.click();
		WebElement wb_user_id = driver.findElement(By.id(Constants.app_Package + ":id/user_id_value"));
		user_id= wb_user_id.getText().substring(0, 5);
		System.out.println("=====Userid is====="+user_id);
		
		wb_Menu.click();
		WebElement wb_ShopMenu = driver.findElement(By.id(Constants.app_Package + ":id/cell_edit_screen").name("Shop"));
		String str_wbShopMenu = wb_ShopMenu.getText();
		wb_ShopMenu.click();
		System.out.println("Clicked on Menu : " + str_wbShopMenu);
		Thread.sleep(Constants.ThreadSleep);

		List<WebElement> pack_price_list = driver.findElements(By.id(Constants.app_Package + ":id/shop_package_price"));

		String str_ShopOfferURL = "http://msfrn-func-cp-qa.mobitv.com/aggregator/v5/managedlist/" + Constants.carrier + "/" + Constants.product + "/" + Constants.version
				+ "/tile/shop-offers.json";

		JSONObject jsnObj_ShopOffeURL = mJsonParser.callHttpRequest(str_ShopOfferURL, "GET", null);
		Thread.sleep(Constants.ThreadSleep);

		JSONArray jsnArry_TileItems = jsnObj_ShopOffeURL.getJSONArray("tile_items");

		for (int i = 0; i < jsnArry_TileItems.length(); i++) {
			
			System.out.println("=====Offer====" + i);
			JSONObject jsnObj_TileItem = jsnArry_TileItems.getJSONObject(i);
			int int_RefID = jsnObj_TileItem.getInt("ref_id");
			System.out.println("int_RefID: " + int_RefID);
			String str_Name = jsnObj_TileItem.getString("name");
			String offer_type = jsnObj_TileItem.getString("offer_type");
			System.out.println("str_Name: " + str_Name);
			
			if (str_Name.equalsIgnoreCase("Premium offer 2")) {
				if (pack_price_list.get(i).getText().startsWith("Subscription ends on")) {
					System.out.println("update DB field");
					db.updateExpireDate(int_RefID,user_id);
					Thread.sleep(90000);

					// Refresh cache to effect DB changes
					System.out.println("======Refresh cache to effect DB changes=====");
					wb_Menu.click();
					WebElement wb_config = driver.findElement(By.id("com.mobitv.client.sprinttvng:id/cell_edit_screen"));
					wb_config.click();
					WebElement wb_clrcache = driver.findElement(By.id("com.mobitv.client.sprinttvng:id/easteregg_btn_clear"));
					wb_clrcache.click();
					WebElement wb_confirm = driver.findElement(By.id("android:id/button1"));
					wb_confirm.click();
					Thread.sleep(10000);

					// Clicked on Price button to purchase package
					System.out.println("======Clicked on Price button to purchase package=====");
					wb_Menu.click();
					wb_ShopMenu.click();
					pack_price_list.get(i).click();
					WebElement wb_OfferDetailsPurchase_Btn = driver.findElement(By.id(Constants.app_Package + ":id/offer_details_purchase_btn"));
					wb_OfferDetailsPurchase_Btn.click();
					System.out.println("=====Clicked on purchase button=====");
					Thread.sleep(10000);

					// Clicked on Subscription confirmation dialogue
					System.out.println("=====Clicked on Subscription confirmation popup=====");
					WebElement wb_SubscribeButton = driver.findElement(By.id(Constants.app_Package + ":id/dialog_offers_positive_btn"));
					wb_SubscribeButton.click();
					System.out.println("=====Clicked on Subscribe=====");
					Thread.sleep(10000);

					// Verify Error code
					System.out.println("=====Error code verification=====");
					WebElement wb_alert_title = driver.findElement(By.id("android:id/alertTitle"));
					WebElement wb_alert_mesg = driver.findElement(By.id("android:id/message"));
					WebElement wb_ok_btn = driver.findElement(By.id("android:id/button2"));

					Assert.assertEquals("Error processing your subscription", wb_alert_title.getText());
					Assert.assertEquals("errand code for error code [91]", wb_alert_title.getText());
					Assert.assertEquals("OK", wb_alert_title.getText());
					wb_ok_btn.click();

					// Navigate to home screen
					driver.navigate().back();
					driver.navigate().back();

				}

			}
		}
	}
}
