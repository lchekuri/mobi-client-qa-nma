package com.mobitv.client.qa.nma.regression.server;

import java.util.List;

import junit.framework.Assert;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.mobitv.client.qa.nma.common.Constants;
import com.mobitv.client.qa.nma.common.DataParser;
import com.mobitv.client.qa.nma.common.ExecuteShellComand;
import com.mobitv.client.qa.nma.common.SwipeAndScroll;
import com.mobitv.client.qa.nma.common.Util;

public class TestMusic {

	WebElement wb_MusicName;
	private static Util util = new Util();
	private static WebDriver driver = null;
	private DataParser mJsonParser = new DataParser();
	private JSONObject jsnObj_MusicReltVideoURL= null;
	private ExecuteShellComand command = new ExecuteShellComand();

	@Before
	public void setUp() throws Exception {		
		driver = util.getAndroidDriver(driver);
		command.executeCommand("Music");
	}

	@After
	public void tearDown() {
		driver.quit();
		command.kill();
	}

	@SuppressWarnings("unused")
	@Test
	public void Test_Music() throws Exception{

		System.out.println("Start Test_Music");
		util.fileWriting("Start Test_Music");
		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");

		WebElement wb_Tabs = driver.findElement(By.id(Constants.app_Package+":id/tabs"));
		SwipeAndScroll.scrollOnTab(driver, wb_Tabs, "Music");
		Thread.sleep(5000);

		WebElement Music_Tab = driver.findElement(By.id(Constants.app_Package+":id/music_tab"));
		String str_MusicTab = Music_Tab.getText();
		Music_Tab.click();		
		System.out.println("Clicked on TAB : "+str_MusicTab);
		util.fileWriting("Clicked on TAB : "+str_MusicTab);
		Thread.sleep(5000);

		util.takeScreenShot(driver,"Test_Music","Music_MainScreen");

		System.out.println(Constants.Music_MainURL);
		JSONObject jsnObj_MusicMainURL = mJsonParser.callHttpRequest(Constants.Music_MainURL,"GET", null);
		Thread.sleep(Constants.ThreadSleep);

		String str_MusicTotal = jsnObj_MusicMainURL.getString("total");
		System.out.println("str_MusicTotal : "+str_MusicTotal);
		util.fileWriting("str_MusicTotal : "+str_MusicTotal);
		JSONArray jsnArray_Hits = jsnObj_MusicMainURL.getJSONArray("hits");

		int l=3;
		for(int i=2; i<jsnArray_Hits.length(); i++){
			System.out.println("**********"+(i+1)+"**********");
			util.fileWriting("**********"+(i+1)+"**********");
			JSONObject json_MusicsInfo = jsnArray_Hits.getJSONObject(i);
			JSONObject json_MusicResult = json_MusicsInfo.getJSONObject("result");
			JSONObject json_MusicSeries = json_MusicResult.getJSONObject("vod");
			String str_MusicID = json_MusicSeries.getString("id");
			//System.out.println("str_MusicID : "+str_MusicID);
			String str_MusicName = json_MusicSeries.getString("name");
			//System.out.println("str_MusicName : "+str_MusicName);
			try{
				if(l==1){
					wb_MusicName = driver.findElement(By.id(Constants.app_Package+":id/base_grid").xpath("//android.widget.LinearLayout["+l+"]").id(Constants.app_Package+":id/cell_area_txt").id(Constants.app_Package+":id/cell_txt_title"));
				}else{
					wb_MusicName = driver.findElement(By.id(Constants.app_Package+":id/base_grid").xpath("//android.widget.LinearLayout["+l+"]/android.widget.LinearLayout[1]/android.widget.TextView[1]"));
				}
			}catch(Exception e){
				System.out.println("X-Path of element not found");
			}
			String str_wbMusicName = wb_MusicName.getText();

			if(str_MusicName != null){
				if(str_MusicName.equalsIgnoreCase(str_wbMusicName)){

					util.takeScreenShot(driver,"Test_Music","Music_"+(i+1));

					wb_MusicName.click();
					System.out.println("Clicked on MUSIC : "+str_wbMusicName);
					util.fileWriting("Clicked on MUSIC : "+str_wbMusicName);
					Thread.sleep(Constants.ThreadSleep);
					if(Constants.app_Package == "com.mobitv.client.connect.mobile"){
						jsnObj_MusicReltVideoURL = mJsonParser.callHttpRequest(Constants.Music_ReltVideoURL+str_MusicID,"GET", null);
						//System.out.println(Constants.Music_ReltVideoURL+str_MusicID);
					}else{
						jsnObj_MusicReltVideoURL = mJsonParser.callHttpRequest(Constants.Music_SprintBoostReltVideoURL+str_MusicID,"GET", null);
						//System.out.println(Constants.Music_SprintBoostReltVideoURL+str_MusicID);
					}

					Thread.sleep(Constants.ThreadSleep);

					String str_MusicReltVideoTotal = jsnObj_MusicReltVideoURL.getString("total");
					//System.out.println("str_MusicTotal : "+str_MusicReltVideoTotal);
					JSONArray jsnArray_ReltVideoHits = jsnObj_MusicReltVideoURL.getJSONArray("hits");

					//List<WebElement> list_MusicsReltVideo = driver.findElements(By.id(Constants.app_Package+":id/details_list").id(Constants.app_Package+":id/cell_txt_title"));
					//if(list_MusicsReltVideo.size()>0){
					List<WebElement> list_MusicsReltVideo = null;
					for(int j=0; j<jsnArray_ReltVideoHits.length(); j++){
						try{
							list_MusicsReltVideo = driver.findElements(By.id(Constants.app_Package+":id/details_list").id(Constants.app_Package+":id/cell_txt_title"));
							if(list_MusicsReltVideo.size()>1){
								JSONObject json_MusicsReltVideosInfo = jsnArray_ReltVideoHits.getJSONObject(j);
								JSONObject json_MusicReltVideosResult = json_MusicsReltVideosInfo.getJSONObject("result");
								JSONObject json_MusicReltVideosVOD = json_MusicReltVideosResult.getJSONObject("vod");
								String str_MusicReltVideosID = json_MusicReltVideosVOD.getString("id");
								//System.out.println("str_MusicReltVideosID : "+str_MusicReltVideosID);
								String str_MusicReltVideosName = json_MusicReltVideosVOD.getString("name");
								System.out.println("str_MusicReltVideosName : "+str_MusicReltVideosName);
								util.fileWriting("str_MusicReltVideosName : "+str_MusicReltVideosName);
								String str_MusicReltVideosDescription = json_MusicReltVideosVOD.getString("description");
								//System.out.println("str_MusicReltVideosDescription : "+str_MusicReltVideosDescription);
								String str_wbMusicReltVideosName = list_MusicsReltVideo.get(j+1).getText();
								System.out.println("wb_MusicReltVideosName : "+str_wbMusicReltVideosName);
								//Assert.assertEquals(str_MusicReltVideosName, str_wbMusicReltVideosName);
								util.takeScreenShot(driver,"Test_Music","Music_"+(i+1)+"-Deatils");
							}
						}catch(JSONException e){
							System.out.println("The Discription Data Not Found");
						}
						if(j>=(list_MusicsReltVideo.size()/2))
							break;

					}
					//}

					driver.navigate().back();
					Thread.sleep(Constants.ThreadSleep);
				}else{
					Assert.failNotEquals("MUSIC-"+(i+1)+" data mismatched with JSON response. ", str_MusicName, str_wbMusicName);
				}
			}else{
				System.out.println("Music Data is not available");
			}

			if (l == 3)
				l = 4;
			else{
				l = 3;
				if(i<4){
					try{
						//driver.swipe(100, 790, 100, 35, 2500);
						if(Constants.DeviceType.equalsIgnoreCase("Large")){
							SwipeAndScroll.swipeWithConstantValues(driver, 100.0, 800.0, 100.0, 30.0, 3.0);
						}else if(Constants.DeviceType.equalsIgnoreCase("Small")){
							SwipeAndScroll.swipeWithConstantValues(driver, 100.0, 420.0, 100.0, 30.0, 3.0);
						}else{
							throw new AssertionError("Please Check DeviceType in Constants.java file");
						}
						Thread.sleep(Constants.ThreadSleep);
					}catch(Exception e){
						e.getMessage();
					}
				}
				else
				{
					//driver.swipe(100, 670, 100, 35,2800);
					if(Constants.DeviceType.equalsIgnoreCase("Large")){
						SwipeAndScroll.swipeWithConstantValues(driver, 100.0, 680.0, 100.0, 30.0, 3.0);
					}else if(Constants.DeviceType.equalsIgnoreCase("Small")){
						SwipeAndScroll.swipeWithConstantValues(driver, 100.0, 350.0, 100.0, 30.0, 3.0);
					}else{
						throw new AssertionError("Please Check DeviceType in Constants.java file");
					}
					Thread.sleep(Constants.ThreadSleep);
				}
			}

			int length=(jsnArray_Hits.length()/4);

			if(i==20)
				break;

		}

		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");
		System.out.println("End Test_Music");
		util.fileWriting("End Test_Music");
		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");
	}
}
