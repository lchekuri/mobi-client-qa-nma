package com.mobitv.client.qa.nma.regression.server;

import junit.framework.Assert;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.mobitv.client.qa.nma.common.Constants;
import com.mobitv.client.qa.nma.common.DataParser;
import com.mobitv.client.qa.nma.common.ExecuteShellComand;
import com.mobitv.client.qa.nma.common.SwipeAndScroll;
import com.mobitv.client.qa.nma.common.Util;

public class TestMovies {

	WebElement wb_MovieName;
	private static Util util = new Util();
	private static WebDriver driver = null;
	private DataParser mJsonParser = new DataParser();
	private ExecuteShellComand command = new ExecuteShellComand();

	@Before
	public void setUp() throws Exception {		
		driver = util.getAndroidDriver(driver);
		command.executeCommand("Movies");
	}

	@After
	public void tearDown() {
		driver.quit();
		command.kill();
	}


	@Test
	public void Test_Movies() throws Exception{

		System.out.println("Start Test_Movies");
		util.fileWriting("Start Test_Movies");
		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");

		//if(Constants.app_Package  == "com.mobitv.client.connect.mobile"){
		if(Constants.app_Package.equals("com.mobitv.client.connect.mobile")){
			WebElement wb_Tabs = driver.findElement(By.id(Constants.app_Package+":id/tabs"));
			SwipeAndScroll.scrollOnTab(driver, wb_Tabs, "Movies");
			Thread.sleep(5000);

			WebElement Movies_Tab = driver.findElement(By.id(Constants.app_Package+":id/movies_tab"));
			String str_MoviesTab = Movies_Tab.getText();
			Movies_Tab.click();		
			System.out.println("Clicked on TAB : "+str_MoviesTab);
			util.fileWriting("Clicked on TAB : "+str_MoviesTab);
			Thread.sleep(5000);

			util.takeScreenShot(driver,"Test_Movies","Movies_MainScreen");

			JSONObject jsnObj_MovieMainURL = mJsonParser.callHttpRequest(Constants.Movies_MainURL,"GET", null);
			Thread.sleep(Constants.ThreadSleep);

			String str_MovieTotal = jsnObj_MovieMainURL.getString("total");
			System.out.println("str_MovieTotal : "+str_MovieTotal);
			util.fileWriting("str_MovieTotal : "+str_MovieTotal);
			JSONArray jsnArray_Hits = jsnObj_MovieMainURL.getJSONArray("hits");

			int l=3;
			for(int i=2; i<jsnArray_Hits.length(); i++){
				System.out.println("**********"+(i+1)+"**********");
				util.fileWriting("**********"+(i+1)+"**********");
				JSONObject json_MoviesInfo = jsnArray_Hits.getJSONObject(i);
				JSONObject json_MovieResult = json_MoviesInfo.getJSONObject("result");
				JSONObject json_MovieSeries = json_MovieResult.getJSONObject("vod");
				String str_MovieID = json_MovieSeries.getString("id");
				System.out.println("str_MovieID : "+str_MovieID);
				util.fileWriting("str_MovieID : "+str_MovieID);
				String str_MovieName = json_MovieSeries.getString("name");
				System.out.println("str_MovieName : "+str_MovieName);
				util.fileWriting("str_MovieName : "+str_MovieName);
				try{
					String str_MovieDescription = json_MovieSeries.getString("description");
					System.out.println("str_MovieDescription : "+str_MovieDescription);
				}catch(JSONException e){
					System.out.println("The Discription Data Not Found");
				}
				String str_MovieNetwork = json_MovieSeries.getString("network");
				//System.out.println("str_MovieNetwork : "+str_MovieNetwork);
				try{
					if(l==1){
						wb_MovieName = driver.findElement(By.id(Constants.app_Package+":id/base_grid").xpath("//android.widget.LinearLayout["+l+"]").id(Constants.app_Package+":id/cell_area_txt").id(Constants.app_Package+":id/cell_txt_title"));
					}else{
						wb_MovieName = driver.findElement(By.id(Constants.app_Package+":id/base_grid").xpath("//android.widget.LinearLayout["+l+"]/android.widget.LinearLayout[1]/android.widget.TextView[1]"));
					}
				}catch(Exception e){
					System.out.println("X-Path of the element not found");
				}
				String str_wbMovieName = wb_MovieName.getText();


				if(str_MovieName!=null){
					if(str_MovieName.equalsIgnoreCase(str_wbMovieName)){
						util.takeScreenShot(driver,"Test_Movies","Movie_"+(i+1));
						wb_MovieName.click();
						System.out.println("Clicked on MOVIE : "+str_wbMovieName);
						util.fileWriting("Clicked on MOVIE : "+str_wbMovieName);
						Thread.sleep(Constants.ThreadSleep);

						WebElement wb_MovieNetwork = driver.findElement(By.id(Constants.app_Package+":id/action_bar").xpath("//android.widget.LinearLayout[1]/android.widget.LinearLayout[1]/android.widget.TextView[1]"));
						String str_wbMovieNetwork = wb_MovieNetwork.getText();
						Assert.assertEquals(str_MovieNetwork, str_wbMovieNetwork);
						System.out.println("str_MovieNetwork : "+str_MovieNetwork);
						util.fileWriting("str_MovieNetwork : "+str_MovieNetwork);

						WebElement wb_MovieDesc = driver.findElement(By.id(Constants.app_Package+":id/details_txt_desc"));
						String str_wbMovieDesc = wb_MovieDesc.getText();
						//Assert.assertEquals(str_MovieDescription, str_wbMovieDesc);
						System.out.println("str_wbMovieDesc : "+str_wbMovieDesc);
						//util.fileWriting("str_MovieDescription : "+str_MovieDescription);
						util.takeScreenShot(driver,"Test_Movies","Movie_"+(i+1)+"-Deatils");
						driver.navigate().back();
						Thread.sleep(Constants.ThreadSleep);
					}else{
						Assert.failNotEquals("MOVIE-"+(i+1)+" data mismatched with JSON response. ", str_MovieName, str_wbMovieName);
					}
				}else{
					System.out.println("Movie Data is not available");
				}

				if (l == 3)
					l = 4;
				else{
					l = 3;
					if(i<4){
						try{
							//driver.swipe(100, 790, 100, 35, 2500);
							if(Constants.DeviceType.equalsIgnoreCase("Large")){
								SwipeAndScroll.swipeWithConstantValues(driver, 100.0, 800.0, 100.0, 30.0, 3.0);
							}else if(Constants.DeviceType.equalsIgnoreCase("Small")){
								SwipeAndScroll.swipeWithConstantValues(driver, 100.0, 420.0, 100.0, 30.0, 3.0);
							}else{
								throw new AssertionError("Please Check DeviceType in Constants.java file");
							}
							Thread.sleep(Constants.ThreadSleep);
						}catch(Exception e){
							e.getMessage();
						}
					}
					else
					{
						//driver.swipe(100, 670, 100, 35,2800);
						if(Constants.DeviceType.equalsIgnoreCase("Large")){
							SwipeAndScroll.swipeWithConstantValues(driver, 100.0, 680.0, 100.0, 30.0, 3.0);
						}else if(Constants.DeviceType.equalsIgnoreCase("Small")){
							SwipeAndScroll.swipeWithConstantValues(driver, 100.0, 350.0, 100.0, 30.0, 3.0);
						}else{
							throw new AssertionError("Please Check DeviceType in Constants.java file");
						}
						Thread.sleep(Constants.ThreadSleep);
					}
				}

				int length=(jsnArray_Hits.length()/4);
				if(i==20)
					break;
			}
		}

		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");
		System.out.println("End Test_Movies");
		util.fileWriting("End Test_Movies");
		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");
	}
}
