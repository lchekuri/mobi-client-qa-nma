package com.mobitv.client.qa.nma.regression.server;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.mobitv.client.qa.nma.common.Constants;
import com.mobitv.client.qa.nma.common.DataParser;
import com.mobitv.client.qa.nma.common.ExecuteShellComand;
import com.mobitv.client.qa.nma.common.SwipeAndScroll;
import com.mobitv.client.qa.nma.common.Util;

public class TestLive {
	private JSONArray tile_items,jsnArray_LiveTileItems, jArray_Programs;
	private static Util util = new Util();
	private static WebDriver driver = null;
	private DataParser mJsonParser = new DataParser();
	private ExecuteShellComand command = new ExecuteShellComand();
	WebElement wb_MovieName = null;
	WebElement wb_ChannelPlayIcon= null;
	WebElement wb_ClipName= null;
	WebElement wb_ClipNetwork = null;
	WebElement wb_MusicName = null;

	@Before
	public void setUp() throws Exception {		
		driver = util.getAndroidDriver(driver);
		command.executeCommand("Live");
	}

	@After
	public void tearDown() {
		driver.quit();
		command.kill();
	}


	/*
	 * 1.Launch Application
	 * 2.Select on LIVE tab
	 * 3.Check Each Channel Number, Name, Current ProgramName, Duration and Scroll UP
	 * 4.Check Thumb Image of Channels
	 * 5.Click on FULL GUIDE
	 * 6.Check Full guide (EPG) is launched as a child page
	 * 7.FULL GUIDE is title bar on child page
	 * 8.Check Each Channel Number and Click on it.
	 * 9.Verify Channel Name, Description and ProgramNames in  ProgramLineUp Screen
	 * 10.Click on Back Button to Navigate Previous Page
	 * 11.Click on Back Button to navigate Live MainPage and check data is refreshed
	 */
	@Test
	public void Test_Live() throws Exception{
		System.out.println("Start TestCase  ---> Test_Live()");
		System.out.println("==========================================================================");

		WebElement Live_Tab = driver.findElement(By.id(Constants.app_Package+":id/live_guide_tab"));
		String str_LiveTab = Live_Tab.getText();
		Live_Tab.click();		
		System.out.println("Clicked on TAB : "+str_LiveTab);
		util.fileWriting("Clicked on TAB : "+str_LiveTab);
		Thread.sleep(5000);

		util.takeScreenShot(driver,"Live","Screen1");

		JSONObject jObj_ChannelListing = mJsonParser.callHttpRequest(Constants.Live_MainURL,"GET", null);
		Thread.sleep(Constants.ThreadSleep);
		jsnArray_LiveTileItems = jObj_ChannelListing.getJSONArray("tile_items");

		WebElement wb_ChannelThumb = driver.findElement(By.id(Constants.app_Package+":id/channel_thumb"));
		Assert.assertTrue(wb_ChannelThumb.isEnabled());
		System.out.println("The Thumbn Image is Available");

		for (int i = 0; i < jsnArray_LiveTileItems.length(); i++) {
			WebElement wb_ChannelNumber = driver.findElement(By.id(Constants.app_Package+":id/channel_number"));
			String str_wbChannelNumber = wb_ChannelNumber.getText();
			System.out.println("str_wbChannelNumber: "+str_wbChannelNumber);
			WebElement wb_ChannelName = driver.findElement(By.id(Constants.app_Package+":id/channel_name"));
			String str_wbChannelName = wb_ChannelName.getText();
			System.out.println("str_wbChannelName: "+str_wbChannelName);
			WebElement wb_ProgramName = driver.findElement(By.id(Constants.app_Package+":id/program_name"));
			String str_wbProgramName = wb_ProgramName.getText();
			System.out.println("str_wbProgramName: "+str_wbProgramName);
			WebElement wb_ProgramDuration = driver.findElement(By.id(Constants.app_Package+":id/program_duration"));
			String str_wbProgramDuration = wb_ProgramDuration.getText();
			System.out.println("str_wbProgramDuration: "+str_wbProgramDuration);

			JSONObject json_TileItem = jsnArray_LiveTileItems.getJSONObject(i);

			String str_ChannelPosition = json_TileItem.getString("display_position");
			//System.out.println("str_ChannelPosition: "+str_ChannelPosition);
			String str_ChannelName = json_TileItem.getString("name");
			//System.out.println("str_ChannelName: "+str_ChannelName);
			String str_ChannelRefID = json_TileItem.getString("ref_id");
			//System.out.println("str_ChannelRefID: "+str_ChannelRefID);

			Assert.assertEquals(str_wbChannelNumber,	str_ChannelPosition);
			Assert.assertEquals(str_wbChannelName,	str_ChannelName);

			util.takeScreenShot(driver,"Live","Screen2");

			try{
				//driver.swipe(100, 790, 100, 35, 2500);
				if(Constants.DeviceType.equalsIgnoreCase("Large")){
					SwipeAndScroll.swipeWithConstantValues(driver, 100.0, 880.0, 100.0, 30.0, 3.0);
				}else if(Constants.DeviceType.equalsIgnoreCase("Small")){
					SwipeAndScroll.swipeWithConstantValues(driver, 100.0, 443.0, 100.0, 30.0, 2.0);
				}else{
					throw new AssertionError("Please Check DeviceType in Constants.java file");
				}
				Thread.sleep(Constants.ThreadSleep);
			}catch(Exception e){
				e.getMessage();
			}
			if(i>=(jsnArray_LiveTileItems.length()/2)){
				break;
			}
		}

		WebElement wb_FullGuide = driver.findElement(By.id(Constants.app_Package+":id/full_guide"));
		String str_FullGuide = wb_FullGuide.getText();
		wb_FullGuide.click();
		Thread.sleep(Constants.ThreadSleep);
		System.out.println("Clicked on "+str_FullGuide+" and Full guide (EPG) is launched as a child page");

		util.takeScreenShot(driver,"Live","Screen3");

		List<WebElement> list_LiveChannelPosition = driver.findElements(By.id(Constants.app_Package+":id/channel_txt_num"));
		List<WebElement> list_LiveChannelThumb = driver.findElements(By.id(Constants.app_Package+":id/channel_thumb"));

		//for (int i = 0; i < jsnArray_LiveTileItems.length(); i++) {
		for (int i = 0; i < list_LiveChannelPosition.size(); i++) {
			String str_wbChannelPosition = list_LiveChannelPosition.get(i).getText();
			System.out.println("str_wbChannelPosition: "+str_wbChannelPosition);
			util.fileWriting("str_wbChannelPosition: "+str_wbChannelPosition);

			JSONObject json_TileItem = jsnArray_LiveTileItems.getJSONObject(i);
			String str_ChannelName = json_TileItem.getString("name");
			System.out.println("str_ChannelName: "+str_ChannelName);
			util.fileWriting("str_ChannelName: "+str_ChannelName);
			String str_ChannelPosition = json_TileItem.getString("display_position");
			System.out.println("str_ChannelPosition: "+str_ChannelPosition);
			util.fileWriting("str_ChannelPosition: "+str_ChannelPosition);
			try{
				String str_ChannelDesc = json_TileItem.getString("description");
				System.out.println("str_ChannelDesc: "+str_ChannelDesc);
				util.fileWriting("str_ChannelDesc: "+str_ChannelDesc);
			}catch(JSONException e){
				System.out.println("The Discription Data Not Found");
			}
			String str_ChannelRefID = json_TileItem.getString("ref_id");
			System.out.println("str_ChannelRefID: "+str_ChannelRefID);
			util.fileWriting("str_ChannelRefID: "+str_ChannelRefID);

			Assert.assertEquals(str_wbChannelPosition,	str_ChannelPosition);

			list_LiveChannelThumb.get(i).click();
			System.out.println("Clicked on Live CHANNEL-"+str_wbChannelPosition);
			util.fileWriting("Clicked on Live CHANNEL-"+str_wbChannelPosition);
			Thread.sleep(5000);

			util.takeScreenShot(driver,"Live","Screen4_"+i);

			WebElement wb_LiveChannelName = driver.findElement(By.id(Constants.app_Package+":id/channel_details_channel_name"));
			String str_wbLiveChannelName = wb_LiveChannelName.getText();
			Assert.assertEquals(str_wbLiveChannelName,	str_ChannelName);

			WebElement wb_LiveChannelDesc = driver.findElement(By.id(Constants.app_Package+":id/channel_details_description"));
			String str_wbLiveChannelDesc = wb_LiveChannelDesc.getText();
			//Assert.assertEquals(str_wbLiveChannelDesc,	str_ChannelDesc);

			long epoch = System.currentTimeMillis() / 1000;

			JSONObject jObj_ChannelEPG = mJsonParser.callHttpRequest(Constants.live_ProgramLineupURL+str_ChannelRefID+ "&ml=en&start-time="+ epoch+ "&timeslice=86400","GET", null);
			Thread.sleep(5000);
			try{
				jArray_Programs = jObj_ChannelEPG.getJSONArray("programs");

				List<WebElement> list_ChannelProgram = driver.findElements(By.id(Constants.app_Package+":id/program_cell_name"));

				//for (int j = 0; j < jArray_Programs.length(); j++) {
				for (int j = 0; j < list_ChannelProgram.size(); j++) {
					String str_wbChannelProgramName = list_ChannelProgram.get(j).getText();
					System.out.println("str_wbChannelProgramName: "+str_wbChannelProgramName);

					JSONObject json_Program = jArray_Programs.getJSONObject(j);
					String str_ChannelProgramName = json_Program.getString("name");
					System.out.println("str_ChannelProgramName: "+str_ChannelProgramName);
					util.fileWriting("str_ChannelProgramName: "+str_ChannelProgramName);


					if(j==3)
						break;

				}
			}catch(Exception e){
				System.out.println(e.getMessage());
			}

			driver.navigate().back();
			Thread.sleep(Constants.ThreadSleep);

			if(i == 5)
				break;
		}

		driver.navigate().back();
		Thread.sleep(Constants.ThreadSleep);

		util.takeScreenShot(driver,"Live","Screen5");

		System.out.println("==========================================================================");
		System.out.println("End TestCase  ---> Test_Live()");
		System.out.println("==========================================================================");
	}

}
