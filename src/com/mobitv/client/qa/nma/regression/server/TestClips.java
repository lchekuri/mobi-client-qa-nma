package com.mobitv.client.qa.nma.regression.server;

import io.appium.java_client.AppiumDriver;
import junit.framework.Assert;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.mobitv.client.qa.nma.common.Constants;
import com.mobitv.client.qa.nma.common.DataParser;
import com.mobitv.client.qa.nma.common.ExecuteShellComand;
import com.mobitv.client.qa.nma.common.SwipeAndScroll;
import com.mobitv.client.qa.nma.common.Util;

public class TestClips {
	WebElement wb_ClipName, wb_ClipNetwork;
	private static Util util = new Util();
	//private static WebDriver driver = null;
	private static AppiumDriver driver = null;
	private DataParser mJsonParser = new DataParser();
	private ExecuteShellComand command = new ExecuteShellComand();

	@Before
	public void setUp() throws Exception {		
		driver = util.getAndroidDriver(driver);
		command.executeCommand("Clips");
	}

	@After
	public void tearDown() {
		driver.quit();
		command.kill();
	}

	@Test
	public void Test_Clips() throws Exception{

		System.out.println("Start Test_Clips");
		util.fileWriting("Start Test_Clips");
		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");

		WebElement wb_Tabs = driver.findElement(By.id(Constants.app_Package+":id/tabs"));
		SwipeAndScroll.scrollOnTab(driver, wb_Tabs, "Clips");
		Thread.sleep(5000);

		WebElement Clip_Tab = driver.findElement(By.id(Constants.app_Package+":id/clips_tab"));
		String str_ClipTab = Clip_Tab.getText();
		Clip_Tab.click();		
		System.out.println("Clicked on TAB : "+str_ClipTab);
		util.fileWriting("Clicked on TAB : "+str_ClipTab);
		Thread.sleep(5000);

		util.takeScreenShot(driver,"Test_Clips","Clips_MainScreen");

		JSONObject jsnObj_ClipMainURL = mJsonParser.callHttpRequest(Constants.Clips_MainURL,"GET", null);
		Thread.sleep(Constants.ThreadSleep);

		String str_ClipTotal = jsnObj_ClipMainURL.getString("total");
		System.out.println("str_ClipTotal : "+str_ClipTotal);
		util.fileWriting("str_ClipTotal : "+str_ClipTotal);
		JSONArray jsnArray_Hits = jsnObj_ClipMainURL.getJSONArray("hits");

		int l=3;
		for(int i=2; i<jsnArray_Hits.length(); i++){
			System.out.println("**********"+(i+1)+"**********");
			util.fileWriting("**********"+(i+1)+"**********");
			JSONObject json_ClipsInfo = jsnArray_Hits.getJSONObject(i);
			JSONObject json_ClipResult = json_ClipsInfo.getJSONObject("result");
			JSONObject json_ClipSeries = json_ClipResult.getJSONObject("vod");

			String str_ClipID = json_ClipSeries.getString("id");
			System.out.println("str_ClipID : "+str_ClipID);
			util.fileWriting("str_ClipID : "+str_ClipID);
			String str_ClipName = json_ClipSeries.getString("name");
			System.out.println("str_ClipName : "+str_ClipName);
			util.fileWriting("str_ClipName : "+str_ClipName);
			try{
				String str_ClipDescription = json_ClipSeries.getString("description");
				System.out.println("str_ClipDescription : "+str_ClipDescription);
			}catch(JSONException e){
				System.out.println("The Discription Data Not Found");
			}

			String str_ClipNetwork = json_ClipSeries.getString("network");
			System.out.println("str_ClipNetwork : "+str_ClipNetwork);

			try{
				if(l==1){
					wb_ClipNetwork = driver.findElement(By.id(Constants.app_Package+":id/base_grid").xpath("//android.widget.LinearLayout["+l+"]").id(Constants.app_Package+":id/cell_area_txt").id(Constants.app_Package+":id/cell_txt_title"));
					wb_ClipName = driver.findElement(By.id(Constants.app_Package+":id/base_grid").xpath("//android.widget.LinearLayout["+l+"]").id(Constants.app_Package+":id/cell_area_txt").id(Constants.app_Package+":id/cell_txt_desc"));
				}else{
					wb_ClipNetwork = driver.findElement(By.id(Constants.app_Package+":id/base_grid").xpath("//android.widget.LinearLayout["+l+"]/android.widget.LinearLayout[1]/android.widget.TextView[1]"));
					wb_ClipName = driver.findElement(By.id(Constants.app_Package+":id/base_grid").xpath("//android.widget.LinearLayout["+l+"]/android.widget.LinearLayout[1]/android.widget.TextView[2]"));
				}
			}catch(Exception e){
				System.out.println("X-Path of element not found");
			}
			String str_wbClipName = wb_ClipName.getText();
			String str_wbClipNetwork = wb_ClipNetwork.getText();

			if(str_ClipName!=null && str_ClipNetwork!=null){
				if(str_ClipName.equalsIgnoreCase(str_wbClipName)|| str_ClipNetwork.equalsIgnoreCase(str_wbClipNetwork)){

					util.takeScreenShot(driver,"Test_Clips","Clip_"+(i+1));

					wb_ClipName.click();

					System.out.println("Clicked on CLIP: "+str_wbClipName);
					util.fileWriting("Clicked on CLIP: "+str_wbClipName);
					Thread.sleep(Constants.ThreadSleep);

					WebElement wb_ActionBarTitle = driver.findElement(By.id(Constants.app_Package+":id/action_bar").xpath("//android.widget.LinearLayout[1]/android.widget.LinearLayout[1]/android.widget.TextView[1]"));
					String str_wbActionBarTitle = wb_ActionBarTitle.getText();
					Assert.assertEquals(str_ClipNetwork, str_wbActionBarTitle);
					System.out.println("str_ClipNetwork : "+str_ClipNetwork);
					util.fileWriting("str_ClipNetwork : "+str_ClipNetwork);

					WebElement wb_ClipDesc = driver.findElement(By.id(Constants.app_Package+":id/details_txt_desc"));
					String str_wbClipDesc = wb_ClipDesc.getText();
					//	Assert.assertEquals(str_ClipDescription, str_wbClipDesc);
					//System.out.println("str_ClipDescription : "+str_ClipDescription);
					//util.fileWriting("str_ClipDescription : "+str_ClipDescription);
					util.takeScreenShot(driver,"Test_Clips","Clip_"+(i+1)+"-Deatils");
					driver.navigate().back();
					Thread.sleep(Constants.ThreadSleep);
				}
				else{
					Assert.failNotEquals("CLIP-"+(i+1)+" data mismatched with JSON response. ", str_ClipName, str_wbClipName);
					Assert.failNotEquals("CLIP-"+(i+1)+" data mismatched with JSON response. ", str_ClipNetwork, str_wbClipNetwork);
				}
			}else{
				System.out.println("Clip Data is not available");
			}

			if (l == 3)
				l = 4;
			else{
				l = 3;
				if(i<4){
					try{
						//driver.swipe(100, 790, 100, 35, 2500);
						if(Constants.DeviceType.equalsIgnoreCase("Large")){
							SwipeAndScroll.swipeWithConstantValues(driver, 100.0, 800.0, 100.0, 30.0, 3.0);
						}else if(Constants.DeviceType.equalsIgnoreCase("Small")){
							SwipeAndScroll.swipeWithConstantValues(driver, 100.0, 420.0, 100.0, 30.0, 3.0);
						}else{
							throw new AssertionError("Please Check DeviceType in Constants.java file");
						}
						Thread.sleep(Constants.ThreadSleep);
					}catch(Exception e){
						e.getMessage();
					}
				}
				else
				{
					//driver.swipe(100, 670, 100, 35,2800);
					if(Constants.DeviceType.equalsIgnoreCase("Large")){
						SwipeAndScroll.swipeWithConstantValues(driver, 100.0, 680.0, 100.0, 30.0, 3.0);
					}else if(Constants.DeviceType.equalsIgnoreCase("Small")){
						SwipeAndScroll.swipeWithConstantValues(driver, 100.0, 350.0, 100.0, 30.0, 3.0);
					}else{
						throw new AssertionError("Please Check DeviceType in Constants.java file");
					}
					Thread.sleep(Constants.ThreadSleep);
				}
			}
			if(i==20)
				break;
		}

		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");
		System.out.println("End Test_Clips");
		util.fileWriting("End Test_Clips");
		System.out.println("==========================================================================");
		util.fileWriting("==========================================================================");
	}
}
