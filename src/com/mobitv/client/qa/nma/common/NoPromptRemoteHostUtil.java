package com.mobitv.client.qa.nma.common;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Properties;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

public class NoPromptRemoteHostUtil {
	private String host;
	private int port;
	private String username;
	private String password;

	private String errorString = null;

	private int exitCode;

	public NoPromptRemoteHostUtil(String host, int port, String username, String password) {
		this.host = host;
		this.port = port;
		this.username = username;
		this.password = password;

	}

	/**
	 * Start a new ssh session.
	 */
	public Session connect() {
		try {
			JSch jsch = new JSch();
			Session session = jsch.getSession(username, host, port);
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			session.setConfig(config);
			session.setPassword(password);

			try {
				session.connect();
			} catch (Exception e) {
				throw e;
			}
			return session;
		} catch (Exception e) {
			String message = "Error creating session to " + host + " as user " + username + ": " + e.getMessage();
			throw new RuntimeException(message, e);
		}
	}

	/**
	 * Closes the ssh session. This is deprecated and does nothing. I just left
	 * it here to preserve backward compatibility. Please remove calls to this
	 * method
	 * 
	 * @deprecated
	 */
	public void disconnect() {
	}

	/**
	 * Remotely execute command.
	 * 
	 * @param command
	 */

	public String exec(String command) {
		return exec(command, false, null);
	}

	public String exec(String command, boolean sudo, String sudoPassword) {

		final String sudoPasswordToken = "e0rtv12";

		StringBuilder sb = new StringBuilder();

		try {
			Session session = connect();
			try {
				ChannelExec channelExec = (ChannelExec) session.openChannel("exec");
				try {

					String fullCommand = sudo ? "sudo -S -p '" + sudoPasswordToken + "\n' " + command : command;

					channelExec.setCommand(fullCommand);

					BufferedReader in = new BufferedReader(new InputStreamReader(channelExec.getInputStream()));
					OutputStream out = channelExec.getOutputStream();
					channelExec.setErrStream(System.err);

					channelExec.setPty(true);
					channelExec.connect();

					String nextLine;
					while ((nextLine = in.readLine()) != null) {

						if (nextLine.equals(sudoPasswordToken)) {
							out.write((sudoPassword + "\n").getBytes());
							out.flush();
						} else {
							sb.append(nextLine + "\n");
						}
						// process the line.
					}
				} finally {
					if (channelExec != null) {
						channelExec.disconnect();
					}

				}

			} finally {
				if (session != null) {
					session.disconnect();
				}

			}

		} catch (Exception e) {

		}

		return sb.toString();
	}

	public Integer getExitValue() {
		return exitCode;
	}

	public String getFileCOntents(String filePath) {
		return exec("cat " + filePath);
	}

	public Properties getProperties(String filePath) {
		Properties remoteProperties = new Properties();

		String fileContents = getFileCOntents(filePath);

		try {
			InputStream is = new ByteArrayInputStream(fileContents.getBytes("UTF-8"));
			remoteProperties.load(is);
		} catch (Exception e) {

		}

		return remoteProperties;
	}

	public String getErrorString() {
		return errorString;
	}
}
