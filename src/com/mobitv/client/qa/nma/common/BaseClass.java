package com.mobitv.client.qa.nma.common;

import io.appium.java_client.AppiumDriver;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class BaseClass {
	public static Util util = new Util();
	//private static WebDriver driver = null;
	public static AppiumDriver driver = null;
	public DataParser mJsonParser = new DataParser();
	public ExecuteShellComand command = new ExecuteShellComand();
	 @BeforeMethod
	  public void beforeMethod() {
		  driver = util.getAndroidDriver(driver);
			command.executeCommand("Clips");
	  }

	  @AfterMethod
	  public void afterMethod() throws InterruptedException {
		  driver.quit();
			command.kill();
			Thread.sleep(Constants.ThreadSleep);
	  }
}
