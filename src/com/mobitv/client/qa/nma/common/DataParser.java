package com.mobitv.client.qa.nma.common;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

public class DataParser {

	private HttpClient httpClient = HttpClientBuilder.create().build();
	
	/**
	 * 
	 * @param url
	 * @param method
	 * @param params
	 * @return
	 */
	public JSONObject callHttpRequest(String url, String method, List<NameValuePair> params) { 
		
		JSONObject m_JSONObject = null;
		HttpResponse httpResponse = null;
		
		System.out.println("Request URL: " + url);
		
		try {

			if ("POST".equalsIgnoreCase(method)) {
				HttpPost httpPost = new HttpPost(url);
				// httpPost.setEntity(new UrlEncodedFormEntity(params));
				httpResponse = httpClient.execute(httpPost);
			} else if ("GET".equalsIgnoreCase(method)) {
				HttpGet httpGet = new HttpGet(url);
				httpResponse = httpClient.execute(httpGet);
			}

			if (httpResponse != null && httpResponse.getEntity() != null) {
				String jsonObj = EntityUtils.toString(httpResponse.getEntity() , "UTF-8");
				m_JSONObject = new JSONObject(jsonObj);
			}else{
				System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ null response from server or someother exception@@@@@@@@@@@@@@@@@@@@@@@@@");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return m_JSONObject;
	}

	/*public JSONObject callHttpRequest1(String url, String method, List<NameValuePair> params) {
		try {
			System.out.println("Request URL: " + url);
			if (method == "POST") {
				DefaultHttpClient httpClient = new DefaultHttpClient();
				HttpPost httpPost = new HttpPost(url);
				// httpPost.setEntity(new UrlEncodedFormEntity(params));
				HttpResponse httpResponse = httpClient.execute(httpPost);
				HttpEntity httpEntity = httpResponse.getEntity();
				is = httpEntity.getContent();

			} else if (method == "GET") {
				DefaultHttpClient httpClient = new DefaultHttpClient();
				
				 * String paramString = URLEncodedUtils.format(params, "utf-8");
				 * url += "?" + paramString;
				 
				HttpGet httpGet = new HttpGet(url);
				HttpResponse httpResponse = httpClient.execute(httpGet);
				HttpEntity httpEntity = httpResponse.getEntity();
				is = httpEntity.getContent();
			}

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return m_JSONObject;

	}
*/
}
