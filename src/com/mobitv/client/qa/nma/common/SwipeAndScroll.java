package com.mobitv.client.qa.nma.common;

import java.util.HashMap;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SwipeAndScroll {
	public static WebDriverWait wait;
	public static int[] location = new int[4];

	public static void scrollOnTab(WebDriver driver, WebElement m_WebElement, String searchTxt){
		HashMap<String, String> scrollObject = new HashMap<String, String>();
		JavascriptExecutor js = (JavascriptExecutor) driver;
		scrollObject.put("text", searchTxt);
		scrollObject.put("element",( (RemoteWebElement) m_WebElement).getId());
		js.executeScript("mobile: scrollTo", scrollObject);
	}

	public static void swipeWithConstantValues(WebDriver driver,Double startX, Double startY, Double endX, Double endY, Double duration) throws Exception, InterruptedException{
		JavascriptExecutor js1 = (JavascriptExecutor) driver;
		HashMap<String, Double> scrollObject = new HashMap<String, Double>();
		scrollObject.put("startX", startX);
		scrollObject.put("startY", startY);
		scrollObject.put("endX", endX);
		scrollObject.put("endY", endY);
		scrollObject.put("duration", duration);
		js1.executeScript("mobile: swipe", scrollObject);
	}


	public static void flicker(WebDriver driver, WebElement m_WebElement){
		Point coords = m_WebElement.getLocation();
		double startX = coords.x;
		double startY = coords.y;
		double endX = startX+(m_WebElement.getSize().width);
		double endY = startY+(m_WebElement.getSize().height);
		startX = endX-startX-30;
		startY = endY-endX;

		JavascriptExecutor js = (JavascriptExecutor) driver;
		HashMap<String, String> flickObject = new HashMap<String, String>();
		flickObject.put("startX", ""+startX);
		flickObject.put("startY", ""+startY);
		flickObject.put("endX", ""+0);
		flickObject.put("endY", ""+0);
		flickObject.put("touchCount", "1");
		flickObject.put("element", ((RemoteWebElement) m_WebElement).getId());
		js.executeScript("mobile: flick", flickObject);
	}

	public static int[]  getLocation(WebElement element){
		location[0] = element.getLocation().getX();
		location[1] = element.getLocation().getY();
		location[2] = element.getSize().getWidth()+location[0];
		location[3] = element.getSize().getHeight()+location[1];
		return location;
	}

	public static void tapOnElement(WebDriver driver, WebElement element) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		HashMap<String, Object> tapObject = new HashMap<String, Object>();
		tapObject.put("tapCount", 1);
		tapObject.put("element", ((RemoteWebElement) element).getId());
		js.executeScript("mobile: tap", tapObject);
		//System.out.println("Called Tap function");
	}

	public static void swipeGrid_One(WebDriver driver) throws Exception {
		Dimension size = driver.manage().window().getSize(); 
		double starty = (int) (size.height * 0.693); 
		double endy = (int) (size.height * 0.307); 
		double startx = size.width / 2; 
		//System.out.println("startx :"+startx+", starty: "+starty+", endy: "+endy);
		swipeWithConstantValues(driver, startx, starty, startx, endy, 2.0);
	}

	public static void swipeGrid_Two(WebDriver driver) throws Exception {
		Dimension size = driver.manage().window().getSize(); 
		double starty = (int) (size.height * 0.665); 
		double endy = (int) (size.height * 0.335); 
		double startx = size.width / 2; 
		//System.out.println("startx :"+startx+", starty: "+starty+", endy: "+endy);
		swipeWithConstantValues(driver, startx, starty, startx, endy, 2.0);
	}

	public static void swipeGrid_Three(WebDriver driver) throws Exception {
		Dimension size = driver.manage().window().getSize(); 
		double starty = (int) (size.height * 0.307); 
		double endy = (int) (size.height * 0.693); 
		double startx = size.width / 2; 
		//System.out.println("startx :"+startx+", starty: "+starty+", endy: "+endy);
		swipeWithConstantValues(driver, startx, starty, startx, endy, 2.0);
	}
	public static void swipeLeft(WebDriver driver) throws Exception {
		Dimension size = driver.manage().window().getSize(); 
		double startx = (int) (size.width * 0.8); 
		double endx = (int) (size.width * 0.20); 
		double starty = size.height / 2;  
		swipeWithConstantValues(driver, startx, starty, endx, starty, 2.0);
	}
	
	public static void swipeRight(WebDriver driver) throws Exception {
		Dimension size = driver.manage().window().getSize(); 
		double endx = (int) (size.width * 0.8); 
		double startx = (int) (size.width * 0.20); 
		double starty = size.height / 2;  
		swipeWithConstantValues(driver, startx, starty, endx, starty, 2.0);
	}

}
