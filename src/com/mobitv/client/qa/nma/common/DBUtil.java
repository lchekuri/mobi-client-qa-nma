package com.mobitv.client.qa.nma.common;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;

public class DBUtil {

	private static final String DB_DRIVER = "com.mysql.jdbc.Driver";
	private static final String DB_CONNECTION = "jdbc:mysql://mydtbvip.func-cp.qa.smf1.mobitv:3306/purchase_manager";
	private static final String DB_USER = "qa_user";
	private static final String DB_PASSWORD = "5unny_Day";

	DateandTimeClass dt = new DateandTimeClass();
	Connection connection = null;
	ResultSet rs;
	String sub_id = null;

	public void updateExpireDate(int offr_id, String user_id) throws SQLException, ParseException, ClassNotFoundException {
		try {
			Class.forName(DB_DRIVER);
			connection = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
			System.out.println("SQL Connection to database established!");
				
			String getSubId = "select * from SUBSCRIPTION where PURCHASED_BY like" + "'%"+ user_id +"%'" + "AND OFFER_ID=" + offr_id + ";";
			System.out.println(getSubId);
			Statement statement = connection.createStatement();
			rs = statement.executeQuery(getSubId);
			System.out.println(rs);
			while (rs.next()) {
				sub_id = rs.getString("SUBSCRIPTION_ID");
			}
			System.out.println("Subscruption id is" + sub_id);
			String utcDate1 = DateandTimeClass.getUTCtimeformat().substring(0, 10);
			String utctime1 = DateandTimeClass.getUTCtimeformat().substring(11, 17);
			String sub_date1 = utcDate1 + " " + utctime1+":00";

			String updateExpDate = "UPDATE purchase_manager.SUBSCRIPTION" + " SET EXPIRES_DATE ='" + sub_date1 + "'" + "where subscription_id = " + sub_id + ";";
			System.out.println(updateExpDate);
			PreparedStatement preparedStmt = connection.prepareStatement(updateExpDate);
			preparedStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		finally {
			try {
				if (connection != null)
					connection.close();
				System.out.println("Connection closed !!");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

}
