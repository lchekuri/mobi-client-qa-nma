package com.mobitv.client.qa.nma.common;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecuteResultHandler;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteException;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;





public class Util {
	private File file_ScreenShot;
	private File myFile = new File("C:/Log_console.txt");
	public static WebDriverWait wait;
	public static File appAPKDir = new File("C:/APK");
	public static File appAPK = new File(appAPKDir, Constants.apk);

	public WebDriver getAndroidDriver(WebDriver driver) {
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability("device", "Android");
		capabilities.setCapability(CapabilityType.BROWSER_NAME, "");
		capabilities.setCapability(CapabilityType.VERSION, "4.4.2");
		capabilities.setCapability(CapabilityType.PLATFORM, "WINDOWS");
		capabilities.setCapability("deviceName", "MobiTV");
		capabilities.setCapability("platformName", "Android");
		capabilities.setCapability("app-package",Constants.app_Package);
		capabilities.setCapability("app-activity", "com.mobitv.client.connect.mobile.StartupActivity");			
		capabilities.setCapability("appAPK", appAPK.getAbsolutePath());	
		capabilities.setCapability("newCommandTimeout", "100");
		try {
			driver = new RemoteWebDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
		} catch (MalformedURLException e) {
			System.out.println(e.getMessage());
		}
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		wait = new WebDriverWait(driver, 30000);
		return driver;		
	}

	public AppiumDriver getAndroidDriver(AppiumDriver driver) {
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability("device", "Android");
		capabilities.setCapability(CapabilityType.BROWSER_NAME, "");
		capabilities.setCapability(CapabilityType.VERSION, "4.4.2");
		capabilities.setCapability(CapabilityType.PLATFORM, "WINDOWS");
		capabilities.setCapability("deviceName", "MobiTV");
		capabilities.setCapability("platformName", "Android");
		capabilities.setCapability("app-package",Constants.app_Package);
		capabilities.setCapability("app-activity", "com.mobitv.client.connect.mobile.StartupActivity");			
		capabilities.setCapability("appAPK", appAPK.getAbsolutePath());			
		try {
			driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
		} catch (MalformedURLException e) {
			System.out.println(e.getMessage());
		}
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		wait = new WebDriverWait(driver, 30000);
		return driver;		
	}


	public void startAppium() {
		CommandLine command = new CommandLine("cmd");
		command.addArgument("cmd");
		command.addArgument("/c");
		command.addArgument("C:/Appium/node.exe");
		command.addArgument("C:/Appium/node_modules/appium/bin/appium.js/",	false);
		command.addArgument("--address", false);
		command.addArgument("127.0.0.1");
		command.addArgument("--port", false);
		command.addArgument("4725");
		command.addArgument("--app-pkg", false);
		command.addArgument(Constants.app_Package);
		command.addArgument("--app-activity");
		command.addArgument("com.mobitv.client.connect.mobile.StartupActivity");		

		DefaultExecuteResultHandler resultHandler = new DefaultExecuteResultHandler();
		DefaultExecutor executor = new DefaultExecutor();
		executor.setExitValue(1);

		try {
			executor.execute(command, resultHandler);
		} catch (ExecuteException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void stopAppium() {
		CommandLine command = new CommandLine("cmd");
		command.addArgument("/c");
		command.addArgument("taskkill");
		command.addArgument("/F");
		command.addArgument("/IM");
		command.addArgument("node.exe");
	}

	public static String timeAmPM(String s) {
		int h = Integer.parseInt(s.substring(0, 2));
		if (h > 12) {
			h = h - 12;
			if (h < 10) {
				return "0" + h + ":" + s.substring(3, 5) + " PM";
			}
			return h + ":" + s.substring(3, 5) + " PM";
		} else if (h < 10) {
			return "0" + h + ":" + s.substring(3, 5) + " PM";
		}
		return h + ":" + s.substring(3, 5) + " AM";
	}

	public void fileWriting(String message) {
		try {
			if (myFile.exists()) {
			} else {
				myFile.createNewFile();
			}
			Calendar c = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("dd:MM:yyyy HH:mm:ss a");
			String strDate = sdf.format(c.getTime());

			FileWriter myOutWriter = new FileWriter(myFile, true);
			myOutWriter.append(strDate + "---------" + message + "\n");
			myOutWriter.close();
		} catch (Exception e) {
		}
	}

	public void takeScreenShot(WebDriver driver, String str_StoryName, String str_ScreenShotName) throws Exception{
		file_ScreenShot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(file_ScreenShot, new File("c:\\"+Constants.buildVersion+"\\"+Constants.product+"\\" +str_StoryName+"\\"+ str_ScreenShotName+ ".png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		Thread.sleep(1000);
	}


}
