package com.mobitv.client.qa.nma.common;

public class Constants {



	public static final long ThreadSleep = 3000;	
	public static final String PLATFORM = "Android";

	public static final String WiFi_Password = "Labs%12&^";
	public static final String TV_DigitCode = "5981";

	public static final String ErrorMessage = "X-Path Of The Element Not Found";

	//public static final String DeviceType = "Small";
	public static final String DeviceType = "Large";

	// --------------Reference Application-------------------------

	/*public static final String apk = "com.mobitv.client.connect.mobile-1.2.13-debug.apk";
	public static final String app_Package = "com.mobitv.client.connect.mobile";
	public static final String vid = "data-prod-cp";
	public static final String product = "reference";
	public static final String carrier = "mobitv";
	public static final String version = "5.0";
	public static final String buildVersion = "1.2.20";*/

	// --------------Sprint Application-------------------------
	/*public static final String apk = "com.mobitv.client.sprinttvng-7.2.18-debug-signed.apk";
	public static final String app_Package = "com.mobitv.client.sprinttvng";
	public static final String vid = "data-prod-msp-sprint";
	public static final String product = "connect";
	public static final String carrier = "sprint";
	public static final String version = "7.0";
	public static final String buildVersion = "1.2.20";*/

	// --------------Boost Application-------------------------
	/*public static final String apk = "com.boostmobile.boosttv-7.1.56-debug_special.apk";
	public static final String app_Package = "com.boostmobile.boosttv";
	public static final String vid = "Data-prod-msp-boost";
	public static final String product = "connect";
	public static final String carrier = "boost";
	public static final String version = "7.0";
	public static final String buildVersion = "1.2.20";*/


	// --------------XL Application-------------------------
	/*public static final String apk = "id.co.movi-7.2.11-debug.apk";
	public static final String app_Package = "id.co.movi";
	public static final String vid = "data-prod-cp.mobitv.com";
	public static final String product = "xl";
	public static final String carrier = "connect_zing";
	public static final String version = "7.0";
	public static final String buildVersion = "1.2.20";*/
	
	// --------------T-Mobile Application-------------------------
	public static final String apk = "com.mobitv.client.tmobiletvhd-7.2.29-debug.apk";
	public static final String app_Package = "com.mobitv.client.tmobiletvhd";
	public static final String vid = "msfrn-func-msp-qa";
	public static final String product = "connect";
	public static final String carrier = "tmobile";
	public static final String version = "7.0";
	public static final String buildVersion = "1.2.20";


	//JSON URL LINKS
	
		
	public static final String Live_MainURL = "http://"+vid+".mobitv.com/aggregator/v5/managedlist/"+carrier+"/"+product+"/"+version+"/tile/channel-listing.json";
	public static final String live_ProgramLineupURL = "http://"+vid+".mobitv.com/guide/v5/program/"+carrier+"/"+product+"/"+version+"/default/programs.json?channels=";
	public static final String NewLive_MainURL = "http://"+vid+".mobitv.com/guide/v5/lineup/"+carrier+"/"+product+"/"+version+"/default/live/mobile-channels.json?offset=0&length=100";

	public static final String Clips_MainURL = "http://"+vid+".mobitv.com:80/guide/v5/search/"+carrier+"/"+product+"/"+version+"/default/search/details.json?ref_types=vod&sort_by=start_of_availability%2Cname&sort_direction=desc%2Casc&offset=0&length=100&em_format_ssi=clip%2Cfeature&category=tv%2Cnews%2Csports";
	public static final String Clips_ReltVideoURL = "http://"+vid+".mobitv.com:80/guide/v5/search/"+carrier+"/"+product+"/"+version+"/default/search/details.json?ref_types=vod&sort_by=start_of_availability&sort_direction=desc&offset=0&length=20&em_format_ssi=clip%2Cfeature&genre_list=reality%2Cinformational";

	public static final String Movies_MainURL = "http://"+vid+".mobitv.com:80/guide/v5/search/"+carrier+"/"+product+"/"+version+"/default/search/details.json?ref_types=vod&sort_by=start_of_availability%2Coriginal_air_date%2Cname&sort_direction=desc%2Cdesc%2Casc&offset=0&length=100&em_format_ssi=clip%2Cfeature&category=movies";

	public static final String Music_MainURL = "http://"+vid+".mobitv.com:80/guide/v5/search/"+carrier+"/"+product+"/"+version+"/default/search/details.json?ref_types=vod&sort_by=ott_popularity%2Cstart_of_availability%2Cname&sort_direction=desc%2Cdesc%2Casc&offset=0&length=100&category=music";
	public static final String Music_ReltVideoURL = "http://"+vid+".mobitv.com:80/guide/v5/search/"+carrier+"/"+product+"/"+version+"/default/search/details.json?ref_types=vod&sort_by=start_of_availability&sort_direction=desc&offset=0&length=10&category=music&genre_list=Pop%2Cmusic";
	public static final String Music_SprintBoostReltVideoURL = "http://"+vid+".mobitv.com:80/guide/v5/search/"+carrier+"/"+product+"/"+version+"/default/search/details.json?ref_types=vod&sort_by=start_of_availability&sort_direction=desc&offset=0&length=10&category=music&genre_list=music%2Cespanol%2Clatino";


	public static final String Network_MainURL = "http://"+vid+".mobitv.com/guide/v5/search/"+carrier+"/"+product+"/"+version+"/default/search.json?ref_types=vod%2Cchannel&offset=0&length=100&facets=network";
	public static final String Network_ChildPageURL = "http://"+vid+".mobitv.com/guide/v5/search/"+carrier+"/"+product+"/"+version+"/default/search/details.json?ref_types=vod&sort_by=start_of_availability%2Cname&sort_direction=desc%2Casc&offset=0&length=10&em_format_ssi=clip%2Cfeature&network=ACC%2BDigital%2BNetwork";
	public static final String Network_EpisodesURL = "http://"+vid+".mobitv.com/guide/v5/search/"+carrier+"/"+product+"/"+version+"/default/search/details.json?ref_types=vod&sort_by=episode_number%2Coriginal_air_date%2Cstart_of_availability%2Cname&sort_direction=desc%2Cdesc%2Cdesc%2Casc&offset=0&length=100&em_format_ssi=episode&series_id=";

	public static final String show_MainURL = "http://"+vid+".mobitv.com/guide/v5/search/"+carrier+"/"+product+"/"+version+"/default/search/details.json?ref_types=series&sort_by=latest_start_of_availability%2Cname&sort_direction=desc%2Casc&offset=0&length=100&em_format_ssi=episode";
	public static final String show_EpisodesURL = "http://"+vid+".mobitv.com/guide/v5/search/"+carrier+"/"+product+"/"+version+"/default/search/details.json?ref_types=vod&sort_by=episode_number%2Coriginal_air_date%2Cstart_of_availability%2Cname&sort_direction=desc%2Cdesc%2Cdesc%2Casc&offset=0&length=100&em_format_ssi=episode&series_id=";

	public static final String Search_Suggestions = "http://"+vid+".mobitv.com/guide/v5/search/"+carrier+"/"+product+"/"+version+"/default/search/suggest.json?q=vivo&length=10&content_types=program%2Cseries%2Cvod";
	public static final String Search_Details = "http://"+vid+".mobitv.com/guide/v5/search/"+carrier+"/"+product+"/"+version+"/default/search/details.json?q=vivo&ref_types=program%2Cseries%2Cvod&offset=0&length=100";
	
	public static final String widget_TileListHomeScreen = "http://"+vid+".mobitv.com/aggregator/v5/managedlist/"+carrier+"/"+product+"/"+version+"/tile-list/home-screen.json";
	public static final String widget_NewsClips = "http://"+vid+".mobitv.com/aggregator/v5/managedlist/"+carrier+"/"+product+"/"+version+"/tile/world-news.json";
	public static final String widget_Live = "http://"+vid+".mobitv.com/aggregator/v5/managedlist/"+carrier+"/"+product+"/"+version+"/tile/channel-listing.json";
	public static final String widget_MarketingTiles = "http://"+vid+".mobitv.com/aggregator/v5/managedlist/"+carrier+"/"+product+"/"+version+"/tile/marketing-tiles.json";
}
