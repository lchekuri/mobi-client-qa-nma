package com.mobitv.client.qa.nma.common;

import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class DateandTimeClass {
	static SimpleDateFormat format1 = new SimpleDateFormat("HHmm");
	static SimpleDateFormat format2 = new SimpleDateFormat("dd MMM yyyy");
	static SimpleDateFormat format3 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
	static Calendar calendar = Calendar.getInstance();
	static Date date = null;
	static String str;

	public static  String getcurrentDate()
	{
		str = getDay() + ", " + format2.format(new Date()) ;

		return str;
	}

	public static String getDay()
	{
		str = new DateFormatSymbols().getWeekdays()[calendar.get(Calendar.DAY_OF_WEEK)];
		return str;
	}

	public static String getCurrentTime()
	{
		Calendar c = Calendar.getInstance();
		String date = format1.format(c.getTime());
		return amorpm(date);
	}

	public static String amorpm(String s)
	{
		int h = Integer.parseInt(s.substring(0, 2));
		if(h>12)
		{
			h = h - 12;
			if(h<10)
			{
				return "0"+h+":"+s.substring(1, 3)+"PM";
			}
			return h+":"+s.substring(1, 3)+"PM";
		}
		else
			if(h<10)
			{
				return "0"+h+":"+s.substring(1, 3)+"PM";
			}
		return h+":"+s.substring(1, 3)+"AM";
	}

	public static String givemeLAtime(String gmtTime)
	{
		String str;
		try {
			date = format1.parse(gmtTime);
		} catch (ParseException e) {
			System.out.println("ParseException in Date and Time class");
		}
		calendar.setTime(date);
		calendar.add(Calendar.HOUR,-7);
		str = format1.format(calendar.getTime());
		return str;
	}

	public static String givemeINDtime(String gmtTime)
	{
		String str;
		try {
			date = format1.parse(gmtTime);
		} catch (ParseException e) {
			System.out.println("ParseException in Date and Time class");
		}
		calendar.setTime(date);
		calendar.add(Calendar.HOUR,5);
		calendar.add(Calendar.MINUTE,30);
		str = format1.format(calendar.getTime());
		return str;
	}

	public static String getdayofweek(long l){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date(l));
		int dow = calendar.get(Calendar.DAY_OF_WEEK);
		switch (dow) {
		case Calendar.MONDAY:
			return "Mon";
		case Calendar.TUESDAY:
			return "Tue";
		case Calendar.WEDNESDAY:
			return "Wed";
		case Calendar.THURSDAY:
			return "Thu";
		case Calendar.FRIDAY:
			return "Fri";
		case Calendar.SATURDAY:
			return "Sat";
		case Calendar.SUNDAY:
			return "Sun";
		}

		return "Unknown";
	}
	public static String getdayoftime(long timeInMilliSec, String dateFormat) {
		/*Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date(l));*/
		/*Date dt = new Date(l);
		System.out.println(dt.getHours());
		System.out.println(dt.getMinutes());

		DateFormat formatter = new SimpleDateFormat("hh:mm");
		System.out.println(l + " = " + formatter.format(calendar.getTime()));
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		System.out.println(l + ":"+sdf.format(new Date(l)));
		SimpleDateFormat sDateFormat = new SimpleDateFormat();
		sDateFormat.applyPattern("HH:mm");
		System.out.println(l + " srikanth = " + sDateFormat.format(dt));

		Date date = new Date();
        date.setTime(DateTimeHelper.getCurrentTimeMillis());
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int minutes = calendar.get(Calendar.MINUTE);
        int mod = minutes % 30;
        calendar.set(Calendar.MINUTE, minutes - mod);
        newScrollX = epochSecondsToScrollX(calendar.getTimeInMillis()/1000);*/
		/*Date date = new Date(timeInMilliSec*1000L); // *1000 is to convert seconds to milliseconds
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
		SimpleDateFormat sdf1 = new SimpleDateFormat("hh:mm");// the format of your date
		SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm");
		SimpleDateFormat sdf3 = new SimpleDateFormat("EEE");
		sdf.setTimeZone(TimeZone.getDefault());
		sdf1.setTimeZone(TimeZone.getDefault());
		String formattedDate = sdf.format(date);
		String formattedtime = sdf1.format(date);
		String formattedtime2 = sdf2.format(date);
		String formattedtime3 = sdf3.format(date);

		System.out.println(formattedDate);
		System.out.println(formattedtime);
		System.out.println(formattedtime2);
		System.out.println(formattedtime3);
		System.out.println(amorpm1(formattedtime2));*/
		Date date = new Date(timeInMilliSec*1000L); // *1000 is to convert seconds to milliseconds
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat); 
		/*SimpleDateFormat sdf1 = new SimpleDateFormat("hh:mm");// the format of your date
		SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm");
		SimpleDateFormat sdf3 = new SimpleDateFormat("EEE");*/
		sdf.setTimeZone(TimeZone.getDefault());
		//sdf1.setTimeZone(TimeZone.getDefault());
		String formattedDate = sdf.format(date);
		/*String formattedtime = sdf1.format(date);
		String formattedtime2 = sdf2.format(date);
		String formattedtime3 = sdf3.format(date);*/

		/*System.out.println(formattedDate);
		System.out.println(formattedtime);
		System.out.println(formattedtime2);
		System.out.println(formattedtime3);
		System.out.println(amorpm1(formattedtime2));*/

		return formattedDate;
	}

	public static String amorpm1(String s)
	{
		int h = Integer.parseInt(s.substring(0, 2));
		if(h>12)
		{
			h = h - 12;
			if(h<10)
			{
				return "0"+h+":"+s.substring(3, 5)+" PM";
			}
			return h+":"+s.substring(3, 5)+" PM";
		}
		else
			if(h<10)
			{
				return "0"+h+":"+s.substring(3, 5)+" PM";
			}
		return h+":"+s.substring(3, 5)+" AM";
	}
	public static String getUTCtimeformat() throws ParseException
	{
		Date date = new Date(System.currentTimeMillis()+1*90*1000);
		
		format3.setTimeZone(TimeZone.getTimeZone("UTC"));   // This line converts the given date into UTC time zone
		return format3.format(date);
	}

}