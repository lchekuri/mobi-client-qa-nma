package com.mobitv.client.qa.nma.common;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class GetPropertyValues {

	public String propFileName = null;

	public String getPropValues(String propName) throws IOException {

		Properties prop = new Properties();
		if (Constants.DeviceType.equalsIgnoreCase("mobile"))
			propFileName = "android-mobile-config.properties";
		else if (Constants.DeviceType.equalsIgnoreCase("tab"))
			propFileName = "android-tab-config.properties";
		InputStream inputStream = getClass().getClassLoader()
				.getResourceAsStream(propFileName);
		prop.load(inputStream);
		if (inputStream == null) {
			throw new FileNotFoundException("property file '" + propFileName+ "' not found in the classpath");
		}
		String result = prop.getProperty(propName);
		return result;

		// get the property value and print it out

	}
}
