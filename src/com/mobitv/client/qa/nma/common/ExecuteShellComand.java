package com.mobitv.client.qa.nma.common;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

public class ExecuteShellComand {
	public void executeCommand(String fileName) {
		//int iExitValue;
		try {
			Runtime.getRuntime().exec("cmd /c adb logcat > perl/"+fileName+".txt");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void executeCommand1og(String fileName) {
		//int iExitValue;
		try {
			Runtime.getRuntime().exec("cmd /c adb logcat > c:/mobi-client-qa-bilog/"+fileName+".txt");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void executeCommandinvokeperl(String perlscript) {		
		try {			
			String currdir = System.getProperty("user.dir")+"\\perl\\";
		//	System.setProperty("user.dir", currdir);
			//String[] cmd = {"C:/Perl64/bin/perl.exe",  perlscript, "C:/perl/"};
			Process p = Runtime.getRuntime().exec("perl.exe " + currdir +  perlscript);
			//ProcessBuilder pb = new ProcessBuilder("C:/Perl64/bin/perl.exe C:\\perl\\"+perlscript);
			//Process p = pb.start();
			BufferedReader is = new BufferedReader
		            ( new InputStreamReader(p.getInputStream()));
		        String sLine;
		        while ((sLine = is.readLine()) != null) {
		            System.out.println(sLine);
		        }
		        System.out.flush();

		        /* print final result of process */
		        System.err.println("Exit status=" + p.exitValue());
		       // System.err.println("Exit status=" + p.getErrorStream().read());
			} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void executeAdbKeyEvent(String key) {
		try {
			Runtime.getRuntime().exec("cmd /c adb shell input keyevent "+key);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void kill(){
		try {
			Runtime.getRuntime().exec("cmd /c adb kill-server");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public void enableGALog()
	{
		try {
			Runtime.getRuntime().exec("cmd /c adb shell setprop log.tag.GAv4 VERBOSE");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
